function rotate(matrix, direction) {
    return direction === 'clockwise' ?
        matrix[0].map((_, i, origin) => matrix.map(a => a[i]).reverse()) :
        matrix[0].map((_, i, origin) => matrix.map(a => a[i])).reverse()
}