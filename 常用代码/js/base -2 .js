/**
 * @param {number} N
 * @return {string}
 */
var baseNeg2 = function (N) {
    if (!N) return '0'
    var result = ''
    while (Math.abs(N) > 0) {
        result = Math.abs(N) % 2 + result;
        N = N >= 0 ? -Math.floor(Math.abs(N) / 2) : Math.round(Math.abs(N) / 2);
    }
    return result;
};