var gcd = (x = Math.abs(x), y = Math.abs(y)) => {
    while (y) {
        var t = y;
        y = x % y;
        x = t;
    }
    return x;
}
const gcd=(a,b)=>b?gcd(b,a%b):a;