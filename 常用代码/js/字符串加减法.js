/**
 * 判断是否为负数
 * @param {字符串或整数} x 
 */
const is_negative = (x) => /^\-\d+$/.test(x);
/**
 * 比较两个正整数字符串大小
 * @param {正整数字符串} a 
 * @param {正整数字符串} b 
 */
function le(a, b) {
    if (a === b) return true;
    if (a.length === b.length) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] !== b[i]) return +a[i] > +b[i];
        }
    } else {
        return a.length > b.length;
    }
}

/**
 * 字符串加法
 * @param {正整数字符串} a 
 * @param {正整数字符串 } b 
 */
function string_add(a, b) {
    var arr1 = [...a].reverse();
    var arr2 = [...b].reverse();
    var added = 0;
    var out = [];
    for (var i = 0; i < Math.max(arr1.length, arr2.length); i++) {
        var sum = +(arr1[i] || 0) + +(arr2[i] || 0) + added;
        added = parseInt(sum / 10);
        out.push(sum % 10);
    }
    if (added) out.push(added);
    return out.reverse().join('')
}

/**
 * 字符串减法
 * @param {正整数字符串} a 
 * @param {正整数字符串 } b 
 */
function string_sub(a, b) {
    if (!le(a, b)) return '-' + string_sub(b, a);
    var arr1 = [...a].reverse();
    var arr2 = [...b].reverse();
    var subed = 0;
    var out = []
    for (var i = 0; i < Math.max(arr1.length, arr2.length); i++) {
        var a = (+arr1[i] || 0) + subed;
        var b = (+arr2[i] || 0);
        var sub = (a < b ? a + 10 : a) - b;
        out.unshift(sub);
        subed = a < b ? -1 : 0;
    }
    var result = out.join('').replace(/^0*/, '');
    return result || '0';
}