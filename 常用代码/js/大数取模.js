/**
 * 大数取模
 * @param {String} numString 一个超级大的整数字符串
 * @param {Integer} divisor 除数
 */
function bigModulo(numString, divisor) {
    //运用公式：(a + b) % m = (a % m + b % m) % m
    var res = 0;
    for (var i = 0; i < numString.length; i++) {
        res = (res * 10 + (numString[i] - '0')) % divisor;
    }
    return res;
}