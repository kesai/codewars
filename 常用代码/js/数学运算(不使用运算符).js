const add = (x, y) => {
    while (y != 0) {
        var carry = x & y;
        x = x ^ y;
        y = carry << 1;
    }
    return x;
};

const subtract = (x, y) => {
    while (y != 0) {
        var borrow = (~x) & y;
        x = x ^ y;
        y = borrow << 1;
    }
    return x;
};

const multiply = (x, y) => {
    if (!x || !y) return 0;
    if (x > 0 && y < 0 || x < 0 && y > 0) {
        var a = multiply(Math.abs(x), Math.abs(y));
        var sign = String.fromCharCode(45);
        return parseInt(`${sign}${a}`)
    }
    if (x < 0 && y < 0) return multiply(Math.abs(x), Math.abs(y));
    var res = 0;
    for (var i = 0; i < y; i = add(i, 1))res = add(res, x);
    return res;
};