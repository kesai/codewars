/**
 * 生成无重复的全排列
 */
function perm(arr) {
    if (arr.length < 2) { return arr; }
    var permutations = [];
    for (var i = 0; i < arr.length; i++) {
        var num = arr[i];

        if (arr.indexOf(num) === i) {
            var rem = arr.slice(0, i).concat(arr.slice(i + 1));
            var sub = perm(rem);

            for (var j = 0; j < sub.length; j++) {
                permutations.push([].concat(num, sub[j]));
            }
        }
    }
    return permutations;
}

console.log(perm([1, 1, 1, 11, 2]))