function ulamSequence(u0, u1, n) {
    var arr = [u0, u1, u0 + u1]
    var visited = new Set(...[arr]);
    var i = u0 + u1 + 1;
    while (arr.length < n) {
        var cnt = 0;
        for (var j = 0; arr[j] < i / 2; j++) {
            if (visited.has(i - arr[j]) && i - arr[j] != arr[j]) {
                cnt++;
                if (cnt > 1) break;
            }
        }
        if (cnt === 1) { arr.push(i); visited.add(i); }
        i++;
    }
    return arr;
}