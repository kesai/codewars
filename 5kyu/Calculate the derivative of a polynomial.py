import re


def derivative(eq):
    arr = []
    for p in re.findall(r'(-?(?:\d+)?x(?:\^\d+)?)', eq):
        k, power = p.split('x')
        if k == '-':
            k = -1
        elif k == '':
            k = 1
        else:
            k = int(k)
        if power == '':
            power = 1
        else:
            power = int(power[1:])
        k *= power
        power -= 1
        exp = ''
        if power == 0:
            exp = str(k)
        elif power == 1:
            exp = f'{k}x'
        else:
            exp = f'{k}x^{power}'
        arr.append(exp)
    result = '+'.join(arr)
    result = re.sub(r'\+-', '-', result)
    return result if result else '0'
