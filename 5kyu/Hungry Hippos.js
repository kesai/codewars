function Game(board) {
    this.board = board;
}
Game.prototype.play = function () {
    var groups = [];
    var N = this.board.length;
    for (var i = 0; i < N; i++) {
        for (var j = 0; j < N; j++) {
            if (this.board[i][j] === 1) {
                var top_idx = groups.findIndex(v => v.includes(`${i - 1},${j}`));
                var left_idx = groups.findIndex(v => v.includes(`${i},${j - 1}`));
                if (top_idx > -1 && left_idx > -1) {
                    groups[top_idx] = groups[top_idx].concat(groups[left_idx]);
                    groups[top_idx].push(`${i},${j}`);
                    groups[top_idx] = [...new Set(groups[top_idx])];
                    if (left_idx != top_idx) groups.splice(left_idx, 1);
                }
                else if (top_idx > -1) groups[top_idx].push(`${i},${j}`);
                else if (left_idx > -1) groups[left_idx].push(`${i},${j}`);
                else groups.push([`${i},${j}`]);
            }
        }
    }
    return groups.length;
}