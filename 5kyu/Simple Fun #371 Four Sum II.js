function fourSum(A, B, C, D) {
    [A, B, C, D] = [A, B, C, D].map(v => [...new Set(v)]);
    var sums1 = new Set()
    counts = {};
    for (var i = 0; i < A.length; i++) {
        for (var j = 0; j < B.length; j++) {
            sums1.add(sum = A[i] + B[j])
            counts[sum] = counts[sum] ? counts[sum] + 1 : 1;
        }
    }
    var res = 0;
    for (var i = 0; i < C.length; i++) {
        for (var j = 0; j < D.length; j++) {
            if (sums1.has(x = -(C[i] + D[j]))) res += counts[x];
        }
    }
    return res;
}
