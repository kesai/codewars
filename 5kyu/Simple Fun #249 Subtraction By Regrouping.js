function subtractionByRegrouping(a, b) {
    var arr1 = [...'' + a].reverse();
    var arr2 = [...'' + b].reverse();
    var subed = 0;
    var out = []
    for (var i = 0; i < Math.max(arr1.length, arr2.length); i++) {
        var a = (+arr1[i] || 0) + subed;
        var b = (+arr2[i] || 0);
        var sub = (a < b ? a + 10 : a) - b;
        out.push(a < b ? a + 10 : a)
        subed = a < b ? -1 : 0;
    }
    return out;
}
