from bs4 import BeautifulSoup
import urllib.request


def get_member_since(username='myjinxin2015'):
    res = urllib.request.urlopen(f'https://www.codewars.com/users/{username}')
    html = res.read()
    data = html.decode("utf-8")
    soup = BeautifulSoup(data, "html.parser")
    for x in soup.select('div .stat'):
        if 'Member Since' in x.text:
            return x.text.split(':')[1]
