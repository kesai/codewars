function prioritizeMissiles(missiles) {
    return missiles.sort((a, b) => a.distance / a.speed - b.distance / b.speed).map(v => v.name)
}