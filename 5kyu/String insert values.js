var format = function (str, obj) {
    if (Array.isArray(obj)) {
        return str.replace(/{(\d+)}/g, (_, k) => obj[+k])
    } else {
        return str.replace(/{([a-z]*)}/g, (v, k) => Object.keys(obj).includes(k) ? obj[k] : v)
    }
};