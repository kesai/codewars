var default_hp = {
    head: 25,
    chest: 200,
    armR: 50,
    armL: 50,
    legR: 75,
    legL: 75
}
var default_stat = {
    spd: 30,
    hit: 30,
    acc: 30,
    crit: 30,
    dmg: 30
}
var Player = function () {
    this.stat = Object.assign({}, default_stat)
    this.hp = Object.assign({}, default_hp)
    this.totalHP = function () {
        return Object.values(this.hp).reduce((s, v) => s += v, 0);
    }
    this.upgrade = function () {
        if (Object.values(this.stat).every(v => v > 99)) throw 'error:all above 99';
        var keys = Object.keys(this.stat).filter(v => this.stat[v] < 100);
        if (keys.length === 1 && keys[0] === 'hit') this.stat.hit++;
        else {
            var i = Math.random() * keys.length | 0;
            this.stat[keys[i]]++;
        }
    }
    this.heal = function () {
        for (k of Object.keys(this.hp)) if (this.hp[k]) this.hp[k] = default_hp[k];
    }
    this.takeDmg = function (body, hp) {
        this.hp[body] -= hp;
        if (this.hp[body] < 0) this.hp[body] = 0;
    }
    this.lifeCheck = function () {
        return Object.keys(this.hp).filter(v => !/leg/.test(v)).some(e => this.hp[e] === 0)
    }
}