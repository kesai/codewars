const switch_case = x => /[a-z]/.test(x) ? x.toUpperCase() : x.toLowerCase();
function workOnStrings(a, b) {
    for (c of new Set(a)) if (a.match(new RegExp(c, 'g')).length % 2) b = b.replace(new RegExp(c, 'g'), switch_case);
    for (c of new Set(b)) if (b.match(new RegExp(c, 'g')).length % 2) a = a.replace(new RegExp(c, 'g'), switch_case);
    return a + b;
}

