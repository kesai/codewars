def parse(s, f):
    while('(' in s):
        s = sub('\((\w+)([^())]+)\)',
                lambda x: str(f[x[1]](*map(eval, x[2].split()))), s)
    return eval(s)
