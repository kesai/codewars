function countZeroRowsAndColumns(arr1, arr2) {
    var rows = new Set();
    var columns = new Set();
    for (var i = 0; i < arr1.length; i++) {
        for (var j = 0; j < arr1[0].length; j++) {
            var sum = arr1[i][j] + arr2[i][j];
            if (sum) {
                rows.add(i); columns.add(j);
            }
        }
    }
    return arr1.length + arr1[0].length - (rows.size + columns.size)
}
