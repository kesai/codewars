function connectedValues(arr, val, coord) {
    if (arr[coord[0]][coord[1]] !== val) return [];
    var diffs = [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]];
    var groups = [];
    function boundCheck(x, y) {
        return x > -1 && x < arr.length && y > -1 && y < arr[0].length;
    }
    function getConnectedGroups(x, y) {
        var res = []
        for (d of diffs) {
            var i = x + d[0], j = y + d[1];
            if (boundCheck(i, j)) res = res.concat(groups.filter(v => v.includes(`${i}_${j}`)));
        }
        return [...new Set(res.map(v => groups.indexOf(v)))].map(v => groups[v])
    }

    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[0].length; j++) {
            if (arr[i][j] != val) continue;
            var connectedGroups = getConnectedGroups(i, j);
            if (!connectedGroups.length) groups.push([`${i}_${j}`]);
            else {
                if (connectedGroups.length === 1) connectedGroups[0].push(`${i}_${j}`);
                else {
                    var idx = groups.indexOf(connectedGroups[0]);
                    for (group of connectedGroups.slice(1)) {
                        groups[idx] = groups[idx].concat(group);
                        if (!groups[idx].includes(`${i}_${j}`)) groups[idx].push(`${i}_${j}`)
                        groups.splice(groups.indexOf(group), 1)
                    }
                }
            }
        }
    }
    return groups.find(v => v.includes(coord.join('_'))).map(v => v.split('_').map(v => +v))
}
