var roadKill = function (photo) {
    photo = photo.replace(/=/g, '')
    if (/[^a-z]/.test(photo)) return '??'
    var x = '^' + photo.replace(/(.)\1+/g, v => `${v[0]}+`) + '$';
    var y = '^' + [...photo].reverse().join('').replace(/(.)\1+/g, v => `${v[0]}+`) + '$';
    for (w of ANIMALS) {
        if (new RegExp(x).test(w) || new RegExp(y).test(w)) return w;
    }
    return "??"
}