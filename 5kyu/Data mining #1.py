class Datamining:

    def __init__(self, train_set):
        n = len(train_set)
        m_x, m_y = sum(x[0] for x in train_set)/n, sum(x[1]
                                                       for x in train_set)/n
        SS_xy = sum(x[0]*x[1] for x in train_set)-n*m_x*m_y
        SS_xx = sum(x[0]**2 for x in train_set)-n*m_x**2
        self.b_1 = SS_xy / SS_xx
        self.b_0 = m_y - self.b_1*m_x
        print(self.b_0, self.b_1)

    def predict(self, x):
        return self.b_0+self.b_1*x
