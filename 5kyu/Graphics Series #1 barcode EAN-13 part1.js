group = ["LLLLLL", "LLGLGG", "LLGGLG",
    "LLGGGL", "LGLLGG", "LGGLLG",
    "LGGGLL", "LGLGLG", "LGLGGL",
    "LGGLGL"];
//lcode----L - digits
lcode = ["0001101", "0011001", "0010011",
    "0111101", "0100011", "0110001",
    "0101111", "0111011", "0110111",
    "0001011"]
//gcode----G - digits
gcode = ["0100111", "0110011", "0011011",
    "0100001", "0011101", "0111001",
    "0000101", "0010001", "0001001",
    "0010111"]
//rcode----R - digits
rcode = ["1110010", "1100110", "1101100",
    "1000010", "1011100", "1001110",
    "1010000", "1000100", "1001000",
    "1110100"]


function readBarcode(first, barcode) {
    var map = {
        'L': (x) => lcode.indexOf(x),
        'R': (x) => rcode.indexOf(x),
        'G': (x) => gcode.indexOf(x)
    }
    var [_, left, right] = barcode.match(/(?:\d{3})(\d{42})(?:\d{5})(\d{42})(?:\d{3})/);
    var a = left.match(/\d{7}/g).map((v, i) => map[group[first][i]](v)).join('');
    var b = right.match(/\d{7}/g).map(v => rcode.indexOf(v)).join('')
    return first + a + b;
}

var barcode = "10101111010000101010011101110010010011001001101010101110011011001001000111001010000101000100101";
console.log(readBarcode(6, barcode))
