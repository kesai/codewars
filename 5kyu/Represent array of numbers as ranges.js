function toRange(arr) {
    arr = Array.from(new Set(arr)).sort((a, b) => a - b)
    for (var i = 0, r = []; i < arr.length;) {
        var s = i
        while (i < arr.length && arr[i] + 1 == arr[i + 1]) i++
        r.push(i == s ? arr[i++] : arr[s] + "_" + arr[i++])
    }
    return r.join(",")
}
function toArray(str) {
    for (var arr = str.split(","), i = 0, r = []; str != "" && i < arr.length; i++) {
        var [a, b] = (arr[i] + "_" + arr[i]).split("_")
        for (var j = +a; j <= +b; j++) r.push(j)
    }
    return r
}