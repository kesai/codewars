//refrence:https://www.geeksforgeeks.org/count-sum-of-digits-in-numbers-from-1-to-n/
function sumOfDigits(from, to) {
    return sumOfDigitsFrom1ToN(to) - sumOfDigitsFrom1ToN(from - 1)
}
function sumOfDigitsFrom1ToN(n) {
    if (n < 10)
        return (n * (n + 1) / 2);
    var d = parseInt(Math.log10(n));
    var a = new Array(d + 1);
    a[0] = 0; a[1] = 45;
    for (var i = 2; i <= d; i++)a[i] = a[i - 1] * 10 + 45 * (Math.ceil(Math.pow(10, i - 1)));
    var p = Math.ceil(Math.pow(10, d));
    var msd = parseInt(n / p);
    return (msd * a[d] + (msd * (msd - 1) / 2) * p +
        msd * (1 + n % p) + sumOfDigitsFrom1ToN(n % p));
}
