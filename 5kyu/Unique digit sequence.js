var caches = [0]
function next() {
    var last = caches[caches.length - 1];
    for (var i = 0; ; i++) {
        var temp = [...'' + last].join(',');
        if (!new RegExp(`[${temp}]`).test(i) && !caches.includes(i)) {
            caches.push(i); break;
        }
    }
}
function findNum(n) {
    for (var i = caches.length; i <= n; i++)next();
    return caches[n];
}
