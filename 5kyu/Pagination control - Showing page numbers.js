const getPages = (l, c, s) => {
    var arr = [...Array(l)].map((_, i) => i + 1)
    return l == 1 ? [] :
        c <= s + 1 ? arr.filter(x => x == 1 || x == l || Math.abs(x - s - 2) <= s) :
            c >= l - s ? arr.filter(x => x == 1 || x == l || Math.abs(x - l + s + 1) <= s)
                : arr.filter(x => x == 1 || x == l || Math.abs(x - c) <= s)
};