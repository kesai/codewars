var f = (x) => x > 1 ? x * f(x - 1) : 1;
function p(lamb, x) {
    return (lamb ** x) * (Math.E ** (-lamb)) / f(x)
}
function probSimpson(lamb, x, op = '=') {
    if (op === '=') return p(lamb, x);
    var res = 0;

    if (op === '>' || op === '>=') {
        var limited = op === '>' ? x : x + 1;
        for (var i = 0; i < limited; i++) {
            res += p(lamb, i);
        }
        return res;
    } else {
        if (op === '<=') return 1 - probSimpson(lamb, x, '>');
        if (op === '<') return 1 - probSimpson(lamb, x, '>=');
    }
}