var gcd = (x, y) => y ? gcd(y, x % y) : x;
function countNewFriends(n) {
    return n < 3 ? 0 : Array.from({ length: n - 2 }, (_, i) => i + 2).filter(v => gcd(v, n) === 1).length
}