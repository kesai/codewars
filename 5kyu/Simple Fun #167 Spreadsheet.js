var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
function spreadsheet(s) {
    if (/^R\d+C\d+$/.test(s)) {
        var [row, col] = s.match(/\d+/g);
        return numToIdx(+col) + row

    } else {
        var [col, row] = s.match(/[A-Z]+|\d+/g);
        return `R${row}C${IdxToNum(col)}`;
    }
}
function IdxToNum(x) {
    return [...x].reverse().reduce((s, v, i) => s += (chars.indexOf(v) + 1) * 26 ** i, 0);
}
function numToIdx(n) {
    var res = ''
    while (n > 26) {
        d = n % 26;
        n = Math.floor(n / 26);
        if (d === 0) { d = 26; n -= 1; }
        res = chars[d - 1] + res;
    }
    res = chars[n - 1] + res;
    return res;
}