function isPalindrome(x) {
    return x === [...x].reverse().join('');
}

function howManyPalindromes(s) {
    var n = s.length;
    var index = {}
    var caches = []
    for (var i = 0; i < s.length; i++) {
        if (!index[s[i]]) index[s[i]] = [i];
        else index[s[i]].push(i);
    }
    for (k of Object.keys(index)) {
        var x = index[k];
        if (x.length > 1) {
            for (var i = 0; i < x.length - 1; i++) {
                for (j = i + 1; j < x.length; j++) {
                    var temp = s.slice(x[i], x[j] + 1);
                    if (isPalindrome(temp)) {
                        n++;
                    }
                }
            }
        }
    }
    return n;
}
