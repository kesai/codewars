function isValid(n) {
    var a = n;
    var b = [...'' + n].reverse().join('');
    if (/^0/.test(b)) return false;
    var d = Math.abs(a - +b)
    return (a + +b) % d === 0
}
var terms = [0]
for (var i = 45, k = 0; k < 70; i++) {
    if (isValid(i)) { terms.push(i); k++; }
}
function sumDifRev(n) {
    return terms[n]
}