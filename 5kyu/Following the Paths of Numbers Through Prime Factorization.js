//ref:https://www.geeksforgeeks.org/total-number-divisors-given-number/
function getNum(arr) {
    var n = 1, min = Infinity;
    var counts = {};
    for (d of arr) {
        n *= d;
        min = Math.min(d, min);
        counts[d] = counts[d] ? counts[d] + 1 : 1;
    }
    var count = Object.values(counts).reduce((s, v) => s *= (v + 1), 1) - 1;
    return [n, count, min, n / min]
}