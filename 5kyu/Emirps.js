function isPrime(num) {
    if (num < 2) return false;
    if (num === 2 || num === 3) return true;
    if (num % 6 != 1 && num % 6 != 5) return false
    var n = parseInt(Math.sqrt(num));
    for (var i = 5; i < n + 1; i += 6) {
        if (num % i === 0 || num % (i + 2) === 0) return false
    }
    return true
}
function isEmirp(x) {
    var b = +[...'' + x].reverse().join('');
    return b !== x && isPrime(b);
}

function findEmirp(n) {
    var primeGen = (function* () {
        var d = 2;
        while (true) {
            while (!isPrime(d)) d++;
            yield d++;
        }
    }());
    var p = primeGen;
    var count = 0;
    var sum = 0;
    var max = 0;
    while ((x = primeGen.next().value) < n) {
        if (isEmirp(x)) {
            console.log(x)
            sum += x;
            max = x;
            count++;
        }
    }
    return [count, max, sum]
}