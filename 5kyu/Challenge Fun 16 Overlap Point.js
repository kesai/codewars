function overlapPoint(c1, c2) {
    var dis_square = ([x1, y1], [x2, y2]) => (x1 - x2) ** 2 + (y1 - y2) ** 2;
    var [x1, y1, r1] = c1;
    var [x2, y2, r2] = c2;
    var min_x = Math.min(x1 - r1, x2 - r2);
    var max_x = Math.max(x1 + r1, x2 + r2);
    var min_y = Math.min(y1 - r1, y2 - r2);
    var max_y = Math.max(y1 + r1, y2 + r2);
    var count = 0
    for (var x = min_x; x <= max_x; x++) {
        for (var y = min_y; y <= max_y; y++) {
            if (dis_square([x, y], [x1, y1]) <= r1 * r1 && dis_square([x, y], [x2, y2]) <= r2 * r2) count++;
        }
    }
    return count;
}