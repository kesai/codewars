//ref:https://www.geeksforgeeks.org/sum-factors-number/
function buddy(start, limit) {
    for (var i = start; i <= limit; i++) {
        var sum = s(i);
        if (sum <= i) continue;
        var m = sum - 1;
        if (s(m) === i + 1) return [i, m]
    }
    return 'Nothing'
}
const s = (n) => {
    let res = 1
    for (let i = 2; i <= Math.sqrt(n); i++) {
        if (n % i === 0) i === n / i ? res += i : res += i + n / i
    }
    return res
}

function sumofFactors(n) {
    var res = 1;
    for (var i = 2; i <= Math.sqrt(n); i++) {
        var curr_sum = 1;
        var curr_term = 1;

        while (n % i == 0) {
            n = Math.floor(n / i);
            curr_term *= i;
            curr_sum += curr_term;
        }
        res *= curr_sum;
    }
    if (n > 2) res *= (1 + n);
    return res;
}
