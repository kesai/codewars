function compress(arr) {
    var out = []
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === arr[i + 1]) {
            var n = 0;
            var first = arr[i];
            while (arr[i + 1] === arr[i]) {
                n++;
                i++;
            }
            out.push(`${first}*${n + 1}`);
        } else if (arr[i + 1] - arr[i] === arr[i + 2] - arr[i + 1]) {
            var d = arr[i + 1] - arr[i];
            var seq = [arr[i]]
            while (arr[i + 1] - arr[i] === d) {
                seq.push(arr[i + 1]);
                i++;
            }
            if (Math.abs(d) === 1) {
                out.push(`${seq[0]}-${seq.slice(-1)[0]}`);
            } else {
                out.push(`${seq[0]}-${seq.slice(-1)[0]}/${Math.abs(d)}`);
            }
        } else {
            out.push(arr[i])
        }
    }
    return out.join(',')
}