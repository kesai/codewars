function boxesPacking(length, width, height) {
    boxes = length.map((v, i) => [v, width[i], height[i], v * width[i] * height[i]]);
    boxes.sort((a, b) => a[3] - b[3]);
    if (new Set(boxes.map(v => v[3])).size < length.length) return false;
    for (var i = 0; i < boxes.length - 1; i++) {
        var a = boxes[i].slice(0, 3).sort((a, b) => a - b)
        var b = boxes[i + 1].slice(0, 3).sort((a, b) => a - b)
        for (var j = 0; j < a.length; j++) {
            if (a[j] >= b[j]) return false;
        }
    }
    return true;
}
