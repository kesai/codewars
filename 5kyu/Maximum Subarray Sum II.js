function findSubarrMaxSum(arr) {
    let maxSum = 0
    let result = []
    for (let i = 0; i < arr.length; i++) {
        for (let j = 1; j <= arr.length; j++) {
            let subArr = arr.slice(i, j)
            let subSum = subArr.reduce((a, b) => a + b, 0)
            if (subSum > maxSum) {
                maxSum = subSum
            }
            result.push([subArr, subSum])
        }
    }
    result = result.filter(v => v[1] == maxSum).map(v => v[0]).filter(v => v.length)
    return result.length == 1 ? result.concat(maxSum) : [result, maxSum]
}