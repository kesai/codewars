function SnakesLadders() {
    this.board = Array.from({ length: 100 }, (_, i) => i + 1);
    this.bridges = [[2, 38], [7, 14], [8, 31], [15, 26], [21, 42], [36, 44], [28, 84], [51, 67], [71, 91], [87, 94], [78, 98], [16, 6], [49, 11], [46, 25], [62, 19], [74, 53], [64, 60], [99, 80], [95, 75], [89, 68], [92, 88]];
    this.players = [0, 0];
    this.idx = 0;
    this.winner = 0;
};
SnakesLadders.prototype.play = function (die1, die2) {
    if (this.winner) return `Game over!`
    var die = die1 + die2;
    var idx = this.idx;
    this.idx = die1 === die2 ? this.idx : (this.idx + 1) % 2;
    this.players[idx] += die;
    if (this.players[idx] === 100) {
        this.winner = idx + 1;
        return `Player ${idx + 1} Wins!`;
    }
    else if (this.players[idx] > 100) {
        this.players[idx] = 200 - this.players[idx];
    }
    var bridge = this.bridges.find(v => v[0] === this.players[idx]);
    if (bridge) this.players[idx] = bridge[1];
    return `Player ${idx + 1} is on square ${this.players[idx]}`
}
