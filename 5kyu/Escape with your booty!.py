class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Navy:
    def __init__(self, x, y, is_down):
        self.pt = Point(x, y)
        self.is_down = is_down


def check_course(sea_map):
    ROW = len(sea_map)
    COL = len(sea_map[0])
    sea_map = list(map(lambda v: list(v), sea_map))

    def is_safe():
        for nav in navies:
            if (nav.pt.x-x_pos.x)**2+(nav.pt.y-x_pos.y)**2 <= 2:
                return False
        return True

    def locateX():
        row = [x for x in sea_map if 'X' in x][0]
        x = sea_map.index(row)
        y = row.index('X')
        return Point(x, y)

    def locateN():
        navies = [Navy(0, i, True)
                  for i, v in enumerate(sea_map[0]) if v == 'N']+[Navy(ROW-1, i, False)
                                                                  for i, v in enumerate(sea_map[-1]) if v == 'N']
        return navies

    def patrol():
        for nav in navies:
            if nav.is_down:
                nav.pt.x -= 1
            else:
                nav.pt.x += 1
            if nav.pt.x == -1:
                nav.is_down = not nav.is_down
                nav.pt.x = 1
            elif nav.pt.x == ROW:
                nav.is_down = not nav.is_down
                nav.pt.x = ROW-2

    x_pos = locateX()
    navies = locateN()
    while x_pos.y < COL:
        if not is_safe():
            return False
        x_pos.y += 1
        patrol()
    return True
