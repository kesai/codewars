String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + 1, this.length);
}
function reversiRow(moves) {
    var s = '........';
    var checkForChar = (str, ch, ch2, ind) => {
        var regex = new RegExp("((?<=^.{" + ind + "}" + ch + ")(" + ch2 + "+)(?=" + ch + "))|((?<=" + ch + "+)(" + ch2 + "+)(?=" + ch + ".{" + (7 - ind) + "}$))", "g");
        return str.replace(regex, (x) => { return "".padStart(x.length, ch) });
    };
    for (var i = 0; i < moves.length; i++) {
        var c = i % 2 == 0 ? "!" : "O";
        var c2 = i % 2 == 0 ? "O" : "!"
        s = checkForChar(s.replaceAt(moves[i], c), c, c2, moves[i]);
    }
    return s.replace(/!/g, '*');
}