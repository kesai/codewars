var kb = {
    'A': 2, 'B': 2, 'C': 2, 'D': 3, 'E': 3, 'F': 3, 'G': 4, 'H': 4, 'I': 4, 'J': 5, 'K': 5, 'L': 5, 'M': 6,
    'N': 6, 'O': 6, 'P': 7, 'Q': 7, 'R': 7, 'S': 7, 'T': 8, 'U': 8, 'V': 8, 'W': 9, 'X': 9, 'Y': 9, 'Z': 9
}
patterns = {}
for (w of Preloaded.WORDS) {
    patterns[w] = [...w].map(v => kb[v]).join('');
}
var check1800 = function (str) {
    var arr = str.split('-');
    var s = arr[2] + arr[3];
    var [_, a1, b1] = s.match(/(\w{3})(\w{4})/);
    var [_, a2, b2] = s.match(/(\w{4})(\w{3})/);
    var array = [[a1, b1], [a2, b2]];
    var out = []
    for ([a, b] of array) {
        var pattern_a = [...a].map(v => kb[v]).join('')
        var pattern_b = [...b].map(v => kb[v]).join('')
        var group_a = Object.keys(patterns).filter(v => patterns[v] === pattern_a);
        var group_b = Object.keys(patterns).filter(v => patterns[v] === pattern_b);
        for (var i = 0; i < group_a.length; i++) {
            for (var j = 0; j < group_b.length; j++) {
                var temp = `1-800-${group_a[i]}-${group_b[j]}`;
                if (!out.includes(temp)) out.push(temp)
            }
        }
    }
    return out
}