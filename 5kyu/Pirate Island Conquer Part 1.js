function conquerIsland(map) {
    var unoccupied = [];
    var marines = [];
    for (var i = 0; i < map.length; i++) {
        for (var j = 0; j < map[0].length; j++) {
            if (map[i][j] === 'u')
                unoccupied.push(new Object({ pos: [i, j], dis: i + j }));
            else if (map[i][j] === 'm') marines.push(new Object({ pos: [i, j], dis: i + j }));
        }
    }
    if (unoccupied.length) {
        unoccupied.sort((a, b) => a.dis - b.dis || a.pos[0] - b.pos[0]);
        return unoccupied.filter(v => v.dis === unoccupied[0].dis).map(v => v.pos);
    } else if (marines.length) {
        marines.sort((a, b) => a.dis - b.dis || a.pos[0] - b.pos[0]);
        return marines.filter(v => v.dis === marines[0].dis).map(v => v.pos);
    }
    return [];
}