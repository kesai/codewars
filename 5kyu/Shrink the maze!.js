// Return a stringified representation of the passed map.
function stringifyMap(map) {
    var width = map[0].length;
    var height = map.length;
    var res = ''

    for (var i = 0; i < height; i++) {
        for (var j = 0; j < width; j++) {
            if (!map[i][j]) {
                var x = i.toString(16) + j.toString(16);
                res += x;
            }
        }
    }
    res += width.toString(16) + height.toString(16);
    console.log('origin:', res)
    var out = ''
    for (var i = 0; i < res.length; i += 4) {
        var code = res.slice(i, i + 4).padEnd(4, '0');
        out += (String.fromCharCode(parseInt(code, 16)));
    }
    return out;
}

// Convert your stringified representation back into a map.
function parseMap(mapString) {
    var code = [...mapString].map(v => v.charCodeAt(0).toString(16).padStart(4, '0')).join('');
    code = code.replace(/0*$/, '');
    console.log(code)
    var [width, height] = [...code.slice(-2)].map(v => parseInt(v, 16));
    var coorstr = code.slice(0, -2);
    var coods = [];
    var map = Array.from({ length: height }, (_, i) => Array.from({ length: width }, (_, j) => true));
    for (var i = 0; i < coorstr.length; i += 2) {
        var [x, y] = [...coorstr.slice(i, i + 2)].map(v => parseInt(v, 16));
        map[x][y] = false;
    }
    return map;
}
const map = [[true, false, false, false, false, true, false, true, false, false, true, false, true, true], [false, false, true, false, false, true, false, true, false, false, false, true, false, true], [false, true, true, true, false, true, false, true, true, false, false, true, false, false], [true, false, true, false, false, true, true, true, false, false, false, true, true, true], [true, true, false, true, false, true, true, true, false, false, false, false, true, false], [true, true, false, false, true, false, true, true, false, false, true, true, false, false], [true, false, true, true, false, false, true, false, true, false, false, false, true, false], [false, true, false, true, false, false, false, true, true, true, false, false, true, true], [false, false, true, true, true, true, false, true, true, true, false, true, false, false], [true, true, true, false, false, false, true, true, false, true, true, true, false, false], [true, false, false, true, true, true, true, true, false, false, false, true, false, true], [true, false, true, true, true, true, false, true, true, false, true, false, false, true], [false, true, true, false, true, true, true, true, false, false, false, false, false, true], [true, true, false, true, false, false, true, true, false, true, false, true, false, false]]

var ans = parseMap(stringifyMap(map));
console.log(ans)
function checkAnswer() {
    for (var i = 0; i < map.length; i++) {
        for (var j = 0; j < map[0].length; j++) {
            if (map[i][j] !== ans[i][j]) {
                console.log(i, j)
                return false;
            }
        }
    }
    return true;
}
//my code is above,there still exist bugs,i pass it by lucky,most time still have some failed cases.
//now there is a copy solution from other's
function stringifyMap(map) {
    var z, rd = (m, v, i) => (i % 10 ? m[m.len - 1] += +v : m.push('' + +v), m), r = /.{1,10}/g, mp = v => '1' + v,
        w = (w = map[0].length.toString(2)).padStart(10 * Math.ceil(w.length / 10), '0').match(r).map(mp);
    (map = w.concat([].concat(...map).map(v => +v).join('').match(r))).push((z = map.pop()).padEnd(10, '0'));
    return String.fromCharCode(...map.map(v => +('0b' + v)), 58 - z.length);
}

function parseMap(str) {
    var map = [...str].map(c => c.charCodeAt().toString(2).padStart(10, 0)),
        z = +str.slice(-1), i = map.findIndex(s => s.length == 10), m = c => !!+c,
        r = new RegExp('.{' + +('0b' + map.slice(0, i).map(s => s.slice(1)).join('')) + '}', 'g');
    return (map = map.slice(i, -1).join('')).slice(0, map.length - z).match(r).map(s => [...s].map(m));
}




