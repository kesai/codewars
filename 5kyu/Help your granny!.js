function tour(friends, fTowns, distTable) {
    var cur_town = 'X0';
    var dis = 0;
    var map = {}
    var has_unvisited = false;
    for (var i = 0; i < distTable.length; i += 2)map[distTable[i]] = distTable[i + 1];
    for (var i = 0; i < friends.length; i++) {
        var friend = friends[i];
        var search = fTowns.find(v => v[0] === friend);
        if (!search) {
            dis += map[cur_town] / 2;
            next_town = cur_town;
            has_unvisited = true;
        } else {
            var next_town = search[1];
            var s1 = map[next_town];
            var s0 = cur_town === 'X0' ? 0 : map[cur_town];
            var s = Math.sqrt(s1 * s1 - s0 * s0);
            dis += s;
            cur_town = next_town;
        }
    }
    dis += has_unvisited ? map[cur_town] / 2 : map[cur_town]
    return Math.floor(dis);
}
