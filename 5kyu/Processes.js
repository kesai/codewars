var processes = function (source, destination, processList) {
    var out = []
    if (source === destination) return out;
    if (processList.findIndex(v => v.includes(destination)) === -1) return []
    var p = processList.filter(v => v[1] === source);
    if (!p) return [];
    var paths = p.map(v => [v]);
    while (paths.length) {
        var new_paths = []
        for (p of paths) {
            var avialbles = processList.filter(v => v[1] === p[p.length - 1][2]);
            if (!avialbles) continue;
            for (x of avialbles) {
                if (p.indexOf(x) === -1) {
                    var new_path = [].concat(p);
                    new_path.push(x);
                    if (x[2] === destination)
                        out.push(new_path);
                    else
                        new_paths.push(new_path);

                }
            }
        }
        paths = new_paths;
    }
    return out.length ? out.sort((a, b) => a.length - b.length)[0].map(v => v[0]) : []
}
var test_processes = [['make axe', 'stone', 'axe'],
['make knife', 'stone', 'knife'],
['make bricks', 'stone', 'bricks'],
['cut tree', 'axe', 'tree'],
['shave with axe', 'axe', 'stubs'],
['build log cabin', 'tree', 'house'],
['shave with knife', 'knife', 'stubs'],
['build house', 'bricks', 'house']]
console.log(processes('stone', 'house', test_processes))