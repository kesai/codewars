// This is here as documentation. The nodes in the tree are instances of
// this class. You don't need to change this implementation.
class Node {
    constructor(value, left = null, right = null) {
        this.value = value;
        this.left = left;
        this.right = right;
    }
}
function in_order(node, arr = []) {
    if (!node) return;
    if (node.left) in_order_traverse(node.left, arr);
    arr.push(node.value);
    if (node.right) in_order_traverse(node.right, arr);
}
function pre_order(node, arr = []) {
    if (!node) return;
    arr.push(node.value);
    if (node.left) pre_order(node.left, arr);
    if (node.right) pre_order(node.right, arr);
}

function post_order(node, arr = []) {
    if (node.left) post_order(node.left, arr);
    if (node.right) post_order(node.right, arr);
    arr.push(node.value);
}

const isBST = node => {
    var arr = []
    in_order(node, arr);
    var s = arr.join(',')
    var inc_arr = [].concat(arr).sort((a, b) => a - b);
    var dec_arr = [].concat(inc_arr).reverse();
    return inc_arr.join(',') === s || dec_arr.join(',') === s
};
const T = (v, l, r) => new Node(v, l, r);
var root = T(5, T(2, T(1), T(3)), T(7, null, T(9)));
var arr = [];
post_order(root, arr)
console.log(arr)