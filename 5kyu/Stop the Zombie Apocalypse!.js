function findZombies(matrix) {
    var visited = matrix.map(v => v.map(e => 0));
    var ROW = matrix.length;
    var COL = matrix[0].length;
    var bound_check = (x, y) => x > -1 && x < ROW && y > -1 && y < COL;
    var moves = [[-1, 0], [1, 0], [0, -1], [0, 1]];
    var [x0, y0, zombie] = [0, 0, matrix[0][0]];
    visited[x0][y0] = 1;
    var queue = [[x0, y0]];
    while (queue.length) {
        var curr = queue.shift();
        for (mov of moves) {
            var [x, y] = [curr[0] + mov[0], curr[1] + mov[1]];
            if (bound_check(x, y) && !visited[x][y] && matrix[x][y] === zombie) {
                visited[x][y] = 1;
                queue.push([x, y]);
            }
        }
    }
    return visited;
}
