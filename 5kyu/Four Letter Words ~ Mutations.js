function getWord(word, words) {
    var a = '[a-z]' + word[1] + word[2] + word[3]
    var b = word[0] + '[a-z]' + word[2] + word[3]
    var c = word[0] + word[1] + '[a-z]' + word[3]
    var d = word[0] + word[1] + word[2] + '[a-z]';
    var regs = [a, b, c, d].map(v => new RegExp(v));
    var word = words.find(w => regs.some(reg => reg.test(w)) && w !== word);
    return word;
}

function mutations(alice, bob, word, first) {
    var cur_player = first;
    var take_turns = () => { cur_player = cur_player ? 0 : 1; }
    var players = [alice.slice().filter(
        w => new Set([...w]).size === 4), bob.slice().filter(
            w => new Set([...w]).size === 4)];
    function remove_used() {
        for (var i = 0; i < players.length; i++) {
            while (players[i].includes(word)) players[i].splice(players[i].indexOf(word), 1);
        }
    }
    remove_used();
    //first turn
    if (!getWord(word, players[cur_player])) {
        take_turns();
        if (!getWord(word, players[cur_player])) {
            return -1;
        } else return cur_player;

    } else {
        word = getWord(word, players[cur_player]);
        remove_used();
        take_turns();
        if (!getWord(word, players[cur_player])) {
            take_turns(); return cur_player;
        } else {
            word = getWord(word, players[cur_player]);
            remove_used();
        }
    }
    take_turns();
    while ((word = getWord(word, players[cur_player]))) {
        remove_used();
        take_turns();
    }
    take_turns();
    return cur_player;
}
const alice = ['tubs',
    'hull',
    'dahl',
    'rope',
    'kuna',
    'weld',
    'delf',
    'hens',
    'prez',
    'fool',
    'edgy',
    'even',
    'cain',
    'alfa',
    'bran',
    'stud',
    'fyce',
    'ropy',
    'smog',
    'take',
    'wavy',
    'fish',
    'cult',
    'wolf',
    'cagy',
    'tack',
    'hang',
    'vrow',
    'sows',
    'send',
    'vacs',
    'eric',
    'race',
    'been',
    'cape',
    'fila',
    'past',
    'help',
    'york',
    'slew',
    'soda',
    'dows',
    'kobs',
    'puja',
    'hoys',
    'scab',
    'bams',
    'size',
    'hate',
    'darn',
    'kant',
    'duma',
    'osar',
    'jows',
    'prau',
    'huff',
    'wens',
    'door',
    'mono',
    'sawn',
    'asci',
    'tips',
    'yoga',
    'gaby',
    'heed',
    'vext',
    'trot',
    'amok',
    'lads',
    'baba',
    'monk',
    'gobs',
    'oval',
    'only',
    'coir',
    'yous',
    'nark',
    'fell',
    'daft',
    'duns',
    'dace',
    'areg',
    'main',
    'feed',
    'jock',
    'oils',
    'buys',
    'carr',
    'ever',
    'acid',
    'deaf',
    'spud',
    'geds',
    'grew',
    'psis',
    'rote',
    'need',
    'haet',
    'zoea',
    'rays']
var bob = ['fans',
    'oohs',
    'peer',
    'bals',
    'wipe',
    'nubs',
    'baby',
    'iron',
    'aves',
    'chaw',
    'rads',
    'whir',
    'grue',
    'curd',
    'topi',
    'boat',
    'frae',
    'vena',
    'hook',
    'cole',
    'lags',
    'bare',
    'flux',
    'test',
    'isba',
    'sall',
    'sale',
    'blam',
    'dexy',
    'pert',
    'pops',
    'yard',
    'maes',
    'oxid',
    'vine',
    'lose',
    'thro',
    'hazy',
    'kart',
    'luvs',
    'bufo',
    'jees',
    'flam',
    'tref',
    'duke',
    'shah',
    'surd',
    'jeon',
    'mong',
    'dhak',
    'ewes',
    'jauk',
    'spiv',
    'bass',
    'post',
    'duns',
    'lues',
    'cues',
    'clop',
    'sura',
    'roan',
    'abba',
    'wars',
    'egis',
    'half',
    'apps',
    'soya',
    'info',
    'meze',
    'bima',
    'kier',
    'alma',
    'buys',
    'idea',
    'gowd',
    'chon',
    'sots',
    'drib',
    'redd',
    'pech',
    'bade',
    'luxe',
    'gams',
    'dang',
    'male',
    'agee',
    'goth',
    'beet',
    'dewy',
    'onus',
    'fins',
    'bubo',
    'york',
    'jury',
    'prey',
    'wots',
    'pili',
    'pate',
    'soba',
    'arid']
console.log(mutations(alice, bob, 'howk', 1))


