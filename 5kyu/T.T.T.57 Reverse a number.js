
function reverseNumber(n) {
    if (/^-/.test(n)) return '-' + reverseNumber(n.slice(1))
    var nums = [...n].map(v => +v)
    var sames = []
    var out = []
    var arr = [];
    var i = 0;
    for (var i = 0; i < nums.length; i++) {
        if (nums[i + 1] > nums[i]) {
            while (nums[i + 1] >= nums[i]) { arr.push(nums[i]); i++ };
            arr.push(nums[i]);
            out.push(sames.concat(arr).reverse().join(''));
            arr.length = 0;
            sames.length = 0;
        } else if (nums[i + 1] < nums[i]) {
            while (nums[i + 1] <= nums[i]) { arr.push(nums[i]); i++ };
            arr.push(nums[i]);
            out.push(sames.concat(arr).reverse().join(''));
            arr.length = 0;
            sames.length = 0;
        } else {
            sames.push(nums[i]);
        }
    }
    out.push(sames.join(''));
    return out.join('').replace(/^0+/, '')
}
