def uniq_c(seq):
    if len(seq) == 0:
        return []
    out = []
    c = seq.pop(0)
    n = 1
    for x in seq:
        if x == c:
            n += 1
        else:
            out.append([c, n])
            n = 1
            c = x
    out.append([c, n])
    return out
