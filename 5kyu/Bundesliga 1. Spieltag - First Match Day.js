function table(results) {
    var teams = []
    for (x of results) {
        var [_, home_goals, away_goals, home_name, away_name] = x.match(/^([\d\-]+):([\d\-]+)\s(.+)\s-\s(.+)$/);
        has_played = home_goals != '-';
        home_goals = has_played ? +home_goals : 0;
        away_goals = has_played ? +away_goals : 0;
        var home_team = {
            name: home_name,
            goals: home_goals,
            point: has_played ? (home_goals > away_goals ? 3 : home_goals === away_goals ? 1 : 0) : 0,
            gd: home_goals - away_goals,
            played: has_played ? 1 : 0,
            score: `${home_goals}:${away_goals}`,
            won: home_goals > away_goals ? 1 : 0,
            tie: (has_played && home_goals === away_goals) ? 1 : 0,
            loss: (has_played && home_goals < away_goals) ? 1 : 0
        }
        var away_team = {
            name: away_name,
            goals: away_goals,
            point: has_played ? (away_goals > home_goals ? 3 : home_goals === away_goals ? 1 : 0) : 0,
            gd: away_goals - home_goals,
            played: has_played ? 1 : 0,
            score: `${away_goals}:${home_goals}`,
            won: away_goals > home_goals ? 1 : 0,
            tie: (has_played && home_goals === away_goals) ? 1 : 0,
            loss: (has_played && home_goals > away_goals) ? 1 : 0
        }
        teams.push(home_team, away_team);
    }
    teams.sort((a, b) => b.point - a.point || b.gd - a.gd || b.goals - a.goals || a.name.localeCompare(b.name))
    var table = [];
    var last = teams[0]
    for (var i = 0, rank = 1; i < teams.length; i++) {
        var t = teams[i];
        if (t.point < last.point || t.gd < last.gd || t.goals < last.goals) {
            rank = i + 1; last = t;
        }
        var s = `${('' + rank).padStart(2, ' ')}. ${t.name.padEnd(30, ' ')}${t.played}  ${t.won}  ${t.tie}  ${t.loss}  ${t.score}  ${t.point}`
        table.push(s)
    }
    return table.join('\n');
}


