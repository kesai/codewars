function deepCompare(o1, o2) {
    if (Array.isArray(o1)) {
        return o1.every((v, i) => deepCompare(v, o2[i]))
    } else if (o1 && o1.constructor && o1.constructor.name === 'Object' && o2 && o2.constructor && o2.constructor.name === 'Object') {
        return Object.keys(o1).sort().join('') === Object.keys(o2).sort().join('') && Object.keys(o1).every(k => Object.keys(o2).includes(k) && deepCompare(o1[k], o2[k]))
    } else {
        return o1 === o2;
    }
};
console.log(deepCompare([1, 2, null, undefined, { name: 'Joe' }],
    [1, 2, null, undefined]))
