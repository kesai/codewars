const compare = (a, b) => a.slice(-4) - b.slice(-4) || a.localeCompare(b);
const sortme = (courses) => courses.sort(compare);