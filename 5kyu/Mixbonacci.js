var caches = {
    'fib': { 0: 0, 1: 1 },
    'pad': { 0: 1, 1: 0, 2: 0 },
    'jac': { 0: 0, 1: 1, 2: 1 },
    'pel': { 0: 0, 1: 1 },
    'tet': { 0: 0, 1: 0, 2: 0, 3: 1 },
    'tri': { 0: 0, 1: 0, 2: 1 }
}
const fib = n => (caches['fib'][n] = Object.keys(caches['fib']).includes('' + n) ? caches['fib'][n] : fib(n - 1) + fib(n - 2));
const pad = n => (caches['pad'][n] = Object.keys(caches['pad']).includes('' + n) ? caches['pad'][n] : pad(n - 2) + pad(n - 3));
const jac = n => (caches['jac'][n] = Object.keys(caches['jac']).includes('' + n) ? caches['jac'][n] : jac(n - 1) + 2 * jac(n - 2));
const pel = n => (caches['pel'][n] = Object.keys(caches['pel']).includes('' + n) ? caches['pel'][n] : 2 * pel(n - 1) + pel(n - 2));
const tet = n => (caches['tet'][n] = Object.keys(caches['tet']).includes('' + n) ? caches['tet'][n] : tet(n - 1) + tet(n - 2) + tet(n - 3) + tet(n - 4));
const tri = n => (caches['tri'][n] = Object.keys(caches['tri']).includes('' + n) ? caches['tri'][n] : tri(n - 1) + tri(n - 2) + tri(n - 3));

function mixbonacci(pattern, length) {
    if (pattern.length === 0 || length === 0) return [];
    var out = []
    var seqs = {
        'fib': { 'f': fib, 'index': 0 },
        'pad': { 'f': pad, 'index': 0 },
        'jac': { 'f': jac, 'index': 0 },
        'pel': { 'f': pel, 'index': 0 },
        'tet': { 'f': tet, 'index': 0 },
        'tri': { 'f': tri, 'index': 0 }
    }

    for (var i = 0; i < length; i++) {
        var p = pattern[i % pattern.length].trim();
        var seq = seqs[p];
        out.push(seq.f(seq.index++));
    }
    return out;
}
