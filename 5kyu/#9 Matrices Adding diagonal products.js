//these codes are modified from an old kata's solution abount connect four.
function sumProdDiags(grid) {
    var ROW = grid.length, COL = grid[0].length
    var sum1 = 0, sum2 = 0;
    //top left to right down 
    for (var row = 1; row < ROW; row++) {
        var temp = 1;
        for (var i = row, j = 0; i < ROW && j < COL; i++, j++)temp *= grid[i][j];
        sum1 += temp
    }
    //top left to right down 
    for (var col = 0; col < COL; col++) {
        var temp = 1;
        for (var j = col, i = 0; i < ROW && j < COL; i++, j++)temp *= grid[i][j];
        sum1 += temp
    }

    //top right to left down
    for (var row = 1; row < ROW; row++) {
        var temp = 1;
        for (var i = row, j = COL - 1; i < ROW && j > -1; i++, j--)temp *= grid[i][j];
        sum2 += temp
    }
    //top right to left down
    for (var col = COL - 1; col > -1; col--) {
        var temp = 1;
        for (var j = col, i = 0; i < ROW && j > -1; i++, j--)temp *= grid[i][j];
        sum2 += temp
    }
    return sum1 - sum2
}