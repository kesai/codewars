function advice(agents, n) {
    agents = agents.filter(([x, y]) => -1 < x && x < n && -1 < y && y < n);
    if (agents.length === n * n) return [];
    var result = [];
    var max_distance = -1;
    for (var i = 0; i < n; i++) {
        for (var j = 0; j < n; j++) {
            var dis = Math.min(...agents.map(([x, y]) => Math.abs(x - i) + Math.abs(y - j)));
            if (dis > max_distance) {
                result.length = 0;
                max_distance = dis;
            }
            if (dis === max_distance) result.push([i, j])
        }
    }
    return result
}