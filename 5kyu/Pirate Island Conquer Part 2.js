function Point(x, y) {
    this.x = x;
    this.y = y;
}
function distance(p1, p2) {
    return Math.abs(p1.x - p2.x) + Math.abs(p1.y - p2.y);
}

function conquerIsland(map) {
    var homes = [];
    var marines = [];
    var untakens = [];
    for (var i = 0; i < map.length; i++) {
        for (var j = 0; j < map[0].length; j++) {
            map[i][j] === 'p' ? homes.push(new Point(i, j)) : map[i][j] === 'm' ? marines.push(new Point(i, j)) : map[i][j] === 'u' ? untakens.push(new Point(i, j)) : [];
        }
    }
    if (untakens.length) {
        untakens = untakens.map(u => new Object({ 'pos': u, 'dis': Math.min(...homes.map(p => distance(u, p))) })).sort((a, b) => a.dis - b.dis || a.pos.x - b.pos.x)
        var res = untakens.filter(v => v.dis === untakens[0].dis).map(v => [v.pos.x, v.pos.y]);
        return res.length === 0 ? res[0] : res;
    } else if (marines.length) {
        marines = marines.map(u => new Object({ 'pos': u, 'dis': Math.min(...homes.map(p => distance(u, p))) })).sort((a, b) => a.dis - b.dis || a.pos.x - b.pos.x)
        res = marines.filter(v => v.dis === marines[0].dis).map(v => [v.pos.x, v.pos.y]);
        return res.length === 0 ? res[0] : res;
    }
    return [];
}