function phone(strng, num) {
    var arr = strng.split('\n').filter(v => v != '').map(v => new Object({
        phone: v.match(/\d{1,2}-\d{3}-\d{3}-\d{4}/)[0],
        name: v.match(/(?<=<).+(?=>)/)[0],
        address: v.replace(/\S*\d{1,2}-\d{3}-\d{3}-\d{4}\S*/, '')
            .replace(/<.*>/, '').trim().replace(/[/;,_]/, ' ').replace(/\s{2,}/, ' ')
    }));
    var result = arr.filter(v => v.phone === num);
    if (!result.length) return `Error => Not found: ${num}`
    if (result.length > 1) return `Error => Too many people: ${num}`
    return `Phone => ${result[0].phone}, Name => ${result[0].name}, Address => ${result[0].address}`
}
