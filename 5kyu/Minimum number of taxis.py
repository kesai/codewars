import queue as Q


class Order():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __lt__(self, other):
        if self.y-self.x != other.y-other.x:
            return self.y-self.x < other.y-other.x
        else:
            if self.x != other.x:
                return self.x < other.x
            else:
                return self.y < other.y

    def __cmp__(self, other):
        if self.y-self.x != other.y-other.x:
            return self.y-self.x < other.y-other.x
        else:
            if self.x != other.x:
                return self.x < other.x
            else:
                return self.y < other.y

    def __repr__(self):
        return f'({self.x},{self.y})'


def min_num_taxis(requests):
    que = Q.PriorityQueue()
    first = requests.pop(0)
    que.put(Order(first[0], first[1]))
    n = 1
    for req in requests:
        tmp = que.get()
        st, end = tmp.x, tmp.y
        if req[0] > end:
            que.put(Order(st, req[1]))
        elif req[1] < st:
            que.put(Order(req[0], end))
        else:
            que.put(Order(st, end))
            que.put(Order(req[0], req[1]))
            n += 1
    return n


min_num_taxis([(6, 16), (16, 17), (16, 17), (14, 15), (8, 10), (14, 20), (9, 16),
               (10, 19), (4, 8), (17, 20), (17, 19), (1, 13), (6, 13), (8, 18), (11, 15)])
