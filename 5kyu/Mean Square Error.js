var solution = function (firstArray, secondArray) {
    return firstArray.reduce((s, v, i) => s += (v - secondArray[i]) ** 2, 0) / firstArray.length
}