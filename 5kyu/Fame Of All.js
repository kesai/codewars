class HallOfFame {
    constructor(size = 5, players = []) {
        this.size = size;
        this.players = players;
        this.sort()
    }
    sort() {
        this.players = this.players.sort((a, b) => b[1] - a[1] || a[0].localeCompare(b[0])).slice(0, this.size);
    }
    get list() {
        var res = this.players.map(v => v.join(': '));
        return res.concat(res.length > this.size ? [] : Array(this.size - res.length).fill(''));
    }
    add(player) {
        var idx = this.players.findIndex(v => v[0] === player[0]);
        if (idx > -1) this.players[idx][1] = Math.max(this.players[idx][1], player[1]); else this.players.push(player);
        this.sort();
        return this;
    }
}
