function alphabetWar(battlefield) {
    if (!/#/.test(battlefield)) return battlefield.replace(/[^a-z]/g, '');
    var battles = battlefield.match(/(\[[a-z]*\])|#/g)
    console.log(battles.join(''))
    var suvivors = []
    for (var i = 0; i < battles.length; i++) {
        if (battles[i] != '#') {
            //look for neighbor nuclears
            var n = 0;
            if (battles[i - 1] === '#') {
                n++;
                if (battles[i - 2] === '#') n++;
            }
            if (battles[i + 1] === '#') {
                n++;
                if (battles[i + 2] === '#') n++;
            }
            if (n <= 1) suvivors.push(battles[i]);
        }
    }
    return suvivors.join('').replace(/[^a-z]/g, '')
}

