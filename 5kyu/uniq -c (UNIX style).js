function uniq_c(a) {
    if (!a.length) return []
    var out = []
    var c = a.shift()
    var n = 1;
    for (x of a) {
        if (x === c) n++; else { out.push([c, n]); n = 1; c = x; }
    }
    out.push([c, n])
    return out;
}