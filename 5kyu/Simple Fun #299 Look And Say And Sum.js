var caches = ['1'];
function lookAndSayAndSum(n) {
    while (caches.length < n) {
        var last = caches[caches.length - 1];
        var next = last.replace(/(.)\1*/g, (v) => `${v.length}${v[0]}`);
        caches.push(next);
    }
    return [...caches[n - 1]].reduce((s, v) => s += +v, 0);
}