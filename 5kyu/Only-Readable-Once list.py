class SecureList():
    def __init__(self, lst):
        self.list = lst.copy()

    def __getitem__(self, i):
        temp = self.list[i]
        del self.list[i]
        return temp

    def __str__(self):
        temp = str(self.list)
        self.list.clear()
        return temp

    def __repr__(self):
        temp = str(self.list)
        self.list.clear()
        return temp

    def __len__(self):
        temp = len(self.list)
        self.list.clear()
        return temp


base = [1, 2, 3, 4]
a = SecureList(base)
print(a[0] == base[0])
