var shakeTree = function (tree) {
    var out = tree[0].map(v => v === 'o' ? 1 : 0);
    for (var i = 1; i < tree.length; i++) {
        for (var j = 0; j < tree[0].length; j++) {
            if (/[/\-\\]/.test(tree[i][j])) {
                if (tree[i][j] === '/') {
                    //go left
                    for (var k = j - 1; k > -1; k--) {
                        if (tree[i][k] === '\\') break;
                        else if (/[^/\-\\]/.test(tree[i][k])) { out[k] += out[j]; break; }
                    }
                } else if (tree[i][j] === '\\') {
                    for (var k = j + 1; k < tree[0].length; k++) {
                        if (tree[i][k] === '/') break;
                        else if (/[^/\-\\]/.test(tree[i][k])) { out[k] += out[j]; break; }
                    }
                }
                out[j] = 0;
            } else if (tree[i][j] === 'o') {
                out[j] += 1;
            }
        }

    }
    return out;
}


var tree = [[' ', 'o', ' ', ' ', ' ', ' ', ' ', ' '],
[' ', '/', ' ', 'o', ' ', 'o', '/', ' '],
[' ', '\\', '/', '/', ' ', ' ', ' ', ' '],
[' ', ' ', ' ', '\\', '/', '/', ' ', ' '],
[' ', ' ', ' ', '|', '|', ' ', ' ', ' '],
[' ', ' ', ' ', '|', '|', ' ', ' ', ' '],
[' ', ' ', ' ', '|', '|', ' ', ' ', ' ']]
console.log(shakeTree(tree))
