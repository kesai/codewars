function changeCase(s, targetCase) {
    if (!['snake', 'camel', 'kebab'].includes(targetCase)) return undefined;
    if (/_/.test(s) && /\-/.test(s)) return undefined;
    if (/[_\-]/.test(s) && /[A-Z]/.test(s)) return undefined;
    if (targetCase === 'camel') {
        if (/[_\-]/.test(s)) {
            var arr = s.toLowerCase().split(/[_\-]/g);
            return arr[0] + arr.slice(1).map(v => v[0].toUpperCase() + v.slice(1)).join('');
        }
        else return s;
    }
    if (targetCase === 'snake') {
        if (/\-/.test(s)) return s.replace(/\-/g, '_');
        if (/[A-Z]/.test(s)) {
            return s.split(/(?=[A-Z])/g).map(v => v.toLowerCase()).join('_')
        } else return s;
    }
    if (targetCase === 'kebab') {
        if (/_/.test(s)) return s.replace(/_/g, '-');
        if (/[A-Z]/.test(s)) {
            return s.split(/(?=[A-Z])/g).map(v => v.toLowerCase()).join('-')
        } else return s;
    }
}
console.log(changeCase("little_shop_of_horrors", "kebab"))