function isSolved(board) {
    var arr = []
    //横向
    arr = board.map(v => v.join(''));
    //纵向
    for (var i = 0; i < 3; i++)arr.push(board.map(v => v[i]).join(''));
    //对角
    arr.push('' + board[0][0] + board[1][1] + board[2][2]);
    arr.push('' + board[0][2] + board[1][1] + board[2][0]);
    for (x of arr) {
        if (x === '111') return 1
        if (x === '222') return 2;
    }
    if (board.findIndex(v => v.includes(0)) > -1) return -1;
    return 0
}