function calc_old(cards, turn = 1) {
    if (cards.length === 1) return (2 ** turn) * cards[0];
    return (2 ** turn) * cards[0] + calc_old(cards.slice(1), turn + 1)
}
console.log(calc_old([98, 22, 89, 44, 50, 96, 32, 34, 78, 58, 95, 60, 32, 4, 27, 24, 50, 92, 30, 92, 79, 90, 62, 84, 31, 91, 22, 26, 43, 69]))

function calc(cards) {
    var turn = 1;
    var res = 0;
    while (cards.length) {
        if (cards[0] > cards.slice(-1)[0]) res += cards.pop() * (2 ** turn++); else res += cards.shift() * (2 ** turn++);
    }
    return res;
}

console.log(calc([4, 9, 2, 7]))
