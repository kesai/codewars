function alphabetWar(fight) {
    var left = { 'w': 4, 'p': 3, 'b': 2, 's': 1, 't': 0 }
    var right = { 'm': 4, 'q': 3, 'd': 2, 'z': 1, 'j': 0 }
    var converter = { 'w': 'm', 'm': 'w', 'p': 'q', 'q': 'p', 'b': 'd', 'd': 'b', 's': 'z', 'z': 's' }
    var specials = fight.match(/(?<=t)[^tj](?=j)|(?<=j)[^tj](?=t)/g) || [];
    fight = fight.replace(/(?<=t)[^tj](?=j)/g, '').replace(/(?<=j)[^tj](?=t)/g, '');
    //convert right to left
    fight = fight.replace(/((?<=t)[mqdz])|[mqdz](?=t)/g, v => converter[v]);
    //convert left  to right
    fight = fight.replace(/((?<=j)[wpbs])|[wpbs](?=j)/g, v => converter[v]);
    power_left = 0, power_right = 0;
    for (c of fight) {
        if (/[wpbs]/.test(c)) power_left += left[c];
        if (/[mqdz]/.test(c)) power_right += right[c];
    }
    for (c of specials) {
        if (/[wpbs]/.test(c)) power_left += left[c];
        if (/[mqdz]/.test(c)) power_right += right[c];
    }
    return power_left > power_right ? 'Left side wins!' : power_left < power_right ? 'Right side wins!' : 'Let\'s fight again!'
}