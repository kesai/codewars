# ref:https://en.wikipedia.org/wiki/Derangement
# https://mathworld.wolfram.com/Derangement.html


def all_permuted(n):
    return 0 if n < 2 else 1 if n == 2 else n*all_permuted(n-1)+(-1)**n
