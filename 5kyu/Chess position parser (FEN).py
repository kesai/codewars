'''
Empty white square: ▇ (2587)
Empty black square: ＿ (FF3F)
White king (K): ♚ (265A)
White queen (Q): ♛ (265B)
White rook (R): ♜ (265C)
White bishop (B): ♝ (265D)
White knight (N): ♞ (265E)
White pawn (P): ♟ (265F)
Black king (k): ♔ (2654)
Black queen (q): ♕ (2655)
Black rook (r): ♖ (2656)
Black bishop (b): ♗ (2657)
Black knight (n): ♘ (2658)
Black pawn (p): ♙ (2659)
'''

import re


def parse_fen(string):
    pieces = {
        'K': u'\u265A',
        'Q': u'\u265B',
        'R': u'\u265C',
        'B': u'\u265D',
        'N': u'\u265E',
        'P': u'\u265F',
        'k': u'\u2654',
        'q': u'\u2655',
        'r': u'\u2656',
        'b': u'\u2657',
        'n': u'\u2658',
        'p': u'\u2659'
    }
    out = []
    wb = string.split(' ')[1]
    s = string.split(' ')[0]
    arr = s.split('/')
    if wb == 'b':
        arr.reverse()
        arr = [''.join(reversed(list(x))) for x in arr]
    for i, v in enumerate(arr):
        v = v.split(' ')[0]
        v = re.sub(r'\d', lambda x: '.'*int(x[0]), v)
        empty = u'\u2587\uff3f'*4
        if i % 2:
            empty = u'\uff3f\u2587'*4
        empties = list(empty)
        for j, c in enumerate(v):
            if c != '.':
                empties[j] = pieces[c]

        out.append(''.join(empties))
    return '\n'.join(out)+'\n'

    #parse_fen("8/8/8/8/8/8/8/8 w - -")
print(parse_fen('7k/1p4rp/p2B4/3P1r2/1P6/8/P6P/R6K b - - 0 31'))
