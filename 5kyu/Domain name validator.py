import re


def check_domain(domain):
    if len(domain) > 63:
        return False
    if re.findall(r'(^[^a-z\d])|([^a-z\d]$)', domain, re.I):
        return False
    if re.fullmatch(r'^[a-z\d\-]+$', domain, re.I):
        return True
    return False


def check_top(top_domain):
    if not check_domain(top_domain):
        return False
    if re.fullmatch(r'^\d+$', top_domain):
        return False
    return True


def validate(domain):
    print(domain)
    domains = domain.split('.')
    if len(domains) > 127 or len(domains) < 2:
        return False
    if len(domain) > 253:
        return False
    # check top level
    top_domain = domains[-1]

    return check_top(top_domain) and all(check_domain(x) for x in domains)


print(validate('1.1.168.192.in-addr.arpa'))
