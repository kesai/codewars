function encoder(data) {
    var dict = { '': 0 };
    var i = 0
    var index = 0;
    var k = 0;
    var result=''
    for(var i=0;i<data.length;i++){
        var x=data[i];
        while(Object.keys(dict).includes(x)){
            k=dict[x];
            if(++i===data.length){result+=k;  break;}
            x += data[i];
        }
        if(i>=data.length)break;
        result+=`${k}${x.slice(-1)}`
        k=0
        dict[x] = ++index;
    }
    return result;
}

function decoder(data) {
    var dict={0:''}
    var arr=data.match(/(\d+)[a-z]/gi)||[];
    var result=''
    var index=0;
    for(var i=0;i<arr.length;i++){
        var d=+arr[i].slice(0,-1);
        var c=arr[i].slice(-1);
        if(d===0){
            result+=c;
            dict[++index]=c;
        }else{
            var prefix=''
            for(var j=0;j<d;j++){
                prefix=dict[j+1];
            }
            result+=prefix+c;
            dict[++index]=prefix+c;
        }
    }
    return result+dict[(data.match(/\d+$/)||[])[0]||0];
}