function superStreetFighterSelection(fighters, position, moves) {
    var out = []
    var [x, y] = position;
    function wrap() {
        y = y === fighters[0].length ? 0 : y === -1 ? fighters[0].length - 1 : y;
        x = x === fighters.length ? x - 1 : x === -1 ? 0 : x;
    }
    for (mov of moves) {
        if (mov === 'right') {
            y++; wrap();
            while (!fighters[x][y]) { y++; wrap(); }
        } else if (mov === 'left') {
            y--; wrap();
            while (!fighters[x][y]) { y--; wrap(); }
        } else if (mov === 'up') {
            x--; wrap(); if (!fighters[x][y]) x++;
        } else if (mov === 'down') {
            x++; wrap(); if (!fighters[x][y]) x--;
        }
        out.push(fighters[x][y]);
    }
    return out;
}
