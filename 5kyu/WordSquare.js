function wordSquare(letters) {
    if (Math.sqrt(letters.length) % 1 != 0) return false;
    var odd_cnt = 0;
    for (x of new Set(letters)) {
        if ([...letters].filter(v => v === x).length % 2) odd_cnt++;
    }
    if (odd_cnt > 3) return false;
    return true;
}

