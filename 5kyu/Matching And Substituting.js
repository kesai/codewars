function change(s, prog, version) {
    var obj = {}
    for ([k, v] of s.split('\n').map(v => v.split(/\s*:\s*/))) obj[k] = v;
    if (!/^\+1-\d{3}-\d{3}-\d{4}$/.test(obj.Phone) || !/^\d+\.\d+$/.test(obj.Version)) return 'ERROR: VERSION or PHONE'
    return `Program: ${prog} Author: g964 Phone: +1-503-555-0090 Date: 2019-01-01 Version: ${obj.Version!='2.0'?version : obj.Version}`;
}