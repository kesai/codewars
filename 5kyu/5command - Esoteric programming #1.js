class FiveCommands {
    read(input, debug) {
        let p = 0, cells = [0], output = '', debugs = [];
        for (let c of input) {
            switch (c) {
                case '+': if (++p === cells.length) cells.push(0); break;
                case '-': if (--p === -1) { cells.unshift(0); p = 0; } break;
                case '^': cells[p]++; break;
                case 'v': cells[p]--; break;
                case '*': output += cells[p]; break;
            }
        }
        if (debug) debugs = cells.map((v, i) => `${i}->${v}`);
        return { 'output': output, 'debug': debugs };
    }
}
var inter = new FiveCommands();

console.log(inter.read('+^^^-^v*+*'))