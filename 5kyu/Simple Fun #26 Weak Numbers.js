function d(x) {
    for (var i = 1, n = 0; i <= x; i++) { if (x % i === 0) n++; }
    return n;
}

function weakNumbers(n) {
    divisors = Array.from({ length: n }, (_, i) => d(i + 1));
    weekness_arr = []
    var max_weekness = -Infinity;
    for (var i = 1; i <= n; i++) {
        var cur = divisors[i - 1];
        var weekness = divisors.slice(0, i).filter(v => v > cur).length;
        max_weekness = Math.max(weekness, max_weekness);
        weekness_arr.push(weekness);
    }
    return [max_weekness, weekness_arr.filter(v => v === max_weekness).length]
}
weakNumbers(1000)