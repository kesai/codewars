
# return the product of the two polynomials p1 and p2.


def poly_multiply(p1, p2):
    if not p1 or not p2:
        return []
    result = []
    n = len(p1)-1+len(p2)
    for p in range(n):
        x = 0
        for i in range(p+1):
            if i > -1 and i < len(p1) and p-i > -1 and p-i < len(p2):
                x += p1[i]*p2[p-i]
        result.append(x)
    return result
