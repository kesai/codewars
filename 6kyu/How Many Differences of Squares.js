//https://www.quora.com/Which-integers-cannot-be-represented-as-the-difference-of-two-squares
function countSquareable(n) {
    cnt = 0;
    for (var i = 1; i <= n; i++) {
        if (i % 4 != 2) cnt++;
    }
    return cnt;
}
