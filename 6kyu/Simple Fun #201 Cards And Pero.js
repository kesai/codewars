function cardsAndPero(s) {
  if (s.match(/.../g).length > new Set(s.match(/.../g)).size) return [-1, -1, -1, -1]
  return [...'PKHT'].map(suit => 13 - (s.match(new RegExp(`${suit}..`, 'g')) || []).length)
}