function matrixElementsSum(matrix) {
    var sum = matrix[0].reduce((s, v) => s += v, 0);
    for (var i = 1; i < matrix.length; i++) {
        for (var j = 0; j < matrix[0].length; j++) {
            var column = matrix.map(v => v[j]);
            var idx = column.indexOf(0);
            if (idx === -1 || idx > i) sum += matrix[i][j];
        }
    }
    return sum
}
