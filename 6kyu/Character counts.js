String.prototype.characterCount = function (chars) {
    if (!this || !chars) return undefined;
    var arr = Array.isArray(chars) ? chars : chars.split('');
    var res = arr.map(v => (this.match(new RegExp(`\\${v}`)) || []).length)
    return res.length === 1 ? res[0] : res;
};