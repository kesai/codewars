function cake(item, n) {
    var recipe = { 'caster sugar': 160, 'butter': 170, 'eggs': 3, 'self-raising flour': 115, 'cocoa powder': 55 };
    var res = {}
    for (k of Object.keys(recipe)) {
        var x = +(n * recipe[k] / recipe[item]).toFixed(1);
        res[k] = k === 'eggs' ? x : `${x}g`
    }
    return res;
}