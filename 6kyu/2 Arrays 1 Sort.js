function linkedSort(aToSort, aLinked, compare) {
    aToSort_copy = [].concat(aToSort);
    aLinked_copy = [].concat(aLinked);
    var arr;
    if (compare) {
        arr = aToSort.map((v, i) => [v, i]).sort((a, b) => compare(a[0], b[0]));
    } else {
        arr = aToSort.map((v, i) => [v, i]).sort();
    }
    for (var i = 0; i < arr.length; i++) {
        aToSort[i] = aToSort_copy[arr[i][1]];
        aLinked[i] = aLinked_copy[arr[i][1]];
    }
    return aToSort
}


