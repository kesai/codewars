import math
def sqr_modulus(z):
	f=lambda x,a,b:a*a+b*b if x=='cart' else a*a
	if z[0] not in ['cart','polar'] or any(type(v)  is not int for v in z[1:]):
		return (False,-1,1)
	total=sum(f(z[0],a,b)  for (a,b) in zip(z[1::2],z[2::2]))
	return (True,total, int(''.join(sorted(str(total),reverse=True))))




print(sqr_modulus(['polar', 2531, 3261]))