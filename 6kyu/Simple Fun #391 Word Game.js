function wordGame(gameMap, wordsList, tapSequence) {
    var change = c => /[a-z]/.test(c) ? c.toUpperCase() : c.toLowerCase();
    var map = gameMap.split('\n').map(v => v.split(''));
    var checked = []
    for ([x, y] of tapSequence) {
        map[x][y] = change(map[x][y]);
        if (/[A-Z]/.test(map[x][y])) checked.push(`${x}_${y}`);
        else {
            if (checked.indexOf(`${x}_${y}`) > -1) checked.splice(checked.indexOf(`${x}_${y}`), 1)
        }
        if (checked.length === 4) {
            var word = checked.map(v => map[+v.split('_')[0]][+v.split('_')[1]]).join('').toLowerCase();
            for (p of checked) {
                var [i, j] = p.split('_').map(v => +v);
                map[i][j] = wordsList.includes(word) ? ' ' : change(map[i][j]);
            }
            checked.length = 0;
        }
    }
    return map.map(v => v.join('')).join('\n')
}
