import re


def i_or_f(arr):
    return bool(re.fullmatch(r'^(\+|-)?(((\d+)?\.\d+)|(\d+(\.)?(\d+)?)|(\d+(\.?)(\d+)?e(\+|-)?\d+))$', arr, re.I))
