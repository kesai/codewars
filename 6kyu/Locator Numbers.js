var letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
function toLocString(num) {
    if (num < 0) return '-' + toLocString(-num);
    return [...num.toString(2)].reverse().map((v, i) => +v ? letters[i] : '').join('');
}
function toInt(str) {
    str = str.replace(/[^a-z\-]/gi, '');
    if (str.startsWith('-')) return -toInt(str.slice(1));
    str = str.replace(/[^a-z]/gi, '');
    return [...str].reduce((s, v) => s += 2 ** letters.indexOf(v), 0)
}
