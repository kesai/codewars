function countourMode(matrix, a, b) {
    var ROW = matrix.length;
    var COL = matrix[0].length;
    var up = matrix[0].slice(0, -1);
    var right = matrix.map(v => v[COL - 1]).slice(0, -1);
    var bottom = matrix[ROW - 1].slice(1).reverse();
    var left = matrix.map(v => v[0]).reverse().slice(0, -1);
    var countour = up.concat(right).concat(bottom).concat(left);
    var arr = countour.filter(v => v >= a && v <= b);
    var counts = {};
    for (x of arr) counts[x] = counts[x] ? counts[x] + 1 : 1;
    if (new Set(Object.values(counts)).size === 1) return []
    var max = Math.max(...Object.values(counts));
    var items = Object.entries(counts).filter(v => v[1] === max).map(v => +v[0]).sort((a, b) => a - b)
    return [max, items]
}
M = [[1, 3, -4, 5, -2, 5, 1],
[2, 0, -7, 6, 8, 8, 15],
[4, 4, -2, -10, 7, -1, 7],
[-1, 3, 1, 0, 11, 4, 21],
[-7, 6, -4, 10, 5, 7, 6],
[-5, 4, 3, -5, 7, 8, 17],
[-11, 3, 4, -8, 6, 16, 4]]
countourMode(M, -1, 7)

