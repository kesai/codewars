function cipher26(message) {
    var chars = 'abcdefghijklmnopqrstuvwxyz';
    var res = '', sum = 0;
    for (c of message) {
        if (res) sum += chars.indexOf(res[res.length - 1]);
        for (var j = 0; j < 26; j++) {
            if ((sum + j) % 26 === chars.indexOf(c)) { res += chars[j]; break; }
        }
    }
    return res;
}