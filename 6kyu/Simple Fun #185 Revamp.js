function revamp(s) {
    var sum = w => [...w].reduce((s, c) => s += c.charCodeAt(0), 0);
    return s.match(/[a-z]+/g).map(w => [...w].sort().join(''))
        .sort((a, b) => sum(a) - sum(b) || a.length - b.length || a.localeCompare(b)).join(' ')
}