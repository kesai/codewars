function uncompress(music) {
    var out = []
    for (x of music.split(',')) {
        if (/^\d+$/.test(x)) {
            out.push(+x);
        } else if (/^-?\d+\*\d+$/.test(x)) {
            var [_, first, cnt] = x.match(/^(-?\d+)\*(\d+)$/).map(v => +v);
            out = out.concat(Array(cnt).fill(first));
        } else {
            var [_, first, last, d] = x.match(/(-?\d+)-(-?\d+)(?:\/(\d+))?/).map(v => +v);
            d = d ? d : 1;
            d = first > last ? -d : d;
            out = out.concat(Array.from({ length: Math.abs((first - last) / d) + 1 }, (_, i) => first + i * d));
        }
    }
    return out;
}

uncompress('1,3-5,8')