function topScores(records, nTop) {
    students = [...new Set(records.map(v => v[0]))];
    students = students.map(v => [v, Math.max(...records.filter(e => e[0] === v).map(x => x[1]))])
    var res = students.sort((a, b) => b[1] - a[1] || a[0].localeCompare(b[0])).slice(0, nTop)
    return nTop <= 0 ? [] : res;
}