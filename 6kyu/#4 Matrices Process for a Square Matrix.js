function avgDiags(m) {
    var sum1 = 0, n1 = 0;
    var sum2 = 0, n2 = 0;
    for (var i = 0; i < m.length; i++) {
        if (i % 2 && m[i][i] >= 0) { sum1 += m[i][i]; n1++; }
        if ((m.length - 1 - i) % 2 === 0 && m[i][m.length - 1 - i] < 0) { sum2 += m[i][m.length - 1 - i]; n2++; }
    }
    return [
        n1 ? Math.round(sum1 / n1) : -1, n2 ? Math.round(Math.abs(sum2 / n2)) : -1]
}
