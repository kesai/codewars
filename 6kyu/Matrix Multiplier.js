function getMatrixProduct(a, b) {
    const row_mul_col = (row, col) => row.reduce((s, v, i) => s += v * col[i], 0)
    if (a[0].length != b.length) return -1;
    return a.map(v => b[0].map((_, j) => row_mul_col(v, b.map(x => x[j]))))
};


console.log(getMatrixProduct([
    [1, 2],
    [3, 4]
], [
    [5, 6],
    [7, 8]
]))