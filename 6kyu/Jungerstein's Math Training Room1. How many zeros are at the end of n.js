
function countZeros(n) {
    if (n % 2) return 0;
    cnt = 0;
    for (i = 10; i <= n; i += 10) {
        var temp = i;
        while (temp % 5 === 0) { cnt++; temp /= 5 }
    }
    return cnt;
}
