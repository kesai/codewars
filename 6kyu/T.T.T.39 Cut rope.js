function cutRope(length, m, n) {
    var s = '-'.repeat(length);
    s = s.match(new RegExp(`.{1,${m}}`, 'g')).join('.')
    var k = 0;
    var rope = ''
    for (var i = 0; i < s.length; i++) {
        rope += s[i];
        if (s[i] != '.') { k++; }
        if (k === n) { rope += '.'; k = 0; }
    }
    var arr = rope.split('.');
    var result = {}
    for (x of new Set(arr)) {
        if (x) result[`${x.length}cm`] = arr.filter(v => v === x).length
    }
    return result;
}
