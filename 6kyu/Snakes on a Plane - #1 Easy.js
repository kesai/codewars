function snakesOn(aPlane) {
    var x = aPlane.map(v => v.join('')).join('')
    return new Set([...x.replace(/[a-z]/gi, '')]).size
}