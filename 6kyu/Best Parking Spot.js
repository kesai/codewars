function bestParkingSpot(arr) {
    var out = [];
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === 'OPEN') {
            var steps = 2 * i;
            var right = Infinity;
            var left = Infinity;
            for (var j = i + 1; j < arr.length; j++) {
                if (arr[j] === 'CORRAL') { right = j - i; break; }
            }
            for (var j = i - 1; j > 0; j--) {
                if (arr[j] === 'CORRAL') { left = i - j; break; }
            }
            steps += 2 * (Math.min(right, left));
            out.push({ 'index': i, 'steps': steps });
        }
    }
    return out.sort((a, b) => a.steps - b.steps || b.index - a.index)[0].index
}