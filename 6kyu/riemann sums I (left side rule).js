function left_riemann(f, n, a, b) {
    var sum = 0;
    var dx = (b - a) / n;
    for (var i = 0; i < n; i++) {
        x = dx * i + a;
        sum += dx * f(x);
    }
    return sum;
}