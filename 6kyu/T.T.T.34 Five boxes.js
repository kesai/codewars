function heaviestBox(arr) {
    var sum = arr.reduce((s, v) => s += v, 0) / 4;
    arr.sort((a, b) => b - a);
    var a_c = arr[1];
    var abde = arr[0] + arr[arr.length - 1];
    var c = sum - abde;
    return a_c - c;
}
