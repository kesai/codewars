function zeroPlentiful(arr) {
    var x = (',' + arr.join(',') + ',').match(/(?<=,)(0,){1,}/g) || [];
    return x.every(v => v.length >= 8) ? x.length : 0
}