class JavaUnicodeEncoder {
    static decode(input) {
        return input.replace(/\\u(.{4})/g, (_, v) => String.fromCharCode(parseInt(v, 16)))
    }
    static encode(input) {
        return input.replace(/./g, (v) => `\\u${(v.charCodeAt(0).toString(16)).padStart(4, '0')}`)
    }
}


