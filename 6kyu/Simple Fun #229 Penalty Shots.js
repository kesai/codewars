function penaltyShots(shots, score) {
    if (shots >= 5) return score[0] === score[1] ? 2 : 1;
    var [a, b] = score.sort((a, b) => b - a)
    return b - a + 5 - shots;
}