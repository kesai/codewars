from bs4 import BeautifulSoup
import urllib.request


def get_honor(username='myjinxin2015'):
    res = urllib.request.urlopen(f'https://www.codewars.com/users/{username}')
    html = res.read()
    data = html.decode("utf-8")
    soup = BeautifulSoup(data, "html.parser")
    for x in soup.select('div .stat'):
        if 'Honor' in x.text:
            return int(x.text.split(':')[1].replace(',', ''))


print(get_honor('kesai'))
