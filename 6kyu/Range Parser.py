import re


def range_parser(string):
    result = []
    for x in string.split(','):
        x = x.strip()
        if re.match(r'^\d+$', x):
            result.append(int(x))
        elif re.match(r'^\d+\-\d+$', x):
            [a, b] = x.split('-')
            result += [i for i in range(int(a), int(b)+1)]
        elif re.match(r'^\d+\-\d+:\d+$', x):
            [a, b, c] = re.split(r'\-|:', x)
            result += [i for i in range(int(a), int(b)+1, int(c))]
    return result


range_parser('1-10,14, 20-25:2')
