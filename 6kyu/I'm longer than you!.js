function cmp(a, b) {
    var s = 'ABCDEFGHIJKMLNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    if (a.length != b.length) return a.length - b.length;
    for (var i = 0; i < Math.max(a.length, b.length); i++) {
        var idx_a = s.indexOf(a[i]);
        var idx_b = s.indexOf(b[i]);
        if (idx_a != idx_b) return idx_a - idx_b;
    }
}
function longer(s) {
    return s.split(' ').sort(cmp).join(' ')
}