function solve(n) {
    var arr = Array.from({ length: n }, (_, i) => i + 1);
    var res = [arr.shift()]
    while (arr.length >= arr[0]) {
        res.push(arr[0]);
        arr = arr.filter((v, i) => i % arr[0]);
    }
    return res.concat(arr).reduce((s, v) => s += v, 0)
}