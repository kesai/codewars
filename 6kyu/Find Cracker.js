function findHack(arr) {
    var points = { 'A': 30, 'B': 20, 'C': 10, 'D': 5 };
    score1 = (arr) => arr.reduce((s, v) => s += points[v] || 0, 0);
    score2 = (arr) => score1(arr) + (arr.filter(v => /[AB]/.test(v)).length > 4 ? 20 : 0);
    return arr.filter(v => (v[1] != score1(v[2]) && v[1] != score2(v[2])) || v[1] > 200).map(v => v[0])
}
