function correctOrder(list) {
    return list.map(get_price)
}

function get_price(o) {
    for (x = 5.98; x <= 19.98; x++) {
        for (y = 5.79; y <= 19.79; y++) {
            if (Math.abs(o.tire * x + o.steeringWheel * y - o.totalPrice) < 0.00001 && y > x && y - x < 3) {
                return { "tire": 4, "steeringWheel": 1, "totalPrice": +(4 * x + y).toFixed(2) }
            }
        }
    }
}
