function quidditchScoreboard(teams, actions) {
    var [_, team1, team2] = teams.match(/^(.+)\svs\s(.+)$/);
    var scores = { [team1]: 0, [team2]: 0 };
    for (x of actions.split(', ')) {
        var [_, team, action] = x.match(/^(.+):\s(.+)$/);
        if (action === 'Caught Snitch') { scores[team] += 150; break; }
        scores[team] += action === 'Quaffle goal' ? 10 : -30;
    }
    return `${team1}: ${scores[team1]}, ${team2}: ${scores[team2]}`
}