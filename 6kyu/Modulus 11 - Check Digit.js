function addCheckDigit(number) {
    x = [...'' + number].reverse();
    factors = ''.padEnd(x.length, '234567');
    var sum = x.reduce((s, v, i) => s += +v * +factors[i], 0);
    r = (r = sum % 11) === 1 ? 'X' : r == 0 ? '0' : 11 - r
    return number + r;
}