var f = (x) => x > 1 ? x * f(x - 1) : 1;
function travelChessboard(s) {
    var [_, A, B] = s.match(/\((\d+ \d+)\)\((\d+ \d+)\)/);
    A = A.split(' ').map(v => +v);
    B = B.split(' ').map(v => +v);
    var m = Math.abs(A[0] - B[0]) + Math.abs(A[1] - B[1])
    var n = Math.abs(A[0] - B[0]);
    return f(m) / f(n) / f(m - n)
}