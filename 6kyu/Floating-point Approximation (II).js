function interp(f, l, u, n) {
    var d = (u - l) / n;
    return Array.from({ length: n }, (_, i) => floor(f(l + i * d) * 100.0) / 100.0);
}