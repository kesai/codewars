function lemmingBattle(battlefield, green, blue) {
    green.sort((a, b) => b - a);
    blue.sort((a, b) => b - a);
    while (green.length && blue.length) {
        var n = Math.min(...[green.length, blue.length, battlefield])
        for (var i = 0; i < n; i++) {
            var temp = green[i];
            green[i] -= blue[i];
            blue[i] -= temp;
        }
        green = green.filter(v => v > 0).sort((a, b) => b - a);
        blue = blue.filter(v => v > 0).sort((a, b) => b - a);
    }
    return green.length ? `Green wins: ${green.join(' ')}` : blue.length ? `Blue wins: ${blue.join(' ')}` : "Green and Blue died"
}