String.prototype.kontti = function () {
    var s = this.valueOf();
    function repl(w) {
        var suffix = 'kontti';
        if (/[aeiouy]/.test(w)) {
            var idx = w.match(/[aeiouy]/i).index;
            var idx2 = suffix.match(/[aeiouy]/i).index;
            return suffix.slice(0, idx2 + 1) + w.slice(idx + 1) + '-' +
                w.slice(0, idx + 1) + suffix.slice(idx2 + 1)
        } else return w;
    }
    return s.replace(/[a-z]+/gi, repl)
}