var gcd = (x, y) => y ? gcd(y, x % y) : x;
var simplify = (a, b) => [a / gcd(a, b), b / gcd(a, b)]
function fareySequence(n, m) {
    var items = new Set()
    items.add('0/1')
    for (var i = 1; i <= n; i++) {
        for (var j = i; j <= n; j++) {
            var [a, b] = simplify(i, j)
            items.add(`${a}/${b}`)
        }
    }
    arr = [...items].sort((a, b) => eval(a) - eval(b))
    return arr[m - 1]
}
console.log(fareySequence(1, 2))