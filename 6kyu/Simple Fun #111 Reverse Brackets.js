function reverseParentheses(s) {
    while (/\([a-z]*\)/.test(s)) {
        s = s.replace(/\(([a-z]*)\)/, (_, v) => [...v].reverse().join(''))
    }
    return s;
}

console.log(reverseParentheses('a(bc)de'))