function getKyu(allHonors, honor) {
    let honors = allHonors.sort((a, b) => a - b);
    var kyus = [1, 2, 2, 3, 3, 3, 4, 4];
    var idx = 0;
    for (var i = 0; i < kyus.length; i++) {
    	var k=kyus[i];
        var arr = honors.slice(idx, idx + k)
        idx += k
        if (arr.includes(honor)) return i + 1
    }
    return 0;
}