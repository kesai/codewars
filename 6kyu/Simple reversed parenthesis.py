import re


def solve(s):
    if len(s) % 2:
        return -1
    while re.findall(r'\(\)', s):
        s = re.sub(r'\(\)', '', s)
    n, stack = 0, []
    for x in s:
        if x == '(':
            stack.append(x)
        else:
            if not stack:
                n += 1
                stack.append('(')
            else:
                stack.pop()
    return n + len(stack)/2

