function findDuplicates(employees) {
    var duplicated = [];
    var visited = [];
    var i = 0;
    while (i < employees.length) {
        var e = employees[i];
        if (visited.findIndex(v => JSON.stringify(v) === JSON.stringify(e)) > -1) {
            if (duplicated.findIndex(v => JSON.stringify(v) === JSON.stringify(e)) === -1) duplicated.push(e);
            employees.splice(i, 1);
        }
        else {
            visited.push(e); i++;
        }
    }
    return duplicated
}