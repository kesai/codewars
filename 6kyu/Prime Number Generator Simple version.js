function isPrime(num) {
    if (num < 2) return false;
    if (num === 2 || num === 3) return true;
    if (num % 6 != 1 && num % 6 != 5) return false
    var n = parseInt(Math.sqrt(num));
    for (var i = 5; i < n + 1; i += 6) {
        if (num % i === 0 || num % (i + 2) === 0) return false
    }
    return true
}
var primeGen = (function* () {
    var d = 2;
    while (true) {
        while (!isPrime(d)) d++;
        yield d++;
    }
}());
var p = primeGen;
var cnt = 0;
while (true) {
    var x = p.next().value;
    cnt++;
    if (x > 784189) break;
}
console.log('how long', cnt)