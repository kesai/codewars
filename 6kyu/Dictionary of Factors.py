def get_divisors(x):
    divisors = [i for i in range(2, x) if x % i == 0]
    return divisors if divisors else ['None']


def factorsRange(n, m):
    result = dict()
    for i in range(n, m+1):
        result[i] = get_divisors(i)
    return result
