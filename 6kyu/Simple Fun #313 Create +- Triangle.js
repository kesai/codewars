function createTriangle(firstLine) {
    var last = firstLine.split(' ');
    var res = [firstLine];
    var d = 1;
    while (last.length > 1) {
        var row = []
        for (var i = 0; i < last.length - 1; i++) {
            [a, b] = last.slice(i, i + 2);
            var c = a === b ? '+' : '-';
            row.push(c);
        }
        last = row;
        var s = ' '.repeat(d) + row.join(' ') + ' '.repeat(d);
        res.push(s);
        d++;
    }
    return res.join('\n')
}


