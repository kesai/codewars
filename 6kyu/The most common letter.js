function replaceCommon(string, letter) {
    var counts = {}
    for (c of string) if (/[a-z]/.test(c)) counts[c] = counts[c] ? counts[c] + 1 : 1;
    var m = Math.max(...Object.values(counts));
    var r = Object.entries(counts).filter(v => v[1] === m).sort((a, b) => string.indexOf(a[0]) - string.indexOf(b[0]))[0]
    return string.replace(new RegExp(r, 'g'), letter);
}
