function deepReverse(l) {
    var res = [];
    for (var i = l.length - 1; i > -1; i--) {
        if (Array.isArray(l[i])) res.push(deepReverse(l[i]))
        else res.push(l[i])
    }
    return res;
}