function randomInts(n, total) {
    var res = [];
    for (var i = 0; i < n - 1; i++) {
        x = Math.random() * total | 0;
        res.push(x)
        total -= x;
    }
    res.push(total)
    return res;
}