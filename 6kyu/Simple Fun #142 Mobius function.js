function isPrime(num) {
    if (num < 2) return false;
    if (num === 2 || num === 3) return true;
    if (num % 6 != 1 && num % 6 != 5) return false
    var n = parseInt(Math.sqrt(num));
    for (var i = 5; i < n + 1; i += 6) {
        if (num % i === 0 || num % (i + 2) === 0) return false
    }
    return true
}

function get_factors(x) {
    var d = 2, res = [];
    while (x > 1) {
        if (isPrime(x)) { res.push(x); break; }
        if (x % d === 0) {
            x /= d;
            res.push(d);
        } else d++;
    }
    return res;
}

function factorize(n) {
    var res = []
    while (!(n % 2 > 0)) {
        n /= 2;
        res.push(2);
    }
    for (i = 3; i <= Math.sqrt(n); i += 2) {
        while (n % i == 0) {
            res.push(i);
            n = n / i;
        }
    }
    if (n > 2) {
        res.push(n);
    }
    return res;
}

function mobius(n) {
    var factors = factorize(n);
    for (x of new Set(factors)) {
        if (factors.filter(v => v === x).length > 1) {
            if (n % (x * x) === 0) return 0;
        }
    }
    return factors.length % 2 ? -1 : 1;
}


