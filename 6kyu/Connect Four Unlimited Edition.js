//modified from a prior kata's solution
function whoWon(grid, players) {
    //horizonal
    for (row of grid) {
        for (p of players) {
            if (new RegExp(`${p}{4,}`).test(row.join(''))) return p;
        }
    }
    //vertical
    for (var j = 0; j < grid[0].length; j++) {
        var colum = grid.map(v => v[j]);
        for (p of players) {
            if (new RegExp(`${p}{4,}`).test(colum.join(''))) return p;
        }
    }
    var ROW = grid.length;
    var COL = grid[0].length;
    //diagonals
    for (var row = 0; row < ROW; row++) {
        var temp = [];
        for (var i = row, j = 0; i < ROW && j < COL; i++, j++) {
            temp.push(grid[i][j]);
        }
        for (p of players) {
            if (new RegExp(`${p}{4,}`).test(temp.join(''))) return p;
        }
    }
    for (var col = 0; col < COL; col++) {
        var temp = [];
        for (var j = col, i = 0; i < ROW && j < COL; i++, j++) {
            temp.push(grid[i][j]);
        }
        for (p of players) {
            if (new RegExp(`${p}{4,}`).test(temp.join(''))) return p;
        }
    }

    for (var row = 0; row < ROW; row++) {
        var temp = [];
        for (var i = row, j = COL - 1; i < ROW && j > -1; i++, j--) {
            temp.push(grid[i][j]);
        }
        for (p of players) {
            if (new RegExp(`${p}{4,}`).test(temp.join(''))) return p;
        }
    }

    for (var col = COL - 1; col > -1; col--) {
        var temp = [];
        for (var j = col, i = 0; i < ROW && j > -1; i++, j--) {
            temp.push(grid[i][j]);
        }
        for (p of players) {
            if (new RegExp(`${p}{4,}`).test(temp.join(''))) return p;
        }
    }
    return null;
}

const connectFour = moves => {
    var players = [...new Set(moves.map(v => v.p))];
    var xs = moves.map(v => v.x);
    var ys = moves.map(v => v.y);
    var min_x = Math.min(...xs);
    var max_x = Math.max(...xs);
    var min_y = Math.min(...ys);
    var max_y = Math.max(...ys);
    var ROW = max_x - min_x + 1;
    var COL = max_y - min_y + 1;
    var grid = Array.from({ length: ROW }, (_, i) => Array.from({ length: COL }, (_, j) => '-'));
    for (mov of moves) {
        var p = mov.p;
        var x = mov.x;
        var y = mov.y;
        grid[x - min_x][y - min_y] = p;
    }
    return whoWon(grid, players)
}
const moves1 = [
    { p: 'R', x: 0, y: 0 },
    { p: 'Y', x: 100, y: 100 },
    { p: 'R', x: 1, y: 0 },
    { p: 'Y', x: 99, y: 100 },
    { p: 'R', x: 2, y: 0 },
    { p: 'Y', x: 98, y: 100 },
    { p: 'R', x: 3, y: 0 },
    { p: 'Y', x: 96, y: 100 }
];
console.log(connectFour(moves1))