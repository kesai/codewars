function solution(input) {
    var fruits = [], meats = [], others = [], vegetables = [];
    for (x of input.split(',')) {
        var [c, v] = x.split('_');
        if (c === 'fruit') fruits.push(v)
        else if (c === 'vegetable') vegetables.push(v)
        else if (c === 'meat') meats.push(v)
        else others.push(v);
    }
    return 'fruit:' + fruits.sort().join(',') + '\nmeat:' + meats.sort().join(',') + '\nother:' + others.sort().join(',') + '\nvegetable:' + vegetables.sort().join(',');
}