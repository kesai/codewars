function loneliest(number) {
  var s = number.toString();
  var arr = []
  var min_weight = Infinity;
  for (var i = 0; i < s.length; i++) {
    var d = +s[i];
    var left = ''
    for (var j = i - 1; j > i - 1 - d; j--)left = (s[j] || '') + left;
    var right = s.slice(i + 1, i + 1 + d);
    var weight = [...(left + right)].reduce((s, v) => s += +v, 0);
    arr.push([+s[i], weight])
    min_weight = Math.min(min_weight, weight)
  }
  return arr.findIndex(v => v[0] === 1 && v[1] === min_weight) > -1;
}