function touchType(str) {
    return str.replace(/\w/gi, v => /[1-5qwertasdfgzxcvb]/.test(v) ? 'L' : 'R')
        .replace(/L\s/g, 'LR').replace(/R\s/g, 'RL').replace(/^\s+/g, v => 'L'.repeat(v.length));
}
touchType('i love programming')