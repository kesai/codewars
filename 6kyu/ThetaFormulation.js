function thetaFormula(radius, arc, angle) {
    if (arguments.length < 3) return null;
    var args = [...arguments];
    if (args.filter(v => v === '?').length >= 2 || args.find(v => typeof (v) === 'string' && v !== '?')) return null;
    if (radius === '?') return +(arc / angle).toFixed(3);
    else if (arc === '?') return +(radius * angle).toFixed(3);
    else if (angle === '?') return +(arc / radius).toFixed(3);
    else return radius === arc / angle;
}