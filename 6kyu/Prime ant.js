function isPrime(num) {
    if (num < 2) return false;
    if (num === 2 || num === 3) return true;
    if (num % 6 != 1 && num % 6 != 5) return false
    var n = parseInt(Math.sqrt(num));
    for (var i = 5; i < n + 1; i += 6) {
        if (num % i === 0 || num % (i + 2) === 0) return false
    }
    return true
}
function get_min_dvisor(x) {
    for (var d = 2; ; d++)if (x % d === 0) return d;
}

function primeAnt(n) {
    var N = n;
    var arr = Array.from({ length: N }, (_, i) => i + 2);
    console.log(arr)
    var p = 0;
    for (var i = 0; i < n; i++) {

        var x = arr[p];
        if (isPrime(x)) {
            p++;
            if (p === arr.length) arr.push(p + 2);
        }
        else {
            var q = get_min_dvisor(x);
            arr[p] /= q;
            arr[p - 1] += q;
            p--;
        }
    }
    return p
}
