function meetingTime(Ta, Tb, r) {
    return Ta * Tb == 0 ? Math.abs(Ta || Tb).toFixed(2) : Math.abs(Tb * Ta / (Tb - Ta)).toFixed(2)
}