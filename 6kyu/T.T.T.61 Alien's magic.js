function lalala(things) {
    const p = /(^(in|at|on)\s.+?(?=,))|((?<=\s)(in|at|on)\s((?!\s(in|at|on)\s).)+?(?=.$))/ig;
    let r = things.reduceRight((acc, val, i, arr, m = val.match(p)) => !i ? [...acc, m] : [m, ...acc], []);
    return things.map((s, i, arr, j = 0) => s.replace(p, c => r[i][j++]));
}