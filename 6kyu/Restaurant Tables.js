function restaurant(a, b, t) {
    var table_single = Array.from({ length: a }, (_, i) => 1);
    var table_double = Array.from({ length: b }, (_, i) => 2);
    var denies = 0;
    for (client of t) {
        if (client === 2) {
            var idx = table_double.findIndex(v => v === 2);
            if (idx === -1) denies += 2;
            else table_double.splice(idx, 1);
        } else {
            if (table_single.length) table_single.pop();
            else {
                var idx = table_double.findIndex(v => v === 2);
                if (idx > -1) table_double[idx] -= 1;
                else if (table_double.length) {
                    table_double.pop();
                } else {
                    denies += 1;
                }
            }
        }
    }
    return denies;
}
console.log(restaurant(0, 2, [1, 1, 2]))