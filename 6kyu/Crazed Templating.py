def letter_pattern(words):
    return ''.join(c if all(w[i] == c for w in words) else '*' for i, c in enumerate(words[0]))
