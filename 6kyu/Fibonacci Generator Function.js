function* fibonacci() {
    var a = 0, b = 1;
    while (true) {
        yield a;
        [a, b] = [b, a + b];
    }
}
var fib = fibonacci();
console.log(fib.next().value)
console.log(fib.next().value)
console.log(fib.next().value)
console.log(fib.next().value)