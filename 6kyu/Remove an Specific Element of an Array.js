function selectSubarray(arr) {
    var subArr = [];
    var min_q = Infinity;
    for (var i = 0; i < arr.length; i++) {
        var sub_arr = [].concat(arr);
        sub_arr.splice(i, 1);
        var subSum = 0;
        var subProduct = 1;
        sub_arr.forEach(v => {
            subSum += v;
            subProduct *= v;
        });
        var q = subSum ? Math.abs(subProduct / subSum) : Infinity;
        min_q = Math.min(q, min_q);
        subArr.push([[i, arr[i]], q]);
    }
    var res = subArr.filter(v => v[1] === min_q).map(v => v[0])
    return res.length > 1 ? res : res[0]
}
console.log(selectSubarray([-1, 53, 62, -186, 132, -9, 34, -185, 57, -163, -145, 188, -133, 97, -115, 147, 170, -159, -14, 125, 194, -70, -34, -108, -177, 114, 46, 149, 200, -158, 6, -141, 10, 28]))