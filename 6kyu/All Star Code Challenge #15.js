function rotate(str) {
    return [...str].map((v, i) => str.slice(i + 1) + str.slice(0, i + 1))
}