function isPrime(num) {
    if (num < 2) return false;
    if (num === 2 || num === 3) return true;
    if (num % 6 != 1 && num % 6 != 5) return false
    var n = parseInt(Math.sqrt(num));
    for (var i = 5; i < n + 1; i += 6) {
        if (num % i === 0 || num % (i + 2) === 0) return false
    }
    return true
}

function solve(a, b) {
    var n = 0
    for (var i = a; i <= b; i++) {
        var first_two = +('' + i).substr(0, 2);
        if (isPrime(first_two)) {
            var square = i * i;
            var first_two1 = +('' + square).substr(0, 2)
            var last_two1 = +('' + square).slice(-2);
            var last_two = +('' + i).slice(-2);
            if (last_two === last_two1 && isPrime(first_two1)) n++;
        }
    }
    return n;
}