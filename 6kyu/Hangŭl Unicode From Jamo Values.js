function unicodeFromJamo(arr) {
    var res = []
    for (var i = 0; i < arr.length; i += 3) {
        var [a, b, c] = arr.slice(i, i + 3).map(v => v.substr(2).replace(';', ''));
        var x = 588 * jamoLookUp['initial jamo'][a] + 28 * jamoLookUp['medial jamo'][b] + jamoLookUp['final jamo'][c] + 44032
        res.push(`&#${x};`);
    }
    return res.join('');
}
