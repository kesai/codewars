function duel(names, guns) {
    guns = guns.slice()
    let alive = names.map(_ => true)
    let nextDead = names.map(_ => false)
    let markDead = idx => {
        let idx2 = idx
        while (!alive[idx2 = (idx2 + 7) % 8]);
        let idx3 = idx
        while (!alive[idx3 = (idx3 + 1) % 8]);
        nextDead[idx2] = nextDead[idx3] = true
    }
    let matches = (name, gun) => name.replace(/[^A-Z]/g, '') == gun

    let rot = 0
    while (alive.filter(x => x).length > 1) {
        nextDead = names.map(_ => false)
        for (let i = 0; i < rot; ++i) guns.unshift(guns.pop())
        rot++
        names.forEach((name, i) => alive[i] && matches(name, guns[i]) && markDead(i))
        alive = alive.map((x, i) => x && !nextDead[i])
    }
    names[-1] = ''
    return names[alive.findIndex(x => x)]
}