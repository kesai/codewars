var flip = (x) => x.replace(/[10]/g, v => v === '0' ? '1' : '0');
var chars = 'zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA';
function encode(s) {
    return s.replace(/[a-z]/gi, v => flip((chars.indexOf(v) + 1).toString(2) + '2'))
}
function decode(s) {
    return s.replace(/([01]+)2/g, (_, v) => chars[parseInt(flip(v), 2) - 1]);
}
console.log(encode("Hello world!"))
console.log(decode("0100102010012000020000200112 0112001120110200002010002!"))

console.log(parseInt('101101', 2))
