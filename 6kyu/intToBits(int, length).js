function intToBits(int, length = 32) {
    if (!Number.isInteger(length) || length < 1) length = 32;
    length = Math.min(length, 32);
    if (!Number.isInteger(int)) return null;
    /*if (int >= 0) {
        s = int.toString(2);
        if (s.length < length) s = s.padStart(length, '0');
        return s
    }*/
    var b = (int >>> 0).toString(2);
    if (b.length < length) b = b.padStart(length, int >= 0 ? '0' : '1');
    return b
}