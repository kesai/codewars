function catchThief(queue) {
    var caught = new Set();
    for (var i = 0; i < queue.length; i++) {
        if (/\d/.test(queue[i])) {
            var d = +queue[i];
            for (var j = i + 1; j <= i + d; j++) if (queue[j] === 'X') caught.add(j);
            for (var j = i - 1; j >= i - d; j--)if (queue[j] === 'X') caught.add(j);
        }
    }
    return caught.size;
}