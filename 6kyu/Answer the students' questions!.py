from operator import itemgetter


def commons(a, b):
    return sum(min(a.split(' ').count(w), b.split(' ').count(w)) for w in set(a.split(' ')))


def answer(question, information):
    res = sorted([(i, commons(question.lower(), i.lower())) for i in information if commons(
        question, i) > 0], key=itemgetter(1) or itemgetter(0), reverse=True)
    return res[0][0] if res else None

