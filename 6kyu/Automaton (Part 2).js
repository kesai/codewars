function automaton(e) {
    if (typeof e === "string") {
        return e.replace(/[^aeiuo]/gi, "").length;
    } else if (typeof e === "number") {
        if (!Number.isInteger(e)) return false;
        for (var i = 2; i < e; i++) if (e % i === 0) return false;
        return true;
    } else if (typeof e === "object") {
        var a = {};
        for (var k in e) a[e[k]] = k;
        return a;
    }
}
