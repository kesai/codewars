def clean_mean(sample, cutoff):
    while True:
        mean = sum(sample)/len(sample)
        std = (sum((x-mean)**2 for x in sample)/len(sample))**0.5
        bound = std*cutoff
        outliers = [x for x in sample if abs(x-mean) > bound]
        sample = [x for x in sample if abs(x-mean) < bound]
        if not outliers:
            return round(mean, 2)

