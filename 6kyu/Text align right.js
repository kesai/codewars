function alignRight(text, width) {
    return text.match(eval(`/.{1,${width}}(\\s|$)/g`)).map(v => v.trim().padStart(width, ' ')).join('\n')
}
