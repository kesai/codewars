def unpack(l):
    res = []
    for x in l:
        if type(x) is list:
            res += unpack(x)
        elif type(x) is tuple:
            res += unpack(x)
        elif type(x) is set:
            res += unpack(x)
        elif type(x) is dict:
            res += unpack(x.keys())
            res += unpack(x.values())
        else:
            res.append(x)
    return res


print(unpack([None, [1, ({2, 3}, {'foo': 'bar'})]]))
