function caesarEncode(phrase, shift) {
    var s = 'abcdefghijklmnopqrstuvwxyz';
    return phrase.split(' ').map((v, i) => v.replace(/[a-z]/g, c => s[(s.indexOf(c) + shift + i) % 26])).join(' ')
}
