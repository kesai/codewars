function pascal(depth) {
    var results = [[1]];
    for (var r = 1; r < depth; r++) {
        results.push(caculate(results[r - 1]))
    }
    return results;
}

function caculate(arr) {
    var out = arr.map((v, i) => (i > 0 ? +arr[i - 1] : 0) + +v);
    out.push(1);
    return out;
}

console.log(pascal(10))