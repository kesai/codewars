def get_prime(x):
    d = 2
    factors = set()
    while(x > 1):
        if x % d == 0:
            x //= d
            factors.add(d)
        else:
            d += 1
    return max(factors)


def ascii_cipher(message, key):
    prime = get_prime(abs(key))
    if key < 0:
        prime = -prime
    return ''.join(chr((ord(c)+prime+128) % 128) for c in message)


print(ascii_cipher("Hello, world", 18))
