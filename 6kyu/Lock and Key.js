function Lock() {
    var keys = []
    this.createKey = function () {
        var code = (Math.random() * 10000000).toString(16).substr(0, 4) + '-' + (new Date()).getTime() + '-' + Math.random().toString().substr(2, 5);
        var key = { expired: false };
        Object.defineProperty(key, "code", {
            value: code,
            enumerable: false,
            writable: false
        });
        keys.push(key);
        return key;
    }

    this.check = function (k) {
        if (!k || !k.code) return false;
        var key = keys.find(v => v.code === k.code);
        return key && key.code === k.code && !key.expired;
    }
    this.expire = function (k) {
        var key = keys.find(v => v.code === k.code);
        if (key) key.expired = true;
    }
}

