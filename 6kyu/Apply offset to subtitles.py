import re


def subs_offset_apply(string, offset):
    def repl(x):
        h, m, s, ms = x.groups()
        total_ms = int(h) * 60 * 60 * 1000 + int(m) * 60 * \
            1000 + int(s) * 1000 + int(ms) + offset
        hour = total_ms // (60 * 60 * 1000)
        total_ms = total_ms % (60 * 60 * 1000)
        minute = total_ms // (60 * 1000)
        total_ms = total_ms % 60000
        second = total_ms // 1000
        mill_second = total_ms % 1000
        if hour > 99 or hour < 0:
            return '啊'
        return f'{hour:02d}:{minute:02d}:{second:02d},{mill_second:03d}'
    s = re.sub(r'(\d{2}):(\d{2}):(\d{2}),(\d{3})', repl, string)
    if re.match(r'啊'):
        return 'Invalid offset'
    return s
