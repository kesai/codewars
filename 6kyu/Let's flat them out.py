def flatten(x):
    res = {}
    for k, v in x.items():
        if v and type(v) is dict:
            for t_k, t_v in flatten(v).items():
                res[k+'/'+t_k] = t_v
        else:
            res[k] = v or ''
    return res
