function telephoneWords(s) {
    var kb = { '1': '1', '2': 'ABC', '3': 'DEF', '4': 'GHI', '5': 'JKL', '6': 'MNO', '7': 'PQRS', '8': 'TUV', '9': 'WXYZ', '0': '0' };
    var arr = [...kb[s[0]]];
    for (var i = 1; i < 4; i++) {
        var temp = []
        for (c of kb[s[i]]) { temp = temp.concat(arr.map(v => v + c)); }
        arr = temp;
    }
    return arr.sort();
}
