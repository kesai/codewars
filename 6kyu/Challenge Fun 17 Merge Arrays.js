function mergeArrays(a, b) {
    var res = new Set()
    var i = 0, j = 0;
    var x = 0, y = 0;
    var common = null;
    while (i < a.length || j < b.length) {
        A = i >= a.length ? Infinity : a[i];
        B = j >= b.length ? Infinity : b[j];
        if (A === B && A != Infinity) {
            common = A;
            while (a[i] === common) { x++; i++; }
            while (b[j] === common) { y++; j++; }
            if (x === y) res.add(common);
            x = 0; y = 0;
        } else {
            if (A < B) { res.add(A); i++; }
            else if (B < A) { res.add(B); j++; }
        }
    }
    return [...res]
}
