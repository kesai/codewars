function stairs(n) {
    var out = []
    for (var i = 0; i < n; i++) {
        var temp = Array.from({ length: i + 1 }, (_, j) => (j + 1) % 10);
        out.push(temp.concat([].concat(temp).reverse()).join(' ').padStart(4 * n - 1, ' '))
    }
    return out.join('\n')
}
