function rowsRearranging(matrix) {
    matrix = matrix.sort((a, b) => a[0] - b[0])
    for (var i = 0; i < matrix.length - 1; i++) {
        if (!matrix[i].every((v, j) => matrix[i + 1][j] > v)) return false;
    }
    return true;
}