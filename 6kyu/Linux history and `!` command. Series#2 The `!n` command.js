function bangN(n,history){
    var cmds=history.split('\n');
    var reg=new RegExp(`^ *${n} *`)
    return cmds.length<n?`!${n}: event not found`:cmds[n-1].replace(reg,'')
}