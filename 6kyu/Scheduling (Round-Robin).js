function roundRobin(jobs, slice, index) {
    var i = 0;
    var result = 0;
    while (jobs[index]) {
        jobs[i] -= (d = Math.min(slice, jobs[i]))
        result += d;
        i = (i + 1) % jobs.length;
    }
    return result;
}