function buyFruits(priceLabels, fruitsList) {
    var sum = (arr) => arr.reduce((s, v) => s += v, 0);
    var prices = priceLabels.sort((a, b) => a - b);
    var categories = {};
    for (c of fruitsList) { categories[c] = categories[c] ? categories[c] + 1 : 1; };
    var units = Object.values(categories);
    var min = units.sort((a, b) => b - a).reduce((s, v, i) => s += v * prices[i], 0)
    prices = priceLabels.sort((a, b) => b - a);
    var max = units.sort((a, b) => b - a).reduce(((s, v, i) => s += v * prices[i]), 0)
    return [min, max]
}
