function instrumental(word) {
    var map = { 'a': 'á', 'e': 'é', 'i': 'í', 'o': 'ó', 'u': 'ú', 'ö': 'ő', 'ü': 'ű' }
    if (/.*[eéiíöőüűaáoóuú]$/.test(word)) {
        if (/.*[aeiouöü]$/.test(word)) word = word.slice(0, -1) + map[word.slice(-1)[0]];
        var x = word.slice(-1)[0];
        return word + (/[aáoóuú]/.test(x) ? 'val' : 'vel')
    } else {
        if (/.*((sz)|(zs)|(cs))$/.test(word)) {
            var end = word.slice(-2);
            word = word.slice(0, -2) + end[0] + end;
        } else {
            word = word + word.slice(-1)[0];
        }
        var x = word.replace(/[^eéiíöőüűaáoóuú]/g, '').slice(-1)[0];
        return word + (/[aáoóuú]/.test(x) ? 'al' : 'el')
    }
}