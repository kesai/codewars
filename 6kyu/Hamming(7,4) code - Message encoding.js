function encodeMessage(message) {
    [d1, d2, d3, d4] = [...message].map(v => +v);
    p1 = d1 ^ d2 ^ d4
    p2 = d1 ^ d4 ^ d3;
    p3 = d2 ^ d3 ^ d4
    return [p1, p2, d1, p3, d2, d3, d4].join('')
}