def ulam_sequence(u0, u1, n):
    arr = [u0, u1, u0+u1]
    s = set(arr)
    i = u0+u1+1
    while(len(arr) < n):
        cnt = 0
        for j in range(len(arr)):
            if arr[j] > i/2:
                break
            if (i-arr[j]) in s and i-arr[j] != arr[j]:
                cnt += 1
            if cnt > 1:
                break
        if cnt == 1:
            arr.append(i)
            s.add(i)
        i += 1
    return arr
