function solveSimultaneous(array) {
    var [a, b] = array;
    var c = a.map(v => (b[1] / a[1]) * v).map(
        (v, i) => v - b[i]);
    x = Math.round(c[2] / c[0]);
    y = Math.round(((a[2] - a[0] * x) / a[1]);
    return [x, y]
}