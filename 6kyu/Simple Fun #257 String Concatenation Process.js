function concatenationProcess(arr) {
    while (arr.length > 1) {
        //find first shortest string
        var a = Math.min(...arr.map(v => v.length));
        var idx_1 = arr.findIndex(v => v.length === a);
        var first = arr[idx_1];
        arr.splice(idx_1, 1);

        //find second shortest string
        var idx_2;
        var b = Math.min(...arr.map(v => v.length));
        for (var i = arr.length - 1; i > -1; i--) {
            if (arr[i].length === b) { idx_2 = i; break; }
        }
        var second = arr[idx_2];
        arr.splice(idx_2, 1);

        //append the result of their concatenation to the right end of the array
        var result = first + second;
        arr.push(result);
    }
    return arr[0]
}