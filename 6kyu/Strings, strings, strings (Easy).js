Number.prototype.toString = function () {
    return '' + this;
}
Boolean.prototype.toString = function () {
    return this.valueOf() ? 'true' : 'false'
}

Array.prototype.toString = function () {
    return `[${this.join(', ')}]`
}

var a = [1, 2, 3, 4, 5];
console.log(a.toString())