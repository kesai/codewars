def wrap(x, N):
    return 0 if x == N else N - 1 if x == -1 else x


def magicSquare(n):
    if not n % 2:
        return "Please enter an odd integer."
    d = 1
    matrix = [[0 for i in range(n)] for j in range(n)]
    i, j = 0, n // 2
    while d <= n * n:
        matrix[i][j] = d
        d += 1
        temp_i, temp_j = wrap(i - 1, n), wrap(j + 1, n)
        if matrix[temp_i][temp_j]:
            i += 1
        else:
            i, j = temp_i, temp_j
    return matrix


magicSquare(3)

