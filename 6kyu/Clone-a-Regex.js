RegExp.clone = function (input) {
    var pattern = input.source;
    return (new RegExp(pattern, input.flags));
};

RegExp.prototype.clone = function () {
    var result = this;
    return result;
};
