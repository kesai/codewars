function isPrime(num) {
    if (num < 2) return false;
    if (num === 2 || num === 3) return true;
    if (num % 6 != 1 && num % 6 != 5) return false
    var n = parseInt(Math.sqrt(num));
    for (var i = 5; i < n + 1; i += 6) {
        if (num % i === 0 || num % (i + 2) === 0) return false
    }
    return true
}

function primeProduct(n) {
    for (var i = Math.floor(n / 2); i > 1; i--) {
        if (isPrime(i) && isPrime(n - i)) return i * (n - i);
    }
    return 0;
}