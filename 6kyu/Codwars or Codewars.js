function findCodwars(str) {
    if (/^(http(s)?:\/\/)?(\w+\.)*codwars.com(\/.*(\?.+)?)?$/.test(str)) return true;
    if (/^(http(s)?:\/\/)?(\w+\.)*codwars.com(\?.+)?$/.test(str)) return true
    if (/^(http(s)?:\/\/)?(\w+\.)*codwars.com$/.test(str)) return true;
    return false
}