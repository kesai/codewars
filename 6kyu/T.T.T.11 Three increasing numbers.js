function increasingNumbers(nums) {
    var threeNums = [-1, -1, -1];
    threeNums[0] = nums[0];
    for (var i = 1; i < nums.length; i++) {
        if (nums[i] > threeNums[0]) {
            if (threeNums[1] === -1 || threeNums[1] >= nums[i]) threeNums[1] = nums[i];
            else return true;
        } else {
            threeNums[0] = nums[i];
        }
    }
    return false;
}

