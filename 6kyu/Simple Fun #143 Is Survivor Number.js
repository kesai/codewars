//ref:https://math.stackexchange.com/questions/143876/remove-every-k1-th-remaining-element-in-kth-pass-of-natural-numbers
//ref:https://oeis.org/A000960
function calc(n) {
    if (n == 1) return 1;
    return calc_rec(n * n, n - 1);
}
function calc_rec(nu, level) {
    var tmp;
    if (level == 1) return (nu - 1);
    tmp = nu % level;
    return calc_rec(nu - (tmp ? tmp : level), level - 1);
}
var caches = [];
for (var i = 1; i < 11000; i++) {
    caches.push(calc(i))
}

function survivor(n) {
    return caches.includes(n)
}

function survivor(n) {
    var check = parseInt(n);
    var dzielnik = parseInt(2);
    while (check >= dzielnik) {
        if (check % dzielnik == 0) return false;
        var k = parseInt(check / dzielnik++);
        check -= k;
    }
    return true;
}