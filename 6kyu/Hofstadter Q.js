var q = function () {
    var memo = [1, 1, 1];
    var Q = function (n) {
        var result = memo[n];
        if (typeof result !== 'number') {
            result = Q(n - Q(n - 1)) + Q(n - Q(n - 2));
            memo[n] = result;
        }
        return result;
    };
    return Q;
}();
function hofstadterQ(n) {
    return q(n)
}