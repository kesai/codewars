function SmartTrafficLight(st1, st2) {
    this.st1 = st1;
    this.st2 = st2;
}
SmartTrafficLight.prototype.turngreen = function () {
    var result = null;
    if (this.st1[0] > this.st2[0]) {
        result = this.st1[1]; this.st1[0] = 0;
    } else if (this.st2[0] > this.st1[0]) {
        result = this.st2[1]; this.st2[0] = 0;
    }
    return result
}