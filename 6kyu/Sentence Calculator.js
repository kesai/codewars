function lettersToNumbers(s) {
    var x = ' abcdefghijklmnopqrstuvwxyz';
    return [...s].reduce((sum, v) => sum += /[A-Z]/.test(v) ? 2 * x.indexOf(v.toLowerCase()) : /[a-z]/.test(v) ? x.indexOf(v) :
        /\d/.test(v) ? +v : 0, 0)
}