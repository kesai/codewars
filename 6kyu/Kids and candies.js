const gcd = (a, b) => b ? gcd(b, a % b) : a;
const lcm = (a, b) => a * b / gcd(a, b);
function candiesToBuy(kids) {
    var n = 1;
    for (var i = 1; i <= kids; i++)n = lcm(n, i);
    return n;
}
