function verticalHistogramOf(s) {
    var chars = [...new Set(s.match(/[A-Z]/g))].sort()
    var counts = chars.map(v => s.match(new RegExp(v, 'g')).length)
    var max = Math.max(...counts);
    var out = [chars.join(' ')];
    for (var i = 1; i <= max; i++)out.unshift(counts.map(v => v >= i ? '*' : ' ').join(' ').trimRight());
    return out.join('\n')
}