function clamp() {
    var number = arguments[0];
    if (arguments.length === 2) {
        min = 0; max = arguments[1];
    }
    else {
        min = arguments[1]; max = arguments[2];
        result = arguments.length === 3 ? null : arguments[3];
    }
    if (number >= min && number <= max) return number;
    if (result === null) return number < min ? min : max;
    if (typeof (result) == 'function') {
        return result(number, min, max);
    } else return result;
}