function frame(score) {
    var result = [0, 0];
    scores = score.replace(/\(.*?\)/g, '').split('; ');
    for (sc of scores) {
        [a, b] = sc.split('-').map(v => +v);
        result[a > b ? 0 : 1] += 1;
    }
    return result;
}
var score = "29-74; 1-111(111); 83(51)-1; 68-13; 7-74(54); 81(81)-0; 112(104)-0; 8-70; 74(67)-67(67); 0-77(77); 69-44";



console.log(frame(score))