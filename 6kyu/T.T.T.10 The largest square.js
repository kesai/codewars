function max(wall) {
    for (var size = wall.length; size > 0; size--) {
        for (var i = 0; i <= wall.length - size; i++) {
            for (var j = 0; j <= wall[0].length - size; j++) {

                if (wall.slice(i, i + size).map(v => v.slice(j, j + size)).findIndex(v => v.includes('X')) === -1)
                    return size ** 2;

            }
        }
    }
}