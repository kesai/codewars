function encryptNum(number) {
    if (/^(0039|\+39|1)[ .-]?\d{3}[ .-]?\d{3}[ .-]?\d{4}$/.test(number)) {
        return number.replace(/(\d\D?){6}$/, v => v.replace(/\d/g, 'X'))
    } return false;
}