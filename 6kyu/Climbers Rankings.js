class RankCalculator {
    static GetRankings(points) {
        var res = points.map(v => {
            return {
                name: v.name,
                points: v.points.sort((a, b) => b - a).slice(0, 6).reduce((s, v) => s += v, 0)
            }
        }).sort((a, b) => b.points - a.points);
        return res;
    }
}
