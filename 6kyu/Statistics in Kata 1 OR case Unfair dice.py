def mutually_exclusive(dice, call1, call2):
    if sum(x[1] for x in dice) != 1:
        return None
    d = list(filter(lambda x: x[0] == call1, dice))[
        0][1]+list(filter(lambda x: x[0] == call2, dice))[0][1]
    return f'{d:.2f}'
