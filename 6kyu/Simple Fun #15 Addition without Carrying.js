function additionWithoutCarrying(a, b) {
    a = '' + a; b = '' + b;
    a = a.padStart(Math.max(a.length, b.length), '0');
    b = b.padStart(Math.max(a.length, b.length), '0');
    return +[...a].map((v, i) => (+v + +b[i]) % 10).join('')
}
