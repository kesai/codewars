
from math import factorial


def sum_fib(n):
    cnt = 0
    a, b = 0, 1
    res = 0
    while cnt < n:
        res += factorial(a)
        cnt += 1
        a, b = b, a+b
    return res
