function predict(candidates, polls) {
    var w = polls.map(v => v[1]);
    p = polls.map(v => v[0]);
    var t = w.reduce((s, v) => s += v, 0);
    var res = {};
    for (var i = 0; i < candidates.length; i++) {
        res[candidates[i]] = Math.round(10 * p.reduce((s, v, j) => s += v[i] * w[j], 0) / t) / 10;
    }
    return res;
}