function factDigits(n) {
    var res = 0;
    for (var i = 1; i <= n; i++)res += Math.log10(i);
    return Math.floor(res)
}