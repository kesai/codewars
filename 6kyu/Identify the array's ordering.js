function orderType(arr) {
    arr = arr.map(v => v.length || ('' + v).length)
    if (new Set(arr).size === 1) return 'Constant';
    var inc = [].concat(arr).sort((a, b) => a - b);
    if (inc.join(',') === arr.join(',')) return 'Increasing';
    else if (inc.reverse().join(',') === arr.join(',')) return 'Decreasing'
    return 'Unsorted'
}
