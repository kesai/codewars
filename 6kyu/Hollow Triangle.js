console.log(['_____#_____', '____#_#____', '___#___#___', '__#_____#__', '_#_______#_', '###########'].join('\n'))

function hollowTriangle(n) {
    var result = ['_'.repeat(n - 1) + '#' + '_'.repeat(n - 1)];
    var bottom = '#'.repeat(2 * n - 1);
    for (var i = 2; i < n; i++) result.push('_'.repeat(n - i) + '#' + '_'.repeat(2 * (i - 1) - 1) + '#' + '_'.repeat(n - i));
    if(n>1)result.push(bottom);
    return result;
}
console.log(hollowTriangle(6))