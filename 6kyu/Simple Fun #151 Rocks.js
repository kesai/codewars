function rocks(n) {
    var s = n.toString();
    var cnt = 0;
    for (var i = 0; i < s.length - 1; i++) {
        cnt += (9 * 10 ** i) * (i + 1);
    }
    cnt += (n - 10 ** (s.length - 1) + 1) * s.length;
    return cnt;
}