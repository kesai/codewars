def T9(words, seq):
    kb = {'2': 'ABC', '3': 'DEF', '4': 'GHI', '5': 'JKL',
          '6': 'MNO', '7': 'PQRS', '8': 'TUV', '9': 'WXYZ'}
    def word_to_num(w): return ''.join(''.join(k for k, v in kb.items() if c.upper() in v)
                                       for c in w)
    res = [w for w in words if word_to_num(w) == seq]
    return res if res else [''.join(kb[d][0].lower() for d in seq)]
