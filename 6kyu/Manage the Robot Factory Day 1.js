function calculateScrap(scraps, numberOfRobots) {
    return Math.round((numberOfRobots * 50) * (100 ** scraps.length) / scraps.reduce((s, v) => s *= (100 - v), 1));
}
console.log(calculateScrap([10], 90))