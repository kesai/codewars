function mostFrequentDigitSum(n) {
    var steps = []
    var counts = {}
    while (n > 0) {
        var step = [...'' + n].reduce((s, v) => s += +v, 0);
        n -= step;
        counts[step] = counts[step] ? counts[step] + 1 : 1;
    }
    return Object.entries(counts).sort((a, b) => b[1] - a[1] || b[0] - a[0])[0][0]
}
