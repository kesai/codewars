function f(x) {
    if (Number.isInteger(x)) return x;
    return x.toFixed(10).replace(/0*$/, '');
}

function secDegSolver(a, b, c) {
    if (!a && !b && !c) return 'The equation is indeterminate';
    if (a === 0) return b ? `It is a first degree equation. Solution: ${f(-c / b)}` : 'Impossible situation. Wrong entries';
    if (b * b - 4 * a * c < 0) return 'There are no real solutions';
    var x1 = f((-b - Math.sqrt(b * b - 4 * a * c)) / (2 * a))
    var x2 = f((-b + Math.sqrt(b * b - 4 * a * c)) / (2 * a))
    return x1 === x2 ? `It has one double solution: ${x1}` : `Two solutions: ${[x1, x2].sort().join(', ')}`
}