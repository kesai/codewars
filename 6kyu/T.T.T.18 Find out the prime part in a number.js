function isPrime(num) {
    if (num < 2) return false;
    if (num === 2 || num === 3) return true;
    if (num % 6 != 1 && num % 6 != 5) return false
    var n = parseInt(Math.sqrt(num));
    for (var i = 5; i < n + 1; i += 6) {
        if (num % i === 0 || num % (i + 2) === 0) return false
    }
    return true
}
function findOutPrime(n) {
    var s = '' + n;
    var result = -Infinity;
    for (var i = s.length; i > 0; i--) {
        var arr = [];
        for (var j = 0; j <= s.length - i; j++) {
            var temp = +s.substr(j, i);
            if (isPrime(temp)) result = Math.max(result, temp)
        }

    }
    return result > 0 ? result : null;
}
