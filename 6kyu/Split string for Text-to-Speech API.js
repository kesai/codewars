const splitText = (text, max) => (text.match(new RegExp('\\b.{1,' + max + '}\\b\\W?', 'g')) || []).map(x => x.trim());
