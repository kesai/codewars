function reverseSurname(surnames) {
    return surnames.match(/.{8}/g).map(rev8).reverse().join('。\n') + '。'
}
function rev8(s) {
    return s.match(/.{4}/g).map(v => rev4(v)).reverse().join('，')
}
function rev4(s = '赵钱孙李') {
    if (special.includes(s.substr(0, 2)) && special.includes(s.substr(2, 2))) {
        return s.substr(2, 2) + s.substr(0, 2)
    } else if (special.includes(s.substr(0, 2))) {
        return s[3] + s[2] + s.substr(0, 2)
    } else if (special.includes(s.substr(2, 2))) {
        return s.substr(2, 2) + s[1] + s[0];
    } else {
        return [...s].reverse().join('');
    }
}
