const centerOf = c => Array.from({ length: c.length }, (a, i) => (c + c)[(i * (i * 2 + 1) + i) % c.length]).join("").replace(/^(.+?)\1*$/, "$1");
