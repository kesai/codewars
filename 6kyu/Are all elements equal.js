Object.defineProperty(String.prototype, "eqAll", {
    value: function eqAll() {
        return this.valueOf() !== '' ? /^(.)\1*$/.test(this.valueOf()) : true;
    }
});

Object.defineProperty(Array.prototype, "eqAll", {
    value: function eqAll() {
        return new Set(this).size <= 1
    }
});
