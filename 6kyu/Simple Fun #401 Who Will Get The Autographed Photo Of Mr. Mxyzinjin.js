function who(initWeights, specWeights, n) {
    fans = initWeights.map((v, i) => new Object({ id1: i + 1, weight: v }));
    fans.sort((a, b) => b.weight - a.weight || a.id1 - b.id1);
    fans.forEach((e, i) => e.weight += specWeights[i % 10]);
    fans.sort((a, b) => b.weight - a.weight || a.id1 - b.id1);
    return fans.slice(0, n).map(v => v.id1);
}

