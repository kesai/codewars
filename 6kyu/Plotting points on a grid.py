class Grid():
    def __init__(self, width, height):
        self.grids = [['0' for j in range(width)] for i in range(height)]

    def plot_point(self, x, y):
        self.grids[y-1][x-1] = 'X'

    def __repr__(self):
        pass

    @property
    def grid(self):
        temp = list(map(lambda x: ''.join(x), self.grids))
        return '\n'.join(temp)


((())()(((())))(
