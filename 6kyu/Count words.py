import re


def word_count(s):
    excludes = ["a", "the", "on", "at", "of", "upon", "in", "as"]
    return len([w for w in re.findall(r'[a-z]+', s.lower()) if w not in excludes])
