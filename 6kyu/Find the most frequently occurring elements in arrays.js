function getMostFrequent(json) {
    var f = (v) => {
        var counts = {}
        for (x of v) counts[x] = counts[x] ? counts[x] + 1 : 1;
        var m = Math.max(...Object.values(counts));
        var arr = Object.keys(counts).filter(v => counts[v] === m);
        return +arr.sort((a, b) => v.lastIndexOf(+b) - v.lastIndexOf(+a))[0]
    }
    return json['temperature'].map(f);
}