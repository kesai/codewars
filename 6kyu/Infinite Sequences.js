function sequence(fun) {
    return {
        take: function (n) {
            return Array.from({ length: n }, (_, i) => fun(i))
        },
        takeWhile: function (condition) {
            var i = 0;
            var res = [];
            while (condition(fun(i))) res.push(fun(i++));
            return res;
        }
    }
}