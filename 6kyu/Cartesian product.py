def set_to_tupleset(X):
    res = set()
    for x in X:
        res.add(tuple([x]))
    return res


def product(a, b):
    res = set()
    for x in a:
        for y in b:
            if type(x) is tuple:
                res.add(x+tuple([y]))
            else:
                res.add((x, y))
    return res


def cart_prod(*sets):
    if not sets or any(not x for x in sets):
        return set()
    result = set_to_tupleset(sets[0])
    for x in sets[1:]:
        result = product(result, x)
    return result
