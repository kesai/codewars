function solve(time) {
    var digits = ['midnight', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
    var tens = ['twenty', 'thirty', 'forty', 'fifty'];
    var [_, hour, minute] = time.match(/(.+):(.+)/).map(v => +v);
    var next_hour = hour + 1;

    function minute_in_en(x) {
        if (x === 1) return 'one minute';
        if (x < 20) return digits[x] + ' minutes';
        return tens[parseInt(x / 10) - 2] + ' ' + digits[x % 10] + ' minutes';
    }

    if (hour !== 12) hour = hour % 12;
    if (next_hour !== 12) next_hour = next_hour % 12;

    if (minute === 0) return hour ? `${digits[hour]} o'clock` : 'midnight';
    if (minute === 30) return `half past ${digits[hour]}`;
    if (minute === 15) return `quarter past ${digits[hour]}`;
    if (minute === 45) return `quarter to ${digits[next_hour]}`;
    if (minute < 30) return `${minute_in_en(minute)} past ${digits[hour]}`
    if (minute > 30) return `${minute_in_en(60 - minute)} to ${digits[next_hour]}`
}
console.log(solve("00:05"))
