function countDigit(number, digit, base, fromBase) {
    var d = parseInt(number, fromBase);
    var s = d.toString(base);
    return (s.match(new RegExp(digit, 'g')) || []).length
};