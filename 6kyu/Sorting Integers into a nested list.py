def group_ints(lst, key=0):
    small, large = [], []
    res = []
    for x in lst:
        if x < key:
            if large:
                res.append(large)
                large = []
            small.append(x)
        else:
            if small:
                res.append(small)
                small = []
            large.append(x)
    if large:
        res.append(large)
    elif small:
        res.append(small)
    return res
