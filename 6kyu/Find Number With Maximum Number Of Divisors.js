function get_divisors(x) {
    n = 1;
    for (var i = 1; i < (x / 2 | 0) + 1; i++)if (x % i === 0) n++;
    return n;
}

const divNum = (a, b) => {
    if (a > b) return 'Error'
    var arr = Array.from({ length: b - a + 1 }, (_, i) =>
        [a + i, get_divisors(a + i)]).sort((a, b) => b[1] - a[1] || a[0] - b[0])
    return arr[0][0]
};
