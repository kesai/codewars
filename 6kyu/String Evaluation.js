function string_evaluation(string, condition) {
    var counts = {};
    for (x of string) counts[x] = counts[x] ? counts[x] + 1 : 1;
    return condition.map(v => {
        var [_, a, op, b] = v.match(/^([^!=<>]+)([!=<>]+)([^!=<>]+)$/);
        a = /\d+/.test(a) ? +a : counts[a];
        b = /\d+/.test(b) ? +b : counts[b];
        return eval(`${a}${op}${b}`)
    })
};
console.log(string_evaluation('aab#HcCcc##l#', ['a<b', '#==4', 'c>=C', 'H!=a']))