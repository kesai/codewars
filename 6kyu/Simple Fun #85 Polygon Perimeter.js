function polygonPerimeter(matrix) {
    var ROW = matrix.length, COL = matrix[0].length;
    var diff = [[0, -1], [0, 1], [-1, 0], [1, 0]];
    var boundCheck = (x, y) => x > -1 && y > -1 && x < ROW && y < COL;
    var res = 0;
    for (var i = 0; i < ROW; i++) {
        for (var j = 0; j < COL; j++) {
            if (!matrix[i][j]) continue;
            var p = 0;
            for (d of diff) {
                x = i + d[0], y = j + d[1];
                //判断是否为边界
                if (!boundCheck(x, y) || !matrix[x][y]) p++;
            }
            res += p;
        }
    }
    return res;
}