function digitsProduct(product) {
    if (product < 10) return 10 + product;
    var arr = []
    var d = 9;
    while (d > 1) {
        if (product % d === 0) { product /= d; arr.push(d); }
        else d--;
    }
    if (product > 1) return -1;
    return +arr.reverse().join('')
}
