class HallOfFame {
    constructor(size = 5, players = null) {
        this.size = size;
        if (players) {
            this.players = players.slice(0, size);
            if (this.players.length < size) this.players = this.players.concat(
                new Array(size - this.players.length).fill(''));
        }
        else this.players = new Array(size).fill('');

    }

    get list() {
        var players = this.players.filter(v => v != '').sort((a, b) => b[1] - a[1] || a[0].localeCompare(b[0]));
        players = players.map(v => `${v[0]}: ${v[1]}`);
        return players.concat(new Array(this.players.length - players.length).fill(''))
    }

    add(player) {
        if (this.players.indexOf('') > -1)
            this.players[this.players.indexOf('')] = player;
        else {
            this.players.push(player);
            this.players = this.players.sort((a, b) => b[1] - a[1] || a[0].localeCompare(b[0])).slice(0, this.size);
        }
        return this
    }
}