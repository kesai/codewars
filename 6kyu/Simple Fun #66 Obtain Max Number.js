function obtainMaxNumber(arr) {
    //coding and coding..
    while (new Set(arr).size < arr.length) {
        var counts = {}
        for (x of arr) counts[x] = counts[x] ? counts[x] + 1 : 1;
        var temp = [];
        for (k of Object.keys(counts)) {
            var n = counts[k];
            if (n % 2) temp.push(+k);
            if (n > 1) {
                for (var j = 0; j < parseInt(n / 2); j++)temp.push((+k) * 2);
            }
        }
        arr = temp;
    }
    return Math.max(...arr)
}
