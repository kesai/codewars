function solve(n) {
    for (var i = Math.floor(Math.sqrt(n)); i > 0; i--) {
        if (n % i === 0) {
            a = i; b = n / i;
            if ((b - a) % 2 === 0 && b !== a) {
                return (b - a) ** 2 / 4
            }
        }
    }
    return -1
}