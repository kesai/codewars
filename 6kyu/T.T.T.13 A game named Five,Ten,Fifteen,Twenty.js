function game(a, b) {
    let resA = a[1].split` `.reduce((acc, curr) => acc + (curr === 'Fist' ? 0 : 5), 0)
    let resB = b[1].split` `.reduce((acc, curr) => acc + (curr === 'Fist' ? 0 : 5), 0)
    let mistakeA = a[0] < resA || a[0] > resA + 10
    let mistakeB = b[0] < resB || b[0] > resB + 10
    if (mistakeA || mistakeB) return mistakeA && mistakeB ? 'Draw!' : mistakeA ? 'B Win!' : 'A Win!'
    let res = resA + resB
    return a[0] === res && b[0] !== res ? 'A Win!' : b[0] === res && a[0] !== res ? 'B Win!' : 'Draw!'
}