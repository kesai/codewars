function f(x) {
    return x > 1 ? x * f(x - 1) : x;
}
function comb(n, m) {
    if (!m || n === m) return 1;
    return f(n) / (f(m) * f(n - m))
}

function v1(n, p) {
    var res = 0;
    for (var k = 0; k <= n; k++) {
        res += ((-1) ** k) * (4 ** (n - k)) * comb(2 * n - k, k)
    }
    return Math.round(res * p);
}

function u1(n, p) {
    var res = 0;
    for (var k = 0; k <= n; k++) {
        res += ((-1) ** k) * (4 ** (n - k)) * comb(2 * n - k + 1, k)
    }
    return Math.round(res * p);
}
