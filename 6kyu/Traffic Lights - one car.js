function trafficLights(road, n) {
    lights = [...road].map((v, i) => new Object({ 'clr': v, 'idx': i, 'secs': 0 })).filter(v => /[RGO]/.test(v.clr));
    var roads = [...road].map(v => '.');
    function switchLight() {
        for (light of lights) {
            light.secs++;
            if (light.clr === 'G' && light.secs === 5) { light.clr = 'O'; light.secs = 0; }
            else if (light.clr === 'O' && light.secs === 1) { light.clr = 'R'; light.secs = 0; }
            else if (light.clr === 'R' && light.secs === 5) { light.clr = 'G'; light.secs = 0; }
            roads[light.idx] = light.clr;
        }
    }
    var car_pos = 0;
    var out = [road];
    for (var i = 1; i <= n; i++) {
        switchLight();
        if (car_pos <= road.length - 1) {
            if (/[^RO]/.test(roads[car_pos + 1])) {
                if (temp = lights.find(v => v.idx === car_pos)) {
                    roads[car_pos] = temp.clr;
                } else roads[car_pos] = '.';
                roads[++car_pos] = 'C';
            } else {
                roads[car_pos] = 'C';
            }
        }
        out.push(roads.join('').slice(0, road.length))
    }
    return out;
}
