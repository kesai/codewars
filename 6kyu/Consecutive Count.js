function getConsectiveItems(items, key) {
    items = items.toString();
    key = key.toString();
    return Math.max(...(items.match(new RegExp(`(${key}){1,}`, 'g')) || ['']).map(v => v.length))
}