var nums = []
for (var i = 0; i < 10 ** 8; i += 13) {
    if (!/[47]/.test(i)) nums.push(i);
}
function unluckyNumber(n) {
    for (var i = 0; i < nums.length; i++) {
        if (nums[i] > n) return i;
    }
}
