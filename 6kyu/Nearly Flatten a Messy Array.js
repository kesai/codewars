function nearFlatten(nested) {
    return JSON.stringify(nested).match(/\[[^\[\]]+\]/g).map(v => JSON.parse(v))
        .sort((a, b) => a[0] === b[0] ? 0 : a[0] < b[0] ? -1 : 1)
}
nearFlatten([[1, 2, 3], [[4, 5], [6, 7, 8]]])