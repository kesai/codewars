function logisticMap(width, height, xs, ys) {
    const manhattan = ([x1, y1], [x2, y2]) => Math.abs(x1 - x2) + Math.abs(y1 - y2);
    if (!xs.length || !ys.length) return Array.from({ length: height }, (_, i) => Array(width).fill(null));
    var out = Array.from({ length: height }, (_, i) => new Array(width));
    for (var i = 0; i < out.length; i++) {
        for (var j = 0; j < out[0].length; j++) {
            out[i][j] = Math.min(...xs.map((v, k) => manhattan([i, j], [ys[k], v])))
        }
    }
    return out;
}