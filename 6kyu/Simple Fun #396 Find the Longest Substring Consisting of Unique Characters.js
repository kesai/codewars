function longestSubstringOf(s) {
    pos = {};//Hash Map to store last occurrence of each already visited character
    st = 0;//starting point of current substring. 
    start = 0;//Hash Map to store last occurrence of each already visited character.
    maxlen = 0;//maximum length substring without repeating characters
    //pos[s[0]] = 0;//Last occurrence of first character is index 0  
    for (var i = 0; i < s.length; i++) {
        var c = s[i];
        if (pos[c] === undefined) pos[c] = i;
        else {
            if (pos[c] >= st) {
                curlen = i - st;
                if (maxlen < curlen) { start = st; maxlen = curlen; }
                st = pos[c] + 1
            }
            pos[c] = i;
        }
    }
    if (maxlen < s.length - st) { start = st; maxlen = s.length - st; }
    return s.substr(start, maxlen)
}