def sort_string(s):
    x = iter(sorted((c for c in s if c.isalpha()), key=str.lower))
    return ''.join(next(x) if v.isalpha() else v for v in s)
