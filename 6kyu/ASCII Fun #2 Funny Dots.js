//n (width) and m (height)
function dot(n, m) {
    var span = '+---'.repeat(n) + '+';
    var row = span + '\n' + '| o '.repeat(n) + '|';
    return Array(m).fill(row).join('\n') + '\n' + span;
}
dot(3, 2)
