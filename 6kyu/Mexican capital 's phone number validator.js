var isValidMXPhoneNumber = function (str) {
    if (/\(5 6\)/.test(str)) return false
    s = str.replace(/\s/g, '');
    if (/^^((\(5[56]\))|(5[56]))\d{6}$$/.test(s)) {
        s = s.replace(/[\(\)]/g, '');
        return s.split(' ').every(v => v.length % 2 === 0)
    }
    return false;
};

