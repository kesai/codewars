def alternate_sort(l):
    l.sort(key=lambda x: abs(x))
    print(l)
    negatives = [x for x in l if x < 0]
    positives = [x for x in l if x > 0]
    n = min(len(negatives), len(positives))
    out = [negatives[i//2] if i % 2 == 0 else positives[i//2]
           for i in range(2*n)]
    return out+negatives[n:]+positives[n:]


alternate_sort([5, -42, 2, -3, -4, 8, 9])
