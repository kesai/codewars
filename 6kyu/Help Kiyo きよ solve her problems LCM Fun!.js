const gcd = (a, b) => b ? gcd(b, a % b) : a;
const lcm = (a, b) => a * b / gcd(a, b);
function kiyoLcm(a) {
    var arr = a.map(v => v.reduce((s, x) => s += Number.isInteger(x) ? x % 2 ? x : 0 : 0, 0));
    var x = arr.pop();
    while (arr.length) x = lcm(x, arr.pop());
    return x || 0;
}
var a = [[4, 3, 7, 8, 9, 2, 1, 5, 'c'], [6, 5, 6, 1, 'v', 1, 0, 5, 1], [4, 4, 'c', 7, 6, 6, 3, 6, 7],
[1, 7, 7, 'l', 5, 8, 9, 5, 9], [0, 't', 8, 2, 8, 9, 0, 8, 0], [4, 6, 2, 6, 'o', 8, 4, 2, 4],
[3, 6, 9, 2, 0, 8, 2, 3, 'u'], [9, 3, 1, 9, 4, 4, 'u', 7, 7], [0, 'n', 9, 0, 0, 0, 9, 2, 2]];
console.log(kiyoLcm(a))
