function gcd(a, b) { return (!b) ? a : gcd(b, a % b); }

function greatestCommonFactor(arr) {
    return arr.reduce((a, b) => gcd(a, b));
}