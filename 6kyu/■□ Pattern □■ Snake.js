function snake(width, height) {
    var left = true;
    var out = []
    for (var i = 0; i < height / 2; i++) {
        if (i % 2) {
            if (left) {
                out.push('■□'.padStart(width, '∷'));
                out.push('□■'.padStart(width, '∷'));
            } else {
                out.push('■□'.padEnd(width, '∷'));
                out.push('□■'.padEnd(width, '∷'));
            }
            left = !left;
        }
        else {

            out.push('■□'.repeat(width / 2));
            out.push('□■'.repeat(width / 2));

        }
    }
    return out.join('\n')
}
console.log(snake(4, 10))
