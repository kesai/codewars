class Point {
    constructor(x, y, z) {
        this.x = x; this.y = y; this.z = z;
    }
    static getOrigin() {
        return new Point(0, 0, 0)
    }
    getDistanceFromOrigin() {
        return (this.x ** 2 + this.y ** 2 + this.z ** 2) ** 0.5
    }
    getDistanceFromPoint(p) {
        return ((this.x - p.x) ** 2 + (this.y - p.y) ** 2 + (this.z - p.z) ** 2) ** 0.5
    }
}