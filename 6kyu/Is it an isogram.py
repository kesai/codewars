import re


def is_isogram(word):
    if type(word) is str:
        word = re.sub(r'[^a-z]', '', word.lower())
        return len(set([word.count(c) for c in set(word)])) == 1
    else:
        return False
