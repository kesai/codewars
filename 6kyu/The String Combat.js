function combat(s1, s2) {
    var chars = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    while (s1 && s2) {
        var a = chars.indexOf(s1[0]); var b = chars.indexOf(s2[0]);
        if (a === b) { s1 = s1.slice(1); s2 = s2.slice(1); }
        else if (a > b) {
            s2 = s2.slice(1);
            s1 = chars[Math.round(a / 3)] + s1.slice(1)
        } else {
            s1 = s1.slice(1);
            s2 = chars[Math.round(b / 3)] + s2.slice(1)
        }
    }
    return s1 ? `Winner: s1(${s1})` : s2 ? `Winner: s2(${s2})` : 'Draw';
}