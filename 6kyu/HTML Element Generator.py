# https://treyhunner.com/2018/04/keyword-arguments-in-python/
def html(tag, *contents, **attributes):
    attr = " ".join(
        f'{param}="{value}"' if param != 'cls' else f'class="{value}"'
        for param, value in attributes.items()
    )
    if contents:
        return '\n'.join(
            f'<{tag} {attr}>{content}</{tag}>' if attr else f'<{tag}>{content}</{tag}>' for content in contents)
    else:
        return f'<{tag} {attr} />' if attr else f'<{tag} />'
