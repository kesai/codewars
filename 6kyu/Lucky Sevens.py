import math


def lucky_sevens(arr):
    n = 0
    diff = [(0, -1), (0, 1), (-1, 0), (1, 0)]

    def check(x, y):
        return x > -1 and y > -1 and x < len(arr) and y < len(arr[0])
    for i in range(len(arr)):
        for j in range(len(arr[0])):
            if arr[i][j] == 7:
                s = sum(arr[i+dx][j+dy] if check(i+dx, j+dy)
                        else 0 for dx, dy in diff)
                if round(s ** (1 / 3)) ** 3 == s:
                    n += 1
    return n
