function KamaSutraCipher(key) {
    this.encode = function (str) {
        return str.replace(/./g, v =>
            (tmp = key.find(e => e[0] === v)) ? tmp[1] : (tmp = key.find(e => e[1] === v)) ? tmp[0] : v);
    };
    this.decode = function (str) {
        return str.replace(/./g, v =>
            (tmp = key.find(e => e[0] === v)) ? tmp[1] : (tmp = key.find(e => e[1] === v)) ? tmp[0] : v);
    }
}
var key = [
    ['d', 'p'],
    ['n', 'o'],
    ['a', 'w'],
    ['f', 'c'],
    ['h', 's'],
    ['l', 'v'],
    ['m', 'j'],
    ['x', 'b'],
    ['e', 'z'],
    ['r', 'i'],
    ['k', 'y'],
    ['u', 'q'],
    ['t', 'g']
];
var c = new KamaSutraCipher(key);
console.log(c.encode('mutt'))//, 'jqgg');
console.log(c.encode('panda'))//, 'dwopw');