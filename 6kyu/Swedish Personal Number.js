function validatePersonalNumber(personalNumber) {
    return /^((19\d{2})|(20((0\d)|(1[0-6]))))((0[1-9])|(1[0-2]))((0[1-9])|([1-2]\d)|(3[01]))[+\-]?\d{4}$/.test(personalNumber);
}

