function sumAccounts(text) {
    return text.match(/-?\$\d+\b/g).reduce((s, v) => s += +v.replace('$', ''), 0)
}