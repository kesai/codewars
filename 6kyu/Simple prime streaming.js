function isPrime(num) {
    if (num < 2) return false;
    if (num === 2 || num === 3) return true;
    if (num % 6 != 1 && num % 6 != 5) return false
    var n = parseInt(Math.sqrt(num));
    for (var i = 5; i < n + 1; i += 6) {
        if (num % i === 0 || num % (i + 2) === 0) return false
    }
    return true
}
var primes = [2, 3, 5, 7]
function solve(a, b) {
    var i = primes[primes.length - 1];
    while (primes.join('').length <= a + b) {
        if (isPrime(++i)) primes.push(i);
    }
    return primes.join('').substr(a, b)
}