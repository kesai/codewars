var wordCounter = function (text) {
    var words = text.match(/[a-z']+/gi) || [];
    var counts = {}
    for (w of words) counts['$' + w] = counts['$' + w] ? counts['$' + w] + 1 : 1;
    return {
        count: function (w) { return counts['$' + w] || 0; }
    }
};