function findMaxRange(ranges) {
    var arr = ranges.map((v, i) => {
        [a, b] = v.match(/-?\d+(\.\d+)?/g).map(v => +v);
        return [i, Math.abs(b - a)];
    });
    var m = Math.max(...arr.map(v => v[1]));
    return arr.filter(v => v[1] === m).map(v => ranges[v[0]])
}