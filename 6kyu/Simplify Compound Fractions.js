const gcd = (a, b) => b ? gcd(b, a % b) : a;
function simplify(integer, numerator, denominator) {
    if (denominator === 0) return [integer, 0, 0]
    integer += Math.floor(numerator / denominator);
    numerator %= denominator;
    var x = gcd(numerator, denominator);
    return numerator ? [integer, numerator / x, denominator / x] : [integer, 0, 0];
}