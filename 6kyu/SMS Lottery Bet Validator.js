function validateBet(game, text) {
    if (!text || /[^\d\s,]/.test(text)) return null;
    var nums = text.match(/\d+/g).map(v => +v);
    if (new Set(nums).size < nums.length || nums.length != game[0] || nums.some(v => v < 1 || v > game[1]))
        return null;
    return nums.sort((a, b) => a - b)
}
