function restFood(gameSituation, foods) {
    function count(food) {
        return foods.filter(v => v === food).length;
    }
    var [beers, nuts, meats] = [count('Beer'), count('Nut'), count('Meat')];
    var counts = {
        Beer: beers, Nut: nuts, Meat: meats
    }
    function enjoy_food(food = 'Beer', count = 1) {
        var n = 0;
        while (n < count) {
            var idx = foods.indexOf(food);
            if (idx === -1) { console.log(`no enouth ${food}`); break; }
            foods.splice(idx, 1);
            counts[food]--;
            n++;
        }
    }
    for (x of gameSituation) {
        if (x === 'German team offense') {
            if (counts['Beer'] > 0 && counts['Nut'] > 2) {
                enjoy_food('Beer');
                enjoy_food('Nut', 3);
            } else break;
        } else if (x === 'German team scored') {
            if (counts['Beer'] > 0 && counts['Meat'] > 1) {
                enjoy_food('Beer');
                enjoy_food('Meat', 2);
            } else break;
        } else if (x === 'Brazil team scored') {
            if (counts['Nut'] > 4) enjoy_food('Nut', 5); else break;
        }
    }
    return foods;
}
