function subsOffsetApply(str, offset) {
    str = str.replace(/(\d{2}):(\d{2}):(\d{2}),(\d{3})/g, (_, h, m, s, ms) => {
        var total_ms = +h * 60 * 60 * 1000 + +m * 60 * 1000 + +s * 1000 + +ms + offset;
        hour = '' + Math.floor(total_ms / (60 * 60 * 1000));
        total_ms = total_ms % (60 * 60 * 1000);
        minute = '' + Math.floor(total_ms / (60 * 1000));
        total_ms = total_ms % 60000;
        second = '' + Math.floor(total_ms / 1000);
        mill_second = '' + total_ms % 1000;
        if (+hour > 99 || +hour < 0) return '刚做了个煞笔提';
        return `${hour.padStart(2, '0')}:${minute.padStart(2, '0')}:${second.padStart(2, '0')},${mill_second.padStart(3, '0')}`;
    });
    return /刚做了个煞笔提/.test(str) ? 'Invalid offset' : str
}
