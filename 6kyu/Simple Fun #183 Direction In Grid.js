function directionInGrid_bruce(n, m) {
    var d = 'R'
    moves = { 'R': [0, 1], 'L': [0, -1], 'U': [-1, 0], 'D': [1, 0] };
    var [x, y] = [0, 0];
    var turn_right = (s = 'RDLU') => d = s[(s.indexOf(d) + 1) % 4];
    var is_valid = (x, y) => x < n && x > -1 && y > -1 && y < m;
    visited = new Set();
    visited.add('0_0')
    while (visited.size < n * m) {
        var mov = moves[d];
        [x1, y1] = [x, y].map((v, i) => v + mov[i]);
        if (is_valid(x1, y1) && !visited.has(`${x1}_${y1}`)) {
            x = x1, y = y1;
            visited.add(`${x1}_${y1}`);
        } else {
            turn_right();
        }
    }
    return d;
}

directionInGrid = (n, m) => m < n ? m % 2 ? 'D' : 'U' : n % 2 ? 'R' : 'L'