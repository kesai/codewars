var rotate = (d, rot, s = 'NESW') => s[(s.indexOf(d) + (rot === 'L' ? -1 : 1) + 4) % 4];
function determineOutcome([X, Y], [x, y], directions) {
    var d = directions.shift();
    var moves = { 'N': [0, 1], 'S': [0, -1], 'W': [-1, 0], 'E': [1, 0] };
    for (action of directions) {
        if (/[LR]/.test(action)) {
            d = rotate(d, action);
        } else {
            var [kx, ky] = moves[d]; x += kx * action; y += ky * action;
        }
    }
    return [[x, y], x > X || y > Y || x < 0 || y < 0]
}