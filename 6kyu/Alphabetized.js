function alphabetized(s) {
    var x = 'abcdefghijklmnopqrstuvwxyz';
    s = s.replace(/[^a-z]/gi, '');
    s = [...s].sort((a, b) =>
        x.indexOf(a.toLowerCase()) - x.indexOf(b.toLowerCase()) || s.indexOf(a) - s.indexOf(b)).join('');
    return s
}
