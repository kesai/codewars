function name_code(name) {
    var words = name.match(/[a-z']+/gi);
    if (words.length === 1) return words[0].substr(0, 6).toUpperCase();
    if (words.length === 2) return words[0].substr(0, 3).toUpperCase() + words[1].substr(0, 3).toUpperCase();
    if (words.length === 3) return words.reduce((s, v) => s += v.substr(0, 2), '').toUpperCase();
    if (words.length === 4) return (words[0][0] + words[1][0] + words[2].substr(0, 2) + words[3].substr(0, 2)).toUpperCase();
}

function createReport(name) {
    var counts = {};
    for (x of name) {
        x = x.replace(/\-/g, ' ');
        var [_, s, num] = x.match(/([a-z'\s]+)(\d+)/i);
        if (/Labrador\s+Duck/.test(x)) return ["Disqualified data"];
        var code = name_code(s);
        counts[code] = counts[code] ? counts[code] + +num : +num;
    }
    var temp = Object.entries(counts).sort((a, b) => a[0].localeCompare(b[0]) || b[1] - a[1]);
    var res = [];
    temp.forEach(e => res = res.concat(e));
    return res;
}
