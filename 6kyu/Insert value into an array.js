Object.defineProperty(Array.prototype, 'insert', {
    enumerable: false,
    value: function (index, value) {
        this.splice(index, 0, value);
        return this;
    }
});