function whoWouldWin(mon1, mon2) {
    var turn = 0;
    mon1['first'] = mon1.hitpoints;
    mon2['first'] = mon2.hitpoints;
    function fight(a, b) {
        var damages = a.damage * a.number;
        if (damages < b.first) b.first -= damages;
        else {
            damages -= b.first; b.number -= 1;
            var deads = parseInt(damages / b.hitpoints);
            b.number -= deads;
            b.first = b.hitpoints - damages % b.hitpoints;
        }
    }
    var debug = 0
    while (mon1.number > 0 && mon2.number > 0) {
        if (turn === 0) {
            fight(mon1, mon2); turn = 1;
        } else {
            fight(mon2, mon1); turn = 0;
        }
    }
    winner = mon1.number > 0 ? mon1 : mon2;
    return `${winner.number} ${winner.type}(s) won`
}