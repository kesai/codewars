def expand(maze, fill):
    n = len(maze)
    return [[fill]*2*n]*(n//2)+[[fill]*(n//2)+x+[fill]*(n//2) for x in maze]+[[fill]*2*n]*(n//2)
