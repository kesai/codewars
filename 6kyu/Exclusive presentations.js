function presentationAgenda(friendList) {
    out = []
    for (x of friendList) {
        var name = x.person;
        var others = friendList.filter(v => v.person != name);
        var dest = x.dest.filter(e => others.findIndex(v => v.dest.includes(e)) == -1);
        if (dest.length) {
            out.push({ 'person': name, 'dest': dest })
        }
    }
    return out;
}