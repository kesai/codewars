
var crack = function (safe) {
    var f = (n) => n.toString().padStart(2, '0');
    //try first part
    function crack_first() {
        for (var i = 0; i < 100; i++) {
            var combs = ['L00-L00', 'R00-R00', 'L00-R00', 'R00-L00'];
            for (x of combs) {
                var tempL = `L${f(i)}`;
                if (/click/.test(safe.unlock(`${tempL}-${x}`))) return tempL;
                var tempR = `R${f(i)}`;
                if (/click/.test(safe.unlock(`${tempR}-${x}`))) return tempR;
            }
        }
    }
    var first = crack_first();

    function crack_second() {
        for (var i = 0; i < 100; i++) {
            var combs = ['L00', 'R00'];
            for (x of combs) {
                var tempL = `L${f(i)}`;
                if (/click-click/.test(safe.unlock(`${first}-${tempL}-${x}`))) return tempL;
                var tempR = `R${f(i)}`;
                if (/click-click/.test(safe.unlock(`${first}-${tempR}-${x}`))) return tempR;
            }
        }
    }
    var second = crack_second();

    function crack_third() {
        for (var i = 0; i < 100; i++) {
            var tempL = `L${f(i)}`;
            if (/click-click-click/.test(safe.unlock(`${first}-${second}-${tempL}`))) return tempL;
            var tempR = `R${f(i)}`;
            if (/click-click-click/.test(safe.unlock(`${first}-${second}-${tempR}`))) return tempR;
        }
    }
    var third = crack_third();
    console.log(`${first}-${second}-${third}`)
    safe.unlock(`${first}-${second}-${third}`);
    return safe.open()
}

