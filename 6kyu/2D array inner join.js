function innerJoin(arrA, arrB, indA, indB) {
    var res = [];
    for (row of arrA) {
        if (row[indA] === null) continue;
        var matches = arrB.filter(v => v[indB] === row[indA])
        if (matches.length) res = res.concat(matches.map(v => row.concat(v)))
    }
    return res;
}