var sloganMaker = function (array) {
    arr = [...new Set(array)];
    if (!arr.length) return [];
    var groups = [[arr.shift()]];
    while (arr.length) {
        var word = arr.shift();
        var new_groups = []
        for (x of groups) {
            for (var i = 0; i <= x.length; i++) {
                new_groups.push(x.slice(0, i).concat([word]).concat(x.slice(i)))
            }
        }
        groups = new_groups;
    }
    return groups.map(v => v.join(' '))
}

function comb(arr, word) {
    var res = []
    for (var i = 0; i <= arr.length; i++) {
        res.push(arr.slice(0, i).concat([word]).concat(arr.slice(i)));
    }
    return res;
}