
function exit_from_maze(board) {
    var moves = { 'right': [1, 0], 'left': [-1, 0], 'up': [0, -1], 'down': [0, 1] }
    var y = board.findIndex(v => v.includes('*'));
    var x = board[y].indexOf('*');
    var mirrors = {
        '/': { 'right': 'up', 'left': 'down', 'down': 'left', 'up': 'right' },
        '\\': { 'right': 'down', 'left': 'up', 'down': 'right', 'up': 'left' }
    }
    var d = !x ? 'right' : !y ? 'down' : y == board.length - 1 ? 'up' : 'left';
    var dist = 0;
    while ((l = board[y][x]) != '#') {
        if (/[\/\\]/.test(l)) d = mirrors[l][d];
        x += moves[d][0];
        y += moves[d][1];
        dist++;
    }
    return { position: [x, y], distance: dist - 1 };
}
