var multiFilter = function (...args) {
    var funs = args;
    return function (x) {
        return funs.every(f => f(x));
    };
};
