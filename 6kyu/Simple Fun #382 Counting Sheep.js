function count_block(arr) {
    var shapes = [[[0, 0], [0, 2], [1, 1], [2, 0], [2, 2]], [[0, 1], [1, 0], [1, 1], [1, 2], [2, 1]]];
    return (shapes.filter(shape => shape.map(v => arr[v[0]][v[1]]).sort().join('') === 'eehps') || []).length;
}
function count(sheep) {
    var arr = sheep.split('\n');
    var n = 0;
    for (var i = 0; i < arr.length - 2; i++) {
        for (var j = 0; j < arr[0].length - 2; j++) {
            var block = Array.from({ length: 3 }, (_, idx) => arr[i + idx].slice(j, j + 3))
            n += count_block(block);
        }
    }
    return n;
}
