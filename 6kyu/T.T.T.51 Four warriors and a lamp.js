function shortestTime(speed) {
    var [a, b, c, d] = speed.sort((a, b) => a - b);
    var x = 2 * a + (b + c + d);
    var y = 3 * b + a + d;
    return Math.min(x, y)
}
