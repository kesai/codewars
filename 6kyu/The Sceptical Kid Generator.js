function AlanAnnoyingKid(input) {
    var [_, action, content] = input.match(/^Today I ((?:didn't )?[a-z]+) (.+)\.$/);
    if (action.includes('didn\'t')) {
        return `I don't think you ${action} ${content} today, I think you did ${action.replace("didn't ", '')} it!`;
    } else {
        return `I don't think you ${action} ${content} today, I think you didn't ${action.replace(/ed$/, '')} at all!`;
    }
}