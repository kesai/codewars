def sequence(phrase):
    kb = {'1': '1', '2': 'ABC2', '3': 'DEF3', '4': 'GHI4', '5': 'JKL5', '6': 'MNO6',
          '7': 'PQRS7', '8': 'TUV8', '9': 'WXYZ9', '*': '*', '0': ' 0', '#': '#'}
    phrase = phrase.upper()
    last_key = None
    res = ''
    for c in phrase:
        k, v = [(k, v) for k, v in kb.items() if c in v][0]
        if last_key == k:
            res += 'p'
        res += k*(v.index(c)+1)
        last_key = k
    return res
