function maximumProfit(price, cost, quantity, extra) {
    var profit0 = (price - cost) * quantity;
    var prices = []
    var i = 0;
    while (true) {
        var price1 = price - i;
        var profit1 = (price1 - cost) * (quantity + extra * i);
        prices.push(new Object({ 'price': price1, 'profit': profit1 }))
        if (profit1 < profit0) break;
        i++;
    }
    return prices.sort((a, b) => b.profit - a.profit || a.price - b.price)[0].price;
}
maximumProfit(100000, 70, 80, 10)