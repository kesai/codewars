function johnChoose(length, oldd, newd) {
    newd = newd.filter(v => v < oldd && v < length);
    var arr = Array.from({ length: length / oldd + 1 }, (_, i) => i * oldd)
    var new_holes = newd.map(
        v => Array.from({ length: length / v + 1 }, (_, i) => i * v).filter(v => v % oldd != 0)
            .length);
    var burry = newd.map(v => arr.filter(e => e % v).length)
    new_holes = new_holes.map((v, i) => v + burry[i]);
    var m = Math.min(...new_holes);
    return newd[new_holes.indexOf(m)] || null
}