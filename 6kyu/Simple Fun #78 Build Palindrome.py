is_palindrome = lambda x: x[::-1] == x


def build_palindrome(s):
    if is_palindrome(s):
        return s
    for i in range(1, len(s)):
        right = s[i:]
        if is_palindrome(right):
            return s + s[0:i][::-1]