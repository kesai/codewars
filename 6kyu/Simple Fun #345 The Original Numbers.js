function originalNumbers(numbers, turns) {
    var sum = numbers.reduce((s, v) => s += v, 0) / (numbers.length - 1);
    return turns ? originalNumbers(numbers.map(v => sum - v), turns - 1) : numbers;
}
console.log([100, 12, 125, 74])