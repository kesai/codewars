function split(arr) {
    arr1 = [];
    arr.forEach(e => arr1 = arr1.concat(e));
    arr2 = arr.map(v => [v.length]);
    return [arr1, arr2]
}

function join(arr1, arr2) {
    var arr = [];
    var i = 0;
    for ([x] of arr2) {
        arr.push(arr1.slice(i, i + x));
        i += x;
    }
    return arr;
}

arr1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], arr2 = [[1], [2], [3], [4]]
join(arr1, arr2)