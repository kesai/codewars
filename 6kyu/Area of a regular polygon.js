function areaOfRegularPolygon(s) {
    try {
        var [_, n, s, unit] = s.match(/^(\d+) sides of (\S+) (\S+) each$/);
        if (+n < 3) return 'Invalid input'
        var a = (((+s) ** 2 * (+n)) / (4 * Math.tan(Math.PI / (+n)))).toFixed(2)
        return `${a} sq.${unit}`
    } catch (error) {
        return 'Invalid input'
    }
}