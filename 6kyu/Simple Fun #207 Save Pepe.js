function savePepe(msg) {
    var res = ''
    var prev = 0;
    for (var i = 0; i < msg.length; i++) {
        for (var x = 1; x < 256; x++) {
            if (((129 * x) ^ prev) % 256 === msg[i]) {
                res += String.fromCharCode(x);
                prev = x;
                break;
            }
        }
    }
    return res;
}

console.log(savePepe([241, 134, 146, 23, 6, 141, 140, 156, 134, 31, 145, 146, 23, 2, 129, 15]))