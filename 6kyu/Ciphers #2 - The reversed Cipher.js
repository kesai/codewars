function encode(plaintext) {
    return plaintext.replace(/[\S]+/gi, w =>
        [...w.slice(0, -1)].reverse().join('') + w.slice(-1))
}