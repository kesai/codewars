function christmasTree(height) {
    var n = parseInt(height / 3);
    var w = 2 * n + 3;
    var tree = []
    for (var i = 0; i < n; i++) {
        var k = 2 * i + 1;
        for (var j = 0; j < 3; j++, k += 2) {
            tree.push(' '.repeat((w - k) / 2) + '*'.repeat(k));
        }
    }
    if (height > 2) tree.push(' '.repeat((w - 3) / 2) + '###');
    return tree.join('\r\n')
}