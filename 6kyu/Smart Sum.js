function smartSum(...args) {
    return args.toString().split(',').reduce((s, v) => s += +v, 0);
}