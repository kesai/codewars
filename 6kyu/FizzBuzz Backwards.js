const gcd = (a, b) => b ? gcd(b, a % b) : a;
function reverseFizzBuzz(array) {
    var fizzs = [], buzzs = [];
    for (var i = 0; i < array.length; i++) {
        var n = i + 1;
        if (array[i] === 'Fizz') fizzs.push(n);
        else if (array[i] === 'Buzz') buzzs.push(n);
        else if (array[i] === 'FizzBuzz') {
            fizzs.push(n); buzzs.push(n);
        }
    }
    var fizz = fizzs.pop();
    var buzz = buzzs.pop()
    fizzs.forEach(v => fizz = gcd(fizz, v));
    buzzs.forEach(v => buzz = gcd(buzz, v));
    return [fizz, buzz]
};