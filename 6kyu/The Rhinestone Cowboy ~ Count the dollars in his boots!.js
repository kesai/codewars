function cowboysDollars([left, right]) {
    left_cnt = (left.replace(/\n/, '').split('&')[0].match(/\(1\)/g) || []).length;
    right_cnt = (right.replace(/\n/, '').split('&')[0].match(/\(1\)/g) || []).length;
    return `This Rhinestone Cowboy has ${right_cnt} dollar bills in his right boot and ${left_cnt} in the left`
}
