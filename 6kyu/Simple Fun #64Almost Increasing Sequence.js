function almostIncreasingSequence(sequence) {
    for (var i = 0; i < sequence.length - 1; i++) {
        if (sequence[i] >= sequence[i + 1]) {
            if (i === sequence.length - 2) sequence.splice(sequence.length - 1, 1);
            else sequence.splice(i, 1);
            break;
        }
    }
    for (var i = 0; i < sequence.length - 1; i++) {
        if (sequence[i] >= sequence[i + 1]) return false;
    }
    return true;
}
console.log(almostIncreasingSequence([1, 1, 1, 2, 3]))