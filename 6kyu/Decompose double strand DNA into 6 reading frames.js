var decomposeDoubleStrand = function (s) {
    map = { 'A': 'T', 'G': 'C', 'T': 'A', 'C': 'G' };
    var r = [...s].reverse().join('').replace(/./g, v => map[v]);
    var arr = Array.from({ length: 3 },
        (_, i) => `Frame ${i + 1}: ${(' '.repeat(3 - i) + s).match(/.{1,3}/g).join(' ').trim()}`);
    var r_arr = Array.from({ length: 3 },
        (_, i) => `Reverse Frame ${i + 1}: ${(' '.repeat(3 - i) + r).match(/.{1,3}/g).join(' ').trim()}`);
    return arr.join('\n') + '\n\n' + r_arr.join('\n')
}