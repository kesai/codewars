function isSubstitutionCipher(string1, string2) {
    var map = {};
    for (var i = 0; i < string1.length; i++) {
        var a = string1[i];
        var b = string2[i];
        if (!map[a]) {
            if (Object.values().includes(b)) return false;
            map[a] = b;
        }
        if (map[a] !== b) return false;
    }
    return true;
}