function luxhouse(houses) {
    var res = [];
    for (var i = 0; i < houses.length; i++) {
        var m = Math.max(...houses.slice(i + 1));
        if (houses[i] > m) res.push(0); else
            res.push(m - houses[i] + 1);
    }
    return res;
}