const select = (population, fitnesses) => {
    const wheel = fitnesses.reduce((a, b) => a + b);
    let spin = Math.random() * wheel;
    return population.find((e, i) => (spin -= fitnesses[i]) <= 0);
};