class JurassicWord {
    lunchTime(scene) {
        var d = scene.length % 2 ? 2 : 3;
        var k = scene.substr(parseInt(scene.length / 2) - d, 5);
        var dinosaur = 'Something';
        var dinosaurs = { 'VvvvV': 'A T-Rex', 'vvvvv': 'A velociraptor', 'uuuuu': 'A brachiosaurus', 'uuVuu': 'A triceratops' }
        if (Object.keys(dinosaurs).includes(k)) dinosaur = dinosaurs[k];
        var food = 'something';
        var foods = [[/iii.{5}iii/, 'flowers'], [/\|\|\|.{5}\|\|\|/, 'leaves'], [/_C.{5}C}>/, 'a dead dino']]
        var temp = foods.find(v => v[0].test(scene));
        if (temp) food = temp[1];
        var rules = {
            'a dead dino': ['A velociraptor', 'A T-Rex'],
            'leaves': ['A brachiosaurus'],
            'flowers': ['A brachiosaurus', 'A triceratops']
        }
        if (dinosaur != 'Something' && food != 'something' && !rules[food].includes(dinosaur)) dinosaur = 'Something';
        return `${dinosaur} is eating ${food}.`
    }
}
