function hammingDistance(a, b) {
    var a1 = a.toString(2)
    var b1 = b.toString(2);
    var l = Math.max(a1.length, b1.length);
    a1 = a1.padStart(l, '0'), b1 = b1.padStart(l, '0')
    return [...a1].reduce((s, v, i) => s += (v != b1[i] ? 1 : 0), 0)
}
const rotate = (x) => x.slice(-1)[0] + x.slice(0, -1)
function hammingRotate(a, b) {
    var stastics = [[0, hammingDistance(a, b)]];
    var n = 0;
    var temp = rotate(a);
    stastics.push([++n, hammingDistance(temp, b)]);
    console.log(temp)
    while (temp != a) {
        temp = rotate(temp);
        console.log(temp)
        stastics.push([++n, hammingDistance(temp, b)]);
    }
    return stastics.length ? stastics.sort((a, b) => a[1] - b[1] || a[0] - b[0])[0][0] : 0
}
console.log(hammingRotate('0110010011001101', '0111100110110011'))