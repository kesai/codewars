function buyNewspaper(s1, s2) {
    var s = s1.replace(/./g, (v) => `${v}?`);
    return [...new Set(s2)].every(c => new RegExp(c).test(s1)) ? s2.match(new RegExp(s, 'g')).length - 1 : -1;
}
