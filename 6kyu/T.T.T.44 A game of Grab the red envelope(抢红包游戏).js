function grabRedEnvelope(grabInEachRound) {
    var players = [2000, 2000, 2000, 2000, 2000];
    var idx = 0;
    for (x of grabInEachRound) {
        if (players.some(v => v < 1000)) break;
        players[idx] -= 100;
        x.forEach((v, i) => players[i] += v);
        idx = x.indexOf(Math.max(...x))
    }
    return players.map(v => Math.round(v * 10) / 10);
}
