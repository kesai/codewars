def score(w):
    return sum(ord(c) for c in w)


def find_word(num_let, max_ssw):
    global WORD_LIST
    res = sorted([(w, score(w)) for w in WORD_LIST if len(
        w) == num_let and score(w) <= max_ssw], key=lambda x: x[1] or x[0])
    return res[-1][0] if res else None
