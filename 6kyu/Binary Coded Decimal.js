function toBcd(number) {
    return number < 0 ? '-' + toBcd(-number) : [...'' + number].map(v => (+v).toString(2).padStart(4, '0')).join(' ')
}