function shareChocolate(n) {
    if (!Number.isInteger(n) || n < 1) return null;
    var counts = [0, 0, 0, 0, 0, 0];
    for (var i = 0; i < n; i++) {
        var temp = counts.map((v, i) => (i + 1) / (counts[i] + 1));
        var idx = temp.findIndex(v => v === Math.max(...temp));
        counts[idx]++;
    }
    return counts;
}