function memorysizeConversion(memorysize) {
    units = {
        'kB': { 'k': 1000 / 1024, 'unit': 'KiB' },
        'MB': { 'k': (1000 ** 2) / (1024) ** 2, 'unit': 'MiB' },
        'GB': { 'k': (1000 ** 3) / (1024) ** 3, 'unit': 'GiB' },
        'TB': { 'k': (1000 ** 4) / (1024) ** 4, 'unit': 'TiB' },
        'KiB': { 'k': 1024 / 1000, 'unit': 'kB' },
        'MiB': { 'k': (1024 ** 2) / (1000) ** 2, 'unit': 'MB' },
        'GiB': { 'k': (1024 ** 3) / (1000) ** 3, 'unit': 'GB' },
        'TiB': { 'k': (1024 ** 4) / (1000) ** 4, 'unit': 'TB' },
    }
    var f = (x) => x.toFixed(3).replace(/0*$/, '')
    var [d, unit] = memorysize.split(' ');
    var x = +d * units[unit].k;
    return `${f(x)} ${units[unit].unit}`;
}