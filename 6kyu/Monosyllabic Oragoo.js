function oragoo(str) {
    var x = 'orag';
    if (/^[^aeiouy]*$/i.test(str)) return str;
    return /^y/i.test(str) ? str.replace(/(?=[aeiou])/i, x) : str.replace(/(?=[aeiouy])/i, x)
}