Array.prototype.average = function () {
    this.reduce((s, v) => s += Array.isArray(v) ? v.average() : parseFloat(v), 0) / this.length;
}