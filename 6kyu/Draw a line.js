function drawLine(start, end) {
    var N = Math.max(Math.abs(end.x - start.x), Math.abs(end.y - start.y));
    var res = []
    for (var i = 0; i <= N; i++) {
        start + i * (end - start) / N
        x = Math.floor(start.x + i * (end.x - start.x) / N);
        y = Math.floor(start.y + i * (end.y - start.y) / N);
        res.push({ x, y })
    }
    return N ? res : [start];
}