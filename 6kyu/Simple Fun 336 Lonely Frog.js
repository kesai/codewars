function Node(val, level = 0, step = 1, prev) {
    this.val = val;
    this.level = level
    this.step = step;
    this.prev = prev;
}
function jumpTo(n) {
    //if (n > 1000) return 'i dont know'
    var st = new Node(0, 0, 1, null);
    var q = [st];
    visited = { 0: true };
    while (q.length) {
        var curr = q.shift();
        if (curr.val === n) {
            return curr.level
        } else {
            var moves = [-curr.step, curr.step];
            for (x of moves) {
                var next = curr.val + x;
                if (!visited[`${next}`]) {
                    visited[`${next}`] = true;
                    q.push(new Node(next, curr.level + 1, curr.step + 1, curr));
                }
            }
        }
    }
}
