function evenBinary(n) {
    var arr = n.split(' ');
    var evens = arr.filter(v => parseInt(v, 2) % 2 === 0).sort((a, b) => parseInt(a, 2) - parseInt(b, 2));
    var odds = arr.filter(v => parseInt(v, 2) % 2);
    var res = []
    for (x of arr) if (parseInt(x, 2) % 2) res.push(odds.shift()); else res.push(evens.shift());
    return res.join(' ')
}