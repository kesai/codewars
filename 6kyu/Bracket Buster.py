def bracket_buster(string):
    if type(string) is not str:
        return "Take a seat on the bench."
    stack = []
    s = ''
    res = []
    for c in string:
        if c == '[':
            if not stack:
                stack.append(c)
            else:
                s += c
        elif c == ']':
            if stack:
                res.append(s)
                stack.pop()
                s = ''
        else:
            if stack:
                s += c
    return res if res else "Take a seat on the bench."
