function decode(number) {
    var chars = 'abcdefghijklmnopqrstuvwxyz';
    var arr = number.split('98');
    var out = []
    for (var i = 0; i < arr.length; i++) {
        if (i % 2) {
            out.push(parseInt(arr[i], 2))
        } else {
            var word = (arr[i].match(/\d{3}/g) || []).reduce((s, v) => s += chars[+v - 101], '');
            if (word) out.push(word)
        }
    }
    return out.join(', ')
}

console.log(decode('103115104105123101118119981001098'))