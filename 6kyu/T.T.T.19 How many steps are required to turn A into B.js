function Node(val, level = 0) {
    this.val = val;
    this.level = level;
}
function howManyStep(a, b) {
    var st = new Node(a);
    var q = [st];
    var visited = { a: true }
    var steps = [(x) => 2 * x, (x) => x + 1];
    while (q.length) {
        var curr = q.shift();
        if (curr.val === b) {
            return curr.level
        }
        for (f of steps) {
            var next = f(curr.val);
            if (!visited[next] && next <= b) {
                visited[next] = true;
                q.push(new Node(next, curr.level + 1));
            }
        }
    }
}