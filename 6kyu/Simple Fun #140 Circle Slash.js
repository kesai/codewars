//ref:https://www.geeksforgeeks.org/josephus-problem-set-2-simple-solution-k-2/
function circleSlash(n) {
    var p = 1;
    while (p <= n) p *= 2;
    // Return 2n - 2^(1+floor(Logn)) + 1 
    return (2 * n) - p + 1;
}
