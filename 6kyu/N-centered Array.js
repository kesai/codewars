function isCenteredN(arr, n) {
    if (n === 0 && arr.length % 2 === 0) return true;
    if (arr.reduce((s, v) => s += v, 0) === n) return true;
    for (var i = 1; i < arr.length / 2; i++) {
        if (arr.slice(i, -i).reduce((s, v) => s += v, 0) === n) return true;
    }
    return false;
}
console.log(isCenteredN([3, 3], 6))
