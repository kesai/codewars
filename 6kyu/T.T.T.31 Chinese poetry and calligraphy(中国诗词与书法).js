function layout(poem, height) {
    poem = poem.replace(/[，。！？、；\s]/g, '')
    var arr = poem.match(new RegExp(`.{1,${height}}`, 'g')).map(v => v.padEnd(height, ' ').split('')
        .map(x => x === ' ' ? '  ' : x)
    )
    return arr[0].map((e, i) => arr.map(v => v[i]).reverse())
}