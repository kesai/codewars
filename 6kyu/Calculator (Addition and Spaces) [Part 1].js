function calculate(str) {
    str = str.replace(/\s/g, '');
    return str.split('+').reduce((s, v) => s += +v, 0);
}