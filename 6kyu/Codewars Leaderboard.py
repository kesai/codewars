from bs4 import BeautifulSoup
import urllib.request


def get_leaderboard_honor():
    res = urllib.request.urlopen('https://www.codewars.com/users/leaderboard')
    html = res.read()
    soup = BeautifulSoup(html.decode("utf-8"), "html.parser")
    return [int(td.text.replace(',', '')) for td in soup.find_all('td')[3::4]]


print(get_leaderboard_honor())
