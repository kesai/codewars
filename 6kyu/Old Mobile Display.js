function mobileDisplay(n, p) {
    p = p < 30 ? 30 : p
    n = n < 20 ? 20 : n;
    var height = parseInt(n * p / 100);
    var out = []
    for (var i = 0; i < height; i++) {
        if (i === 0 || i === height - 1) s = '*'.repeat(n);//top and bottom
        else if (i === height - 2) {
            var left = '* Menu'; var right = 'Contacts *';
            s = left + ' '.repeat(n - left.length - right.length) + right;
        } else if (i === parseInt(height / 2) - 1) {
            var title = 'CodeWars';
            var a = parseInt((n - title.length) / 2);
            var b = parseInt((n - title.length) / 2) + (n - title.length) % 2;
            s = '*'.padEnd(a, ' ') + title + '*'.padStart(b, ' ');
        } else s = '*' + ' '.repeat(n - 2) + '*'
        out.push(s)
    }
    return out.join('\n');
}

mobileDisplay(14, 30)
var s = '********************\\n*                  *\\n*     CodeWars     *\\n*                  *\\n* Menu    Contacts *\\n********************'
console.log(s.split('\\n').length)
