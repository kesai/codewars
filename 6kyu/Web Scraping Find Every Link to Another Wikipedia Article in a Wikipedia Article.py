from bs4 import BeautifulSoup as bs
import urllib.request
import re


def wikiscraping(url):
    res = urllib.request.urlopen(url)
    html = res.read()
    data = html.decode("utf-8")
    soup = bs(data, "html.parser")
    result = dict()
    for x in soup.select('#bodyContent a'):
        if x.has_attr('href') and re.fullmatch(r"\/wiki\/[^/:]+$", x['href']):
            result[x['title']] = 'https://en.wikipedia.org'+x['href']
    return result
