function distSameLetter(s) {
    var max = -Infinity;
    stastics = {}
    for (c of new Set(s)) {
        d = s.lastIndexOf(c) - s.indexOf(c) + 1;
        max = Math.max(max, d);
        stastics[c] = d;
    }
    var res = []
    for (c in stastics) {
        if (stastics[c] === max) res.push(`${c}${stastics[c]}`)
    }
    return res.sort((a, b) => s.indexOf(a[0] - s.indexOf(b[0])))[0]
}