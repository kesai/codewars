function findDiscounted(prices) {
    if (!prices) return '';
    prices = prices.split(' ').sort((a, b) => +a - +b);
    out = []
    while (prices.length) {
        temp = +prices.shift();
        var org = parseInt(temp * 4 / 3)
        prices.splice(prices.findIndex(v => v === '' + org), 1)
        out.push(temp)
    }
    return out.join(' ')
}
findDiscounted("15 20 60 75 80 100")