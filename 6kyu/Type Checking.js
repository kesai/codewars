function type(val) {
    if (val === null) return 'Null'
    if (val === undefined) return 'Undefined';
    if (typeof (val) === 'boolean') return 'Boolean';
    if (typeof (val) === 'function') return 'Function';
    if (typeof (val) === 'number') {
        if (Number.isNaN(val)) return 'Number NaN';
        if (!Number.isFinite(val)) return 'Number Infinite';
        if (Number.isInteger(val)) return 'Number Integer';
        return 'Number Float';
    }
    if (typeof (val) === 'string') {
        if (/^\d+(\.\d+)?$/.test(val) || /^0x[\dA-F]+$/.test(val)) return 'String Numeric'
        return 'String';
    }
    if (Array.isArray(val)) return 'Array'
    return val.constructor.name;
}
