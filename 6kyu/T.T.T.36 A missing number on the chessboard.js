function missingNumber(board1, board2) {
    var sum11 = 0;
    var sum12 = 0;
    var sum21 = 0;
    var sum22 = 0;
    var in_sum21 = true;
    for (var i = 0; i < board1.length; i++) {
        for (var j = 0; j < board1.length; j++) {
            if (i % 2 === j % 2) {
                sum11 += board1[i][j];
                if (board2[i][j] != '?') sum21 += board2[i][j]
            } else {
                sum12 += board1[i][j];
                if (board2[i][j] != '?') sum22 += board2[i][j];
                else in_sum21 = false;
            }
        }
    }
    return in_sum21?(sum22 - sum12 + sum11 - sum21):(sum21 - sum11 - sum22 + sum12);
}
