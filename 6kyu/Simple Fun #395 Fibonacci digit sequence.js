function _find(a, b, n) {
    if (a == 0 && b == 0) return 0;
    var s = `${a}${b}`;
    while (s.length <= n) {
        var [x, y] = s.slice(-2).split('');
        s += +x + +y;
    }
    return s[n]
}
function find(a, b, n) {
    if (n < 50) return +_find(a, b, n)
    var s = ''
    for (var i = 1; i < 50; i++)s += _find(a, b, i);
    var cats = ['8134711235', '1459']
    for (cat of cats) {
        if (s.includes(cat)) {
            var k = (n - s.indexOf(cat)) % cat.length - 1;
            if (k === -1) k = cat.length - 1;
            return +cat[k]
        }
    }
    return 0;
}