function get_parent_node(o, key) {
    var cur = o;
    for (var i = 0; i < key.length - 1; i++) {
        cur = cur[key.substr(0, i + 1)];
    }
    return cur;
}
function buildTrie(...words) {
    var n = Math.max(...words.map(v => v.length));
    var o = {}
    var last = o;
    for (var i = 0; i < n; i++) {
        var keys = new Set(words.filter(v => v.length > i).map(v => v.substr(0, i + 1)));
        for (k of keys) {
            var node = get_parent_node(o, k);
            node[k] = {};
        }
    }
    var res = JSON.parse(JSON.stringify(o).replace(/\{\}/g, 'null'))
    return res || {}
}
