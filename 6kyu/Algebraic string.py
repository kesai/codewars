import re


def sum_prod(s):
    nums, opers = [], []
    fn = {'+': lambda x, y: x+y, '*': lambda x, y: x*y}
    for m in re.findall(r'((?:\d+(\.\d+)?)|[+*])', s):
        x = m[0]
        if x == '+':
            if opers:
                while(opers):
                    nums.append(fn[opers.pop()](nums.pop(), nums.pop()))
            opers.append(x)
        elif x == '*':
            if opers and opers[-1] == '*':
                nums.append(fn[opers.pop()](nums.pop(), nums.pop()))
            opers.append(x)
        else:
            nums.append(float(x))
    while(opers):
        nums.append(fn[opers.pop()](nums.pop(), nums.pop()))
    return f'{nums[0]:.5e}'
