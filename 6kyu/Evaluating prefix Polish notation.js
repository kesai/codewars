function calculate(expression) {
    var arr = expression.split(' ').reverse();
    var nums = []
    for (v of arr) {
        if (/^[+\-*/]$/.test(v)) {
            var exp = nums.pop() + v + nums.pop();
            nums.push(eval(exp.replace(/--/g, '+')))
        } else {
            nums.push(+v);
        }
    }
    return nums[0]
}
