// Calculate a pair of closest points
function closestPair(points) {
    var dist = (a, b) => Math.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)
    var arr = []
    for (var i = 0; i < points.length; i++) {
        for (var j = i + 1; j < points.length; j++) {
            var temp = [points[i], points[j], dist(points[i], points[j])]
            arr.push(temp);
        }
    }
    return arr.sort((a, b) => a[2] - b[2])[0].slice(0, 2)
}