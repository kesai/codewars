function findHobbitsIn(manifest) {
    var keyes = ['halfling', 'hobbit', 'shire', 'farthing', 'hobbiton'];
    var reg = /halfling|hobbit|shire|farthing|hobbiton/i;
    return manifest.filter(v => reg.test(v.home + v.race))
}
