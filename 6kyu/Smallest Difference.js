function smallestDiff(arr1, arr2) {
    var res = Infinity;
    if (!arr1.length) return arr2.length ? Math.min(...arr2.map(v => Math.abs(v))) : -1;
    if (!arr2.length) return Math.min(...arr1.map(v => Math.abs(v)));
    for (var i = 0; i < arr1.length; i++) {
        for (var j = 0; j < arr2.length; j++) {
            res = Math.min(res, Math.abs(arr1[i] - arr2[j]));
        }
    }
    return res
}
