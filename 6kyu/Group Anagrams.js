function groupAnagrams(words) {
    var groups = {}
    for (x of words) {
        var sort_word = [...x].sort().join('');
        if (Object.keys(groups).includes(sort_word)) {
            groups[sort_word].push(x);
        } else {
            groups[sort_word] = [x];
        }
    }
    return Object.values(groups)
}