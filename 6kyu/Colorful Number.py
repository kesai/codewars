def colorful(number):
    s = str(number)
    arr = list(map(int, list(s)))
    x = 1
    for i in range(len(s)-1):
        temp = int(s[i])*int(s[i+1])
        x *= int(s[i])
        arr.append(temp)
    x *= int(s[-1])
    if(len(s) > 2):
        arr.append(x)
    return len(set(arr)) == len(arr)
