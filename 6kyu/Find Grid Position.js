function createGrid(m, n, position) {
    var arr = Array.from({ length: m }, (_, i) => Array(n).fill(0));
    if (position.x >= n || position.y >= m) return arr.map(v => v.join('')).join('\n')
    for (var j = 0; j < n; j++)arr[position.y][j] = 1;
    for (var i = 0; i < m; i++)arr[i][position.x] = 1;
    arr[position.y][position.x] = '*';
    return arr.map(v => v.join('')).join('\n');
}