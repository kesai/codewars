class BullsAndCows:
    def __init__(self, n):
        self.check(n)
        self.secret = str(n)
        self.turns = 0
        self.has_won = False

    def check(self, n):
        if not (1234 <= n <= 9876 and len(set(c for c in str(n))) == 4):
            raise ValueError

    def compare_with(self, n):
        if self.has_won:
            return 'You already won!'
        if self.turns >= 8:
            return 'Sorry, you\'re out of turns!'
        if(self.secret == str(n)):
            self.has_won = True
            return 'You win!'
        self.check(n)
        self.turns += 1
        bull, cow = 0, 0
        str_bull, str_cow = 'bulls', 'cows'
        for i, x in enumerate(str(n)):
            if(x in self.secret):
                if(i == self.secret.index(x)):
                    bull += 1
                else:
                    cow += 1
        if(bull == 1):
            str_bull = 'bull'
        if(cow == 1):
            str_cow = 'cow'
        return f'{bull} {str_bull} and {cow} {str_cow}'
