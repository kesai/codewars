function lineUp(commands) {
    var states = ['a', 'b', 'c', 'd'];
    var students = ['a', 'a', 'a', 'a'];
    function turn(x, d) {
        var idx;
        if (d === 'L') {
            idx = states.indexOf(x) - 1;
            if (idx < 0) idx = states.length - 1;
        }
        else if (d === 'R') idx = (states.indexOf(x) + 1) % states.length;
        else if (d === 'A') idx = (states.indexOf(x) + 2) % states.length;
        return states[idx];
    }
    var counter = { 'L': 'R', 'R': 'L', 'A': 'A' };
    var cnt = 0;
    for (cmd of commands) {
        for (var i = 0; i < students.length; i++) {
            if (i === 1) {
                students[i] = turn(students[i], counter[cmd]);
            } else
                students[i] = turn(students[i], cmd);
        }
        if (new Set(students).size === 1) cnt++;
    }
    return cnt;
}