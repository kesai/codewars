function splitWithoutLoss(str, split_p) {
    return str.replace(new RegExp((split_p.replace(/\|/g, '')), 'g'), split_p).split('|').filter(i => i != '')
}