
function get_factors(x) {
    var d = 2;
    factors = [1];
    while (x > 1) {
        if (x % d === 0) {
            if (!factors.includes(d)) factors.push(d);
            x /= d;
        } else d++;
    }

    return factors;
}
function minDistance(n) {
    var factors = get_factors(n);
    if (!factors.includes(n)) factors.push(n);
    var d = Infinity;
    for (var i = 0; i < factors.length - 1; i++)d = Math.min(d, factors[i + 1] - factors[i]);
    return d;
}
console.log(minDistance(19))