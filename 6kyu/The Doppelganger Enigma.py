from unicodedata import normalize
from collections import namedtuple


def create_namedtuple_cls(cls_name, fields):
    class C(namedtuple(cls_name, fields)):
        def __getattr__(self, item):
            key = normalize('NFKC', item)
            return super().__getattribute__(key)
    C.__name__ = cls_name
    return C
