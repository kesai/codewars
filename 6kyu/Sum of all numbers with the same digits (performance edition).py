import math


def sum_arrangements(num):
    n = len(str(num))
    return math.factorial(n-1)*sum(map(int, str(num)))*int('1'*n)
