//https://stackoverflow.com/questions/20833295/how-can-i-match-overlapping-strings-with-regex
function transFactors(seq) {
    m = { "TGACGT": 'ATF6', "TGACGCA": 'CREB', "CACGTG": 'cMyc', "GATT": 'Gata1', "TGCGTG": 'AhR' }
    var pat = /(?=(TGACGT|TGACGCA|CACGTG|GATT|TGCGTG))./g;
    var res = {}
    var match;
    while ((match = pat.exec(seq)) != null) {
        var type = m[match[1]];
        if (!res[type]) {
            res[type] = [match.index + 1];
        } else res[type].push(match.index + 1);
    }
    var result = {}
    for (k of ['ATF6', 'CREB', 'cMyc', 'Gata1', 'AhR']) {
        if (res[k]) result[k] = res[k];
    }
    return result;
}