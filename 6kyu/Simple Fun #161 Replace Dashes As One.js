function replaceDashesAsOne(str) {
    return str.replace(/(?<=-)\s+(?=-)/g, '').replace(/\-+/g, '-');
}