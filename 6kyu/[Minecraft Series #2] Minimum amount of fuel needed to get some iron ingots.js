function calcFuel(n) {
    var total = 11 * n;
    var units = [['lava', 800], ['blazeRod', 120], ['coal', 80], ['wood', 15], ['stick', 1]];
    var res = {}
    for ([k, v] of units) {
        res[k] = Math.floor(total / v);
        total = total % v;
    }
    return res;
};
