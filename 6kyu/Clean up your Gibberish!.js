function cleanItUp(s) {
    s = s.replace(/[^a-z]/gi, '').toLowerCase();
    return (s.match(/.{1,5}/g) || []).map(v => v.length === 5 ? v[0].toUpperCase() + v.slice(1) : '').join(' ').trim()
}