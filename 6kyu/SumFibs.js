function sumFibs(n) {
    var [a, b] = [0, 1];
    if (n < 2) return 0;
    var res = 0;
    for (var i = 1; i <= n; i++) {
        [a, b] = [b, a + b];
        if (a % 2 === 0) res += a;
    }
    return res;
};