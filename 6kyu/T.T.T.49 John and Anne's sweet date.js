function sweetDate(w1, r1, w2, r2, timePeriod) {
    var s1 = ''.padEnd(timePeriod, '1'.repeat(w1) + '0'.repeat(r1));
    var s2 = ''.padEnd(timePeriod, '1'.repeat(w2) + '0'.repeat(r2));
    return [...s1].filter((v, i) => v === s2[i] && v === '0').length
}