function riemann_trapezoidal(f, n, a, b) {
    var sum = 0;
    var dx = (b - a) / n;
    for (var i = 0; i < n; i++) {
        x1 = dx * i + a; x2 = x1 + dx;
        sum += (f(x1) + f(x2)) * dx / 2
    }
    return Math.round(sum * 100) / 100;
}