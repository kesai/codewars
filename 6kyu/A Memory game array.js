function createTiles(n) {
    if (n % 2) return []
    var numbers = Array.from({ length: n / 2 }, (_, i) => i + 1);
    numbers = numbers.concat(numbers);
    var tiles = []
    for (var i = 0; i < n; i++) {
        var idx = (Math.random() * (numbers.length - 1)) | 0;
        tiles.push(numbers[idx]);
        numbers.splice(idx, 1);
    }
    return tiles;
}