function Allergies(score) {
    if (!Number.isInteger(score) || score < 0)
        throw TypeError('invalid score')
    var scores = ['eggs', 'peanuts', 'shellfish', 'strawberries', 'tomatoes', 'chocolate', 'pollen', 'cats']
    this.list = [...score.toString(2)].reverse().map((v, i) => +v ? scores[i] : '').filter(v => v != '' && v != undefined)
    this.isAllergicTo = function (allergen) {
        return this.list.includes(allergen)
    }

    this.allergies = function () {
        return this.list.sort()
    }
}
