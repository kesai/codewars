function countDays(d) {
    var now = new Date();
    now.setHours(0, 0, 0, 0);
    d.setHours(0, 0, 0, 0)
    dateSpan = d - now;
    if (d < now) return "The day is in the past!"
    if (dateSpan === 0) return "Today is the day!"
    return `${Math.floor(dateSpan / (24 * 3600 * 1000))} days`
}
console.log(countDays(new Date("May 29, 2020")))