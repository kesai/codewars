def find_similar_dogs(breed):
    tempers = dogs[breed]
    counts = {}
    for b in dogs:
        if b == breed:
            continue
        counts[b] = len([t for t in tempers if t in dogs[b]])
    m = max(counts.values())
    return set(k for k, v in counts.items() if v == m)
