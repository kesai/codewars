
function isNarcissistic(...nums) {
    return nums.every(check)
}
function check(n) {
    if (!n && !/^0$/.test(n)) return false;
    var l = ('' + n).length;
    return [...'' + n].reduce((s, v) => s += (+v) ** l, 0) === +n;
}