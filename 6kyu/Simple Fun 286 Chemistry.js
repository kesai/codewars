var gcd = (x = Math.abs(x), y = Math.abs(y)) => {
    while (y) {
        var t = y;
        y = x % y;
        x = t;
    }
    return x;
}

function chemistry(first, second) {
    first = first.match(/\w+\(\d+\)/g);
    second = second.match(/\w+\(\d+\)/g);
    var simpify = (x) => {
        var [e1, e2] = x.match(/\w+\(\d+\)/g);
        n1 = +(e1.match(/\(\d+\)/)[0].match(/\d+/)[0])
        e1 = e1.replace(/\(\d+\)/, '')
        n2 = +(e2.match(/\(\d+\)/)[0].match(/\d+/)[0])
        e2 = e2.replace(/\(\d+\)/, '');
        var d = gcd(n1, n2);
        return `${e1}(${n1 / d})${e2}(${n2 / d})`

    }
    return [simpify(second[0] + first[1]), simpify(first[0] + second[1])]

}
