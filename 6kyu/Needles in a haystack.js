function search(haystack, needle, parent = '') {
    var res = []
    for (k in haystack) {
        var key = parent ? parent + '.' + k : k;
        if (typeof (haystack[k]) === 'string') {
            if (haystack[k].includes(needle))
                res.push(key);
        } else {
            res = res.concat(search(haystack[k], needle, key))
        }
    }
    return res.sort();
}