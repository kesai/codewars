function stringConstructing(a, s) {
    var x = a.replace(/./g, (v) => `${v}?`)
    var arr = s.match(new RegExp(x, 'g')).slice(0, -1);
    return arr.length + arr.reduce((s, v) => s += a.length - v.length, 0)
}