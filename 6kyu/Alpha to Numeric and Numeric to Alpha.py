import re


def AlphaNum_NumAlpha(string):
    s = 'abcdefghijklmnopqrstuvwxyz'
    return re.sub(r'\d+|[a-z]', lambda x: s[int(x.group())-1] if re.fullmatch(r'\d+', x.group()) else str(s.index(x.group())+1), string)


print(chr(10))
