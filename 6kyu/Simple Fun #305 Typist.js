function typist(s) {
    var caps = false;
    var n = 0
    for (c of s) {
        if ((/[a-z]/.test(c) && caps) || (/[A-Z]/.test(c) && !caps)) { caps = !caps; n += 2; }
        else n++;
    }
    return n;
}