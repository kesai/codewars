function sumOfThrees(n) {
    var s = n.toString(3);
    return /2/.test(s) ? 'Impossible' : s.split('').map((v, i) => v === '1' ? `3^${s.length - i - 1}` : '').filter(v => v != '').join('+')
}
sumOfThrees(84)