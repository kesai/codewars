function check(board) {
    var q_row = board.findIndex(v => v.includes('q'));
    var q_col = board[q_row].indexOf('q');
    //check by row
    if (board[q_row].includes('k')) return true;
    //check by col
    if (board.map(v => v[q_col]).includes('k')) return true;

    //check diagonals
    var moves = [[-1, -1], [-1, 1], [1, -1], [1, 1]];
    for (mov of moves) {
        var [temp_row, temp_col] = [q_row, q_col]
        while (temp_col < 6 && temp_col > -1 && temp_row > -1 && temp_row < 6) {
            temp_row += mov[0];
            temp_col += mov[1];
            if (board[temp_row] && board[temp_row][temp_col] === 'k') return true;
        }
    }
    return false;

}