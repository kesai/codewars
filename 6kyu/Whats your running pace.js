function runningPace(distance, time) {
    var [m, s] = time.split(':').map(v => +v);
    var t = m * 60 + s;
    var pace = t / distance;
    var [min, sec] = [Math.floor(pace / 60), Math.floor(pace % 60)].map(v => '' + v);
    return `${min}:${sec.padStart(2, '0')}`
}