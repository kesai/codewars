function triangularSum(n) {
    for (var i = 1; i * (i + 1) < n; i++) {
        var t1 = i * (i + 1) / 2;
        var t2 = (i + 1) * (i + 2) / 2;
        if (t1 * t1 + t2 * t2 === n) return true;
    }
    return false;
}
