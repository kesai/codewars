function solveIt(excel, n) {
    var sorted = [].concat(excel).sort((a, b) => a - b);
    var x = excel[n];
    return sorted.indexOf(x) + 1 + (excel.slice(0, n).filter(v => v === x) || []).length
}