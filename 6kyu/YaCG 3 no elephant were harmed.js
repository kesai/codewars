function winnerOfTrick(cards, players, trump) {
    var s = '23456789TJQKA';
    var trumps = cards.filter(v => v.includes(trump));
    var suit = trumps.length ? trump : cards[0][1];
    var max = Math.max(...cards.filter(v => v.includes(suit)).map(v => s.indexOf(v[0])));
    var win_idx = cards.indexOf(`${s[max]}${suit}`);
    return `${players[win_idx]} wins`;
}