function ageAndZodiac(born, sex) {
    var zodiacs = ['猴', '鸡', '狗', '猪', '鼠', '牛', '虎', '兔', '龙', '蛇', '马', '羊'];
    var Titles = {
        'man': {
            0: '襁褓之中', 2: '孩提之年', 4: '黄口小儿', 8: '龆年', 10: '总角之年', 13: '舞勺之年',
            15: '舞象之年', 20: '弱冠之年', 30: '而立之年', 40: '不惑之年', 50: '知命之年', 60: '花甲之年',
            70: '古稀之年', 80: '耄耋之年', 90: '鲐背之年', 100: '期颐之年'
        }, 'women': {
            0: '襁褓之中', 2: '孩提之年', 4: '黄口小儿', 7: '髫年', 10: '总角之年', 12: '金钗之年', 13: '豆蔻年华',
            15: '及笄之年', 16: '碧玉年华', 20: '桃李年华', 24: '花信年华', 30: '半老徐娘', 40: '不惑之年',
            50: '知命之年', 60: '花甲之年', 70: '古稀之年', 80: '耄耋之年', 90: '鲐背之年', 100: '期颐之年'
        }
    }
    var age = new Date().getFullYear() - born;
    if (age < 0 || age > 150) return '无效的年份'
    var zodiac = zodiacs[born % 12];
    var titles = Titles[sex];
    var title = titles[age];
    if (!title) {
        while (!titles[age]) age--;
        return `你已逾${titles[age]}，生肖属${zodiac}`
    } else {
        return `你正值${title}，生肖属${zodiac}`
    }
}
