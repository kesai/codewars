function noOrder(s) {
    s = s.replace(/\s/g, '');
    var items = s.match(/\d+|[^\d]/g);
    var d = null;
    var op = null;
    for (x of items) {
        if (/\d+/.test(x)) { d = d === null ? +x : Math.floor(eval(`${d}${op}${x}`)); }
        else { op = x === '^' ? '**' : x; }
    }
    return Number.isNaN(d) || d === Infinity ? null : d;
}

