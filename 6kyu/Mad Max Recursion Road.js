function max(arr) {
    if (arr.length === 0) return -Infinity;
    if (arr.length === 1) return arr[0];
    if (arr.length === 2) return arr[0] >= arr[1] ? arr[0] : arr[1];
    temp = arr.slice(2);
    temp.push(max(arr.slice(0, 2)));
    return max(temp);
}
