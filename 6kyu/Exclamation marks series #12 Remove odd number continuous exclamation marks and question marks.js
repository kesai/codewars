function remove(s) {
    debug = 0
    while (s && (s.match(/!{3,}|\?{3,}/g) || []).some(v => v.length % 2)) {
        s = s.replace(/!{3,}|\?{3,}/g, v => v.length % 2 ? '' : v);
        console.log(s)
        if (debug++ > 10) break;
    }
    return s
}
