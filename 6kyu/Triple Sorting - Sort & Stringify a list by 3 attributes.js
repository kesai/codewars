function sort(students) {
    var x = students.sort((a, b) => b.gpa - a.gpa || a.fullName.split(' ')[1][0].localeCompare(b.fullName.split(' ')[1][0]) || a.age - b.age)
    return x.map(v => v.fullName).join(',')
};