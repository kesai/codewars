function segmentSumsMatrix(arr) {
    var out = [];
    for (var i = 0; i < arr.length; i++) {
        var row = []
        for (var j = 0; j < arr.length; j++) {
            var temp = arr.slice(Math.min(i, j), Math.max(i, j) + 1).reduce((s, v) => s += v, 0)
            row.push(temp)
        }
        out.push(row)
    }
    return out;
}