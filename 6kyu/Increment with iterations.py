def increment(number, iterations, spacer):
    p = 0
    for i in range(iterations):
        s = str(number)
        p = (p+spacer) % len(s)
        number += 10**(len(s)-p-1)
        if len(str(number)) > len(s):
            p += 1
    return number
