class Automaton(object):
    def __init__(self):
        self.state = 'q1'

    def read_commands(self, commands):
        states = {'q1': {'1': 'q2', '0': 'q1'}, 'q2': {
            '0': 'q3', '1': 'q2'}, 'q3': {'0': 'q2', '1': 'q2'}}
        for cmd in commands:
            self.state = states[self.state][cmd]
        return state == 'q2'


my_automaton = Automaton()
