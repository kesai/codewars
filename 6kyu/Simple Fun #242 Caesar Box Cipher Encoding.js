function caesarBoxCipherEncoding(message) {
    var l = message.length;
    var count = 0;
    for (var i = 2; i < l; i++) {
        if (l % i === 0) {
            if (check(i, message)) count++;
        }
    }
    return count;
}
function encode(n, message) {
    var arr = message.match(new RegExp(`.{${n}}`, 'g'));
    return [...arr[0]].map((e, i) => arr.map(row => row[i]).join('')).join('')
}
function check(n, message) {
    var code = encode(n, message);
    return encode(n, code) === message;
}
