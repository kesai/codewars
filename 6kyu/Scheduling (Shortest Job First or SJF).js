function SJF(jobs, index) {
    jobs = jobs.map((v, i) => [v, i]).sort((a, b) => a[0] - b[0] || a[1] - b[1]);
    var sum = 0;
    for (x of jobs) {
        sum += x[0];
        if (x[1] === index) break;
    }
    return sum
}

console.log(SJF([3, 10, 20, 1, 2], 0))