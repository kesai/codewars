function buyTofu(cost, box) {
    var items = box.split(' ');
    var mon = (items.filter(v => v === 'mon') || []).length;
    var monme = (items.filter(v => v === 'monme') || []).length;
    var total = mon + 60 * monme;
    if (total < cost) return 'leaving the market';
    var least_monme = parseInt(cost / 60)
    if (least_monme >= monme) least_monme = monme;
    if (cost - least_monme * 60 > mon) return 'leaving the market';
    var min_count = cost - 59 * least_monme;
    if (min_count < 0 || min_count > mon + monme) return 'leaving the market';
    return [mon, monme, total, min_count];
}
