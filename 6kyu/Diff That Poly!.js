function diff(poly) {
    var out = []
    for (var i = 0; i < poly.length - 1; i++) {
        var power = poly.length - i - 1;
        var k = poly[i];
        out.push(k * power);
    }
    return out;
}