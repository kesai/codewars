def invalid(x):
    return x is None or type(x) is not int or x < 0


def xp_to_target_lvl(current_xp=None, target_lvl=None):
    MAX_LEVEL = 170
    level_xp, p = 314, 25
    need_xp = 0
    if invalid(current_xp) or invalid(target_lvl) or target_lvl > MAX_LEVEL or target_lvl < 1:
        return 'Input is invalid.'
    for i in range(1, target_lvl):
        if i % 10 == '0':
            p -= 1
        level_xp = int(level_xp*(p+100)/100) if i > 1 else level_xp
        need_xp += level_xp
    return f'You have already reached level {target_lvl}.' if current_xp >= need_xp else need_xp-current_xp
