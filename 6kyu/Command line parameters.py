def args_to_string(args):
    out = []
    for x in args:
        if type(x) is str:
            out.append(x)
        else:
            if len(x) == 1:
                out.append(x[0])
            else:
                out.append(f'{"--" if len(x[0])>1 else "-"}{x[0]} {x[1]}')

    return ' '.join(out)
