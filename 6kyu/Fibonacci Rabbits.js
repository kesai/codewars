function fib_rabbits(n, b) {
    immature = 1;
    adult = 0;
    for (var i = 0; i < n; i++) {
        var temp = immature;
        immature = adult * b;
        adult += temp;
    }
    return adult;
}