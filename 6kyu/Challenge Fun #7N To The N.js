function String_Mul(a, b) {
    var res = Array(a.length + b.length).fill(0);
    for (var i = a.length - 1; i > -1; i--) {
        var add = 0;
        for (var j = b.length - 1; j > -1; j--) {
            var mul = +a[i] * +b[j];
            var sum = res[i + j + 1] + add + mul % 10
            res[i + j + 1] = sum % 10;
            add = Math.floor(mul / 10) + Math.floor(sum / 10);
        }
        res[i] += add;
    }
    return res.join('').replace(/^0*/g, '')
}

function n2n(n, k) {
    var x = '1';
    for (var i = 0; i < k; i++)x = String_Mul(x, '' + n);
    console.log(x)
    return x.slice(-k);
}
console.log(n2n(9, 9))