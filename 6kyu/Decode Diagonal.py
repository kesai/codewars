def get_diagonale_code(grid: str) -> str:
    grids = grid.replace(' ', '').split('\n')
    i, j, is_down, res = 0, 0, True, ''
    while j < len(grids[i]):
        res += grids[i][j]
        i, j = i+(1 if is_down else -1), j+1
        if i == len(grids):
            is_down = False
            i = len(grids)-2
        elif i == -1:
            i = 1
            is_down = True
    return res
