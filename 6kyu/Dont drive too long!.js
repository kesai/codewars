function shouldIBeTired(dailyShedule) {
    drive_time = dailyShedule.filter(v => v[1] === 'Drive').map(v => {
        var [st, end] = v[0].split('-');
        st = +st.split(':')[0] * 60 + +(st.split(':')[1] || 0);
        end = +end.split(':')[0] * 60 + +(end.split(':')[1] || 0)
        return end - st;
    }).reduce((s, v) => s += v, 0);
    return drive_time > 9 * 60;
}
