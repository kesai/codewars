function factorizedGCD(a, b) {
    var res = 1;
    for (x of a) {
        if (b.includes(x)) {
            res *= x;
            b.splice(b.indexOf(x), 1);
        }
    }
    return res;
}
