function leftRightOrBoth(text) {
    text = text.replace(/\s/g, '');
    if (!text) return ''
    if (/^[1-5qwertasdfgzxcvb~`!@#$%]+$/i.test(text)) return 'Left';
    if (/^[06-9yuiophjklnm,.<>?/:;"'{}\[\\+=_\-\^&*()\]]+$/i.test(text)) return 'Right';
    return 'Both';
}