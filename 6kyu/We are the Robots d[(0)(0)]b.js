function countRobots(a) {
    var reg = /[a-z][|};&#[\]\/><()*]{2}0[|};&#[\]\/><()*]{2}0[|};&#[\]\/><()*]{2}[a-z]/gi;
    var x = y = 0;
    for (s of a) {
        var n = (s.match(reg) || []).length;
        if (/\bautomatik\b/i.test(s)) x += n;
        else if (/\bmechanik\b/i.test(s)) y += n;
    }
    return [`${x || 0} robots functioning automatik`, `${y || 0} robots dancing mechanik`]
}