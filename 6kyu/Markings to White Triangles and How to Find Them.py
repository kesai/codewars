def how_to_find_them(t):
    a = t.get('a', 0)
    b = t.get('b', 0)
    c = t.get('c', 0)
    if not a:
        a = (c*c-b*b)**0.5
    elif not b:
        b = (c*c-a*a)**0.5
    else:
        c = (a*a+b*b)**0.5
    return {'a': a, 'b': b, 'c': c}

