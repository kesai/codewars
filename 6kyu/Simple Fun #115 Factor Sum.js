function next(x) {
    var d = 2;
    var res = 0
    while (x > 1) {
        if (x % d === 0) {
            res += d;
            x /= d;
        } else d++;
    }
    return res;
}
function factorSum(n) {
    while (n != (m = next(n))) n = m;
    return n;
}
