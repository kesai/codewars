function chmodCalculator(perm) {
    var m = { 'rwx': '7', 'r-x': '5', 'rw-': '6', '-wx': '3', 'r--': '4', '-w-': '2', '--x': '1', '---': '0' };
    return ['user', 'group', 'other'].reduce((s, v) => s += (k = perm[v]) ? m[k] : '0', '')
}