function minValue(arr, n) {
    var out = []
    for (var i = 0; i < arr.length - n + 1; i++) {
        out.push(Math.min(...arr.slice(i, i + n)))
    }
    return out;
}
console.log(minValue([1, 2, 3, 10, -5], 2))
