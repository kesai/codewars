function minSteps(apples) {
    var sum = apples.reduce((s, v) => s += v, 0);
    if (sum % apples.length) return -1;
    var avg = sum / apples.length;
    var rest = 0, need = 0;
    for (x of apples) {
        if (Math.abs(x - avg) % 2) return -1;
        if (x > avg) rest += x - avg; else need += avg - x;
    }
    if (rest != need) return -1;
    return rest / 2;
}