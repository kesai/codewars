function trickOrTreat(children, candies) {
    var candies = candies.map(v =>
        v.includes('bomb') ? 0 : (v.filter(e => e === 'candy') || []).length);
    candies = candies.filter(v => v >= 2);
    if (candies.length < children || new Set(candies).size > 1) return "Trick or treat!";
    return 'Thank you, strange uncle!'
}