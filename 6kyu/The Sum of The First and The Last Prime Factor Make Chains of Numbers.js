function get_sflpf(n) {
    var d = 2;
    var res = []
    while (n > 1) if (n % d === 0) { n /= d; res.push(d); } else d++;
    return res.length > 1 ? (res[0] + res.slice(-1)[0]) : 0;
}
function sflpfData(k, nMax) {
    var res = [];
    for (var i = 1; i <= nMax; i++) {
        if (get_sflpf(i) === k) res.push(i);
    }
    return res;
}