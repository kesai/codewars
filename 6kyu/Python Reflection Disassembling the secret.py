import re
import dis


def find_the_secret(f):
    s = dis.Bytecode(f).dis()
    return re.search(r'\'(.*)\'', s).groups()[0]
