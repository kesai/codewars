var states = {
    'AZ': 'Arizona',
    'CA': 'California',
    'ID': 'Idaho',
    'IN': 'Indiana',
    'MA': 'Massachusetts',
    'OK': 'Oklahoma',
    'PA': 'Pennsylvania',
    'VA': 'Virginia'
}
function byState(str) {
    var stastics = {}
    for (v of str.split('\n')) {
        if (!v) continue;
        var [name, street, city_state] = v.split(', ');
        var state = states[city_state.match(/[A-Z]{2}$/)[0]];
        var address = `${street} ${city_state.replace(/[A-Z]{2}$/, state)}`
        if (!stastics[state]) stastics[state] = [];
        //stastics[state].push(new Object({ name, address }));
        stastics[state].push(`..... ${name} ${address}`);
    }
    var out = []
    for (st of Object.keys(stastics).sort()) {
        var temp = []
        temp.push(st);
        temp = temp.concat(stastics[st].sort());
        out.push(temp.join('\r\n'))
    }
    return out.join('\r\n ')
}

var case0 = `Fanny Hem, 200 Station Road, Mountain View CA
Chris Maker, 45 Bridge Port, Richmond ID
Paul Pep, 11354 East Bridge Road, Hill View IN
Ama Zon, 5AA Clear Bd, Plymouth MA
Laurel Tango, 2134 Clint Street, Hill View IN
Paul Cartney, 22 Prin Broadway, Plymouth MA
Chris Maker, 11345 Oak Bridge Road, Hill View IN
Rafa String, 2134 Clint Street, Plymouth MA
Antony None, 420 Land Road, Plymouth MA`;
console.log(byState(case0))
