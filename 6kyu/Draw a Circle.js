function circle(radius) {
    var res = [];
    var x0 = y0 = radius;
    var distance = (x1, y1, x2, y2) => Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
    for (var i = 1; i < 2 * radius; i++) {
        var row = []
        for (var j = 1; j <= 2 * radius; j++) {
            if (distance(i, j, x0, y0) < radius) row.push('█'); else row.push(' ');
        }
        row = row.join('').trim('');
        var d = (2 * radius - 1 - row.length) / 2;
        res.push(' '.repeat(d) + row + ' '.repeat(d))
    }
    return radius > -1 ? res.join('\n') + '\n' : ''
}
