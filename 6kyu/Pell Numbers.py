class Pell(object):
    def __init__(self):
        self.caches = {'0': 0, '1': 1}

    def get(self, n):
        if str(n) not in self.caches.keys():
            self.caches[str(n)] = 2*self.get(n-1)+self.get(n-2)
        return self.caches[str(n)]
