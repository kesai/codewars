repeatingFractions = (n, d) => (n / d).toString().replace(/(?<=\..*)(\d)\1+/g, '($1)');

console.log(repeatingFractions(2, 888))