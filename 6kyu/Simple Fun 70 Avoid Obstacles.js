function avoidObstacles(arr) {
    for (var i = 1; ; i++) {
        if (arr.every(v => v % i)) return i;
    }
}