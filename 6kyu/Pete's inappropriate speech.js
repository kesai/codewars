function peteTalk(speech, ok) {
    ok = ok.map(v => v.toLowerCase());
    return speech.split(/(?<=[.!?]\s)/g).map(v => sentence(v.toLowerCase(), ok)).join('');
}
function sentence(s, ok = []) {
    s = s.replace(/[a-z]{3,}/g, w => ok.includes(w) ? w : w[0] + '*'.repeat(w.length - 2) + w.slice(-1));
    return s[0].toUpperCase() + s.slice(1);
}
