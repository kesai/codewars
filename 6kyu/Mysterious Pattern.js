function fib(n) {
    var a = 1;
    var b = 1;
    if (n === 1) return [1];
    var res = [a, b];
    for (var i = 2; i < n; i++) {
        var temp = a;
        a = b;
        b += temp;
        res.push(b);
    }
    return res;
}


function mysteriousPattern(m, n) {
    var seqs = fib(m).map(v => v % n);
    return Array.from({ length: n }, (_, i) => seqs.map(v => v === i ? 'o' : ' ').join('').replace(/\s+$/, '')).join('\n').replace(/(^\n+)|(\n+$)/g, '')
}