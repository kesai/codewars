function findDuplicatePhoneNumbers(phoneNumbers) {
    map = { 'A': 2, 'B': 2, 'C': 2, 'D': 3, 'E': 3, 'F': 3, 'G': 4, 'H': 4, 'I': 4, 'J': 5, 'K': 5, 'L': 5, 'M': 6, 'N': 6, 'O': 6, 'P': 7, 'R': 7, 'S': 7, 'T': 8, 'U': 8, 'V': 8, 'W': 9, 'X': 9, 'Y': 9 }
    counts = {}
    for (x of phoneNumbers) {
        s = x.replace(/[a-z]/gi, c => map[c.toUpperCase()]).replace(/-/g, '');
        phone = s.slice(0, 3) + '-' + s.slice(3);
        counts[phone] = counts[phone] ? counts[phone] + 1 : 1;
    }
    return Object.entries(counts).filter(v => v[1] > 1).map(v => v.join(':')).sort()
}

