function sepStr(arr) {
    arr = arr.match(/[a-z]+/gi);
    var n = Math.max(...arr.map(v => v.length));
    return Array.from({ length: n },
        (_, i) => arr.map(v => v[i] || ''))
}