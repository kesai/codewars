def is_leap(n): return n % 4 == 0 and (n % 400 == 0 or n % 100 != 0)
