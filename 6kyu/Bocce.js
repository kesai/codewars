function bocceScore(balls) {
    var jack = balls.pop();
    balls.map(v => v.l = ((v.distance[0] - jack.distance[0]) ** 2 + (v.distance[1] - jack.distance[1]) ** 2) ** 0.5)
    balls.sort((a, b) => a.l - b.l);
    var red = balls.filter(v => v.type === 'red');
    var black = balls.filter(v => v.type === 'black');
    var m_red = Math.min(...red.map(v => v.l));
    var m_black = Math.min(...black.map(v => v.l));
    var n = 0;
    var winner = null;
    if (m_red < m_black) {
        n = red.filter(v => v.l < m_black).length;
        winner = 'red';
    } else if (m_black < m_red) {
        n = black.filter(v => v.l < m_red).length;
        winner = 'black'
    }

    return !winner ? 'No points scored' : `${winner} scores ${n}`
}
console.log(bocceScore([
    { type: "black", distance: [60, 1] },
    { type: "black", distance: [61, 3] },
    { type: "red", distance: [61, 3] },
    { type: "red", distance: [65, -1] },
    { type: "jack", distance: [60, 2] }
]))
