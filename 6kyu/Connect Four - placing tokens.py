def connect_four_place(columns):
    colors = 'YR'
    idx = 0
    grid = [list('-'*7) for i in range(6)]
    for col in columns:
        for i in range(len(grid)-1, -1, -1):
            if grid[i][col] == '-':
                grid[i][col] = colors[idx]
                idx = (idx+1) % 2
                break
    return grid
