def check_grid(grid, pos, size):
    row = grid[pos[1]]
    col = [grid[i][pos[0]] for i in range(size)]
    diff0 = [(1, 1), (-1, -1)]
    diff1 = [(1, -1), (-1, 1)]
    diagnoal0 = [grid[pos[1]][pos[0]]]
    diagnoal1 = [grid[pos[1]][pos[0]]]
    for d in diff0:
        x, y = pos
        while x > -1 and y > -1 and x < size and y < size:
            if x != pos[0] and y != pos[1]:
                diagnoal0.append(grid[y][x])
            x, y = x+d[0], d[1]+y
    for d in diff1:
        x, y = pos
        while x > -1 and y > -1 and x < size and y < size:
            if x != pos[0] and y != pos[1]:
                diagnoal1.append(grid[y][x])
            x, y = x+d[0], d[1]+y
    return [row, col, diagnoal0, diagnoal1]


grid = [[1,  2,  3,  4,  5],
        [6,  7,  8,  9,  0],
        [11, 12, 13, 14, 15],
        [16, 17, 18, 19, 20],
        [21, 22, 23, 24, 25]]
g_pos = [2, 3]
g_size = 5

print(check_grid(grid, g_pos, g_size))
