function oneCharDifference(s1, s2) {
    if (s1 === s2) return false;
    var only_one = false;
    var [a, b] = [s1, s2].sort((a, b) => b.length - a.length);
    if (a.length === b.length) {
        for (var i = 0; i < a.length; i++) {
            var sub_a = a.slice(0, i) + a.slice(i + 1);
            var sub_b = b.slice(0, i) + b.slice(i + 1);
            //edited check
            if (sub_a === sub_b) { only_one = true; break; }
        }

    } else if (a.length - b.length === 1) {
        for (var i = 0; i < a.length; i++) {
            var sub = a.slice(0, i) + a.slice(i + 1);
            //added or removed check
            if (sub === b) { only_one = true; break; }
        }
    }
    return only_one;
}

console.log(oneCharDifference('grant', 'grant'))