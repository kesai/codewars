// dictionary of symbols for short and long syllables.
const syllables = {
    short: [".", "*", "x"],
    long: ["/", "_"]
}
// return the type of meter
const identifyMeter = str => {
    if (/^[.*x][/_]$/.test(str)) return 'iamb';
    if (/^[/_][.*x]$/.test(str)) return 'trochee'
    if (/^[/_]{2}$/.test(str)) return 'spondee'
    if (/^[.*x]{2}$/.test(str)) return 'pyrrhic';
    if (/[^.*x/_]/.test(str)) return 'What is this?'
    return 'Not a dissyllable.'
}