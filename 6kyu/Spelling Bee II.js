const howManyBees = h => {
    const f = a => (a.join``.match(/(?=bee|eeb)/g) || []).length; //bee check function

    if (!h || !h.length) return 0

    const diagonals = [];     // calculating diagonals
    const size = Math.max(h.length, h[0].length);
    for (let y = 0; y < size; y++) {
        let a = [], b = [], c = [], d = [];
        for (let x = 0; x < size; x++) {
            a.push((h[y + x] || [])[x] || '-');
            b.push((h[y + x] || [])[h[0].length - x - 1] || '-');
            c.push((h[x] || [])[x + 1 + y] || '-');
            d.push((h[x] || [])[h[0].length - x - 2 - y] || '-');
        }
        diagonals.push(a, b, c, d);
    }
    return h.concat(h[0].map((_, y) => h.map(r => r[y]))).concat(diagonals).map(e => f(e)).reduce((a, b) => a + b, 0)
}