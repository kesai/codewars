
function countTriangles(chars, n, m) {
    //learves
    var s = chars.padEnd((1 + n) * n / 2, chars);
    var a = 0;
    var leaves = []
    for (var i = 1; i <= n; i++) {
        var temp = '*'.repeat(n - i) + [...s.slice(a, a + i)].join('*') + '*'.repeat(n - i);
        leaves.push([...temp].map(Number));
        a += i;
    }
    console.log(leaves.join('\n'))
    var cnt = 0;
    for (var i = 0; i < leaves.length - 1; i++) {
        for (var j = 1; j < leaves[0].length - 1; j++) {
            if (leaves[i][j] + leaves[i + 1][j - 1] + leaves[i + 1][j + 1] === m) {
                console.log(i, j, i + 1, j - 1, i + 1, j + 1)
                cnt++;
            }
            if (leaves[i + 1][j] + leaves[i][j - 1] + leaves[i][j + 1] === m) {
                console.log(i + 1, j, i, j - 1, i, j + 1)
                cnt++;
            }
        }
    }
    return cnt;
}