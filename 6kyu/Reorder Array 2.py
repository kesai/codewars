def reorder(a, b):
    b = a//2-b % (a//2)
    return [v[b:]+v[0:b] for v in [[i for i in range(a//2)],
                                   [a//2+i for i in range(a//2)]]]
