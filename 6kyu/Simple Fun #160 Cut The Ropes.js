function cutTheRopes(a) {
    var res = [];
    while (a.length) {
        var m = Math.min(...a);
        a = a.map(v => v - m).filter(v => v != 0);
        res.push(a.length);
    }
    return res;
}