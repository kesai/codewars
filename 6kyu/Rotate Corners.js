
function rotateCorners(matrix) {
    var f = x => typeof (x) === 'string' ? x.charCodeAt(0) : +x;
    var row = matrix.length;
    var col = matrix[0].length;
    var res = [[matrix[0][0], matrix[0][col - 1]], [matrix[row - 1][0], matrix[row - 1][col - 1]]];
    var corner_sum = res.reduce((s, v) => s += v.reduce((sum, e) => sum += f(e), 0), 0);
    var sum = matrix.reduce((s, v) => s += v.reduce((sum, e) => sum += f(e), 0), 0);
    var times = ((sum - corner_sum) * corner_sum) % 4;
    for (var i = 0; i < times; i++)res = res[0].map((e, i) => res.map(v => v[i]).reverse());
    return res;
}
