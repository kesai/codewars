function redAlert(n) {
    n = n % 2040;
    var colors = ['#000000'];
    var r = 0;
    var k = 1;
    for (var i = 0; i < 2040; i++) {
        if (i % 4 === 0) r += k;
        if (r === 256) { k = -1; r -= 2; }
        //console.log(r, r.toString(16).padStart(2, '0'))
        var x = `#${r.toString(16).padStart(2, '0').toUpperCase()}0000`;
        //console.log(i, x)
        colors.push(x)
    }
    return colors[n]
}
