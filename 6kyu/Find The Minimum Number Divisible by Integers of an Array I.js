const gcd = (a, b) => b ? gcd(b, a % b) : a;
const lcm = (a, b) => a * b / gcd(a, b);
function minSpecialMult(arr) {
    var x = 1;
    var err = [];
    for (i of arr) {
        if (!i) continue;
        else if (/^\-?\d+$/.test(i)) x = lcm(x, Math.abs(+i));
        else err.push(i);
    }
    var f = (err) => err.length > 1 ? '[' + err.map(v => `'${v}'`).join(', ') + ']' : err[0]
    return err.length ? `There ${err.length > 1 ? 'are' : 'is'} ${err.length} invalid ${err.length > 1 ? 'entries' : 'entry'}: ${f(err)}` : x;
}
