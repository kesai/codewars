def isPrime(num):
    if num < 2:
        return False
    if num == 2 or num == 3:
        return True
    if num % 6 != 1 and num % 6 != 5:
        return False
    n = int(num**0.5)
    for i in range(5, n+1, 6):
        if num % i == 0 or num % (i + 2) == 0:
            return False
    return True


def goldbach_partitions(n):
    if n % 2:
        return []
    partitions = []
    for i in range(2, n//2+1):
        if isPrime(i) and isPrime(n-i):
            partitions.append(f'{i}+{n-i}')
    return partitions
