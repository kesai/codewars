function vodkaConsumption(shots) {
    shots = shots.map(v => {
        var [_, d, unit] = v.match(/(\d+(?:\.\d+)?)(ml|l)/);
        return unit === 'l' ? +d * 1000 : +d;
    });
    return shots.reduce((s, v) => s += v, 0) + 'ml';
}
