function sumtheDifference(equation) {
    return (equation.replace(/ /g, '').match(/-?\d/g) || []).reduce((s, v) => s += (+v % 2 === 0 ? +v : - +v), 0);
}