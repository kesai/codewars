import re


def encode(inp):
    final = re.sub(r'(.)\1*', lambda x: f'{x[0][0]}{len(x[0])}', inp)
    return final
