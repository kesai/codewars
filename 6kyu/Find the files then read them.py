import os


def create_file_dict():
    path = os.getcwd()
    res = {}
    for name in os.listdir(path):
        if os.path.isfile(name):
            with open(name, encoding='utf-8') as f:
                res[name] = f.readline()
                f.close()
    return res
