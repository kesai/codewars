var shakeTree = function (tree) {
    var out = tree[0].map(v => v === 'o' ? 1 : 0);
    for (var i = 1; i < tree.length; i++) {
        for (var j = 0; j < tree[0].length; j++) {
            if (tree[i][j] === '_') {
                out[j] = 0;
            } else if (tree[i][j] === '/') {
                if (j - 1 > -1) out[j - 1] += out[j];
                out[j] = 0;
            } else if (tree[i][j] === '\\') {
                if (j + 1 < tree[0].length) out[j + 1] += out[j];
                out[j] = 0;
            }
        }
    }
    return out;
}

function _makeTree(tree) {
    var ary = []
    for (var y = 0; y < tree.length; y++) {
        var row = []
        for (var x = 0; x < tree[y].length; x++) {
            row.push(tree[y][x])
        }
        ary.push(row)
    }
    return ary;
}
var tree = _makeTree([
    " o o o  ",
    " /    / ",
    "   /    ",
    "  /  /  ",
    "   ||   ",
    "   ||   ",
    "   ||   "
]);
shakeTree(tree)
