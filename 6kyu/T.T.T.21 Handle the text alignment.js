function formatScoreList(list) {
    var avg_title = 'The average score';
    var name_len = Math.max(avg_title.length, Math.max(...list.map(v => v.name.length)));
    var avg = (list.reduce((s, v) => s += v.score, 0) / list.length).toFixed(1);
    var score_len = Math.max(...[`${avg} pts`.length, Math.max(...list.map(v => `${v.score} pts`.length)), 7]);
    var split_line = '+' + '-'.repeat(name_len) + '+' + '-'.repeat(score_len) + '+';
    var temp = Math.floor(score_len / 2) - 3;
    if (temp === 0) temp = 1;
    var title = '|' + ' '.repeat(Math.floor(name_len / 2) - 2) + 'Name' + ' '.repeat(Math.floor(name_len / 2) + name_len % 2 - 2)
        + '|' + ' '.repeat(temp) + 'Score' + ' '.repeat(Math.floor(score_len / 2) - 2) + '|';
    var table = []
    table.push(split_line, title, split_line);
    list.push({ name: avg_title, score: avg })
    for (x of list) {
        var name = `${x.name.padEnd(name_len, ' ')}`
        var score = x.score + ' '.repeat(score_len - x.score.toString().length - 3) + 'pts';
        table.push(`|${name}|${score}|`, split_line)
    }
    return table.join('\n')
}