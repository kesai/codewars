import heapq


def comb(fruits):
    heap = list(fruits)
    heapq.heapify(heap)
    res = 0
    while len(heap) > 1:
        x = heapq.heappop(heap)+heapq.heappop(heap)
        res += x
        heapq.heappush(heap, x)
    return res
