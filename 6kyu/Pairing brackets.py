def bracket_pairs(string):
    stack = []
    result = dict()
    for i, v in enumerate(string):
        if v == '(':
            stack.append(('(', i))
        elif v == ')':
            if not stack:
                return False
            result[stack.pop()[1]] = i
    return False if stack else result
