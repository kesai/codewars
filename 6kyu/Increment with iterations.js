function increment(number, iterations, step) {
    for (var i = 0, p = 0; i < iterations; i++) {
        var s = number.toString();
        p = (p + step) % s.length;
        number += 10 ** (s.length - p - 1);
        if (number.toString().length > s.length) p++;
    }
    return number;
}