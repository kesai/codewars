def find_discounted(prices):
    if not prices:
        return ''
    prices = sorted(map(int, prices.split(' ')))
    out = []
    while(len(prices)):
        temp = prices.pop(0)
        org = int(temp*4/3)
        prices.remove(org)
        out.append(str(temp))
    return ' '.join(out)
