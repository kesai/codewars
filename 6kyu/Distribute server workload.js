function distribute(nodes, workload) {
    var counts = Array.from({ length: nodes }, (_, i) => 0);
    var i = 0;
    while (workload > 0) {
        counts[i]++;
        workload--;
        i = (i + 1) % nodes
    }
    var servers = [];
    var i = 0;
    for (x of counts) {
        var server = Array.from({ length: x }, (_, j) => j + i)
        servers.push(server);
        i = server[x - 1] + 1;
    }
    return servers;
}
distribute(4, 10)