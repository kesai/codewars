function userNumber(n) {
    var s = '01235678';
    var result = ''
    while (n >= 8) {
        d = n % 8;
        n = Math.floor(n / 8);
        result = s[d] + result;
    }
    return (n > 0 ? s[n] : '') + result;
}