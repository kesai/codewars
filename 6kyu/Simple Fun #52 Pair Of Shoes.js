function pairOfShoes(shoes) {
    var left = {};
    var right = {};
    for (x of shoes) {
        if (x[0]) right[x[1]] = right[x[1]] ? right[x[1]] + 1 : 1;
        else left[x[1]] = left[x[1]] ? left[x[1]] + 1 : 1;
    }
    if (Object.keys(left).length!=Object.keys(right).length) return false
    for (k of Object.keys(left)) {
        if (left[k] != right[k]) return false;
    }
    return true
}