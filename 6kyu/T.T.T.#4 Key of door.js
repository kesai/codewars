function findKey(nums) {
    return [...'' + nums[0]].map((v, i) => nums.map(v => ('' + v)[i]).sort().join('').replace(/(\d)\1+/, '')).join('')
}
findKey([153456, 123406, 124456, 323456, 123458, 123756])