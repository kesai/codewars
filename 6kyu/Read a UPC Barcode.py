import re


def read_barcode(barcode):
    barcode = barcode.replace('▍', '1').replace(' ', '0')
    left, right = re.search(
        r'^101((?:[10]{7}){6})01010((?:[10]{7}){6})101$', barcode).groups()
    left = re.sub(r'\d{7}', lambda x: str(LEFT_HAND[x[0]]), left)
    right = re.sub(r'\d{7}', lambda x: str(RIGHT_HAND[x[0]]), right)
    return f'{left[0]} {left[1:]} {right[0:5]} {right[-1]}'
