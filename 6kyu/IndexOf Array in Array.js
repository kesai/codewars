var searchArray = function (arrayToSearch, query) {
    if (!arrayToSearch.every(v => Array.isArray(v) && v.length === 2) || !Array.isArray(query) || query.length != 2)
        throw '出错啦'
    return arrayToSearch.findIndex(v => v.join(',') === query.join(','))
}