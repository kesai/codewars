import re


def domino_reaction(s):
    return re.sub(r'^\|+(?= |\/)?', lambda x: '/'*len(x[0]), s)
