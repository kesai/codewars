var dis = (a, b) => Math.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)
function goldMineRace(cities, speed, goldmine) {
    var times = speed.map((v, i) => dis(cities[i], goldmine) / v);
    var x = [...new Set(times)].sort((a, b) => a - b);
    for (t of x) {
        if (times.filter(v => v === t).length === 1)
            return times.indexOf(t);
    }
    return -1;
}