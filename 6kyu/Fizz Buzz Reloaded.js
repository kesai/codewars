function fizzBuzzReloaded(start, stop, step, funs) {
    var arr = Array.from({ length: Math.abs(Math.floor((stop - start) / step)) + 1 }, (_, i) => i * step + start);
    var res = []
    for (i of arr) {
        var s = ''
        funs.forEach((v, k, map) => s += v(i) ? k : '');
        res.push(s ? s : i)
    }
    return res.join(' ')
}