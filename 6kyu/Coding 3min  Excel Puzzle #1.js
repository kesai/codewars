function solveIt(excel) {
    var row_idx = -1, col_idx = -1;
    for (var i = 0; i < excel.length; i++) {
        var row = excel[i];
        if (row.slice(0, -1).reduce((s, v) => s += v, 0) != row[row.length - 1]) { row_idx = i; break; }
    }

    for (var j = 0; j < excel[0].length - 1; j++) {
        var column = excel.map(v => v[j]);
        if (column.slice(0, -1).reduce((s, v) => s += v) != column[column.length - 1]) { col_idx = j; break; }
    }
    console.log(row_idx, col_idx)
    if (row_idx != -1 && col_idx != -1) {
        var row = excel[row_idx];
        var sum = 0;
        for (var i = 0; i < row.length - 1; i++) {
            if (i != col_idx) sum += row[i];
        }
        return row[row.length - 1] - sum;
    }
    if (row_idx != -1) return excel[row_idx].slice(0, -1).reduce((s, v) => s += v, 0);
    if (col_idx != -1) return excel.map(v => v[col_idx]).slice(0, -1).reduce((s, v) => s += v, 0);
}