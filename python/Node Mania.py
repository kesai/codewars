def search_k_from_end(linked_list, k):
    arr=[]
    head=linked_list.head
    while head:
    	arr.append(head.data)
    	head=head.next
    return arr[-k] if k<len(arr) else  None

