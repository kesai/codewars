def directions(goal):
    m = {'N': 'S', 'S': 'N', 'E': 'W', 'W': 'E'}
    sequence = 'NESW'
    arr = []
    for d in goal:
        if m[d] in arr:
            arr.reverse()
            arr.remove(m[d])
            arr.reverse()
        else:
            arr.append(d)
    arr.sort(key=lambda x: sequence.index(x))
    return arr


goal =["N","N","N","N","N","E","N","N"]
directions(goal)
