def get_factors(n):
    d = 2
    factors = []
    while(n > 1):
        if n % d == 0:
            factors.append(d)
            n /= d
        else:
            d += 1
    return factors


def get_products(n):
    return [i for i in range(2, n) if n % i == 0]


def f(n, exclude=None):
    products = get_products(n)
    print(products)
    if(len(products) == 0):
        return n
    else:
        out = []
        for prod in products:
            sub = f(prod)
            if(type(sub) == list):
                for x in sub:
                    x.append(n//prod)
                    x.sort()
                    if(x not in out):
                        out.append(x)
            else:
                temp = sorted([sub, n//prod])
                if(temp not in out):
                    out.append(temp)
                if([n] not in out and n != exclude):
                    out.append([n])
        return out


def prod_int_partII(n, s):
    products = f(n, n)
    x = list(filter(lambda prod: len(prod) == s, products)) if type(products)==list else []
    return [0 if type(products)!=list else len(products), len(x), x[0] if len(x) == 1 else [] if len(x) == 0 else x]

print(f(10))
print(prod_int_partII(37, 2))
