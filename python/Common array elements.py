def common(a,b,c):
    counts_a={}
    counts_b={}
    counts_c={}
    for i in range(len(a)):
        x,y,z=a[i],b[i],c[i]
        counts_a[x]=counts_a[x]+1 if x in counts_a.keys() else 1
        counts_b[y]=counts_a[x]+1 if y in counts_b.keys() else 1
        counts_c[z]=counts_a[x]+1 if z in counts_c.keys() else 1
    result=0
    for k in counts_a:
        if k in counts_b.keys() and k in counts_c.keys() and counts_a[k]==counts_b[k] and counts_b[k]==counts_c[k]:
            result+=k*counts_a[k]
    return result

print(common([1,2,2,3],[5,3,2,2],[7,3,2,2]))

