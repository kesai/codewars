import itertools
def ssc_forperm(arr):
    p=list(set(i for i in itertools.permutations(arr,len(arr))))
    p=list(map(lambda x:sum( (i+1)*v  for i,v in enumerate(x)),p))
    return [{"total perm":len(p)}, {"total ssc": sum(p)}, {"max ssc": max(p)}, {"min ssc": min(p)}]
