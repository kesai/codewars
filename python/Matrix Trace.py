def trace(matrix):
    if len(matrix) == 0 or len(matrix) != len(matrix[0]):
        return None
    return sum(matrix[i][i] for i in range(len(matrix)))
