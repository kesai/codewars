def to_table(data, header=False, index=False):
    arr = data[1:] if header else data
    thead=''
    if header:
        thead='<thead><tr>'+'<th></th>'+''.join(f'<th>{"" if x==None else x}</th>' for x in data[0])+'</tr></thead>' if index else '<thead><tr>'+''.join(f'<th>{x}</th>' for x in data[0])+'</tr></thead>'
    tbody = ''.join([f'<tr><td>{i+1}</td>'+''.join([f'<td>{"" if v==None else v}</td>' for v in x])+'</tr>' for i, x in enumerate(arr)]) if index else ''.join(['<tr>'+''.join([f'<td>{"" if v==None else v}</td>' for v in x])+'</tr>' for x in arr])
    return f'<table>{thead}<tbody>{tbody}</tbody></table>' if header else f'<table><tbody>{tbody}</tbody></table>'

