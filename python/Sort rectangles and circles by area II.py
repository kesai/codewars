import math
def sort_by_area(seq):
	return sorted(seq,key=lambda x: x[0]*x[1] if type(x) is tuple else math.pi*x*x)
print(sort_by_area([ (2, 5), 6,3,(1,1) ]))