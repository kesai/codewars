import re


def binary_simulation(s, q):
    out = []

    def repl(v):
        return '0'*len(v.group()) if v.group()[0] == '1' else '1'*len(v.group())
    for x in q:
        if x[0] == 'I':
            i, j = x[1]-1, x[2]-1
            s = s[0:i]+re.sub(r'(0+)|(1+)', repl, s[i:j+1])+s[j+1:]
        else:
            out.append(s[x[1]-1])
    return out


binary_simulation("0011001100", [['I', 1, 10], ['I', 2, 7], ['Q', 2], ['Q', 1], ['Q', 7], ['Q', 5]])