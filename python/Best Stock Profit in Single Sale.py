def max_profit(prices):
    m = best = float('-inf')
    for x in reversed(prices):
        m, best = max(m, best-x), max(best,x)
    return m