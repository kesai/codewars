def finaldist_crazyrobot(moves, init_pos):
    (x,y)=init_pos
    offsets={'L':(-1,0),'R':(1,0),'U':(0,1),'D':(0,-1)}
    for mov in moves:
    	x+=offsets[mov[0]][0]*mov[1]
    	y+=offsets[mov[0]][1]*mov[1]
    return ((x-init_pos[0] )**2+(y-init_pos[1] )**2)**0.5
