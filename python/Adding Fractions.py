from fractions import Fraction
def add_fracs(*args):
    return str(sum(Fraction(x) for x in args)) if len(args) else ''
print(add_fracs('1/2', '1/4'))


