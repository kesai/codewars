from functools import cmp_to_key
def sort_csv_columns(csv_file_content):
    arr = list(map(lambda v: v.split(';'), csv_file_content.split('\n')))
    titles = list(map(lambda x: x[0],sorted([[i, v] for i, v in enumerate(arr[0])],key=lambda x:x[1].lower())))
    return '\n'.join(map(lambda v: ';'.join(sorted(v, key=cmp_to_key(
        lambda x, y:  titles.index(v.index(x))-titles.index(v.index(y))))), arr))


pre_sorting = (
            "myjinxin2015;raulbc777;smile67;Dentzil;SteffenVogel_79\n"
            "17945;10091;10088;3907;10132\n"
            "2;12;13;48;11"
        )
print(sort_csv_columns(pre_sorting))
