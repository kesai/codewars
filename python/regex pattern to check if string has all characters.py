import re

def regex_contains_all(s):
    return ''.join(f'(?=.*{re.escape(c)})' for c in s)

print(regex_contains_all('abc'))
