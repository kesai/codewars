def jumbled_string(s, n):
    period = 1
    temp = s[::2] + s[1::2]
    while(temp != s):
        period += 1
        temp = temp[::2]+temp[1::2]
    for i in range(n % period):
        s = s[::2]+s[1::2]
    return s
