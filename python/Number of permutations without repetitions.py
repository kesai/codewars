from math import factorial
from functools import reduce
def perms(element):
    return factorial(len(str(element)))/reduce(lambda x, y: x*factorial(str(element).count(y)),list(set(str(element))), 1)


print(perms(1223))
