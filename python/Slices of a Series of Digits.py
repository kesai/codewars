def series_slices(digits, n):
    s=list(map(int,list(str(digits))))
    if len(s)<n:
        raise 'invalid length'
    return [s[i:i+n] for i in range(len(s)-n+1)]