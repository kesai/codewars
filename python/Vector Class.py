class Vector:
    def __init__(self, *args):
        arr = list(args) if len(args) == 3 else args[0]
        self.x = arr[0]
        self.y = arr[1]
        self.z = arr[2]
        self.magnitude = (self.x**2+self.y**2+self.z**2)**0.5

    def __str__(self):
        return f'<{self.x}, {self.y}, {self.z}>'

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z

    def __add__(self, other):
        return Vector(self.x+other.x, self.y+other.y, self.z+other.z)

    def __sub__(self, other):
        return Vector(self.x-other.x, self.y-other.y, self.z-other.z)

    def cross(self, other):
        return Vector(self.y*other.z-self.z*other.y, self.z*other.x-self.x*other.z, self.x*other.y-self.y*other.x)

    def dot(self, other):
        return self.x*other.x+self.y*other.y+self.z*other.z

    def to_tuple(self):
        return tuple([self.x, self.y, self.z])
