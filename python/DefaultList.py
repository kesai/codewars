class DefaultList(list):
    def __init__(self, lst, default):
        list.__init__(self, lst)
        self.default = default

    def __getitem__(self, index):
        return self.default if index > len(self)-1 or index < -len(self) else list.__getitem__(self, index)

