def toCamelCase(s, n):
    arr = s.split(' ')
    if n == 1:
        return arr[0].lower() + ''.join(map(lambda x: x.capitalize(), arr[1:]))
    elif n == 2:
        return ''.join(map(lambda x: x[0:-1].lower()+x[-1].upper(), arr[0:-1]))+arr[-1].lower()
    elif n == 3:
        return arr[0][0:-1].lower()+arr[0][-1].upper()+''.join(map(lambda x: x[0].upper() +x[1:-1].lower()+x[-1].upper(), arr[1:-1]))+arr[-1].capitalize()


print(toCamelCase("Each number plus one", 2))
