def get_products(n):
    return [i for i in range(2, n) if n % i == 0]


def f(n, exclude=None):
    products = get_products(n)
    if(len(products) == 0):
        return n
    else:
        out = []
        for prod in products:
            sub = f(prod)
            if(type(sub) == list):
                for x in sub:
                    x.append(n//prod)
                    x.sort()
                    if(x not in out):
                        out.append(x)
            else:
                temp = sorted([sub, n//prod])
                if(temp not in out):
                    out.append(temp)
                if([n] not in out and n != exclude):
                    out.append([n])
        return out


def prod_int_part(n):
    prods = f(n, n)
    return [len(prods), list(filter(lambda v: len(v) == max(map(lambda x: len(x), prods)), prods))[0]] if type(prods) is list else [0, []]


print(prod_int_part(8))
