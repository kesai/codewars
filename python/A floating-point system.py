import re


def mant_exp(a_number, digits_number):
    old=a_number
    d = len(str(int(a_number))) if int(a_number) else 0
    x=len(str(a_number).split('.')[1])
    if d >= digits_number:
        return str(int(a_number))[0:digits_number]+'P'+str(d-digits_number)
    a_number = str(a_number).replace('.', '')
    a_number = re.sub(r'^0+', '', a_number)
    a_number = int(int(a_number)*10**(digits_number - len(a_number)))
    z=str(old/a_number)
    y=0
    if '-' in z:
        y=z.split('-')[1]
    else:
        y=digits_number-d
    return f'{a_number}P-{y}'


print(mant_exp(72.0,12))
