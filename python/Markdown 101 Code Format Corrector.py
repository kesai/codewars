def markdown_code_corrector(text):
    text_splitted = text.split('\n')
    state = 0
    res = []
    for i, x in enumerate(text_splitted + [""]):
        if x[:1] == '&':
            if state == 0:
                res.append("```")
            state = 1
        elif state == 1 and(text_splitted[i-1][-1:] != "\\" or len(text_splitted) == i):
            res.append("```")
            state = 0
        res.append(x)
    return '\n'.join(res[:-1])
