def string_letter_count(s):
	s=s.lower()
	letters='abcdefghijklmnopqrstuvwxyz'
	return ''.join([f'{s.count(c)}{c}'for c in letters if s.count(c)>0])
