import re

DIGITS={'S': '1B1W1B', 'E': '1B1W1B', 'M': '1W1B1W1B1W', '0': '3W2B1W1B', '1': '2W2B2W1B', '2': '2W1B2W2B', '3': '1W4B1W1B', '4': '1W1B3W2B', '5': '1W2B3W1B', '6': '1W1B1W4B', '7': '1W3B1W2B', '8': '1W2B1W3B', '9': '3W1B1W2B'}

def barcode_scanner(barcode):
    mapL,mapR={'B':'1','W':'0'},{'B':'0','W':'1'}
    L=dict()
    R=dict()
    for (k,v) in DIGITS.items():
        L[re.sub(r'\d+[BW]',lambda x: mapL[x.group()[1]]*int(x.group()[0]),v)]=k
        R[re.sub(r'\d+[BW]',lambda x: mapR[x.group()[1]]*int(x.group()[0]),v)]=k
    x=re.search(r'101(\d{7})(\d{7})(\d{7})(\d{7})(\d{7})(\d{7})01010(\d{7})(\d{7})(\d{7})(\d{7})(\d{7})(\d{7})101',barcode)
    sl=''.join(list(map(lambda v:L[v],list(x.groups())[0:6])))
    sr=''.join(list(map(lambda v:R[v],list(x.groups())[6:])))
    return sl+sr
