def isSameGroup(a,b):
    if len(a)!=len(b):
        return False
    for i in range(len(b)):
        if a==b[i:]+b[0:i]:
            return True
    return False

def group_cities(seq):
    seq=list(set(seq))
    groups = []
    while len(seq):
        group=[]
        first=seq.pop()
        group.append(first)
        for x in seq:
            if isSameGroup(first.lower(),x.lower()):
                group.append(x)
        for yy in group:
            if yy in seq:
                seq.remove(yy)
        groups.append(sorted(group))
    return sorted(groups,key=lambda x:(-len(x),x))


