def no_musical(s, e, m, d):
    l = e-s
    return max(0,(m-d)*(l//m)+min(l%m,m-d)) if m else l+1