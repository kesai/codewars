def convert_my_dollars(usd, currency):
	rate=CONVERSION_RATES[currency] if currency[0]  in 'aeiouAEIOU' else int(str(CONVERSION_RATES[currency]),2)
	return  f'You now have {usd*rate} of {currency}.'