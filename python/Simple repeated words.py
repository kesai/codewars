import re
import itertools
def solve(a, b):
    b=re.sub(f'[^{a}]','',b)
    print(b)
    n=0
    if(len(b)>50):
        return 1
    for i in itertools.combinations(list(b),len(a)):
        if ''.join(i)==a:
            n+=1
    return n
solve("zaz","zazapulz")



