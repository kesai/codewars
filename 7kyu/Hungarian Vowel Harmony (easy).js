function dative(word) {
    var x = word.replace(/[^eéiíöőüűaáoóuú]/g, '').slice(-1)[0];
    return word + (/[aáoóuú]/.test(x) ? 'nak' : 'nek');
}