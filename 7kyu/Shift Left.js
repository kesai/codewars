function shiftLeft(s, t) {
    if (s === t) return 0
    for (var i = 1; i <= Math.max(s.length, t.length); i++) {
        if (s.slice(-i) != t.slice(-i)) return s.length - i + 1 + t.length - i + 1;
    }
}