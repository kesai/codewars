def is_orthogonal(u, v):
    return sum(x*v[i] for i, x in enumerate(u)) == 0
