function spinningRings(innerMax, outerMax) {
    var n = 1;
    inner = innerMax;
    outer = 1;
    while (inner != outer) {
        inner = (inner - 1 + innerMax) % innerMax;
        outer = (outer + 1) % outerMax;
        n++;
    }
    return n;
};