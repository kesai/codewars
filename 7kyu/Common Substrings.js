function SubstringTest(str1, str2) {
    for (var l = 2; l < str1.length; l++) {
        for (var i = 0; i <= str1.length - l; i++) {
            var substr = str1.slice(i, i + l).toLowerCase();
            if (str2.toLowerCase().includes(substr)) return true;
        }
    }
    return false;
}