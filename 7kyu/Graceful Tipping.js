function gracefulTipping(bill) {
    bill = Math.floor(bill * 1.15) + 1;
    if (bill < 10) return bill;
    else {
        var n = ('' + bill).length - 2;
        var d = 5 * 10 ** n;
        while (bill % d) bill++;
        return bill;
    }
}
console.log(gracefulTipping(86))
