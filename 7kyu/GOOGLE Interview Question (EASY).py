def get_strings(city):
    city = city.lower().replace(' ', '')
    return ','.join(sorted([f'{c}:{"*"*city.count(c)}' for c in set(city)
                            ], key=lambda x: city.index(x[0])))
