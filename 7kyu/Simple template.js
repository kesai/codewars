function createTemplate(template) {
    return function (o) {
        return template.replace(/{{([^{}]+)}}/g, (_, k) => o[k])
    };
}

