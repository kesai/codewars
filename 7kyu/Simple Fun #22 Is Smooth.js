function isSmooth(arr) {
    var i = Math.floor(arr.length / 2);
    var mid = arr.length % 2 ? arr[i] : arr[i - 1] + arr[i];
    return arr[0] === arr[arr.length - 1] && arr[0] === mid;
}