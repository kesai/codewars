function solve(n) {
    var dollars = [500, 200, 100, 50, 20, 10];
    var x = 0
    for (d of dollars) {
        x += Math.floor(n / d);
        n %= d;
    }
    return n ? -1 : x;
}
console.log(solve(125))