var decomposeSingleStrand = function (s) {
	return Array.from({ length: 3 },
		(_, i) => `Frame ${i + 1}: ${(' '.repeat(3-i) + s).match(/.{1,3}/g).join(' ').trim()}`).join('\n')
}