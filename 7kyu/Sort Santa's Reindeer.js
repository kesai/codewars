function sortReindeer(reindeerNames) {
    return reindeerNames.sort((a, b) => a.split(' ').slice(-1)[0].localeCompare(b.split(' ').slice(-1)[0]))
}