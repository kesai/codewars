function getWeight(name) {
    return [...name.replace(/[^a-z]/gi, '')].reduce((s, v) => s += /[a-z]/.test(v) ? v.toUpperCase().charCodeAt(0) : v.toLowerCase().charCodeAt(0), 0)
}