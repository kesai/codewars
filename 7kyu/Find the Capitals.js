function capital(capitals) {
    return capitals.map(v => `The capital of ${v.state || v.country} is ${v.capital}`)
}
mixed_capitals = [{ "state": 'Maine', capital: 'Augusta' }, { country: 'Spain', "capital": "Madrid" }]
console.log(capital(mixed_capitals))
