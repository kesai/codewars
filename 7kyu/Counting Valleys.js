function countingValleys(s) {
    var level = 0;
    var in_valley = false;
    var n = 0;
    for (x of s) {
        if (level < 0 && !in_valley) { in_valley = true; }
        if (level === 0 && in_valley) { in_valley = false; n++; }
        if (x === 'U') level++;
        if (x === 'D') level--;
    }
    return n;
}