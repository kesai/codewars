function codewarsTshirts(n, orders) {
    const stock = ['White', 'Orange', 'Blue', 'Purple', 'Red', 'Black'].reduce((acc, color) => {
        acc[color] = n / 6
        return acc
    }, {})

    return checkOrders(orders, stock)
}

function checkOrders([order, ...restOrders], stock) {
    if (!order) return true

    return order.some(color => {
        if (stock[color] === 0) return false

        const restStock = Object.assign({}, stock)
        restStock[color] -= 1
        return checkOrders(restOrders, restStock)
    })
}