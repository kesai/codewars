/**
 * 判断是否为负数
 * @param {字符串或整数} x 
 */
const is_negative = (x) => /^\-\d+$/.test(x);
/**
 * 比较两个正整数字符串大小
 * @param {正整数字符串} a 
 * @param {正整数字符串} b 
 */
function le(a, b) {
    if (a === b) return true;
    if (a.length === b.length) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] !== b[i]) return +a[i] > +b[i];
        }
    } else {
        return a.length > b.length;
    }
}

/**
 * 字符串加法
 * @param {正整数字符串} a 
 * @param {正整数字符串 } b 
 */
function string_add(a, b) {
    var arr1 = [...a].reverse();
    var arr2 = [...b].reverse();
    var added = 0;
    var out = [];
    for (var i = 0; i < Math.max(arr1.length, arr2.length); i++) {
        var sum = +(arr1[i] || 0) + +(arr2[i] || 0) + added;
        added = parseInt(sum / 10);
        out.push(sum % 10);
    }
    if (added) out.push(added);
    return out.reverse().join('')
}

/**
 * 字符串减法
 * @param {正整数字符串} a 
 * @param {正整数字符串 } b 
 */
function string_sub(a, b) {
    if (!le(a, b)) return '-' + string_sub(b, a);
    var arr1 = [...a].reverse();
    var arr2 = [...b].reverse();
    var subed = 0;
    var out = []
    for (var i = 0; i < Math.max(arr1.length, arr2.length); i++) {
        var a = (+arr1[i] || 0) + subed;
        var b = (+arr2[i] || 0);
        var sub = (a < b ? a + 10 : a) - b;
        out.unshift(sub);
        subed = a < b ? -1 : 0;
    }
    var result = out.join('').replace(/^0*/, '');
    return result || '0';
}

function bigAdd(a, b) {
    a = '' + a, b = '' + b;
    if (is_negative(a) && is_negative(b)) {
        //负+负
        return '-' + string_add(a.slice(1), b.slice(1));
    } else if (!is_negative(a) && !is_negative(b)) {
        //正+正
        return string_add(a, b);
    } else {
        var positive = is_negative(a) ? b : a;
        var negative = is_negative(a) ? a : b;
        return string_sub(positive, negative.slice(1));
    }
}

function bigSub(a, b) {
    a = '' + a, b = '' + b;
    if (is_negative(a) && is_negative(b)) {
        //负-负
        return string_sub(b.slice(1), a.slice(1));
    } else if (!is_negative(a) && !is_negative(b)) {
        //正-正
        if (le(a, b)) return string_sub(a, b);
        else return '-' + string_sub(b, a);
    } else if (!is_negative(a) && is_negative(b)) {
        //正-负
        return string_add(a, b.slice(1));
    } else if (is_negative(a) && !is_negative(b)) {
        //负-正
        return '-' + string_add(a.slice(1), b);
    }
}
//test code
function test_string_sub(a, b) {
    var result = string_sub(a, b);
    var expected = '' + eval(`${a}-${b}`);
    console.log(`${a}-${b}->result=${result},expected=${expected},${result === expected}`);
}

test_string_sub('122', '48')
test_string_sub('115', '48')
test_string_sub('1234242342342342', '123123154350')
test_string_sub('879054345123123', '45433468');

function test_le(a, b) {
    var result = le(a, b);
    var expected = +a >= +b;
    console.log(`${a}>=${b},result=${result},expected=${expected},${result === expected}`);
}

test_le('123', '43');
test_le('123', '443');
test_le('32', '4355234234');
test_le('112312312323', '43234234234');
function test_bigAdd(a, b) {
    console.log(bigAdd(a, b));
}
test_bigAdd('454', "34")
test_bigAdd('500', "-443")
test_bigAdd('5', "-443")
test_bigAdd('-451', "43")
test_bigAdd('-23', "-43")
test_bigAdd('23', "-23")
function test_bigSub(a, b) {
    console.log(bigSub(a, b));
}
test_bigSub(-2, "-1")