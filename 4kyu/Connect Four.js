//modified from a prior kata's solution
function whoWon(grid, color) {
    //horizonal
    for (row of grid) {
        if (new RegExp(`${color}{4,}`).test(row.join(''))) return color
    }
    //vertical
    for (var j = 0; j < grid[0].length; j++) {
        var colum = grid.map(v => v[j]);
        if (new RegExp(`${color}{4,}`).test(colum.join(''))) return color
    }
    var ROW = grid.length;
    var COL = grid[0].length;
    //diagonals
    for (var row = 0; row < ROW; row++) {
        var temp = [];
        for (var i = row, j = 0; i < ROW && j < COL; i++, j++) {
            temp.push(grid[i][j]);
        }
        if (new RegExp(`${color}{4,}`).test(temp.join(''))) return color
    }
    for (var col = 0; col < COL; col++) {
        var temp = [];
        for (var j = col, i = 0; i < ROW && j < COL; i++, j++) {
            temp.push(grid[i][j]);
        }
        if (new RegExp(`${color}{4,}`).test(temp.join(''))) return color
    }

    for (var row = 0; row < ROW; row++) {
        var temp = [];
        for (var i = row, j = COL - 1; i < ROW && j > -1; i++, j--) {
            temp.push(grid[i][j]);
        }
        if (new RegExp(`${color}{4,}`).test(temp.join(''))) return color
    }

    for (var col = COL - 1; col > -1; col--) {
        var temp = [];
        for (var j = col, i = 0; i < ROW && j > -1; i++, j--) {
            temp.push(grid[i][j]);
        }
        if (new RegExp(`${color}{4,}`).test(temp.join(''))) return color
    }
    return false;
}

function whoIsWinner(drops) {
    var color_map = { 'Y': 'Yellow', 'R': 'Red' }
    var grid = Array.from({ length: 6 }, (_, i) => Array.from({ length: 7 }, (_, j) => '-'));
    var column_str = 'ABCDEFG';
    for (drop of drops) {
        var [col, color] = drop.split('_');
        col = column_str.indexOf(col);
        color = color[0];
        for (var i = grid.length - 1; i > -1; i--) {
            if (grid[i][col] === '-') {
                grid[i][col] = color;
                var result = whoWon(grid, color);
                if (result) return color_map[result];
                break;
            }
        }
    }
    return 'Draw'
}