function simplify(poly) {
    var polys = poly.match(/(\+|\-)?(\d*)[a-z]+/g).map(v => v.replace(/[a-z]/g, '') + [...v.replace(/[^a-z]/g, '')].sort().join(''));
    var stastics = {}
    for (p of polys) {
        var key = p.replace(/[^a-z]/g, '');
        var k = p.replace(/[a-z]/g, '');
        k = (k === '+' || k === '') ? 1 : k === '-' ? -1 : +k;
        stastics[key] = Object.keys(stastics).includes(key) ? (stastics[key] + k) : k;
    }
    var out = []
    for (k of Object.keys(stastics)) if (d = stastics[k]) out.push((d === 1 ? '' : d === -1 ? '-' : '' + d) + k);
    out = out.sort((a, b) => (x = a.replace(/[^a-z]/g, '')).length - (y = b.replace(/[^a-z]/g, '')).length || x.localeCompare(y))
    return out.join('+').replace(/(\+\-)|(\-\+)/g, '-').replace(/^\+/, '')
}