https://binary-system.base-conversion.ro/convert-real-numbers-from-decimal-system-to-32bit-single-precision-IEEE754-binary-floating-point.php
var float2bin = function (input) {
    input = '' + input
    var sign = +input < 0 ? 1 : 0;
    input = Math.abs(+input).toString();
    var i = +input.split('.')[0];//integer part    
    var f = +('0.' + (input.split('.')[1] || 0));//fractional part
    var int_part = ''
    while (i) {
        int_part = i % 2 + int_part;
        i = Math.floor(i / 2);
    }
    console.log(`int_part:\n  ${int_part}`)
    var fractional_part = ''
    var cnt = 0;//what the limited.i need to check out.
    while (cnt < 38) {
        var r = Math.floor(f * 2);
        fractional_part += r;
        f = f * 2 % 1;
        cnt++;
    }
    console.log(`fractional_part:\n  ${fractional_part}`);
    var exponent = 0;
    var mantissa = int_part + '.' + fractional_part;
    if (int_part.indexOf('1') > -1) {
        exponent = int_part.length - 1;
        mantissa = int_part[0] + '.' + int_part.slice(1) + fractional_part;
    } else {
        exponent = -fractional_part.indexOf('1') - 1;
        mantissa = fractional_part.replace(/^0*/, '');
        mantissa = (mantissa[0] + '.' + mantissa.slice(1))
    }
    console.log(`Sign: ${sign}, Mantissa (not normalized): ${mantissa}, Exponent (unadjusted): ${exponent}`);

    // Adjust the exponent in 8 bit excess/bias notation and then convert it from decimal (base 10) to 8 bit binary, 
    //by using the same technique of repeatedly dividing by 2
    exponent = exponent + 2 ** (8 - 1) - 1
    var exponent_ajusted = ''
    while (exponent) {
        exponent_ajusted = exponent % 2 + exponent_ajusted;
        exponent = Math.floor(exponent / 2);
    }
    exponent_ajusted = exponent_ajusted.padStart(8, '0');
    console.log(`exponent(adjusted): ${exponent_ajusted}`)
    //Normalize mantissa, remove the leading  and then adjust its length to 23 bits
    mantissa = mantissa.split('.')[1].slice(0, 23);
    console.log(`Mantissa (normalized): ${mantissa} `)
    console.log(`result: ${sign}-${exponent_ajusted}-${mantissa}`)
    return sign + exponent_ajusted + mantissa;
}
console.log(float2bin('0.0001'))


