function pattern(legs) {
    legs = legs.sort((a, b) => b - a);
    var max_leg = legs.shift()
    var sort_legs = [max_leg];
    var left = (legs.filter((v, i) => i % 2 === 0) || []).reverse();
    var right = legs.filter((v, i) => i % 2) || [];
    sort_legs = left.concat(sort_legs).concat(right);
    var photos = []
    for (var i = 0; i < sort_legs.length; i++) {
        var temp = single(sort_legs[i], max_leg, sort_legs[i - 1] || Infinity, sort_legs[i + 1] || Infinity);
        photos.push(temp);
    }
    var result = photos[0].map((e, j) => photos.map(v => v[j]).join(''))
    return result.join('\n')
}
function single(leg, max_leg, left, right) {
    var d_left = leg > left ? leg - left : 0;
    var d_right = leg > right ? leg - right : 0;
    var arr = [
        ' '.repeat(d_left) + '   + +   ' + ' '.repeat(d_right),
        ' '.repeat(d_left) + '  +o o+  ' + ' '.repeat(d_right),
        ' '.repeat(d_left) + ' +  u  + ' + ' '.repeat(d_right),
        ' '.repeat(d_left) + '  + ~ +  ' + ' '.repeat(d_right),
        ' '.repeat(d_left) + '    |    ' + ' '.repeat(d_right),
        ' '.repeat(d_left) + '  +-o-+  ' + ' '.repeat(d_right)]
    var j = 0; k = 0;
    for (var i = 0; i < leg + 1; i++) {
        if (i === 0) {
            var center = '| o |';
            var x = '_/'.padStart(d_left + 2, ' ');
            var y = '\\_'.padEnd(d_right + 2, ' ');
            if (d_left) { x = '/'.padStart(d_left + 2, ' '); j++; }
            if (d_right) { y = '\\'.padEnd(d_right + 2, ' '); k++; }
            arr.push(x + center + y);
        } else if (i === 1) {
            var center = '+-o-+';
            var x = ' '.repeat(d_left + 2);
            var y = ' '.repeat(d_right + 2);
            if (d_left) {
                if (j === d_left) { x = '_/' + ' '.repeat(j); } else if (d_left > j) {
                    x = '/' + ' '.repeat(j);
                }
                x = x.padStart(d_left + 2, ' ')
                j++;
            }
            if (d_right) {
                if (k === d_right) { y = ' '.repeat(k) + '\\_'; } else if (d_right > k) { y = ' '.repeat(k) + '\\'; }
                y = y.padEnd(d_right + 2, ' ');
                k++;
            }
            arr.push(x + center + y);
        } else {
            var center = ' | | ';
            var x = ' '.repeat(d_left + 2);
            var y = ' '.repeat(d_right + 2);
            if (d_left) {
                if (j === d_left) x = '_/' + ' '.repeat(j); else if (d_left > j) x = '/' + ' '.repeat(j);
                x = x.padStart(d_left + 2, ' '); j++;
            }
            if (d_right) {
                if (k === d_right) y = ' '.repeat(k) + '\\_'; else if (d_right > k) y = ' '.repeat(k) + '\\';
                y = y.padEnd(d_right + 2, ' '); k++;
            }
            arr.push(x + center + y);
        }
    }
    arr.push(' '.repeat(d_left) + '   I I   ' + ' '.repeat(d_right))
    var spaces = Array.from({ length: max_leg - leg }, (_, i) => ' '.repeat(arr[0].length))
    arr = spaces.concat(arr);
    return arr;
}


console.log(pattern([2, 3, 2, 1, 1]))
console.log('_______________________________________________')
var result =
    `                       + +                       
             + +      +o o+                      
   + +      +o o+    +  u  +      + +      + +   
  +o o+    +  u  +    + ~ +      +o o+    +o o+  
 +  u  +    + ~ +       |       +  u  +  +  u  + 
  + ~ +       |       +-o-+      + ~ +    + ~ +  
    |       +-o-+    /| o |\\       |        |    
  +-o-+    /| o |\\__/ +-o-+ \\    +-o-+    +-o-+  
_/| o |\\__/ +-o-+      | |   \\__/| o |\\__/| o |\\_
  +-o-+      | |       | |       +-o-+    +-o-+  
   I I       I I       I I        I I      I I   `
console.log(result)

