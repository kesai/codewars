function Event() {
    var subscribes = [];
    function _subscribe(args) {
        for (f of args) {
            if (typeof (f) === 'function') subscribes.push(f);
        }
    }
    function _unsubscribe(args) {
        for (f of args) {
            if ((i = subscribes.lastIndexOf(f)) > -1) {
                subscribes.splice(i, 1);
            }
        }
    }
    function _emit(args, context) {
        var temp = [].concat(subscribes);
        for (f of temp) {
            f.apply(context, args)
        }
    }
    var that = this;
    this.subscribe = function (...args) {
        _subscribe(args);
    }
    this.unsubscribe = function (...args) {
        _unsubscribe(args);
    }
    this.emit = function () {
        _emit(arguments, this)
    }
}
