function DocumentParser(reader) {
  this.reader = reader;
  this.reset();
}

DocumentParser.prototype.reset = function () {
  this.wordCount = 0;
  this.charCount = 0;
  this.lineCount = 0;
};
DocumentParser.prototype.parse = function () {
  var text = ''
  while ((c = this.reader.getChunk()))text += c;
  this.lineCount = (text.split('\n') || []).length;
  this.charCount = text.length - this.lineCount + 1;
  this.wordCount = text.split('\n').reduce((s, v) => s += (v.split(' ').filter(x => x.trim() != '') || []).length, 0)
  if (!text) this.reset();
};