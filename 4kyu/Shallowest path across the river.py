class Node:
    def __init__(self, x, y, depth):
        self.x = x
        self.y = y
        self.depth = depth

    def __repr__(self):
        return f'{self.x},{self.y},{self.depth}'


def creare_graph(river):
    N = len(river)*len(river[0])
    print(N)


def shallowest_path(river):
    # create graph

    return [[0, 0]]


river = [[2, 3, 2],
         [1, 1, 4],
         [9, 5, 2],
         [1, 4, 4],
         [1, 5, 4],
         [2, 1, 4],
         [5, 1, 2],
         [5, 5, 5],
         [8, 1, 9]]
creare_graph(river)
