function parseCSV(input, separator = ',', quote = '"') {
    const newString = []
        , newLine = '\n'
        , specialCharacters = '$\\'
        , quoteRegStr = specialCharacters.indexOf(quote) > -1
            ? '\\' + quote + '\\' + quote
            : quote + quote
    let tempWord = ''
        , char = ''
        , tmp = []
        , pos
        , replaceRegEx = new RegExp(quoteRegStr, 'g')
        , replaceQuotes = function (str) {
            const self = str + ''
            let replaced = self.replace(replaceRegEx, quote)
            if (replaced[0] === quote) {
                replaced = replaced.slice(1)
            }
            if (replaced[replaced.length - 1] === quote) {
                replaced = replaced.slice(0, self.length - 1)
            }
            return replaced
        }
        , pushResult = () => {
            newString.push(tmp)
            tmp = []
        }
    const addCurrentChar = function (i = 1, push = true) {
        const part = input.slice(0, pos)
        tmp.push(replaceQuotes(part))
        if (push) {
            pushResult()
        }
        input = input.slice(pos + i)
    }
    while (input.length) {
        char = input[0]
        if (char === quote) {
            pos = input.indexOf(quote + separator, 1)
            if (pos === -1) {
                pos = input.indexOf(quote + newLine, 1)
                if (pos === -1) {
                    pos = input.indexOf(quote, 1)
                    addCurrentChar()
                } else {
                    addCurrentChar(2)
                }
            } else {
                addCurrentChar(2, false)
            }
        } else if (char === separator || char === newLine) {
            tmp.push(tempWord)
            tempWord = ''
            input = input.slice(1)
            if (char === newLine) {
                pushResult()
            }
        } else {
            tempWord += char
            input = input.slice(1)
        }
    }
    if (tmp.length > 0) {
        tmp.push(tempWord)
        newString.push(tmp)
    }
    return newString.length === 0
        ? [[""]]
        : newString
}
/**
 * CSV Parser.  Takes a string as input and returns
 * an array of arrays (for each row).
 * 
 * @param input String, CSV input
 * @param separator String, single character used to separate fields.
 *        Defaults to ","
 * @param quote String, single character used to quote non-simple fields.
 *        Defaults to "\"".
 */
function parseCSV(input, separator, quote) {
    separator = separator || ',';
    quote = quote || '"';
    let fieldSplitter = new RegExp('\\' + separator + '(?=(?:[^\\' + quote + ']|\\' + quote + '[^\\' + quote + ']*\\' + quote + ')*$)');
    let quoteReplacer = new RegExp('^\\' + quote + '|\\' + quote + '$|\\' + quote + '(?=\\' + quote + '(?!$))', 'g');
    let rowsSplitter = new RegExp('\\n' + '(?=(?:[^\\' + quote + ']|\\' + quote + '[^\\' + quote + ']*\\' + quote + ')*$)', 'm');
    let rows = input.split(rowsSplitter).map(i => i.split(fieldSplitter).map(j => j.replace(quoteReplacer, '')));
    return rows;
}