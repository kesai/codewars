function to_weirdbinary(x) {
    var result = ''
    while (Math.abs(x) > 1) {
        result = Math.abs(x % 2) + result;
        x = x > 0 ? -(Math.floor(x / 2)) : Math.round(-x / 2);
    }
    return (x === -1 ? '11' : x) + result
}
let to_decimal = (x) => [...'' + x].reverse().reduce((s, v, i) => s += (-2) ** i * +v, 0);
function skrzat(type, input) {
    if (type === 'b') return `From binary: ${input} is ${to_decimal(input)}`
    return `From decimal: ${input} is ${to_weirdbinary(input)}`
}