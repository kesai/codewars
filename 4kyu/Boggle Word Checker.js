function Point(x, y, char) {
    this.x = x;
    this.y = y;
    this.char = char;
}
function checkWord(board, word) {
    var starts = []
    for (var i = 0; i < board.length; i++) {
        for (var j = 0; j < board[0].length; j++) {
            if (board[i][j] === word[0]) starts.push(new Point(i, j, word[0]));
        }
    }
    for (start of starts) {
        if (check(board, word, start)) return true;
    }
    return false;
}

function check(board, word, start) {
    var ROW = board.length;
    var COL = board[0].length;
    function isValid(x, y) {
        return x > -1 && x < ROW && y > -1 && y < COL;
    }
    var moves = [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, 1], [1, 0], [1, -1]]
    var paths = [{
        'points': [start],
        'chars': start.char,
        'visited': board.map(v => v.map(e => false))
    }]
    paths[0].visited[start.x][start.y] = true;

    while (paths.length) {
        var new_paths = [];
        for (path of paths) {
            if (path.chars === word) return true;
            for (mov of moves) {
                var last_p = path.points[path.points.length - 1];
                var row = last_p.x + mov[0];
                var col = last_p.y + mov[1];
                if (isValid(row, col) && path.visited[row][col] === false) {
                    if (board[row][col] === word.replace(new RegExp('^' + path.chars), '')[0]) {
                        var new_path = {};
                        new_path.chars = path.chars + board[row][col];
                        new_path.visited = path.visited.map(v => v.map(e => e));
                        new_path.visited[row][col] = true;
                        new_path.points = [].concat(path.points)
                        new_path.points.push(new Point(row, col, board[row][col]));
                        new_paths.push(new_path);
                    }
                }
            }
        }
        paths = new_paths;
    }
}
var testBoard = [
    ['N', 'B', 'R', 'A'],
    ['C', 'R', 'P', 'A'],
    ['L', 'A', 'A', 'P'],
    ['S', 'O', 'A', 'A']]

console.log(checkWord(testBoard, 'PARAPARAS'))