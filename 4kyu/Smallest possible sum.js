function small(tmp, start) {
    while (tmp != start) {
        if (tmp > start) {
            if (tmp % start == 0) return start;
            tmp %= start;
        } else {
            if (start % tmp == 0) return tmp;
            start %= tmp;
        }
    }
    return tmp;
}
function solution(numbers) {
	if(numbers.length===1)return numbers[0]
    var start = numbers[0];
    for (var i = 1; i < numbers.length; i++) {
        tmp = numbers[i];
        tmp = small(tmp, start);
        if (tmp == 1) return numbers.length;
        start = tmp;
    }
    return tmp*numbers.length;
}