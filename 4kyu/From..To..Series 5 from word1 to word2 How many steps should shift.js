function howManyShift(word1, word2) {

    var s = 'abcdefghijklmnopqrstuvwxyz';
    var n = 0;
    var a = [...word1].sort()
    var b = [...word2]
    for (x of a) {
        var idx = s.indexOf(x);
        var arr = b.map((v, i) => {
            var d = s.indexOf(v);
            var m = Math.max(d, idx);
            var n = Math.min(d, idx);
            return [v, Math.min(m - n, 26 - m + n)];
        }).sort((a, b) => a[1] - b[1]);
        var i = b.indexOf(arr[0][0]);
        n += arr[0][1];
        b.splice(i, 1);
    }
    return n;
}