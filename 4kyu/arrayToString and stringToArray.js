function arrayToString(arr) {
    var out = []
    for (var i = 0; i < arr.length; i++) {
        if (i + 2 < arr.length && arr[i + 1] === arr[i] && arr[i + 2] === arr[i]) {
            var n = 0;
            var first = arr[i];
            while (arr[i + 1] === arr[i]) {
                n++;
                i++;
            }
            out.push(`${first}:${n + 1}`);
        } else if (i + 2 < arr.length && arr[i + 1] - arr[i] === arr[i + 2] - arr[i + 1]) {
            var d = arr[i + 1] - arr[i];
            var sub = [arr[i]]
            while (arr[i + 1] - arr[i] === d) {
                if (sub.length > 2) {
                    if (arr.length &&
                        arr[i + 3] - arr[i + 2] === arr[i + 2] - arr[i + 1] &&
                        arr[i + 2] - arr[i + 1] != d && arr[i + 4] - arr[i + 3] != arr[i + 3] - arr[i + 2]) break;
                }
                sub.push(arr[i + 1]);
                i++;
            }
            out.push(`${sub[0]}:${sub.length}${(d > 0 ? '+' : '') + d}`);
        } else {
            out.push(arr[i])
        }

    }
    return out.join(',')
}
function stringToArray(str) {
    var out = []
    for (x of str.split(',')) {
        if (/^(-?\d+):(\d+)((\+|-)\d+)$/.test(x)) {
            var [_, first, n, d] = x.match(/^(-?\d+):(\d+)((\+|-)\d+)$/);
            out = out.concat(Array.from({ length: +n }, (_, i) => +first + i * (+d)));
        } else if (/^(-?\d+):\d+$/.test(x)) {
            var [_, first, n] = x.match(/(-?\d+):(\d+)/);
            out = out.concat(Array.from({ length: +n }, (_, i) => +first));
        } else {
            out.push(+x);
        }
    }
    return out;
}
