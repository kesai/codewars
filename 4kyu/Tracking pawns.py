import re


def pawn_move_tracker(moves):
    board = [
        [".", ".", ".", ".", ".", ".", ".", "."],
        ["p", "p", "p", "p", "p", "p", "p", "p"],
        [".", ".", ".", ".", ".", ".", ".", "."],
        [".", ".", ".", ".", ".", ".", ".", "."],
        [".", ".", ".", ".", ".", ".", ".", "."],
        [".", ".", ".", ".", ".", ".", ".", "."],
        ["P", "P", "P", "P", "P", "P", "P", "P"],
        [".", ".", ".", ".", ".", ".", ".", "."]
    ]
    COL, ROW = 'abcdefgh', '12345678'

    def pawn_loc(pawn):
        return (7-ROW.index(pawn[1]), COL.index(pawn[0]),)

    for i, mov in enumerate(moves):
        if i % 2 == 0:
            # white move
            if re.fullmatch(r'^[a-h][1-8]$', mov):
                row, col = pawn_loc(mov)
                if board[row][col] != '.':
                    return f'{mov} is invalid'
                if row == 4:
                    if board[row+1][col] == 'P':
                        board[row+1][col] = '.'
                        board[row][col] = 'P'
                    elif board[row+2][col] == 'P':
                        board[row+2][col] = '.'
                        board[row][col] = 'P'
                    else:
                        return f'{mov} is invalid'
                else:
                    if board[row+1][col] == 'P':
                        board[row+1][col] = '.'
                        board[row][col] = 'P'
                    else:
                        return f'{mov} is invalid'
            elif re.fullmatch(r'^[a-h]x[a-h][1-8]$', mov):
                from_col = COL.index(mov[0])
                to_col = COL.index(mov[2])
                to_row = 7-ROW.index(mov[3])
                from_row = to_row+1
                if board[from_row][from_col] == 'P' and board[to_row][to_col] == 'p' and abs(to_col-from_col) == 1:
                    board[from_row][from_col] = '.'
                    board[to_row][to_col] = 'P'
                else:
                    return f'{mov} is invalid'
            else:
                return f'{mov} is invalid'
        else:
            # black move
            if re.fullmatch(r'^[a-h][1-8]$', mov):
                row, col = pawn_loc(mov)
                if board[row][col] != '.':
                    return f'{mov} is invalid'
                if row == 3:
                    if board[row-1][col] == 'p':
                        board[row-1][col] = '.'
                        board[row][col] = 'p'
                    elif board[row-2][col] == 'p':
                        board[row-2][col] = '.'
                        board[row][col] = 'p'
                    else:
                        return f'{mov} is invalid'
                else:
                    if board[row-1][col] == 'p':
                        board[row-1][col] = '.'
                        board[row][col] = 'p'
                    else:
                        return f'{mov} is invalid'
            elif re.fullmatch(r'^[a-h]x[a-h][1-8]$', mov):
                from_col = COL.index(mov[0])
                to_col = COL.index(mov[2])
                to_row = 7-ROW.index(mov[3])
                from_row = to_row-1
                if board[from_row][from_col] == 'p' and board[to_row][to_col] == 'P' and abs(to_col-from_col) == 1:
                    board[from_row][from_col] = '.'
                    board[to_row][to_col] = 'p'
                else:
                    return f'{mov} is invalid'
            else:
                return f'{mov} is invalid'
    return board
