function parseData() {
    var arr = dataFile.trim().split('\n');
    var countries = []
    var max_Ammonia = -Infinity;
    var max_Nitrogen = -Infinity;
    var max_Carbon = -Infinity;
    for (var i = 0; i < arr.length; i += 7) {
        var block = arr.slice(i, i + 7);
        var item = { 'idx': i, 'country': block[1].split(':')[1].trim() };
        for (var j = 3; j < block.length - 1; j++) {
            var temp = block[j].split(':');
            var name = temp[0].trim();
            var value = +temp[1].replace(/[^\d]/g, '');
            if (name === 'Ammonia') max_Ammonia = Math.max(max_Ammonia, value);
            if (name === 'Nitrogen Oxide') max_Nitrogen = Math.max(max_Nitrogen, value);
            if (name === 'Carbon Monoxide') max_Carbon = Math.max(max_Carbon, value);
            item[name] = value;
        }
        countries.push(item);
    }
    var Ammonia = countries.filter(v => v['Ammonia'] === max_Ammonia).sort((a, b) => a.idx - b.idx).map(v => v.country).join(', ')
    var Nitrogen = countries.filter(v => v['Nitrogen Oxide'] === max_Nitrogen).sort((a, b) => a.idx - b.idx).map(v => v.country).join(', ')
    var Carbon = countries.filter(v => v['Carbon Monoxide'] === max_Carbon).sort((a, b) => a.idx - b.idx).map(v => v.country).join(', ')
    return `Ammonia levels in ${Ammonia} are too high. Nitrogen Oxide levels in ${Nitrogen} are too high. Carbon Monoxide levels in ${Carbon} are too high.`

}
