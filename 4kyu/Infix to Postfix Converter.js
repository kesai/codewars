function toPostfix(infix) {
    var nums = [];
    var opers = [];

    for (x of infix) {
        if (/\d/.test(x)) {
            nums.push(x);
        } else if (x === '(') {
            opers.push(x)
        } else if (x === ')') {
            while ((p = opers.pop()) != '(') {
                var a = nums.pop();
                var b = nums.pop();
                nums.push(b + a + p)
            }
        } else if (/[+\-]/.test(x)) {
            while (opers.length && opers[opers.length - 1] != '(') {
                var a = nums.pop();
                var b = nums.pop();
                nums.push(b + a + opers.pop());
            }
            opers.push(x);
        } else if (/[*/\^]/.test(x)) {
            if (opers.length && /[*/\^]/.test(opers[opers.length - 1])) {
                var a = nums.pop();
                var b = nums.pop();
                nums.push(b + a + opers.pop());
                opers.push(x);
            } else {
                opers.push(x);
            }
        }
    }
    while (opers.length) {
        var a = nums.pop();
        var b = nums.pop();
        nums.push(b + a + opers.pop());
    }
    return nums[0]
}



console.log(toPostfix('(5-4-1)+9/5/2-7/1/7'))