function Cell(x, y) {
    this.x = x;
    this.y = y;
}

function knight(start, finish) {
    var moves = [[1, 2],[2, 1],[2, -1],[1, -2],[-1, -2],[-2, -1],[-2, 1],[-1, 2]];
    var s = ' abcdefgh';
    var [x0, y0] = [s.indexOf(start[0]), +start[1]];
    var [x1, y1] = [s.indexOf(finish[0]), +finish[1]];
    var start_p = new Cell(x0, y0);
    var end_p = new Cell(x1, y1);
    var arr = []
    var paths = []
    arr.push(start_p);
    while (true) {
        if (paths.length) {
            var new_paths = [];
            for (path of paths) {
                for (mov of moves) {
                    var [x, y] = [path[path.length - 1].x + mov[0], path[path.length - 1].y + mov[1]];
                    if (x === x1 && y === y1) return path.length + 1;
                    if (x > 0 && x < 9 && y > 0 && y < 9) {
                        var temp = new Cell(x, y);
                        if (arr.indexOf(temp) > -1) break; //this path is dead
                        arr.push(temp);
                        var new_path = [].concat(path);
                        new_path.push(temp);
                        new_paths.push(new_path);
                    }
                }
            }
            paths = new_paths;
        } else {
            for (mov of moves) {
                var [x, y] = [start_p.x + mov[0], start_p.y + mov[1]];
                if (x === x1 && y === y1) return 1;
                if (x > 0 && x < 9 && y > 0 && y < 9) {
                    var temp = new Cell(x, y);
                    arr.push(temp)
                    paths.push([temp]);
                }
            }
        }
    }
}
