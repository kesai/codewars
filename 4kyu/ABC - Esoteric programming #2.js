class Output {
    constructor(out, debug) {
        this.output = out;
        this.debug = debug;
    }
}

class Interpreter {
    read(input) {
        let out = new Output('', []), p = -1, accumulator = 0, ASCII_mode = false;
        while (++p < input.length) {
            switch (input[p]) {
                case 'a': accumulator++; break;
                case 'b': accumulator--; break;
                case 'c': out.output += (ASCII_mode ? String.fromCharCode(accumulator) : accumulator); break;
                case 'd': accumulator *= -1; break;
                case 'r': accumulator = (Math.random() * accumulator) | 0; break;
                case 'n': accumulator = 0; break;
                case '$': ASCII_mode = !ASCII_mode; break;
                case 'l': input = input.slice(0, p) + input.slice(p + 1); p = -1; break;
                case ';': out.debug.push(`${accumulator}->${String.fromCharCode(accumulator)}`); break;
            }
        }
        return out;
    }
}

var inter = new Interpreter();
var o = inter.read('aaaaalllllaaa$c;');
console.log(o.debug)