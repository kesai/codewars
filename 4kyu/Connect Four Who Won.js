function connectFour(board) {
    var a = board.map(v => v.join(''));
    if (a.findIndex(v => /RRRR/.test(v)) > -1) return 'R';
    if (a.findIndex(v => /YYYY/.test(v)) > -1) return 'Y';
    for (var i = 0; i < board[0].length; i++) {
        var column = Array.from({ length: board.length }, (_, j) => board[j][i]).join('');
        if (/RRRR/.test(column)) return 'R';
        if (/YYYY/.test(column)) return 'Y';
    }
    //left to right
    var start_points = [[0, 0], [0, 1], [0, 2], [0, 3], [1, 0], [2, 0]];
    for ([row, col] of start_points) {
        var s = ''
        for (var i = row, j = col; i < 6 && j < 7; i++, j++)s += board[i][j];
        if (/RRRR/.test(s)) return 'R';
        if (/YYYY/.test(s)) return 'Y';
    }
    //right to left:
    start_points = [[0, 6], [0, 5], [0, 4], [0, 3], [1, 6], [2, 6]];
    for ([row, col] of start_points) {
        var s = ''
        for (var i = row, j = col; i < 6 && j > -1; i++, j--)s += board[i][j];
        if (/RRRR/.test(s)) return 'R';
        if (/YYYY/.test(s)) return 'Y';
    }
    if (a.findIndex(v => /\-/.test(v)) > -1) return 'in progress';
    return 'draw'
}

var board = [['-', '-', '-', '-', '-', '-', '-'],
['-', '-', '-', '-', '-', '-', '-'],
['-', '-', '-', 'R', 'R', 'R', 'R'],
['-', '-', '-', 'Y', 'Y', 'R', 'Y'],
['-', '-', '-', 'Y', 'R', 'Y', 'Y'],
['-', '-', 'Y', 'Y', 'R', 'R', 'R']];

connectFour(board)