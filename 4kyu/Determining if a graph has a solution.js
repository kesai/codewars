function solve_graph(start, end, arcs) {
    var paths = (arcs.filter(v => v.start === start) || []).map(v => [v.start, v.end]);
    while (true) {
        var new_paths = []
        for (path of paths) {
        	if(path[1]===end||path[0]===end)return true;
            var new_path = []
            var last = path.slice(-1)[0];
            var aviables = (arcs.filter(v => v.start === last) || []).map(v => v.end);
            for (p of aviables) {
                if (p === end) return true;
                if (path.includes(p)) continue;
                new_path = new_path.concat(path);
                new_path.push(p);
            }
            if (new_path.length) new_paths.push(new_path);
        }
        if (new_paths.length) paths = new_paths;
        else return false;
    }
}

var r = solve_graph('a', 'b', [{ start: 'a', end: 'b' }])
console.log(r)