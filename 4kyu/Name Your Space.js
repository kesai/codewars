function namespace(root, path, value) {
    var arr = path.split('.');
    if (arguments.length === 3) {
        var k = arr.pop();
        var o = {};
        o[k] = value;
        while (arr.length > 1) {
            k = arr.pop()
            var temp = o;
            o = {};
            o[k] = temp;
        }
        root[arr.pop()] = o;
    } else {
        var o = root[arr.shift()];
        if (!o) return undefined;
        while (arr.length) {
            if (!Object.keys(o).indexOf(k = arr.pop())) return undefined;
            o = o[k];
            if (!o) return undefined;
        }
        return o
    }
}
