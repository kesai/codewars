var s = ' abcdefghij';
function Cell(x, y) {
    this.x = x;
    this.y = y;
}
Cell.prototype.toStr = function () {
    return s[this.x] + (this.y % 10)
}
function get_check_cells(x, y, size) {
    var checks = []
    for (var i = 1; i <= size; i++) {
        checks.push(new Cell(i, y).toStr())
        checks.push(new Cell(x, i).toStr())
    }
    var moves = [[-1, 1], [1, 1], [-1, -1], [1, -1]];
    for ([dx, dy] of moves) {
        var [x1, y1] = [x, y];
        while (x1 > 0 && x1 <= size && y1 > 0 && y1 <= size) {
            checks.push(new Cell(x1, y1).toStr());
            x1 += dx; y1 += dy;
        }
    }
    return checks
}

function is_checked(a, b) {
    var dx = Math.abs(a.x - b.x);
    var dy = Math.abs(a.y - b.y);
    if (dx === 0 && dy === 1) return true;
    if (dx === 1 && dy === 0) return true;
    if (dx === 0 && dy === 0) { console.log('what?some cell'); return true; }
    if (dx === 1 && dy === 1) return true;
}



function queens(position, size) {
    var [x0, y0] = [s.indexOf(position[0]), +position[1]];
    if (y0 === 0) y0 = 10;
    var start_p = new Cell(x0, y0);
    var checks = get_check_cells(start_p.x, start_p.y, size)
    var paths = [{
        checked: checks,
        path: [start_p]
    }];
    //go right:

    for (var j = x0 + 1; j <= size; j++) {
        var new_paths = []
        for (p of paths) {
            var last = p.path.slice(-1)[0];
            for (var i = 1; i <= size; i++) {
                var cell = new Cell(last.x + 1, i);
                if (p.checked.indexOf(cell.toStr()) > -1) continue;
                if (!is_checked(last, cell)) {
                    var new_path = {};
                    var temp = [].concat(p.path);
                    temp.push(cell);
                    new_path.path = temp;
                    var temp1 = [].concat(p.checked);
                    temp1 = temp1.concat(get_check_cells(cell.x, cell.y, size));
                    new_path.checked = temp1;
                    new_paths.push(new_path);
                }
            }
        }
        paths = new_paths;
    }


    //go left:
    if (x0 > 1) {
        for (var j = x0 - 1; j > 0; j--) {
            var new_paths = []
            for (p of paths) {
                var first = p.path[0];
                for (var i = 1; i <= size; i++) {
                    var cell = new Cell(first.x - 1, i);
                    if (p.checked.indexOf(cell.toStr()) > -1) continue;
                    if (!is_checked(first, cell)) {
                        var new_path = {};
                        var temp = [].concat(p.path);
                        temp.unshift(cell);
                        new_path.path = temp;
                        var temp1 = [].concat(p.checked);
                        temp1 = temp1.concat(get_check_cells(cell.x, cell.y, size));
                        new_path.checked = temp1;
                        new_paths.push(new_path);
                    }
                }
            }
            paths = new_paths;
        }
    }
    var result = paths[0].path.map(v => v.toStr()).join(',');
    return result
}
console.log(queens('h5', 10))

