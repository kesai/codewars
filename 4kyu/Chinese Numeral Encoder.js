var numerals = {
    "-": "负",
    ".": "点",
    0: "零",
    1: "一",
    2: "二",
    3: "三",
    4: "四",
    5: "五",
    6: "六",
    7: "七",
    8: "八",
    9: "九",
    10: "十",
    100: "百",
    1000: "千",
    10000: "万"
};
function toChineseNumeral(num) {
    var sign = num < 0 ? '负' : '';
    num = Math.abs(num);
    var i = Math.floor(num);
    var decimals = ('' + num).split('.')[1] || '';
    var result = intToChinese(i);
    var d = ''
    for (x of decimals) d += numerals[x];
    if (d) result += '点' + d;
    return sign + result;
}
function intToChinese(num) {
    if (num < 10) return numerals[num]
    else if (num < 100) {
        var ten = parseInt(num / 10);
        var digit = num % 10;
        return (ten > 1 ? numerals[ten] : '') + '十' + (digit ? numerals[digit] : '')
    } else if (num < 1000) {
        var [hundred, ten, digit] = [...'' + num];
        if (ten === '0' && digit === '0') return numerals[hundred] + '百';
        else if (ten === '0') return numerals[hundred] + '百' + '零' + numerals[digit];
        else if (digit === '0') return numerals[hundred] + '百' + numerals[ten] + '十'
        else return numerals[hundred] + '百' + numerals[ten] + '十' + numerals[digit];
    } else if (num < 10000) {
        var [thousand, hundred, ten, digit] = [...'' + num];
        if (num % 1000 === 0) return numerals[thousand] + '千';
        else if (hundred === '0' && ten === '0' && digit != '0') {
            return numerals[thousand] + '千' + '零' + numerals[digit];
        } else if (hundred === '0' && digit === '0' && ten != '0') {
            return numerals[thousand] + '千' + '零' + numerals[ten] + '十';
        } else if (hundred != '0' && ten === '0' && digit === '0') {
            return numerals[thousand] + '千' + numerals[hundred] + '百';
        } else if (hundred != '0' && ten != '0' && digit === '0') {
            return numerals[thousand] + '千' + numerals[hundred] + '百' + numerals[ten] + '十';
        } else if (hundred != '0' && ten === '0' && digit !== '0') {
            return numerals[thousand] + '千' + numerals[hundred] + '百' + '零' + numerals[digit];
        }
        else if (hundred === '0' && ten != '0' && digit !== '0') {
            return numerals[thousand] + '千' + '零' + numerals[ten] + '十' + numerals[digit];
        }
        else {
            return numerals[thousand] + '千' + numerals[hundred] + '百' + numerals[ten] + '十' + numerals[digit];
        }
    } else {
        var [wan, thousand, hundred, ten, digit] = [...'' + num];
        if (num % 10000 === 0) return numerals[wan] + '万';
        else if (thousand != '0') return numerals[wan] + '万' + intToChinese(num % 10000);
        else return numerals[wan] + '万' + '零' + intToChinese(num % 1000);
    }
}
console.log(intToChinese(31000))
console.log(intToChinese(31400))
console.log(intToChinese(31050))
console.log(intToChinese(31006))
console.log(intToChinese(1305))
console.log(intToChinese(31305))
console.log(intToChinese(31045))
console.log(intToChinese(30045))
console.log(intToChinese(30004))
console.log(intToChinese(30040))
console.log(intToChinese(30105))

console.log(toChineseNumeral(-30105.434))
console.log(toChineseNumeral(-0.434))