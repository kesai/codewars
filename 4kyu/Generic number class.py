def create_number_class(alphabet):
    class MyNumber(object):
        base = len(alphabet)  # 表示多少进制
        chars = list(alphabet)  # 进制使用的字符集

        @classmethod
        def to_decimal(self, target):
            return sum(v*MyNumber.base**i for i, v in enumerate(target.indies))

        @classmethod
        def from_decimal(self, d):
            base = MyNumber.base
            result = ''
            while(d >= base):
                r = d % base
                d //= base
                result = MyNumber.chars[r]+result
            result = MyNumber.chars[d]+result
            return MyNumber(result)

        def __init__(self, value):
            self.value = value
            # 数值对应字符索引，从末位数起
            self.indies = list(
                map(lambda v: MyNumber.chars.index(v), list(value)[::-1]))

        def __add__(self, other):
            d = MyNumber.to_decimal(self)+MyNumber.to_decimal(other)
            return MyNumber.from_decimal(d)

        def __sub__(self, other):
            d = MyNumber.to_decimal(self)-MyNumber.to_decimal(other)
            return MyNumber.from_decimal(d)

        def __mul__(self, other):
            d = MyNumber.to_decimal(self)*MyNumber.to_decimal(other)
            return MyNumber.from_decimal(d)

        def __div__(self, other):
            d = MyNumber.to_decimal(self)/MyNumber.to_decimal(other)
            return MyNumber.from_decimal(d)

        def __floordiv__(self, other):
            d = MyNumber.to_decimal(self)//MyNumber.to_decimal(other)
            return MyNumber.from_decimal(d)

        def __eq__(self, other):
            self.value == other.value

        def __str__(self):
            return self.value

        def convert_to(self, to_type):
            d = MyNumber.to_decimal(self)
            return to_type.from_decimal(d)
    return MyNumber


BinClass = create_number_class('01')
Abcd = create_number_class('abcd')
x = BinClass('101')
print(x.convert_to(Abcd))
