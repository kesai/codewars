function encode(s) {
    if (!s) return ["", -1];
    var matirx = [...Array.from({length:s.length},(_,i)=> s.slice(-i)+s.slice(0,-i))].sort();
    return [matirx.map(v =>v.slice(-1)[0]).join(''), matirx.findIndex(v => v === s)]
}
function decode(s, i) {
    if (!s) return ''
    var column = [...s].sort();
    for (var k = 0; k < s.length - 1; k++)column = column.map((v, i) => s[i] + v).sort();
    return column[i]
}
