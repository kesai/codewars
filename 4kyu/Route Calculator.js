function calculate(sum) {
    if (/[^.+\-*$\d]/.test(sum)) return '400: Bad request';
    var items = sum.match(/[+\-*$]|(\d+(\.\d+)?)/g);
    var nums = [];
    var ops = [];
    var weight = { '+': 0, '-': 1, '*': 2, '$': 3 };
    var fn = { '+': (a, b) => a + b, '-': (a, b) => b - a, '*': (a, b) => a * b, '$': (a, b) => b / a }
    for (x of items) {
        if (/(\d+(\.\d+)?)/.test(x)) nums.push(+x);
        else {
            while (weight[ops.slice(-1)[0]] >= weight[x]) nums.push(fn[ops.pop()](nums.pop(), nums.pop()));
            ops.push(x);
        }
    }
    while (ops.length) nums.push(fn[ops.pop()](nums.pop(), nums.pop()));
    return nums.pop();
}