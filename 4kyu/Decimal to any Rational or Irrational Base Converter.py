from math import pi, log


def converter(n, decimals=0, base=pi):
    chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    if n < 0:
        return '-'+converter(-n, decimals, base)
    result = ''
    pow_max = math.floor(math.log(n, base)) if n else 0
    for p in range(pow_max, -decimals-1, -1):
        k = math.floor(n/(base**p))
        n -= k*base**p
        if p == -1:
            result += '.'
        result += chars[k]
    return result
