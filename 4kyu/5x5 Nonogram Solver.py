import re


class Nonogram:

    def __init__(self, clues):
        self.clues = clues
        self.matrix = [[0 for j in range(5)] for i in range(5)]

    def fill_with_pattern(self):
        for i, x in enumerate(self.clues[0]):
            if x == (1, 1, 1):
                self.matrix[0][i] = self.matrix[2][i] = self.matrix[4][i] = 1
                self.matrix[1][i] = self.matrix[3][i] = 'x'
            elif x == (1, 3):
                self.matrix[0][i] = self.matrix[2][i] = self.matrix[3][i] = self.matrix[4][i] = 1
                self.matrix[1][i] = 'x'
            elif x == (3, 1):
                self.matrix[0][i] = self.matrix[1][i] = self.matrix[2][i] = self.matrix[4][i] = 1
                self.matrix[3][i] = 'x'
            elif x == (2, 2):
                self.matrix[0][i] = self.matrix[1][i] = self.matrix[3][i] = self.matrix[4][i] = 1
                self.matrix[2][i] = 'x'
            elif x == (3,) or x == (3):
                self.matrix[2][i] = 1
            elif x == (4,) or x == (4):
                self.matrix[1][i] = self.matrix[2][i] = self.matrix[3][i] = 1
        for i, x in enumerate(self.clues[1]):
            if x == (1, 1, 1):
                self.matrix[i][0] = self.matrix[i][2] = self.matrix[i][4] = 1
                self.matrix[i][1] = self.matrix[i][3] = 'x'
            elif x == (2, 2):
                self.matrix[i][0] = self.matrix[i][1] = self.matrix[i][3] = self.matrix[i][4] = 1
                self.matrix[i][2] = 'x'
            elif x == (1, 3):
                self.matrix[i][0] = self.matrix[i][2] = self.matrix[i][3] = self.matrix[i][4] = 1
                self.matrix[i][1] = 'x'
            elif x == (3, 1):
                self.matrix[i][0] = self.matrix[i][1] = self.matrix[i][2] = self.matrix[i][4] = 1
                self.matrix[i][3] = 'x'
            elif x == (4,) or x == (4):
                self.matrix[i][1] = self.matrix[i][2] = self.matrix[i][3] = 1
            elif x == (3,) or x == (3):
                self.matrix[i][2] = 1

    def valid_line(self, line, rule):
        return tuple(map(lambda x: len(x),
                         re.findall(r'1+', ''.join(map(str, line))))) == rule

    def solve(self):
        self.fill_with_pattern()
        for i, x in enumerate(self.clues[0]):
            line = [row[i] for row in self.matrix]
            if self.valid_line(line, x):
                for row in self.matrix:
                    if row[i] == 0:
                        row[i] = 'x'
        for i, x in enumerate(self.clues[1]):
            line = self.matrix[i]
            if self.valid_line(line, x):
                for cell in line:
                    if cell == 0:
                        cell = 'x'

        print(self.matrix)


clues = (((1, 1), (4,), (1, 1, 1), (3,), (1,)),
         ((1,), (2,), (3,), (2, 1), (4,)))
Nonogram(clues).solve()
