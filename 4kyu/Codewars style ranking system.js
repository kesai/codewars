class User {
    constructor() {
        this.rank = -8;
        this.progress = 0;
    }
    incProgress(level) {
        if (level > 8 || level < -8 || level === 0) throw 'invalid activity'
        var levels = [-8, -7, -6, -5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 6, 7, 8]
        var d = levels.indexOf(level) - levels.indexOf(this.rank);
        var added = d > 0 ? 10 * d * d : d === 0 ? 3 : d === -1 ? 1 : 0;
        var up_grades = parseInt((this.progress + added) / 100);
        for (var i = 1; i <= up_grades; i++)this.rank = (this.rank === -1 ? 1 : this.rank + 1);
        this.progress = (this.progress + added) % 100;
        if (this.rank >= 8) { this.rank = 8; this.progress = 0; }
    }
}