var ELEMENTS = {
    H: 'Hydrogen',
    He: 'Helium',
    Li: 'Lithium',
    Be: 'Beryllium',
    B: 'Boron',
    C: 'Carbon',
    N: 'Nitrogen',
    O: 'Oxygen',
    F: 'Fluorine',
    Ne: 'Neon',
    Na: 'Sodium',
    Mg: 'Magnesium',
    Al: 'Aluminium',
    Si: 'Silicon',
    P: 'Phosphorus',
    S: 'Sulfur',
    Cl: 'Chlorine',
    Ar: 'Argon',
    K: 'Potassium',
    Ca: 'Calcium',
    Sc: 'Scandium',
    Ti: 'Titanium',
    V: 'Vanadium',
    Cr: 'Chromium',
    Mn: 'Manganese',
    Fe: 'Iron',
    Co: 'Cobalt',
    Ni: 'Nickel',
    Cu: 'Copper',
    Zn: 'Zinc',
    Ga: 'Gallium',
    Ge: 'Germanium',
    As: 'Arsenic',
    Se: 'Selenium',
    Br: 'Bromine',
    Kr: 'Krypton',
    Rb: 'Rubidium',
    Sr: 'Strontium',
    Y: 'Yttrium',
    Zr: 'Zirconium',
    Nb: 'Niobium',
    Mo: 'Molybdenum',
    Tc: 'Technetium',
    Ru: 'Ruthenium',
    Rh: 'Rhodium',
    Pd: 'Palladium',
    Ag: 'Silver',
    Cd: 'Cadmium',
    In: 'Indium',
    Sn: 'Tin',
    Sb: 'Antimony',
    Te: 'Tellurium',
    I: 'Iodine',
    Xe: 'Xenon',
    Cs: 'Caesium',
    Ba: 'Barium',
    Hf: 'Hafnium',
    Ta: 'Tantalum',
    W: 'Tungsten',
    Re: 'Rhenium',
    Os: 'Osmium',
    Ir: 'Iridium',
    Pt: 'Platinum',
    Au: 'Gold',
    Hg: 'Mercury',
    Tl: 'Thallium',
    Pb: 'Lead',
    Bi: 'Bismuth',
    Po: 'Polonium',
    At: 'Astatine',
    Rn: 'Radon',
    Fr: 'Francium',
    Ra: 'Radium',
    Rf: 'Rutherfordium',
    Db: 'Dubnium',
    Sg: 'Seaborgium',
    Bh: 'Bohrium',
    Hs: 'Hassium',
    Mt: 'Meitnerium',
    Ds: 'Darmstadtium',
    Rg: 'Roentgenium',
    Cn: 'Copernicium',
    Uut: 'Ununtrium',
    Fl: 'Flerovium',
    Uup: 'Ununpentium',
    Lv: 'Livermorium',
    Uus: 'Ununseptium',
    Uuo: 'Ununoctium',
    La: 'Lanthanum',
    Ce: 'Cerium',
    Pr: 'Praseodymium',
    Nd: 'Neodymium',
    Pm: 'Promethium',
    Sm: 'Samarium',
    Eu: 'Europium',
    Gd: 'Gadolinium',
    Tb: 'Terbium',
    Dy: 'Dysprosium',
    Ho: 'Holmium',
    Er: 'Erbium',
    Tm: 'Thulium',
    Yb: 'Ytterbium',
    Lu: 'Lutetium',
    Ac: 'Actinium',
    Th: 'Thorium',
    Pa: 'Protactinium',
    U: 'Uranium',
    Np: 'Neptunium',
    Pu: 'Plutonium',
    Am: 'Americium',
    Cm: 'Curium',
    Bk: 'Berkelium',
    Cf: 'Californium',
    Es: 'Einsteinium',
    Fm: 'Fermium',
    Md: 'Mendelevium',
    No: 'Nobelium',
    Lr: 'Lawrencium'
}

const capitalize = x => x ? x[0].toUpperCase() + x.slice(1).toLowerCase() : x;

function elementalForms(word) {
    word = capitalize(word);
    var matched = Object.keys(ELEMENTS).filter(v => word.indexOf(v) === 0) || [];
    if (!matched.length) return [];
    var paths = matched.map(v => {
        return {
            'elements': [v],
            'rest': capitalize(word.replace(v, ''))
        }
    })
    var out = []
    while (paths.length) {
        var new_paths = [];
        for (path of paths) {
            if (path.rest === '') { out.push(path.elements); continue; }
            var matched = Object.keys(ELEMENTS).filter(v => path.rest.indexOf(v.trim()) === 0) || [];
            if (matched.length) {
                for (v of matched) {
                    var new_path = {}
                    new_path.elements = [].concat(path.elements);
                    new_path.elements.push(v);
                    new_path.rest = capitalize(path.rest.replace(v, ''));
                    if (new_path.rest === '') out.push(new_path.elements); else new_paths.push(new_path);
                }
            }
        }
        paths = new_paths;
    }
    return out.map(v => v.map(e => `${ELEMENTS[e]} (${e})`));
}
elementalForms('biblical')