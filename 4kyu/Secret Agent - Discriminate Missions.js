function newMission(action, target) {
    if (['murder', 'kill', 'torture'].indexOf(action) !== -1) return false;
    if (goodPeople.indexOf(target) !== -1) return false;
    if (newMission.caller.toString().indexOf('secret') !== -1) return false
    if (new Error().stack.indexOf('EvilGovernment') !== -1) return false;
    return true;
}