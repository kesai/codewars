// https://stackoverflow.com/questions/36040233/total-amount-of-increasing-and-decreasing-numbers-in-a-range
function totalIncDec(x) {
    if (x === 0) return 1;
    var arr = [Array.from({ length: 10 }, (_, i) => i + 1)];
    for (var i = 1; i < x; i++) {
        var last = arr[arr.length - 1]
        var row = [1];
        for (var j = 1; j < 10; j++) {
            row.push(row[j - 1] + last[j])
        }
        arr.push(row)
    }
    return arr.reduce((s, v) => s += v[9], 0) + arr[arr.length - 1][9] - 10 * x
}