function converter(n, decimals = 0, base = Math.PI) {
    if (n < 0) return '-' + converter(-n, decimals, base);
    var pow_max = Math.max(0, Math.floor(Math.log(n) / Math.log(base)));
    var result = '';
    for (var p = pow_max; p >= -decimals; p--) {
        var k = Math.floor(n / (base ** p))
        n -= k * base ** p;
        if (p === -1) result += '.'
        result += k.toString(36);
    }
    return result.toUpperCase();
}

