var fixImage = function (statement, image) {
    var fn = { 'multiply': (a, b) => a / b, 'plus': (a, b) => a - b, 'divide': (a, b) => a * b, 'minus': (a, b) => a + b }
    var colors = statement.match(/Green|Red|Blue/g);
    colors = colors ? colors.map(v => v[0].toLowerCase()) : ['r', 'g', 'b'];
    var opers = statement.match(/(multiply|plus|divide|minus) \d+/g).reverse();
    function fix(cell) {
        var res = new pixel(cell.r, cell.g, cell.b)
        for (p of opers) {
            var [op, k] = p.split(' ');
            for (c of colors) res[c] = fn[op](res[c], +k);
        }
        res.r = Math.round(res.r);
        res.g = Math.round(res.g);
        res.b = Math.round(res.b);
        return res;
    }
    return image.map(row => row.map(fix))
}
