var Result = { "win": 1, "loss": 2, "tie": 3 }
var CARD = "23456789TJQKA"

function PokerHand(hand) {
    var cards = hand.split(' ')
    var values = cards.map(v => v[0]).sort((a, b) => CARD.indexOf(a) - CARD.indexOf(b));
    var suits = cards.map(v => v[1]);
    var is_flush = new Set(suits).size === 1;
    var is_straight = CARD.includes(values.join(''));
    var sum = values.map(v => values.filter(x => x === v).length).reduce((s, v) => s += v, 0);
    this.score = 2 * sum + (is_flush ? 14 : 0) + (is_straight ? 13 : 0);
    this.values = values.map(v => CARD.indexOf(v)).reverse();

}

PokerHand.prototype.compareWith = function (opponent) {
    console.log(this.values, opponent.values)
    if (this.score > opponent.score) return Result.win;
    if (this.score < opponent.score) return Result.loss;
    for (var i = 0; i < this.values.length; i++) {
        if (this.values[i] > opponent.values[i]) return Result.win;
        if (this.values[i] < opponent.values[i]) return Result.loss;
    }
    return Result.tie;
}

var player = new PokerHand("AS AD AC AH JD")
var opponent = new PokerHand("KS AS TS QS JS")
console.log(player.compareWith(opponent))

