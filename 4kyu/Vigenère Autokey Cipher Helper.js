function VigenèreCipher(key, abc, l = abc.length) {
    this.encode = function (str, full_key = key.padEnd(str.length, key)) {
        return [...str].map((v, i) => abc.includes(v) ? abc[(abc.indexOf(v) + abc.indexOf(full_key[i])) % l] : v).join('')
    }
    this.decode = function (str, full_key = key.padEnd(str.length, key)) {
        return [...str].map((v, i) => abc.includes(v) ? abc[(abc.indexOf(v) - abc.indexOf(full_key[i]) + l) % l] : v).join('')
    };
}
