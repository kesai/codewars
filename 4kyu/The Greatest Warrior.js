const MAX_EXP = 10000;
const rank_by_level = (level) => level / 10 | 0;
const Ranks = ["Pushover", "Novice", "Fighter", "Warrior", "Veteran", "Sage", "Elite", "Conqueror", "Champion", "Master", "Greatest"];
class Warrior {
    constructor() {
        this._rank = 0;//'Pushover'
        this._experience = 100;
        this._achievements = [];
    }
    level() { return parseInt(this._experience / 100); }
    experience() { return this._experience; }
    rank() { return Ranks[this._rank]; }
    achievements() { return this._achievements; }
    training([desc, exp_points, min_level]) {
        if (this.level() >= min_level) {
            this._experience = Math.min(this._experience + exp_points, MAX_EXP);
            this._rank = rank_by_level(this.level())
            this._achievements.push(desc);
            return desc;
        } else
            return 'Not strong enough';
    }
    battle(enemy_level) {
        if (!Number.isInteger(enemy_level) || enemy_level > 100 || enemy_level < 1) return 'Invalid level';
        var enemy_rank = rank_by_level(enemy_level);
        var diff = enemy_level - this.level();
        if (enemy_rank - this._rank > 0 && diff >= 5) return `You've been defeated`;
        var added_exp = diff > 0 ? 20 * diff ** 2 : diff === -1 ? 5 : diff === 0 ? 10 : 0;
        this._experience = Math.min(this._experience + added_exp, MAX_EXP);
        this._rank = rank_by_level(this.level())
        return diff <= -2 ? 'Easy fight' : diff <= 0 ? 'A good fight' : 'An intense fight';
    }
}
var Goku = new Warrior();
console.log(Goku.level())
console.log(Goku.rank())
console.log(Goku.achievements())
console.log(Goku.training(["Do ten push-ups", 95, 1]))
console.log(Goku.level())
console.log(Goku.battle(0))
console.log(Goku.battle(1))
console.log(Goku.level())
console.log(Goku.rank())
console.log(Goku.battle(3))
console.log(Goku.training(["Survive one night at the Forest of Death", 170, 2]))
console.log(Goku.training(["Mastered the Spirit Bomb", 1580, 10]))
console.log(Goku.achievements())
console.log(Goku.battle(2))
console.log(Goku.level())
console.log(Goku.experience())
console.log(Goku.battle(9))
console.log(Goku.battle(14))