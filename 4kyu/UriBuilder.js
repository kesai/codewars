const qs = require('qs')
class UriBuilder {
  constructor(uri) {
    let [base, queryString] = uri.split('?')
    this.base = base
    this.params = qs.parse(queryString)
  }
  build() {
    return `${this.base}?${qs.stringify(this.params)}`
  }
}
