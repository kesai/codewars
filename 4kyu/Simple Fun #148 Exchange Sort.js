function exchangeSort(input) {
    var count = 0;
    var sorted = [].concat(input).sort((a, b) => a - b);
    for (var i = 0; i < input.length; i++) {
        var a = input[i];
        var b = sorted[i];
        if (a !== b) {
            var index = -1;
            for (var j = i + 1; j < input.length; j++) {
                if (input[j] === b && sorted[j] === a) {
                    input[i] = 'x';
                    input[j] = 'x'
                    sorted[i] = 'x'
                    sorted[j] = 'x'
                    count++;
                    break;
                }
            }
        } else {
            input[i] = 'x';
            sorted[i] = 'x';
        }
    }
    var arr = input.map((v, i) => [v, sorted[i]]).filter(v => v[0] != v[1]);
    sorted = arr.map(v => v[0]);
    input = arr.map(v => v[1])
    for (var i = 0; i < input.length; i++) {
        var a = input[i];
        var b = sorted[i];
        if (a !== 'x' && a != b) {
            for (var j = i + 1; j < input.length; j++) {
                if (input[j] === b) {
                    input[i] = b;
                    input[j] = a;
                    count++;
                    break;
                }
            }
        }
    }
    return count;
}
