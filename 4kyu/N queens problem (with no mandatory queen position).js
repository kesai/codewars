// Credits: https://en.wikipedia.org/wiki/Eight_queens_puzzle#Existence_of_solutions
function nQueen(N) {
    var r = N % 6;
    var nums = Array.from({ length: N }, (_, i) => i + 1);
    var odds = nums.filter(v => v % 2)
    var evens = nums.filter(v => v % 2 === 0)
    if (N === 2 || N === 3) return []
    if (r === 2) {
        //swap 1,3 in odd list,the index is 0,1,so:
        odds[0] = 3;
        odds[1] = 1;
        //remove 5 which index=2
        odds.splice(2, 1);
        //add 5 to end of odd list
        odds.push(5);
        //Append odd list to the even list
        return evens.concat(odds).map(v => v - 1);

    } else if (r === 3) {
        //move 2 to the end of even list
        evens.push(evens.shift());
        //move 1,3 to the end of odd list
        odds.push(odds.shift(), odds.shift());
        //Append odd list to the even list
        return evens.concat(odds).map(v => v - 1);
    } else {
        return (evens.concat(odds)).map(v => v - 1);
    }
}

