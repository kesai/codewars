function toAndFrom(a, b, t) {
    var d = Math.abs(a - b);
    t = t % (2 * d);
    return b > a ? (d >= t ? a + t : 2 * b - t - a) : (d >= t ? a - t : 2 * b - a + t)
}