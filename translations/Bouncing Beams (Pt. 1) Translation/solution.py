
def exit_from_maze(board):
    moves = {'right': [1, 0], 'left': [-1, 0], 'up': [0, -1], 'down': [0, 1]}
    y = [i for i, v in enumerate(board) if '*' in v][0]
    x = board[y].index('*')
    mirrors = {
        '/': {'right': 'up', 'left': 'down', 'down': 'left', 'up': 'right'},
        '\\': {'right': 'down', 'left': 'up', 'down': 'right', 'up': 'left'}
    }

    def init_direction():
        if x == 0:
            return 'right'
        elif y == 0:
            return 'down'
        elif y == len(board)-1:
            return 'up'
        else:
            return 'left'
    d = init_direction()
    dist = 0
    while board[y][x] != '#':
        l = board[y][x]
        if l in '/\\':
            d = mirrors[l][d]
        x += moves[d][0]
        y += moves[d][1]
        dist += 1
    return {'position': [x, y], 'distance': dist - 1}

