@test.describe('Basic mazes')
def example_tests():
    @test.it('Tests a sample maze')
    def example_test_case():
        board = [
            "##############",
            "#        \\   #",
            "*   \\        #",
            "#            #",
            "#   \\    /   #",
            "##############"]
        res = {
            'position': [0, 1],
            'distance': 22
        }
        test.assert_equals(exit_from_maze(board), res)
