//ref:https://gist.github.com/pfgallagher/80a4838f46fb364af447ac47129f438c#file-fizzbuzzfinal-js
//ref:https://medium.com/@pfgallagher/my-quest-for-the-worlds-shortest-javascript-fizzbuzz-d0d169f753d2
fizzBuzz=(a,b,x,y)=>[...Array(b-a+1)].map(i=>(a%x?'':'Fizz')+(a++%y?'':'Buzz')||a-1)
console.log(fizzBuzz(1, 10,2,4))

