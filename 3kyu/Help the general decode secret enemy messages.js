device.decode = function (w) {
    var s = ''
    for (var i = 0; i < w.length; i++) {
        var c = w[i];
        var chars = 'bdhpF,82QsLirJejtNmzZKgnB3SwTyXG ?.6YIcflxVC5WE94UA1OoD70MkvRuPqHa!@#$%^&*()_+-';
        for (x of chars) {
            var temp = device.encode(x.repeat(i + 1));
            if (temp[i] === c) { s += x; break; }
        }
    }
    return s;
}
