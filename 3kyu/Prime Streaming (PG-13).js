function isPrime(num) {
    if (num < 2) return false;
    if (num === 2 || num === 3) return true;
    if (num % 6 != 1 && num % 6 != 5) return false
    var n = parseInt(Math.sqrt(num));
    for (var i = 5; i < n + 1; i += 6) {
        if (num % i === 0 || num % (i + 2) === 0) return false
    }
    return true
}

class Primes {
    static * stream() {
        var p = 0;
        while (true) {
            if (isPrime(p)) yield p;
            p++;
        }
    }
}
var p = Primes.stream();
var x = p.next()
for (var i = 0; i < 1000000; i++) {
    x = p.next();
}
console.log(x)
