class Point:
    def __init__(self, x, y, prev=None):
        self.x = x
        self.y = y
        self.prev = prev


def find_shortest_path(grid, start_node, end_node):

    if not grid:
        return []
    X = len(grid)
    Y = len(grid[0])
    print(grid[0][0])

    def is_valid(x, y):
        return x > -1 and y > -1 and x < X and y < Y

    visited = [[False for i in range(X)] for j in range(Y)]
    moves = [(-1, 0), (1, 0), (0, -1), (0, 1)]
    st = Point(start_node.position.x, start_node.position.y)
    ed = Point(end_node.position.x, end_node.position.y)
    visited[st.y][st.x] = True
    queue = [st]
    last = None
    while queue:
        curr = queue.pop(0)
        if curr.x == ed.x and curr.y == ed.y:
            print('reach target')
            last = curr
            break
        for mov in moves:
            x = curr.x+mov[0]
            y = curr.y+mov[1]
            if is_valid(x, y) and not visited[y][x] and grid[x][y].passable:
                queue.append(Point(x, y, curr))
                visited[y][x] = True
    result = []
    while last:
        result.append(grid[last.x][last.y])
        last = last.prev
    return list(reversed(result))
