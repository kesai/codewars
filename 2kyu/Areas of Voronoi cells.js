// Find the areas of the Voronoi cells associated with the Point objects in the array p.
class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
var dist = (p1, p2) => Math.sqrt((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2);

function voronoi_areas(p) {
    var min_x = Math.min(...p.map(v => v.x));
    var max_x = Math.max(...p.map(v => v.x));
    var min_y = Math.min(...p.map(v => v.y));
    var max_y = Math.max(...p.map(v => v.y));
    var total_area = (max_x - min_x) * (max_y - min_y);
    var step = 0.05;
    var counts = p.map(v => 0);
    var max_dists = p.map(v => 0);
    var n = 0;
    for (var x = min_x; x <= max_x; x += step) {
        for (var y = min_y; y <= max_y; y += step) {
            var temp = new Point(x, y);
            var arr = p.map((v, i) => [i, dist(v, temp)]).sort((a, b) => a[1] - b[1]);
            var min_s = arr[0][1];

            arr = arr.filter(v => v[1] === min_s);
            if(arr.length>1)continue;
            var [i,s]=arr[0]
            counts[i]++;
            max_dists[i] = Math.max(max_dists[i], s)
            /*if (arr.length > 1) console.log(arr)
            for ([i, s] of arr) {
                counts[i]++;
                max_dists[i] = Math.max(max_dists[i], s);
            }*/
            n++;
        }
    }

    function is_infinity(i) {
        var t = p[i];
        var temp = Math.min(...[Math.abs(t.x - min_x), Math.abs(t.x - max_x), Math.abs(t.y - min_y), Math.abs(t.y - max_y)])
        if (max_dists[i] >= temp) return true;
        return false;
    }
    return counts.map((v, i) => is_infinity(i) ? -1 : v * total_area / n)
}

voronoi_areas([new Point(0, 0),
new Point(2, 0),
new Point(-2, 0),
new Point(0, 2),
new Point(0, -2)
])