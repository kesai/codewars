function numberOfCarries(a, b) {
    var arr1 = [...'' + a].reverse();
    var arr2 = [...'' + b].reverse();
    var added = 0;
    var out = [];
    var count = 0
    for (var i = 0; i < Math.max(arr1.length, arr2.length); i++) {
        if (added) count++;
        var sum = +(arr1[i] || 0) + +(arr2[i] || 0) + added;
        added = parseInt(sum / 10);
        out.push(sum % 10);
    }
    if (added) {out.push(added);count++;}
    return count
}
