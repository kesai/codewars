function simplestProblem(ratings) {
    var easiest = ratings[0].map(v => 0);
    var hardest = ratings[0].map(v => 0);
    for (member of ratings) {
        var a = Math.min(...member);
        var b = Math.max(...member);
        for (var i = 0; i < member.length; i++) {
            if (member[i] === a) easiest[i]++;
            if (member[i] === b) hardest[i]++;
        }
    }
    var indexes = easiest.map((v, i) => [i, v]).filter(v => v[1] > ratings.length / 2).map(v => [v[0], hardest[v[0]]])
    return indexes.filter(v => v[1] === 0).map(v => v[0]);
}
