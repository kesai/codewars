var caches = []
for (var i = 2; i < 99; i++) {
    for (var j = 2; j < 42; j++) {
        var k = i ** j;
        if (i === [...'' + k].reduce((s, v) => s += +v, 0)) caches.push(k)
    }
}
caches.sort((a, b) => a - b)
function powerSumDigTerm(n) {
    return caches[n - 1]
}
console.log(caches)