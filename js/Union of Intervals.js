intervalInsert = function(myl, interval) {
    var sets = myl.map(v => new Set(Array.from({ length: v[1] - v[0] + 1 }, (_, i) => i + v[0])));
    var b = new Set(Array.from({ length: interval[1] - interval[0] + 1 }, (_, i) => i + interval[0]))
    sets.push(b);

    function existIntersect() {
        for (var i = 0; i < sets.length; i++) {
            for (var j = i + 1; j < sets.length; j++) {
                let a = sets[i];
                let b = sets[j];
                let intersect = new Set([...a].filter(x => b.has(x)));
                if (intersect.size) {
                    return true;
                }
            }
        }
    }
    var count = 0;
    while (existIntersect()) {
        for (var i = 0; i < sets.length; i++) {
            for (var j = i + 1; j < sets.length; j++) {
                let a = sets[i];
                let b = sets[j];
                let intersect = new Set([...a].filter(x => b.has(x)));
                if (intersect.size) {
                    let union = new Set([...a, ...b]);
                    sets.splice(i, 1, union)
                    sets.splice(j, 1)
                }
            }
        }

    }
    return sets.map(v => [Math.min(...v), Math.max(...v)]).sort((a,b)=>a[0]-b[0])
}

intervalInsert([
    [0, 2],
    [3, 6],
    [7, 7],
    [9, 12]
], [1, 8])