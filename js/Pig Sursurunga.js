function sursurungal(txt) {
    return txt.replace(/(\d+) (\w+)/g, (_, count, unit) => {
        count = +count;
        unit = count > 1 ? unit.slice(0, -1) : unit;
        unit = count <= 1 ? unit : count === 2 ? `bu${unit}` : count > 9 ? `ga${unit}ga` : `${unit}zo`
        return `${count} ${unit}`
    })
}
