function wordStep(str) {
    var words = str.split(' ')
    var verticals = words.filter((v, i) => i % 2);
    var horizonals = words.filter((v, i) => i % 2 === 0)
    var width = horizonals.join('').length - horizonals.length + 1;
    var length = verticals.join('').length - verticals.length + 1;
    var k = 0;
    var out = []
    for (w of verticals) {
        for (var i = k > 0 ? 1 : 0; i < w.length; i++) {
            if (i === 0 || i === w.length - 1) {
                var x = horizonals.slice(0, k).join('').length - k;
                if (horizonals[k]) {
                    var temp = ' '.repeat(x) + horizonals[k] + ' '.repeat(width - x - horizonals[k].length);
                    out.push(temp.split(''))
                } else {
                    var temp = ' '.repeat(x) + w[i] + ' '.repeat(width - x - 1);
                    out.push(temp.split(''))
                }
                k++;
            } else {
                var x = horizonals.slice(0, k).join('').length - k;
                var temp = ' '.repeat(x) + w[i] + ' '.repeat(width - x - 1);
                out.push(temp.split(''))
            }
        }
    }
    return out
}