function badApples(input) {
    input = input.filter(v => v[0] + v[1] > 0);
    var out = [];
    for (var i = 0; i < input.length; i++) {
        if (input[i].includes(0)) {
            var foundRepl = false;
            if (input[i][0] + input[i][1] === 0) continue;
            for (var j = i + 1; j < input.length; j++) {
                if (input[j].includes(0)) {
                    var x = input[j][0] + input[j][1];
                    if (x === 0) continue;
                    input[i] = input[i][0] ? [input[i][0], x] : [input[i][1], x]
                    input[j] = [0, 0]
                    foundRepl = true;
                    break;
                }
            }
            if (!foundRepl) input[i] = [0, 0]
        } //else out.push(pkg)
    }
    return input.filter(v => v[0] + v[1] != 0);
}
