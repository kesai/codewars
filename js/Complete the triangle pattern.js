function makeTriangle(m, n) {
    var N = n - m + 1;
    var size = (Math.sqrt(1 + 8 * N) - 1) / 2;
    if (!Number.isInteger(size)) return ''
    var arr = Array.from({ length: size }, (_, i) => []);

    function triangle1(start = 0, end = size) {
        for (var i = start; i < end; i++) {
            //the bottom edge
            if (i == end - 1) {
                var temp = []
                for (var j = 0; j < end - start; j++) {
                    temp.splice(0, 0, m % 10);
                    m++;
                }
                arr[i] = arr[i].slice(0, arr[i].length / 2 | 0).concat(temp).concat(arr[i].slice(arr[i].length / 2 | 0))
            } else {
                //insert into middle
                arr[i].splice(arr[i].length / 2 | 0, 0, m % 10)
                m++;
            }
        }
        for (var i = end - 2; i > start; i--) {
            arr[i].splice(arr[i].length / 2 | 0, 0, m % 10)
            m++;
        }
    }
    var d = 0
    for (var i = 0; m <= n; i += 2) {
        triangle1(i, size - d);
        d++;
    }
    return arr.map((v, i) => ' '.repeat(size - i - 1) + v.join(' ')).join('\n')
}
