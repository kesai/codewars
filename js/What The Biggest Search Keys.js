function theBiggestSearchKeys(...args) {
    if (args.length) {
        var max = Math.max(...args.map(v => v.length));
        return args.filter(v => v.length === max).sort().map(v=>`'${v}'`).join(', ');
    }
    return "''"
}