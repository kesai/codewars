function unusualSort(arr) {
    var cmp = (a, b) => (+a === +b) ? (typeof(a) === 'string' ? 1 : -1) : +a - +b;
    return arr.filter(v => /[a-z]/gi.test(v)).sort().concat(arr.filter(v => /\d/.test(v)).sort(
        cmp))
}