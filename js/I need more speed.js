function reverse(arr) {
    var head = 0;
    var tail = arr.length - 1;
    while (head < arr.length / 2) {
        var temp = arr[head];
        arr[head] = arr[tail];
        arr[tail] = temp;
        head++;
        tail--;
    }
    return arr;
}

console.log(reverse([1, 2, 3, 4]))
console.log(reverse([1, 2, 3, 4, 5]))
console.log(reverse([1, 2, 3, 4, 5, 6, 7, 8, 9]))