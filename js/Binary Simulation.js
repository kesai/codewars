function binarySimulation(s, q) {
    var out = []
    var k = 0;
    for (x of q) {
        if (x[0] === 'Q') {
            out.push(s[x[1] - 1])
        } else {
            var i = x[1] - 1;
            var j = x[2] - 1;
            s = s.slice(0, i) + s.slice(i, j + 1).replace(/(0+)|(1+)/g, (v) => +v[0] ? '0'.repeat(v.length) : '1'.repeat(v.length)) + s.slice(j + 1)
        }
    }
    return out;
}
binarySimulation("0011001100", [
    ['I', 1, 10],
    ['I', 2, 7],
    ['Q', 2],
    ['Q', 1],
    ['Q', 7],
    ['Q', 5]
])