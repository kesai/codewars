function lookAndSaySequence(firstElement, n) {
    var f = (x) => x.replace(/(.)\1*/g, v => `${v.length}${v[0]}`)
    for (var i = 1; i < n; i++) firstElement = f(firstElement);
    return firstElement;
}