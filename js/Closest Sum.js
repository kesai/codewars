const closestSum = (ints, num) => {
    var d = Infinity;
    var out = null;
    for (var i = 0; i < ints.length; i++) {
        for (var j = i + 1; j < ints.length; j++) {
            for (var k = j + 1; k < ints.length; k++) {
                var x = ints[i] + ints[j] + ints[k];
                if (d > Math.abs(num - x)) { d = Math.abs(num - x);
                    out = x; }
            }
        }
    }
    return out;
}

closestSum([-1, 2, 1, -4], 1)
