function convertRecipe(recipe) {
    return recipe.replace(/([\d\/]+)\s(tbsp|tsp)/g, (x, val, unit) => `${x} (${Math.ceil((unit === 'tbsp' ? 15 : 5) * (eval(val)))}g)`)
}