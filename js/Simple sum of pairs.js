var f = (x) => [...'' + x].reduce((s, v) => s += +v, 0);

function solve(n) {
    var x = +'9'.repeat(('' + n).length - 1);
    var y = n - x;
    return f(x) + f(y)
}