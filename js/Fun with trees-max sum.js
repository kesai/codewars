var TreeNode = function(value, left, right) {
    this.value = value;
    this.left = left;
    this.right = right;
};

function maxSum(root) {
    function calc(root) {
        var paths = [];
        if (root == null) return paths;
        if (root.left == null && root.right == null) paths.push(root.value);
        var left_paths = calc(root.left);
        var right_paths = calc(root.right);
        for (p of left_paths) paths.push(root.value + p)
        for (p of right_paths) paths.push(root.value + p)
        return paths;
    }
    return Math.max(...calc(root).concat([0]))
}
/**
 *      5
 *    /   \
 *  -22    11
 *  / \    / \
 * 9  50  9   2
 */
var root = new TreeNode(5, new TreeNode(-22, new TreeNode(9), new TreeNode(50)), new TreeNode(11, new TreeNode(9), new TreeNode(2)));
console.log(maxSum(root))