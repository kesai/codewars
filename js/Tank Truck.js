function tankvol(h, d, vt) {
    var r = d / 2;
    function little(h) {
        var length = vt / (Math.PI * r * r);
        var t_height = r - h;
        t_area = t_height * r * Math.sin(Math.acos(t_height / r));
        c_area = r * r * Math.acos(t_height / r)
        var area = c_area - t_area;
        return area * length;
    }
    return h < r ? Math.floor(little(h)) : h === r ? Math.floor(vt / 2) : Math.floor(vt - little(d - h));
}