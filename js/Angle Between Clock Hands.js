function handAngle(d) {
    var h = d.getHours() % 12;
    var m = d.getMinutes();
    var angle = Math.abs(30 * h + m / 2 - 6 * m)
    if (angle > 180) angle = 360 - angle;
    return Math.PI * angle / 180
}

handAngle(new Date())