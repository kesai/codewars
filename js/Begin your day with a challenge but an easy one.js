function oneTwoThree(n) {
    var nine_cnt = n / 9 | 0;
    var rest = n % 9;
    return  n?[`${'9'.repeat(nine_cnt)}${rest?rest:''}`, '1'.repeat(n)]:['0','0']
}