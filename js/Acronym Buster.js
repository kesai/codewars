function acronymBuster(string) {
    var acronyms = {
        'KPI': 'key performance indicators',
        'EOD': 'the end of the day',
        'TBD': "to be decided",
        'WAH': "work at home",
        'IAM': "in a meeting",
        'OOO': "out of office",
        'NRN': "no reply necessary",
        'CTA': "call to action",
        'SWOT': "strengths, weaknesses, opportunities and threats"
    }
    string = string.replace(/[A-Z]{3,}/g, v => acronyms[v] || v);
    return /[A-Z]{3,}/g.test(string) ? `${string.match(/[A-Z]{3,}/g)[0]} is an acronym. I do not like acronyms. Please remove them from your email.` : string.replace(/^\w|(?<=\.\s)\w/,v=>v.toUpperCase());
}

