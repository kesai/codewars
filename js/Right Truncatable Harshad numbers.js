const isValid = (n) => {
    var x = n;
    if (x < 10) { return false; }
    var sum = [...'' + x].reduce((s, v) => s += +v, 0)
    while (x > 0) {
        if (x % sum) return false;
        sum -= x % 10;
        x = parseInt(x / 10)
    }
    return true;
}

function rthnBetween(a, b) {
    var out = []
    for (var i = a; i <= b; i++)
        if (isValid(i)) out.push(i)
    return out;
}

console.log(rthnBetween(10, 100))