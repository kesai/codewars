function palindromize(number) {
    var isPalindrom = (x) => +[...'' + x].reverse().join('') === x;
    var cnt = 0
    while (!isPalindrom(number)) {
        number = number + +[...'' + number].reverse().join('');
        cnt++;
        if (cnt > 1000) {console.log('maybe a Lychrel number.');break;}
    }
    return `${cnt} ${number}`
}
console.log(palindromize(195))