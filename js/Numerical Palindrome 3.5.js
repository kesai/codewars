function palindrome(num, s = num.toString()) {
    var isPalindrom = (x) => [...x].reverse().join('') === x;
    if (!Number.isInteger(num) || num < 0) return 'Not valid';
    var out = new Set();
    for (var i = 2; i <= s.length; i++) {
        for (var j = 0; j <= s.length - i; j++) {
            var temp = s.slice(j, j + i);
            if (!/^0\d+/g.test(temp) && isPalindrom(temp)) out.add(s.slice(j, j + i))
        }
    }
    return out.size ? [...out].map(v => +v).sort((a, b) => a - b) : 'No palindromes found'
}

console.log(palindrome(1551))