function replaceNounPhrases(sentence, pronouns, dictionary) {
    let N = Object.entries(dictionary).filter(v => v[1] === 'N').map(v => v[0]).join('|')
    let aux = Object.entries(dictionary).filter(v => v[1] === 'aux').map(v => v[0]).join('|')
    let adj = Object.entries(dictionary).filter(v => v[1] === 'adj').map(v => v[0]).join('|');
    return sentence.replace(eval(`/(${aux}) ((${adj}) )*(${N})/g`), x => pronouns.shift())
}
