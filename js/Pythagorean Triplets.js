function pythagoreanTriplet(n) {
    var k = n ** (1 / 3) | 0;
    for (var a = 1; a <= k; a++) {
        for (var b = a + 1; b < 2 * k; b++) {
            var c = Math.sqrt(a ** 2 + b ** 2);
            if (c === parseInt(c) && a * b * c === n) { return [a, b, c] }
        }
    }
}

console.log(pythagoreanTriplet(1803060))