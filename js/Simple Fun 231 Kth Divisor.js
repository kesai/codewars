function kthDivisor(n, k) {
    //coding and coding..
    for (var i = 1, j = 0; i <= n; i++) {
        if (n % i === 0) {
            j++;
            if (j === k) return i;
        }
    }
    return -1;
}
console.log(kthDivisor(83919951, 8))

function getFactors(n) {
    var d = 2;
    var factors = []
    while (n > 1) {
        if (n % d === 0) {
            factors.push(d);
            n /= d;
        } else d++;
    }
    return factors;
}

console.log(getFactors(83919951))