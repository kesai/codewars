function splitWorkload(workload) {
    if (!workload.length) return [null, null]
    if (workload.length === 1) return [0, 1]
    var sum = workload.reduce((s, v) => s += v, 0);
    var arr = workload.map((v, i) => [i, Math.abs(workload.slice(0, i).reduce((s, x) => s += x, 0) -
        workload.slice(i).reduce((s, x) => s += x, 0))])
    arr = arr.sort((a, b) => a[1] - b[1] || a[0] - b[0])
    return arr[0]
}
splitWorkload([0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0])