var WordDictionary = function() {
    this.words = ''
};

WordDictionary.prototype.addWord = function(word) {
    this.words += word + '\n';
};

WordDictionary.prototype.search = function(word) {
    return new RegExp(`\\b${word}\\b`, 'g').test(this.words)
};