function boolfuck(code, input = "") {
    var inputs = [...input].map(c => [...c.charCodeAt(0).toString(2).padStart(8, '0')].reverse().join('')).join('')
    var out = ''
    var p = 0;
    var mems = [0];
    const jumpStack = []
    const jumps = {}
    for (let i = 0; i < code.length; ++i) {
        if (code[i] === "[") jumpStack.push(i)
        else if (code[i] === "]") {
            const j = jumpStack.pop()
            jumps[j] = i
            jumps[i] = j
        }
    }
    for (var i = 0; i < code.length; i++)
        switch (code[i]) {
            case '+': mems[p] ^= 1; break;
            case ',': mems[p] = +inputs[0] || 0; if (inputs.length) inputs = inputs.substr(1); break;
            case ';': out += ('' + mems[p]); break;
            case '<': if (--p === -1) { mems.unshift(0); p = 0 } break;
            case '>': if (++p === mems.length) { mems.push(0); p = mems.length - 1; } break;
            case '[': mems[p] || (i = jumps[i]); break;
            case ']': mems[p] && (i = jumps[i]); break;
        }
    var result = ''
    for (var i = 0; i < out.length; i += 8) {
        var temp = out.slice(i, i + 8).padEnd(8, '0');
        result += String.fromCharCode(parseInt([...out.slice(i, i + 8).padEnd(8, '0')].reverse().join(''), 2))
    }
    return result;
}