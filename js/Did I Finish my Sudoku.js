function rotate(matrix, direction) {
    return direction === 'clockwise' ? matrix[0].map((_, i, origin) => matrix.map(a => a[i]).reverse()) : matrix[0].map((_, i, origin) => matrix.map(a => a[i])).reverse()
}
function isValid(data) {
    for (var i = 0; i < data.length; i++) { if (data[i].length != data[0].length) return false; }
    var N = data.length;
    var pattern = Array.from({ length: N }, (v, i) => i + 1).join(',')
    //check each row
    for (x of data) { if ([].concat(x).sort((a, b) => a - b).join(',') != pattern) return false; }
    //check each column
    var arr = rotate(data);
    for (x of arr) { if (x.sort((a, b) => a - b).join(',') != pattern) return false; }
    //check square
    var size = Math.sqrt(N);
    for (var i = 0; i < N; i += size) {
        var sub = [].concat(data.slice(i, i + size));
        for (var j = 0; j < N; j += size) {
            var temp = []
            for (var k = 0; k < size; k++) { temp = temp.concat(sub[k].slice(j, j + size)) }
            if (temp.sort((a, b) => a - b).join(',') != pattern) return false;
        }
    }
    return true;
}
function doneOrNot(board) {
    if (isValid(board)) return 'Finished!'
    return 'Try again!';
}
console.log(doneOrNot([[5, 3, 4, 6, 7, 8, 9, 1, 2],
[6, 7, 2, 1, 9, 5, 3, 4, 8],
[1, 9, 8, 3, 4, 2, 5, 6, 7],
[8, 5, 9, 7, 6, 1, 4, 2, 3],
[4, 2, 6, 8, 5, 3, 7, 9, 1],
[7, 1, 3, 9, 2, 4, 8, 5, 6],
[9, 6, 1, 5, 3, 7, 2, 8, 4],
[2, 8, 7, 4, 1, 9, 6, 3, 5],
[3, 4, 5, 2, 8, 6, 1, 7, 9]]))