function solve(arr) {
    arr = arr.map(v => [...v].sort().join('').replace(/(.)\1+/g, v => v[1]));
    var o = {}
    for (var i = 0; i < arr.length; i++) {
        (o[arr[i]]) ? o[arr[i]].push(i): o[arr[i]] = [i];
    }
    return Object.values(o).filter(v => v.length > 1).map(v => v.reduce((s, x) => s += x, 0)).sort((a, b) => a - b)
};
solve(["abc", "abbc", "ab", "xyz", "xy", "zzyx"])