function isSumOfCubes(s) {
    var isCube = (x) => [...x].reduce((s, v) => s += (+v) ** 3, 0) === +x;
    var numbers = (s.match(/\d(\d{1,2})?/g) || []).filter(v => isCube(v)).map(v => +v);
    return numbers.length ? `${numbers.join(' ')} ${numbers.reduce((s, v) => s += v, 0)} Lucky` : 'Unlucky'
}
