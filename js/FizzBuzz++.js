function fizzbuzzPlusPlus(numbers, words, p = numbers.reduce((s, v) => s *= v, 1)) {
    var out = []
    for (var i = 1; i % p; i++) {
        var temp = ''
        for (num of numbers) { if (i % num === 0) temp += words[numbers.indexOf(num)] }
        out.push((!temp) ? temp = i : temp)
    }
    out.push(words.join(''))
    return out;
}

fizzbuzzPlusPlus([5, 3], ['a', 'b'])