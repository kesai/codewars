var gcd = (x = Math.abs(x), y = Math.abs(y)) => {
    while (y) {
        var t = y;
        y = x % y;
        x = t;
    }
    return x;
}

function Rational(numerator, denominator) {
    this.numerator = numerator;
    this.denominator = denominator || 1;
    this.simplify();
};

Rational.prototype.valueOf = function () {
    return this.numerator / this.denominator;
}

Rational.prototype.toString = function () {
    this.simplify()
    return this.numerator + "/" + this.denominator;
};

Rational.prototype.simplify = function () {
    var [a, b] = [this.numerator, this.denominator];
    this.numerator = a / gcd(a, b);
    this.denominator = b / gcd(a, b);
    if (a * b < 0) {
        this.numerator = -Math.abs(this.numerator);
        this.denominator = Math.abs(this.denominator)
    }
};

Rational.prototype.equals = function (b) {
    return this.valueOf() == b.valueOf()
};

Rational.prototype.lessThan = function (b) {
    return this.valueOf() < b.valueOf();
};

Rational.prototype.greaterThan = function (b) {
    return this.valueOf() > b.valueOf();
};

Rational.prototype.lessThanOrEquals = function (b) {
    return this.valueOf() <= b.valueOf();
};

Rational.prototype.greaterThanOrEquals = function (b) {
    return this.valueOf() >= b.valueOf();
};

Rational.prototype.add = function (b) {
    var denominator = this.denominator * b.denominator;
    var numerator = this.numerator * b.denominator + this.denominator * b.numerator;
    return new Rational(numerator, denominator)
};

Rational.prototype.sub = function (b) {
    var denominator = this.denominator * b.denominator;
    var numerator = this.numerator * b.denominator - this.denominator * b.numerator;
    return new Rational(numerator, denominator)
};

Rational.prototype.mul = function (b) {
    return new Rational(this.numerator * b.numerator, this.denominator * b.denominator)
};

Rational.prototype.div = function (b) {
    return new Rational(this.numerator * b.denominator, this.denominator * b.numerator)
};
