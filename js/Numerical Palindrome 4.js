var isPalindrom = (x) => x > 10 && +[...'' + x].reverse().join('') === x;

function palindrome(num) {
    if (!Number.isInteger(num) || num < 0) return 'Not valid'
    if (isPalindrom(num)) return num;
    var out = []
    for (var i = 1;; i++)
        if (isPalindrom(num + i)) { out.push(num + i); break; }

    for (var i = 1; num - i > 10; i++)
        if (isPalindrom(num - i)) { out.push(num - i); break; }
    out.sort((a, b) => Math.abs(a - num) - Math.abs(b - num));
    return out[0];
}

