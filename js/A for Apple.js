function a(n) {
    if (n < 4) return ''
    if (n % 2) n = n - 1;
    var len = 2 * n - 1;
    var out = []
    for (var i = 0; i < n; i++) {
        var temp = ''
        if (i === 0) temp = ' '.repeat(len / 2) + 'A' + ' '.repeat(len / 2);
        else if (i === n / 2) {
            temp = ' '.repeat((len - 2 * i) / 2) + 'A' + ' ' + Array(i - 1).fill('A').join(' ') + ' ' + 'A' + ' '.repeat((len - 2 * i) / 2)
        } else {
            temp = ' '.repeat((len - 2 * i) / 2) + 'A' + ' '.repeat(2 * i - 1) + 'A' + ' '.repeat((len - 2 * i) / 2)
        }
        out.push(temp)
    }
    return out.join('\n')
}