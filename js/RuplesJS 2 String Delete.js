String.prototype.delete = function(...args) {
    var temp = this.valueOf();
    for (x of args) {
        if (typeof(x) != 'string' && !(x instanceof RegExp)) continue;
        var repl = x;
        if (typeof(repl) === 'string') repl = eval(`/${x}/g`)
        temp = temp.replace(repl, '')
    }
    return temp
}
