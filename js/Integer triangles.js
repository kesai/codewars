function giveTriang(per) {
    // your code here
    var k = Math.cos(120 * Math.PI / 180);
    var arr = []
    for (var a = 1; a < per / 3; a++) {
        for (var b = a; b < per / 2; b++) {
            var c = Math.sqrt(a * a + b * b - 2 * a * b * k);
            if (Math.abs(Math.round(c) - c) < 0.00000001 && a + b + c < per) arr.push([a, b, c])
        }
    }
    return arr.length; // number of integer triangles with one angle of 120 degrees.
}

console.log(giveTriang(50))