const sendMessage = message => {
    var keyboards = {
        '1': '.,?!',
        '2': 'abc',
        '3': 'def',
        '4': 'ghi',
        '5': 'jkl',
        '6': 'mno',
        '7': 'pqrs',
        '8': 'tuv',
        '9': 'wxyz',
        '0': ' ',
        '*': '\'-+=',
        '#': 'case'
    }
    var find_key = (c) => Object.keys(keyboards)[Object.values(keyboards).findIndex(v => v.includes(c.toLowerCase()))]
    var caps = false;
    var out = ''
    var pre_key = null;
    for (c of message) {
        var key = find_key(c) || c
        var temp = ''
        if (/[^0-9*#]/.test(c)) {
            //switch case
            if ((/[a-z]/.test(c) && caps) || (/[A-Z]/.test(c) && !caps)) {
                caps = !caps;
                temp += '#';
            } else {
                if (pre_key === key && !out.endsWith('-')) temp += ' '
            }
            temp += key.repeat(keyboards[key].indexOf(c.toLowerCase()) + 1);
        } else {
            if (pre_key === key && !out.endsWith('-')) temp += ' '
            temp += key + '-'
        }
        pre_key = key;
        out += temp
    }
    return out
}