function bishopsAndRooks(chessboard) {
    //rook
    var rooks = chessboard.filter(x => x.includes(1));
    rooks_rows = rooks.map(v => chessboard.indexOf(v));
    var rook_positions = []
    for (r of rooks_rows) {
        for (var i = 0; i < 8; i++) {
            if (chessboard[r][i] === 1) rook_positions.push([r, i])
        }
    }
    //rook 计算
    for ([row, col] of rook_positions) {
        //向右
        for (var i = col + 1; i < 8; i++) {
            if (chessboard[row][i] === 1 || chessboard[row][i] === -1) break;
            if (chessboard[row][i] === 0) chessboard[row][i] = 8;
        }
        //向左
        for (var i = col - 1; i > -1; i--) {
            if (chessboard[row][i] === 1 || chessboard[row][i] === -1) break;
            if (chessboard[row][i] === 0) chessboard[row][i] = 8;
        }
        //向下
        for (var i = row + 1; i < 8; i++) {
            if (chessboard[i][col] === 1 || chessboard[i][col] === -1) break;
            if (chessboard[i][col] === 0) chessboard[i][col] = 8;
        }
        //向上
        for (var i = row - 1; i > -1; i--) {
            if (chessboard[i][col] === 1 || chessboard[i][col] === -1) break;
            if (chessboard[i][col] === 0) chessboard[i][col] = 8;
        }
    }
    //bishop
    var bishops = chessboard.filter(x => x.includes(-1));
    var bishop_rows = bishops.map(v => chessboard.indexOf(v));
    var bishop_positions = []
    //获取bishop的位置
    for (b of bishop_rows) {
        for (var i = 0; i < 8; i++) {
            if (chessboard[b][i] === -1) bishop_positions.push([b, i])
        }
    }
    //bishop 计算
    for ([row, col] of bishop_positions) {
        var [x, y] = [col, 8 - row - 1];
        //右上
        var b1 = y - x;
        for (var j = col + 1; j <= Math.min(7, 7 - b1); j++) {
            var i = 8 - j - b1 - 1;
            if (chessboard[i][j] === 1 || chessboard[i][j] === -1) break;
            if (chessboard[i][j] === 0) chessboard[i][j] = 8;
        }
        //左下
        for (var j = col - 1; j >= Math.max(0, -b1); j--) {
            var i = 8 - j - b1 - 1;
            if (chessboard[i][j] === 1 || chessboard[i][j] === -1) break;
            if (chessboard[i][j] === 0) chessboard[i][j] = 8;
        }
        //左上
        var b2 = x + y;
        for (var j = col - 1; j >= Math.max(0, b2 - 7); j--) {
            var i = 8 + j - b2 - 1;
            if (chessboard[i][j] === 1 || chessboard[i][j] === -1) break;
            if (chessboard[i][j] === 0) chessboard[i][j] = 8;
        }
        //右下
        for (var j = col + 1; j <= Math.min(7, b2); j++) {
            var i = 8 + j - b2 - 1;
            if (chessboard[i][j] === 1 || chessboard[i][j] === -1) break;
            if (chessboard[i][j] === 0) chessboard[i][j] = 8;
        }
    }
    return chessboard.reduce((s, v) => s += (v.join('').match(/0/g) || []).length, 0)
}
chessboard = [
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, -1, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, -1, -1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0]
]

console.log(bishopsAndRooks(chessboard))