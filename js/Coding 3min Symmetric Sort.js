function sc(array) {
    array = array.sort((a, b) => a - b);
    return array.filter((v, i) => i % 2 == 0).concat(array.filter((v, i) => i % 2).reverse())
}