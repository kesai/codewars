function group(p, blacks) {
    var [x, y] = [+p[0], +p[1]];
    var diff = [[-1, 0], [1, 0], [0, 1], [0, -1]];
    var groups = [p]
    for (d of diff) {
        var temp = `${x + d[0]}${y + d[1]}`;
        if (blacks.includes(temp)) {
            groups.push(temp);
            blacks.splice(blacks.indexOf(temp), 1);
            groups = groups.concat(group(temp, blacks))
        }
    }
    return groups;
}
function solution(input) {
    if (input === '') return 0;
    var blacks = input.split(',').filter(v => /^\d{2}$/.test(v));
    if (!blacks.length) return 0;
    var groups = []
    while (blacks.length) groups.push([...new Set(group(blacks.shift(), blacks))].length);
    return Math.max(...groups)
}

