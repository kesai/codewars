function service(score) {
    var [a, b] = score.split(':').map(v => +v);
    var sum = a + b;
    if (sum < 40) {
        if (sum < 5 || (sum >= 10 && sum < 15) || (sum >= 20 && sum < 25) || (sum >= 30 && sum < 35)) return 'first'
        else return 'second'
    } else {
        sum -= 40;
        var first = false;
        for (var i = 0; i <= sum; i += 2) {
            first = !first;
        }
        return first ? 'first' : 'second'
    }
}