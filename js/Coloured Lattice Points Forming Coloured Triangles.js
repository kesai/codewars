function isValid(A, B, C) {
    var dis = ([x1, y1], [x2, y2]) => Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2);
    var [a, b, c] = [dis(A, B), dis(A, C), dis(B, C)].sort((x, y) => x - y)
    if (a + b > c && a + c > b && c + b > a && Math.abs(a - b) < c && Math.abs(a - c) < b && Math.abs(b - c) < a) return true;
    return false;
}

function calc_triangle_count(points) {
    if (points.length < 3) return 0;
    var n = 0;
    for (var i = 0; i < points.length; i++) {
        for (var j = i + 1; j < points.length; j++) {
            for (var k = j + 1; k < points.length; k++) {
                if (isValid(points[i][0], points[j][0], points[k][0])) n++;
            }
        }
    }
    return n;
}

function countColTriang(pointsList) {
    var points_count = pointsList.length;
    var colors = [...new Set(pointsList.map(v => v[1]))];
    var color_groups = colors.map(v => { return { 'color': v, 'pts': pointsList.filter(x => x[1] === v) } })
    var color_triangle_counts =
        color_groups.map(v => [v.color, calc_triangle_count(v.pts)])
            .sort((a, b) => b[1] - a[1]);
    var trigngles_count = color_triangle_counts.reduce((s, v) => s += v[1], 0);
    var m = color_triangle_counts[0][1];
    var max_colors = color_triangle_counts.filter(v => v[1] === m && m > 0).map(v => v[0])
        .sort((a, b) => a.localeCompare(b))
    if (m) max_colors.push(m)
    return [points_count, colors.length, trigngles_count, max_colors];
}