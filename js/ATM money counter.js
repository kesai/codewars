function atm(value) {
    var d = +value.match(/\d+/g)[0];
    var val = d;
    var currency = value.match(/[a-z]/gi).join('').toUpperCase();
    if (!Object.keys(VALUES).includes(currency)) return `Sorry, have no ${currency}.`
    var changes = VALUES[currency];
    var out = []
    for (var i = changes.length - 1; i > -1; i--) {
        var cnt = parseInt(d / changes[i]);
        d = d % changes[i]
        if (cnt > 0) out.push(`${cnt} * ${changes[i]} ${currency}`)
    }
    if (d > 0) return `Can't do ${val} ${currency}. Value must be divisible by ${changes[0]}!`
    return out.join(', ')
}