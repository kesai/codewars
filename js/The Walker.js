function solve(a, b, c, alpha, beta, gamma) {
    var xc = a * Math.cos(alpha * Math.PI / 180) - b * Math.sin(beta * Math.PI / 180) - c * Math.cos(gamma * Math.PI / 180);
    var yc = a * Math.sin(alpha * Math.PI / 180) + b * Math.cos(beta * Math.PI / 180) - c * Math.sin(gamma * Math.PI / 180);
    var oc = Math.round(Math.sqrt(xc ** 2 + yc ** 2))
    var toc = Math.atan2(yc, xc) * 180 / Math.PI;
    var degree = Math.floor(toc);
    toc = (toc - degree) * 60;
    var minute = Math.floor(toc);
    var second = Math.floor((toc - minute) * 60);
    return [oc, degree, minute, second]
}

solve(12, 20, 18, 45, 30, 60)