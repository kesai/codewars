function josephus(items, k) {
    var p = 0;
    var out = []
    while (items.length) {
        p = (p + k - 1) % items.length;
        out.push(items[p])
        items.splice(p, 1);
    }
    return out
}