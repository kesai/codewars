function swap(ar, y) {
    for (var i = 0; i < ar.length; i++) {
        if (ar[i][y] === '1') {
            y += 1;
        } else if (ar[i][y - 1] === '1') {
            y -= 1;
        }
    }
    return y;
}

function amidakuji(ar) {
    var len = ar[0].length;
    var out = Array.from({ length: len }, (_, i) => i)
    for (var k = 0; k < out.length; k++) {
        out[swap(ar, k)] = k;
    }
    if (!out.includes(len)) out.push(len)
    return out;
}