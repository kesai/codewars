function count(addresses) {
    var states = []
    var stastics = {}
    for (addr of addresses) {
        if (!Object.keys(addr).includes('state')) throw 'no state attribute'
        var st = addr.state;
        stastics[st] = stastics[st] ? stastics[st] + 1 : 1;
        if (!states.includes(st)) states.push(st);
    }
    return states.map(v => stastics[v])
}