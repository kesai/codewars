function findArea(points) {
    var result = 0;
    for (var i = 0; i < points.length - 1; i++) {
        var dx = points[i + 1].X - points[i].X;
        result += dx * (points[i + 1].Y + points[i].Y) / 2
    }
    return result;
}