function regexTicTacToeWinChecker(board) {
    if (/^(X{3}.*)|(.{3}X{3}.{3})|(.{6}X{3})|(X.{3}X.{3}X)|(..X.X.X..)|((X..){3})|((.X.){3})|((..X){3})$/.test(board)) return true
    if (/^(O{3}.*)|(.{3}O{3}.{3})|(.{6}O{3})|(O.{3}O.{3}O)|(..O.O.O..)|((O..){3})|((.O.){3})|((..O){3})$/.test(board)) return true
    return false
}