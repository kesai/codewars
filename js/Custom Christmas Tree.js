function customChristmasTree(chars, n) {
    //learves
    var s = chars.padEnd((1 + n) * n / 2, chars);
    var a = 0;
    var leaves = []
    for (var i = 1; i <= n; i++) {
        leaves.push(' '.repeat(n - i) + [...s.slice(a, a + i)].join(' '));
        a += i;
    }
    //trunks
    var h = Math.floor(n / 3);
    trunks = Array(h).fill(' '.repeat(n - 1) + '|');
    return leaves.concat(trunks).join('\n')
}