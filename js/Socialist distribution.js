function socialistDistribution(population, minimum) {
    //step 1:removing minimum
    population = population.map(v => v - minimum);
    //step 2:repeat add 1 to poorest,plus 1 to richest(lowest index,from left),until poorest equal 0
    while (Math.min(...population) < 0) {
        var poorest_idx = population.findIndex(v => v === Math.min(...population));
        var richest_idx = population.findIndex(v => v === Math.max(...population));
        if (Math.max(...population) <= 0) return []
        population[poorest_idx]++;
        population[richest_idx]--;
    }
    //step 3:adding minimum
    population = population.map(v => v += minimum)
    return population;
}

socialistDistribution([2, 3, 5, 45, 45], 30)