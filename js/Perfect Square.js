function perfectSquare(string) {
    if (string === '.') return true;
    return /^(\.+\n)\1+$/.test(string + '\n') && RegExp.$1.length - 1 === string.split('\n').length
}