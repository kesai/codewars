function solution(data, replace) {
    if (!data) return data;
    for (k of Object.keys(data)) {
        if (typeof(data[k]) === 'object') data[k] = solution(data[k], replace)
        if (data[k] === 'dynamic') data[k] = replace;
    }
    return data;
}
