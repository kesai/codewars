function getReversedColor(hexColor) {
    var f = (hex) => (255 - parseInt(hex, 16)).toString(16).padStart(2, '0').toUpperCase();
    if (typeof(hexColor) != 'string' || (hexColor[0] != '#' && hexColor.length >= 7) || !/^#?[0-9a-f]*$/gi.test(hexColor))
        throw 'invalid input'
    hexColor = hexColor.trim().replace(/^\#/, '').toLowerCase();
    if (hexColor.length === 0) hexColor = '000000';
    else if (hexColor.length === 3) hexColor = hexColor.replace(/./g, (v) => '0' + v);
    else if (hexColor.length === 4) hexColor = hexColor.padStart(6, '0')
    return '#' + hexColor.replace(/../g, f);
}