function merge(line) {
    line = line.filter(v => v).concat(line.filter(v => !v))
    for (var i = 0; i < line.length; i++) {
        if (line[i] === line[i + 1]) {
            line[i] *= 2;
            line[i + 1] = 0;
        }
    }
    return line.filter(v => v).concat(line.filter(v => !v))
};

console.log(merge([2, 2, 2, 2, 2]))