Number.prototype.times = function(fn) {
    for (var i = 0; i < Math.round(this.valueOf()); i++) fn(this)
}