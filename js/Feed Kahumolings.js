function serve(food, k, mouths) {
    var f = (x) => Math.round(x * 1000) / 1000;
    return Array.from({ length: mouths }, (_, i) => k != 1 ? f(food * (1 - k) / (1 - k ** mouths) * k ** i) : food / mouths);
}
