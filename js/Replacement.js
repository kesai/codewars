function replacement(a) {
    a = a.sort((a, b) => a - b);
    if (new Set(a).size === 1 && a[0] === 1) return [2].concat(a.slice(0, -1))
    return [1].concat(a.slice(0, -1));

}