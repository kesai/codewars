function customFib(sig, index, n) {
    let i = 0,
        sum = 0;
    while (i < n) {
        sum = 0;
        for (let j = 0; j < index.length; j++)
            sum += sig[index[j] + i];
        sig.push(sum);
        i++;
    }
    return sig[n];
}