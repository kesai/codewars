var caches = {}
function countPairsInt(diff, nMax) {
    var count = (x) => {
        if (caches[x]) return caches[x]
        var d = 2;
        for (var i = 1; i <= x / 2; i++) {if(x % i === 0) d++;}
        caches[x] = d;
        return d;
    }
    var result = 0
    for (var i = 1; i < nMax - diff; i++) {
        if (count(i) === count(i + diff))result++;
    }
    return result;
}

console.log(countPairsInt(6, 350, 86))