
var sum_pairs = function (ints, s) {
    var exists = {}
    var pairs = []
    for (var i = 0; i < ints.length; i++) {
        var [a, b] = [ints[i], s - ints[i]];
        var a_idx = i;
        var b_idx = -1;
        if (!exists[b]) {
            if (a === b) {
                var subs = [].concat(ints);
                subs[i] = '逗你玩儿呢';
                exists[b] = b_idx = subs.indexOf(b);
            } else {
                exists[b] = b_idx = ints.indexOf(b);
            }
        }
        if (b_idx != -1) { pairs.push([{ idx: a_idx, value: a }, { idx: b_idx, value: b }].sort((x, y) => x.idx - y.idx)) }
    }
    var p = pairs[0];
    for (var i = 1; i < pairs.length; i++) {
        var [a, b] = pairs[i];
        if (b.idx <= p[1].idx) p = [a, b];
    }
    return p ? p.map(v => v.value) : p
}