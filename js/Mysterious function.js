var getNum = function(n) {
    return [...('' + n).replace(/[^0689]*/g, '')].reduce((s, v) => s += v === '8' ? 2 : 1, 0)
};