function maxConsecZeros(n) {
    var words = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten','Eleven','Twelve','Thirteen','Fourteen'
    var m = Math.max(...((+n).toString(2).match(/0+/g) || []).map(v => v.length));
    return words[m > 0 ? m : 0]
}

console.log(maxConsecZeros("7"))