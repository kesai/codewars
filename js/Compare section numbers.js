function cmp(section1, section2) {
    section1 = section1.split('.').map(v => +v).join('.').replace(/(\.0+)\1*$/g, '');
    section2 = section2.split('.').map(v => +v).join('.').replace(/(\.0+)\1*$/g, '')
    if (section1 === section2) return 0;
    var arr1 = section1.split('.');
    var arr2 = section2.split('.')
    for (var i = 0; i < Math.max(arr1.length, arr2.length); i++) {
        if (i >= arr1.length && i < arr2.length) return -1;
        if (i >= arr2.length && i < arr1.length) return 1;
        if (+arr1[i] > +arr2[i]) return 1
        if (+arr1[i] < +arr2[i]) return -1
    }
    return 0;
}