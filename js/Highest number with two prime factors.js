function check(n, p1, p2) {
    //if (n % (p1 * p1)===) return false;
    var i = 0;
    j = 0;
    while (true) {
        if (n % (p1 * p2) === 0) {
            n /= (p1 * p2);
            i++;
            j++;
        } else if (n % p1 === 0) {
            n /= p1;
            i++;
        } else if (n % p2 === 0) {
            n /= p2;
            j++;
        } else {
            return n === 1 && i * j > 0 ? [i, j] : false
        }
    }
}

function highestBiPrimeFac(p1, p2, nMax) {
    while ((x = check(nMax, p1, p2)) === false) nMax--;
    return [nMax].concat(x)
}

console.log(highestBiPrimeFac(5, 11, 1000))