function estSubsets(arr) {
    var n = new Set(arr).size;
	var comb = (m, n) => {
		if(2*n>m)return comb(m,m-n)
		var [a,b]=[1,1]
		var x=1
	    for(var i=0;i<n;i++)x*=(m-i)/(n-i)
		return x
	}
    var sum = 0
    for (var i = 1; i <= n; i++) sum += comb(n, i)
    return Math.round(sum)
}