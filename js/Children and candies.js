function distributionOfCandy(candies) {
    var count = 0
    while (new Set(candies).size > 1) {
        candies = candies.map(v => v % 2 ? v + 1 : v);
        var halves = candies.map(v => v / 2);
        adds = halves.slice(-1).concat(halves.slice(0, -1))
        candies = halves.map((v, i) => v + adds[i]);
        count++;
    }
    return [count, candies[0]]
}

distributionOfCandy([10, 2, 8, 22, 16, 4, 10, 6, 14, 20])