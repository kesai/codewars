function myFirstInterpreter(code) {
    code = code.replace(/[^+.]/g, '')
    var value = 0;
    var out = ''
    for (c of code) {
        if (c === '+') value=(++value)%256;
        if (c === '.') out += String.fromCharCode(value)
    }
    return out;
}