function allNonConsecutive(arr) {
    var out = []
    for (var i = 1; i < arr.length; i++) {
        if (arr[i] - arr[i - 1] != 1) out.push({ 'i': i, 'n': arr[i] })
    }
    return out;
}