function race(v1, v2, g) {
    if (v1 >= v2) return null;
    var t = 3600 * g / (v2 - v1);
    var h = parseInt(t / 3600);
    t = t - h * 3600;
    var m = parseInt(t / 60)
    var s = Math.floor(t - m * 60)
    return [h, m, s]
}
console.log(race(820, 850, 550))