function get_divisors_cnt(n) {
    var cnt = 1
    for (var i = 1; i <= n/2; i++)
        if (n % i === 0) cnt++;
    return cnt;

}

function findMinNum(num) {
    for (var i = num;; i++) {
        if (get_divisors_cnt(i) === num) return i;
    }
}