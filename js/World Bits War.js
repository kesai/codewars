function bitsWar(numbers) {
    var x = numbers.reduce((s, v) => s += (v % 2 ? 1 : -1) * (v > 0 ? 1 : -1) * (v.toString(2).match(/1/g) || []).length, 0)
    return x > 0 ? 'odds win' : x === 0 ? 'tie' : 'evens win'
}