const head = s => s[0],
    tail = s => s.slice(1);
const init = s => s.slice(0, -1),
    last = s => s[s.length - 1];

function findDifferent(s, t) {
    if (head(s) && head(t) && head(s) === head(t)) {
        const { index, addedText, deletedText } = findDifferent(tail(s), tail(t));
        return { index: index + 1 || -1, addedText, deletedText };
    } else if (last(s) && last(t) && last(s) === last(t))
        return findDifferent(init(s), init(t));
    else
        return { index: s === t ? -1 : 0, addedText: t, deletedText: s };
}