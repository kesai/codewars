const f = (x) => x > 1 ? x * f(x - 1) : 1;

var perms = (x, str = x.toString()) => f(str.length) / [...new Set(str)].reduce((s, v) => s *= f(str.match(eval(`/${v}/g`)).length), 1);

perms(737)