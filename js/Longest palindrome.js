function longestPalindrome(str) {
    var arr = [...str.toLowerCase().replace(/[^a-z0-9]/g, '')].sort().join('').match(/(.)\1*/g).map(v => v.length);
    console.log([...str.toLowerCase().replace(/[^a-z0-9]/g, '')].sort().join('').match(/(.)\1*/g))
    var evens = arr.filter(v => v % 2 === 0);
    var odds = arr.filter(v => v % 2);
    return (evens.concat(odds.map(v => v - v % 2))).reduce((s, v) => s += v, 0) + (odds.length ? 1 : 0)
}
