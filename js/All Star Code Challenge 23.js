function sumPoints(player) {
    var sum = player.normKill * 100;
    sum += player.assist * 50;
    sum += player.damage * 0.5;
    sum += player.healing;
    sum += Math.pow(2, player.streak);
    sum += player.envKill * 500;
    return sum;
}

function scoring(players) {
    return players.sort(function(a, b) {
        return sumPoints(a) < sumPoints(b);
    }).map(function(player) {
        return player.name;
    })
}