function determinant(m) {
    if (m.length === 1) return m[0][0];
    if (m.length === 2) return m[0][0] * m[1][1] - m[0][1] * m[1][0];
    else {
        var sum = 0;
        for (var i = 0; i < m.length; i++) {
            var sub = sub_matrix(m, 0, i);
            sum += m[0][i] * determinant(sub) * (i % 2 ? -1 : 1);
        }
        return sum;
    }
};

function sub_matrix(m, row, col) {
    var sub = []
    for (var i = 0; i < m.length; i++) {
        if (i === row) continue;
        var temp = []
        for (var j = 0; j < m.length; j++) {
            if (j === col) continue;
            temp.push(m[i][j]);
        }
        sub.push(temp);
    }
    return sub;
}
