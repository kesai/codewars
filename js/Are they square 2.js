var isSquare = function(arr) {
    return arr.length ? arr.every(v =>
        Array.isArray(v) ? isSquare(v) || v.length === 0 : Number.isInteger(Math.sqrt(v))
    ) : undefined
}