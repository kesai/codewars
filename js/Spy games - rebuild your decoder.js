function decrypt(code) {
    var alphas = ' abcdefghijklmnopqrstuvwxyz';
    return code.split(' ').map(v => alphas[(v.match(/\d/g) || []).reduce((s, x) => s += +x, 0) % 27]).join('')
}