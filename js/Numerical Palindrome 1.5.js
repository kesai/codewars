function palindrome(num, s) {
    var isPalindrom = (x) => +[...'' + x].reverse().join('') === x;
    if (!Number.isInteger(num) || !Number.isInteger(s) || num < 0 || s < 0) return 'Not valid';
    var out = [];
    while (out.length < s) {
        if (num > 10 && isPalindrom(num)) out.push(num);
        num++;
    }
    return out;
}

console.log(palindrome(6, 4))