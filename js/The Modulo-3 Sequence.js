function sequence(n=n%8) {
    n = n % 8;
    return n > 2 ? (sequence(n - 1) + sequence(n - 2)) % 3 : [0, 0, 1][n];
}