Number.prototype.twos = function(n) {
    return Math.abs(this).toString(2).padStart(n, this >= 0 ? '0' : '1')
}