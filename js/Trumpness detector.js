function trumpDetector(trumpySpeech) {
    var groups = (trumpySpeech.match(/([aeiou])\1+/gi) || []).map(v => v.length - 1).reduce((s, v) => s += v, 0)
    var total = trumpySpeech.replace(/([aeiou])\1+/gi, (v) => v[1]).replace(/[^aeiou]/gi, '').length;
    return Math.round(100 * groups / total) / 100
} 