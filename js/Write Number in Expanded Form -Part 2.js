function expandedForm(num) {
    var arr = num.toString().split('.');
    var [a, b] = arr;
    var A = [...a].map((v, i) => +v ? +v * 10 ** (a.length - i - 1) : null).filter(v => v);
    var B = [...b].map((v, i) => +v ? `${v}/${10 ** (i+1)}` : null).filter(v => v)
    return A.concat(B).join(' + ')
}
console.log(expandedForm(0.304))