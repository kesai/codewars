function simple_assembler(cmds) {
    var vars = {}
    for (var i = 0; i < cmds.length; i++) {
        var [cmd, x, y] = cmds[i].split(' ')
        switch (cmd) {
            case 'mov':if (Object.keys(vars).includes(y)) vars[x] = vars[y];else vars[x] = +y;break;
            case 'inc':vars[x]++;break;
            case 'dec':vars[x]--;break;
            case 'jnz':if (vars[x] != 0) i += +y - 1;break;
        }
    }
    return vars
}