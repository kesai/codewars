function selectNames(table) {
    return map(rest(table), v => first(v))
}

function selectVoices(table) {
    return map(rest(table), v => third(v))
}

function selectNamesAndVoices(table) {
    return zip(selectNames(table),selectVoices(table))
}