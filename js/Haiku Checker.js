function isHaiku(text) {
    return text.split('\n').map(v => v.split(' ').reduce((s, x) => s += (x.replace(/(.*[aeiouy]+[^aeiouy]+)e[^a-z]*$/i, '$1').match(/[aeiouy]+/gi) || []).length, 0)).join(',') === '5,7,5'
}
