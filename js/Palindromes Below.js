Number.prototype.palindromeBelow = function(base) {
    var isPalindrom = (x) => [...x].reverse().join('') === x;
    return Array.from({ length: this }, (_, i) => i + 1).filter(v => isPalindrom(v.toBase(base)))
}

/**
 * 进制转换，目前支持到36进制，字母数字系列
 */
Number.prototype.toBase = function(base) {
    var s = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXY';
    var numbers = '0123456789';
    if (base > s.length + numbers.length) throw 'base out of range'
    if (base > 10) numbers = numbers + s.slice(0, base - 10);
    var n = this;
    var out = []
    while (n) {
        out = [numbers[n % base]].concat(out);
        n = (n / base) | 0;
    }
    return out.join('')
}