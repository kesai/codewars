function Ticket(code) {
    this.code = code;
    this.used = false;
    this.useTicket = function() {
        this.used = true;
        Object.freeze(this)
    };
}

function validTicket(ticket, correctCode) {
    return ticket.code === correctCode && !ticket.used
}