function arrayDepth(array, depth = 0) {
    if (Array.isArray(array)) {
        if (array.length == 0) return depth + 1;
        var m = 1;
        for (x of array) {
            var temp = 0;
            temp += arrayDepth(x, depth + 1)
            m = Math.max(temp, m)
        }
        return m;
    } else {
        return depth;
    }
}

