function sortCsvColumns(csvFileContent) {
    var arr = csvFileContent.split('\n').map(v => v.split(';'));
    var sorts = [].concat(arr[0]).map((v, i) => [i, v]).sort((a, b) => a[1].localeCompare(b[1])).map(v => v[0])
    return arr.map(v => {
        var temp = v.sort((a, b) => sorts.indexOf(v.indexOf(a)) - sorts.indexOf(v.indexOf(b))).join(';')
        return temp;
    }).join('\n')
}
