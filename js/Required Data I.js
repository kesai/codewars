function countSel(lst) {
    var counts = {}
    for (x of lst) {
        counts[x] ? counts[x]++ : counts[x] = 1;
    }
    var max = Math.max(...Object.values(counts))
    var max_arr = []
    var once = 0
    for (k of Object.keys(counts)) {
        if (counts[k] === max) max_arr.push(+k);
        if (counts[k] === 1) once++;
    }
    return [lst.length, Object.keys(counts).length, once, [max_arr.sort((a, b) => a - b), max]]
}
