function bucketize(arr) {
    var counts = {}
    for (x of new Set(arr)) {
        var n = arr.filter(v => v === x).length;
        !counts[n] ? counts[n] = [x] : counts[n].push(x)
    }
    return [null].concat(arr.map((v, i) => counts[i + 1] ? counts[i + 1].sort((a, b) => a - b) : null))
}
