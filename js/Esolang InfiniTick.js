function interpreter(tape) {
    tape = tape.replace(/[^+\-<>*&\\/]/g, '')
    var p = 0;
    var mems = []
    var out = []
    while (true) {
        for (var i = 0; i < tape.length; i++) {
            var code = tape[i];
            //console.log(code)
            switch (code) {
                case '+':
                    if (!mems[p]) mems[p] = 0;
                    mems[p] = (mems[p] + 1) % 256
                    break;
                case '-':
                    if (!mems[p]) mems[p] = 0;
                    mems[p] = mems[p] ? mems[p] - 1 : 255;
                    break;
                case '>':
                    p++;
                    if (!mems[p]) mems[p] = 0;
                    break;
                case '<':
                    p--;
                    if (!mems[p]) mems[p] = 0;
                    break;
                case '*':
                    out.push(String.fromCharCode(mems[p]))
                    break;
                case '&':
                    return out.join('')
                case '/':
                    if (mems[p] === 0) i++;
                    break;
                case '\\':
                    if (mems[p]) i++;
                    break;
            }
        }
    }
}

