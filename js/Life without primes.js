function isPrime(num) {
    if (num < 2) return false;
    if (num === 2 || num === 3) return true;
    if (num % 6 != 1 && num % 6 != 5) return false
    var n = parseInt(Math.sqrt(num));
    for (var i = 5; i < n + 1; i += 6) {
        if (num % i === 0 || num % (i + 2) === 0) return false
    }
    return true
}
var caches = []
for (var i = 1; i <= 50000; i++) {
    if (!isPrime(i) && !/[2357]/.test(i)) caches.push(i)
}

function solve(n) {
    return caches[n];
};