function partialKeys(obj) {
  return new Proxy(obj, {
    get: (obj, prop) => {
      const key = Object.keys(obj).sort().find(x => x.startsWith(prop));
      return key ? obj[key] : undefined;
    }
  });
}