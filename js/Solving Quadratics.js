function solveQuadratic(a, b, c) {
  if (a == 0) return b ? [ -c / b] : undefined;
  var d = b * b - 4 * a * c;
  if (d < 0) return;
  if (d == 0) return [-b / (2 * a)];
  d = !a || !c ? b : Math.sqrt(d);
  return [(-b + d) / (2 * a), (-b - d) / (2 * a)]
}