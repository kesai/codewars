
function isInteresting(n, arr) {
    var checks = [
        (n) => /^\d0*$/.test(n) || /^(\d)\1+$/.test(n),
        (n) => '1234567890'.indexOf(n) > -1 || '9876543210'.indexOf(n) > -1,
        (n) => n === +[...'' + n].reverse().join(''),
        (n) => arr.indexOf(n) > -1
    ];
    const is_interesting = (n) => n > 99 && checks.some(f => f(n));
    return is_interesting(n) ? 2 : (is_interesting(n + 1) || is_interesting(n + 2)) ? 1 : 0;
}