var wordWrap = function(str) {
    //return str.match(/.{1,24}/g).join('-\n')
    var s = ''
    while (str.length > 25) {
        if (str[25] != ' ') {
            s += str.slice(0, 24) + '-\n';
            str = str.slice(24);
        } else {
            s += str.slice(0, 25) + '\n';
            str = str.slice(25);
        }
    }
    return s + str;
};

console.log(wordWrap('The quick brown fox jumped over the lazy developer.'))