function longestCollatz(arr) {
    temp = arr.map((v, i) => [i, collatz(v)]).sort((a, b) => b[1] - a[1] || a[0] - b[0])
    return arr[temp[0][0]]
}
function collatz(n) {
    var count = 0;
    while (n > 1) {
        n = n % 2 ? 3 * n + 1 : n / 2;
        count++;
    }
    return count
}