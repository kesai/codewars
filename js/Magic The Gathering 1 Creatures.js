function battle(player1, player2) {
    var arr1 = [];
    var arr2 = [];
    for (var i = 0; i < Math.max(player1.length, player2.length); i++) {
        if (i < player1.length && i >= player2.length) arr1.push(player1[i])
        else if (i < player2.length && i >= player1.length) arr2.push(player2[i])
        else {
            if (player1[i][1] > player2[i][0]) arr1.push(player1[i])
            if (player2[i][1] > player1[i][0]) arr2.push(player2[i]);
        }
    }
    return { 'player1': arr1, 'player2': arr2 }
}