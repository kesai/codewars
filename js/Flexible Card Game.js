function Card(suit, rank) {
    this.suit = suit;
    this.rank = rank;
    this.face_card = this.rank > 10
}
Card.dict = { 1: 'Ace', 11: 'Jack', 12: 'Queen', 13: 'King' }
Card.HEARTS = 'Hearts';
Card.DIAMONDS = 'Diamonds';
Card.CLUBS = 'Clubs';
Card.SPADES = 'Spades';
Card.prototype = {
    valueOf: function() {
        return this.rank;
    },
    toString: function() {
        return `${[1, 11, 12, 13].includes(this.rank) ? Card.dict[this.rank] : this.rank} of ${this.suit}`
    }
};

function Deck() {
    this.cards = []
    for (var i = 1; i <= 13; i++) {
        this.cards = this.cards.concat([new Card(Card.HEARTS, i), new Card(Card.DIAMONDS, i), new Card(Card.CLUBS, i), new Card(Card.SPADES, i)])
    }
}

Deck.prototype = {
    count: function() {
        return this.cards.length;
    },
    draw: function(n) {
        var out = this.cards.slice(-n);
        this.cards = this.cards.slice(0, this.cards.length - n);
        return out;
    },
    shuffle: function() {
        var k = Math.random() * this.cards.length;
        this.cards = this.cards.slice(k).concat(this.cards.slice(0, k))
    }
};