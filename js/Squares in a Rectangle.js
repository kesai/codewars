function findSquares(x, y) {
    var n = 0
    for (var i = 0; i < Math.min(x, y); i++) {
        n += (x - i) * (y - i)
    }
    return n;
}