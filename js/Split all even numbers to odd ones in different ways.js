var ways = [x => (x / 2) % 2 ? [x / 2, x / 2] : [(x / 2 | 0) - 1, (x / 2 | 0) + 1],x => [1, x - 1],
    (x, temp = x) => { while (!(temp % 2)) temp /= 2; return Array(x / temp).fill(temp); },x => Array(x).fill(1)];
var splitAllEvenNumbers = (arr, way) => arr.reduce((s, x) => s.concat(x % 2 ? x : ways[way](x)), [])
