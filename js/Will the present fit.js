function willFit(present, box) {
    present = present.sort((a, b) => a - b)
    box = box.sort((a, b) => a - b);
    return box.every((v, i) => v - 2 > present[i]);
}
console.log(willFit([58, 34, 29], [36, 31, 60]))