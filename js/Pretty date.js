function toPretty(seconds) {
    var format = (x, unit) => `${  x>1?x: unit!='hour'?'a':'an'} ${unit}${x>1?'s':''} ago`;
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(seconds / 3600);
    var days = Math.floor(seconds / (24 * 3600));
    var weeks = Math.floor(seconds / (7 * 24 * 3600));
    return seconds ? weeks ? format(weeks, 'week') : days ? format(days, 'day') : hours ? format(hours, 'hour') : minutes ? format(minutes, 'minute') : format(seconds, 'second') : 'just now'
}