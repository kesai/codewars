var caches = [
    [1n],
    [1n, 1n]
];

function f(n) {
    if (caches.length >= n) return caches[n - 1];
    var arr = []
    var up_row = f(n - 1);
    for (var i = 0; i < f(n - 1).length + 1; i++) {
        arr.push((arr[i - 1] || 0n) + (up_row[i] || 0n))
    }
    caches.push(arr);
    return arr;
}

function preCache() {
    for (var i = 1; i < 400; i++) f(i)
}

function solve(n) {
    return caches[n].slice(-1)[0]
}