var toexuto = function(text) {
    var s = 'abcdefghijklmnopqrstuvwxyz';
    return text.replace(/[^aeiou]/gi, (x) => s.indexOf(x.toLowerCase()) > 0 ? x + s.slice(0, s.indexOf(x.toLowerCase())).replace(/[^aeiou]/g, '').slice(-1) : x)
};