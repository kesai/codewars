function findDupsMiss(arr) {

    var [max, min] = [-Infinity, Infinity]
    for (let x of arr) {
        max = Math.max(x, max)
        min = Math.min(x, min)
    }
    let counts = Array(max - min + 1).fill(0);
    var dups = []
    for (let x of arr) {
        counts[x - min]++;
        if (counts[x - min] > 1) dups.push(x)
        max = Math.max(x, max)
        min = Math.min(x, min)
    }
    //console.log(counts.indexOf(0) + min, dups)
    return [counts.indexOf(0) + min, dups.sort((a, b) => a - b)]
}
var arr1 = [20, 19, 6, 9, 7, 17, 16, 17, 12, 5, 6, 8, 9, 10, 14, 13, 11, 14, 15, 19]
findDupsMiss(arr1)