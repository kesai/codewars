function isOre(n) {
    var divisors = Array.from({ length: n }, (_, i) => i + 1).filter(v => n % v === 0);
    return Number.isInteger(divisors.length * n / divisors.reduce((s, v) => s += n / v, 0))
}
console.log(isOre(8190))