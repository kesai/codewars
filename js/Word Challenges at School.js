function wantedWords(n, m, forbid_let) {
    var words =
        wordList.filter(x => x.length === n + m && x.match(/[aeiou]/g).length === n && x.match(/[^aeiou]/g).length === m);
    return words.filter(x => !eval(`/[${forbid_let.join('')}]/g`).test(x))

}