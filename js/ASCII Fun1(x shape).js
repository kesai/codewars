function x(n) {
    var m = Math.round(n / 2);
    var out = []
    for (var i = 0; i < m; i++) {
        var temp = '□'.repeat(i) + '■' + '□'.repeat(m - i - 1);
        temp += temp.split('').reverse().slice(1).join('')
        out.push(temp)
    }
    out = out.concat(out.slice(0, -1).reverse())
    return out.join('\n')
}
