function rotate(matrix, direction) {
    var result = [],
        n = matrix.length,
        m = matrix[0].length,
        i, j, row;
    for (i = 0; i < m; ++i) {
        row = [];
        for (j = 0; j < n; ++j) {
            row.push(direction === 'clockwise' ? matrix[n - j - 1][i] : matrix[j][m - i - 1]);
        }
        result.push(row);
    }
    return result;
}

function draw(waves) {
    var wave_l = waves.length;
    var wave_h = Math.max(...waves);
    graph = waves.map(v => '■'.repeat(v).padEnd(wave_h, '□').split(''));
    return rotate(graph).map(v => v.join('')).join('\n')
}
draw([1, 2, 3, 3, 2, 1])