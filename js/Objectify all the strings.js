String.prototype.hashify = function() {
    var o = {}
    for (var i = 0; i < this.length; i++) {
        var value = i + 1 < this.length ? this[i + 1] : this[0];
        if (!o[this[i]]) o[this[i]] = value;
        else {
            if (Array.isArray(o[this[i]])) {
                o[this[i]].push(value);
            } else {
                o[this[i]] = [o[this[i]], value]
            }
        }
    }
    return o;
}