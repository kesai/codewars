function waveSort(arr) {
    for (var i = 0; i < arr.length; i++) {
        if ((i % 2 === 0 && arr[i] <= arr[i + 1]) || (i % 2 && arr[i] >= arr[i + 1])) {
            var temp = arr[i];
            arr[i] = arr[i + 1];
            arr[i + 1] = temp;
        }
    }
    console.log(arr)
}

waveSort([1, 2, 34, 4, 5, 5, 5, 65, 6, 65, 5454, 4])