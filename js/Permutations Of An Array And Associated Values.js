function sscForperm(arr) {
  let permArr = [];
  let usedChars = [];
  let permutation_sum = [];
  let unique_permutations;

  // --------------------------------------------------------

  // Permutations
  function permute(input) {
    let i, ch;
    for (i = 0; i < input.length; i++) {
      ch = input.splice(i, 1)[0];
      usedChars.push(ch);
      if (input.length == 0) {
        permArr.push(usedChars.slice());
      }
      permute(input);
      input.splice(i, 0, ch);
      usedChars.pop();
    }
    return permArr
  };
  // End Permutations

  // --------------------------------------------------------

  // Sum Permutations
  function sum_to_permutations() {
    for (let i = 0; i < unique_permutations.length; i++) {
      permutation_sum.push(unique_permutations[i].reduce(function (before, current, index) {
        return before + (current * (index + 1));
      }))
    }
  };
  // End Sum Permutations

  // --------------------------------------------------------

  // Sorting Sum Permutations
  function sort_number(before, current) {
    return before - current;
  };
  // End Sorting Sum Permutations

  // --------------------------------------------------------

  // Total Score
  function total_score() {
    return permutation_sum.reduce(function (a, b) {
      return a + b;
    });
  };
  // End Total Score

  // --------------------------------------------------------

  // Start Proccess Permutations
  let permutes = permute(arr);
  let set = new Set(permutes.map(JSON.stringify));

  unique_permutations = Array.from(set).map(JSON.parse);
  sum_to_permutations();
  permutation_sum = permutation_sum.sort(sort_number);

  let total_ssc = total_score();
  let validate_sum = permutation_sum.length > 1;
  let max_ssc = validate_sum ? permutation_sum.pop() : permutation_sum[0];
  let min_ssc = validate_sum ? permutation_sum.shift() : permutation_sum[0];
  // End Start Proccess Permutations

  // --------------------------------------------------------

  return [{ "total perm": unique_permutations.length }, { "total ssc": total_ssc }, { "max ssc": max_ssc }, { "min ssc": min_ssc }];
}


