function select(memory) {
    var memories = memory.split(', ');
    var marks = memories.filter(v => /^\!.+/g.test(v));
    var temp = []
    for (mark of marks) {
        temp.push(memories[memories.indexOf(mark) + 1])
    }
    marks = marks.concat(temp);
    marks = marks.join(', ').replace('!', '').split(', ')
    return memory.replace('!', '').split(', ').filter(v => !marks.includes(v)).join(', ')
}