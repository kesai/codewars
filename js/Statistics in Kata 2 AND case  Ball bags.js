function ballProbability(balls) {
    var [bags, choice, replace] = balls;
    var [a, b, n] = [1, 1, bags.length]
    if (replace) {
        a = choice.reduce((s, v) => s *= (bags.filter(x => x === v) || []).length, 1);
        b = bags.length ** choice.length;
    } else {
        var stastics = {}
        for (color of new Set(bags)) stastics[color] = bags.filter(v => v === color).length;
        for (c of choice) {
            a *= ((stastics[c] ? stastics[c]-- : 0))
            b *= n--;
        }
    }
    return Math.round(a * 1000 / b) / 1000;
}