//reference:https://www.geeksforgeeks.org/count-numbers-from-1-to-n-that-have-4-as-a-a-digit/
function nines(n) {
    if (n < 9n) return 0n;
    var d = n.toString().length - 1;
    var a = Array(d + 1);
    a[0] = 0n;
    a[1] = 1n;
    for (var i = 2; i <= d; i++) {
        a[i] = a[i - 1] * 9n + BigInt(`1${'0'.repeat(i - 1)}`)
    }
    var p = BigInt(`1${'0'.repeat(d)}`);
    var msd = n / p;
    if (msd === 9n) return msd * a[d] + (n % p) + 1n;
    if (msd > 9n) return (msd - 1) * a[d] + p + nines(n % p);
    return (msd) * a[d] + nines(n % p);
}