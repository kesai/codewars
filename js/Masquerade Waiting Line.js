function friendFind(line) {
    var n = 0
    for (var i = 0; i < line.length; i++) {
        if (line[i] === 'red') {
            if (line[i + 1] === 'blue' && line[i + 2] === 'blue') n++;
            else if (line[i + 1] === 'blue' && line[i - 1] === 'blue') n++;
            else if (line[i - 1] === 'blue' && line[i - 2] === 'blue') n++;
        }
    }
    return n;
}