function tripledouble(num1, num2) {
    var triples = (num1.toString().match(/(\d)\1+/g) || []).filter(v => v.length >= 3);
    for (triple of triples) {
        var d = triple[0];
        if (eval(`/(${d})\\1+/g`).test(num2)) return 1;
    }
    return 0;
}