function area(d, l) {
    return d > l ? Math.round(Math.sqrt(d * d - l * l) * l * 100) / 100 : 'Not a rectangle'
}