function thueMorse(n) {
    var m = Math.round(Math.log2(n)) + 1;
    var s = '0'
    for (var i = 1; i <= m; i++) {
        s = s.replace(/./g, (v) => +v ? '10' : '01');
    }
    return s.slice(0, n)
}