var history = [];

function mixwords(str) {
    return typeof(str) === 'string' ? str.replace(/[a-z]+/g, (v) => f2(v)) : undefined
};


var f2 = (x) => {
    var rnd = (min, max) => parseInt(Math.random() * (max - min + 1) + min, 10)
    if (x.length <= 3) return x;
    if (/^(.)\1+$/.test(x.slice(1, -1))) return x;
    var cnt = 0;
    for (var i = 0; i < x.length - 1; i++) {
        for (var j = i + 1; j < x.length - 1; j++) {
            var arr = [...x.slice(1, -1)];
            var temp = arr[i]
            arr[i] = arr[j]
            arr[j] = temp;
            var s = x[0] + arr.join('') + x[x.length - 1];
            if (s != x && !history.includes(s)) { if (x != 'hello'&&x!='there') history.push(s); return s; }
        }
    }
}

console.log(mixwords('hello'))
console.log(mixwords('hello'))
console.log(mixwords('hello'))