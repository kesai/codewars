function recordDepth(tree, depth = 0) {
    if (!tree ||Array.isArray(tree)||!(tree instanceof Object)) return null
    tree['depth'] = depth;
    for (k of Object.keys(tree)) {
        recordDepth(tree[k], depth + 1)
    }
    return tree;
}