var sum = function(...args1) {
    if (args1.length === 1) return function(...args2) {
        return args2.concat(args1).reduce((s, v) => s += v, 0)
    }
    else
        return args1.reduce((s, v) => s += v, 0)
}

