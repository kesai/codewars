const availableMoves = p => {
  if (typeof p != 'string') return [];
  const [y,x] = ((p||'').match(/([A-H])|([1-8]$)/g)||[]).map((e,i)=>i?e-1:e.charCodeAt()-65);
  if (isNaN(+x)) return []
  return [].concat(...[...Array(8)].map((_,a)=>[...Array(8)].map((_,b)=>y==a||x==b||y+x==a+b||x-y==b-a?String.fromCharCode(a+65)+(++b):''))).filter(a=>a&&a!=p);
}