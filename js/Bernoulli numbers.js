var gcd = (x = Math.abs(x), y = Math.abs(y)) => {
    while (y) {
        var t = y;
        y = x % y;
        x = t;
    }
    return x;
}
var simplify = ([a, b]) => [a / gcd(a, b), b / gcd(a, b)]

var arr = [
    [1n, 1n],
    [-1n, 2n]
]

var factorial_caches = { '0': 1n, '1': 1n }

var f = (x) => {
    if (factorial_caches[x]) return factorial_caches[x];
    if (x > 1n) {
        var temp = x * f(x - 1n);
        factorial_caches[x] = temp;
        return factorial_caches[x];
    } else {
        factorial_caches[x] = 1n;
        return factorial_caches[x];
    }
}


function comb(m, n) {
    return f(m) / (f(n) * f(m - n))
}

function _b(i) {

    if (i % 2 == 0) {
        arr.push([0n, 1n]);
    } else {
        var n = BigInt(i);
        var product_denominators = 1n
        var triangles = []
        for (var k = 0n; k < n; k += 1n) {
            var p = comb(n, k);
            triangles.push(p);
            if (arr[k]) product_denominators *= arr[k][1];
        }
        //console.log('分母乘积为', product_denominators)
        //console.log('系数为', triangles)
        var sum_numerator = 0n;
        for (var k = 0n; k < n - 1n; k += 1n) {
            var p = (product_denominators * arr[k][0] / arr[k][1]) * triangles[k];
            //console.log(`第${k}项分子通分后为${p}`)
            sum_numerator += p;
        }
        var [a, b] = simplify([-sum_numerator, product_denominators * triangles[triangles.length - 1]])
        if (a * b < 0n) {
            if (a > 0n) arr.push([-1n * a, -1n * b]);
            else arr.push([a, b]);
        } else {
            arr.push([a, b]);
        }
    }
}


function b(i) {
    if (i % 2 && i > 1) return [0n, 1n]
    if (i < arr.length) return arr[i];
    else {
        for (var k = arr.length + 1; k <= i + 1; k++) _b(k);
        return arr[i]
    }
}
console.log(b(500))