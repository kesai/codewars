function countSpecMult(n, mxval) {
    var primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]
    var x = primes.slice(0, n).reduce((s, v) => s *= v, 1);
    return parseInt(mxval / x);
}