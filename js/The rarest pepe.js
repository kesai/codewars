function findRarestPepe(pepes) {
    var uniques = [...new Set(pepes)]
    var counts = uniques.map(v => pepes.filter(x => x === v).length);
    var rares = Math.min(...counts);
    var rarests = uniques.filter(v => pepes.filter(x => x === v).length === rares).sort();
    return rares >= 5 ? 'No rare pepes!' : rarests.length > 1 ? rarests : rarests[0]
}
var pepes = ['Donald Trump Pepe',
    'Melania Trump Pepe',
    'Clown Pepe',
    'Clown Pepe',
    'Donald Trump Pepe'
]
findRarestPepe(pepes)