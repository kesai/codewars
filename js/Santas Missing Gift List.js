function gifts(number) {
    var gifts = []
    while (number > 0) {
        var k = Math.log2(number) | 0;
        gifts.push(GIFTS[2 ** k])
        number -= 2 ** k;
    }
    return gifts.sort()
}
