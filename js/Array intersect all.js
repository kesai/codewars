var intersect = function(...args) {
    var _intersect = (a, b) => a.filter(v => b.includes(v));
    var out = args[0]||[];
    for (var i = 1; i < args.length; i++) {
        out = _intersect(out, args[i])
    }
    return out
};
