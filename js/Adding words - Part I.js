const Numbers = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty", "twenty one", "twenty two", "twenty three", "twenty four", "twenty five", "twenty six", "twenty seven", "twenty eight", "twenty nine", "thirty", "thirty one", "thirty two", "thirty three", "thirty four", "thirty five", "thirty six", "thirty seven", "thirty eight", "thirty nine", "forty", "forty one", "forty two", "forty three", "forty four", "forty five", "forty six", "forty seven", "forty eight", "forty nine", "fifty", "fifty one", "fifty two", "fifty three", "fifty four", "fifty five", "fifty six", "fifty seven", "fifty eight", "fifty nine", "sixty", "sixty one", "sixty two", "sixty three", "sixty four", "sixty five", "sixty six", "sixty seven", "sixty eight", "sixty nine", "seventy", "seventy one", "seventy two", "seventy three", "seventy four", "seventy five", "seventy six", "seventy seven", "seventy eight", "seventy nine", "eighty", "eighty one", "eighty two", "eighty three", "eighty four", "eighty five", "eighty six", "eighty seven", "eighty eight", "eighty nine", "ninety", "ninety one", "ninety two", "ninety three", "ninety four", "ninety five", "ninety six", "ninety seven", "ninety eight", "ninety nine"];
//only support to hundreds
String.prototype.toNumber = function() {
    if (/hundred/.test(this)) {
        var arr = this.split(/hundred/);
        if (/hundred\s+and/.test(this)) arr = this.split(/hundred\s+and/);
        return arr[0].toNumber() * 100 + arr[1].toNumber()
    } else {
        var r = Numbers.indexOf(this.trim());
        return r > -1 ? r : 0;
    }
}
Number.prototype.toEnglish = function() {
    if (this == 1000) return 'one thousand'
    if (this > 100 && this < 1000) {
        return `${((this/100)|0).toEnglish()} hundred` +
            (this % 100 ? ` and ${(this%100).toEnglish()}` : '')
    }
    return Numbers[this]
}

class Arith {
    constructor(value) {
        this._value = value;
    }
    add(x) {
        return (this._value.toNumber() + x.toNumber()).toEnglish()
    }
}