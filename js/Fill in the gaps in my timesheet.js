function fill_gaps(arr) {
	var timesheet=[].concat(arr)
    for (var i = 0; i < timesheet.length; i++) {
        if (!timesheet[i]) {
            var left = -1;
            var right = -1;
            for (var j = i - 1; j > -1; j--) {
                if (timesheet[j]!=null) { left = j; break; }
            }
            for (var k = i + 1; k < timesheet.length; k++) {
                if (timesheet[k]!=null) { right = k; break; }
            }
            if (left != -1 && right != -1 && timesheet[left] === timesheet[right]) timesheet[i] = timesheet[left];
        }
    }
    return timesheet
}