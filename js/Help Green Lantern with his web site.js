function yellowBeGone(x) {
    names = {
        'gold': 'ForestGreen',
        'khaki': 'LimeGreen',
        'lemonchiffon': 'PaleGreen',
        'lightgoldenrodyellow': 'SpringGreen',
        'lightyellow': 'MintCream',
        'palegoldenrod': 'LightGreen',
        'yellow': 'Lime'
    }
    if (/^\#/.test(x)) {
        var [r, g, b] = x.match(/[\da-f]{2}/gi).map(v => [v, parseInt(v, 16)]);
        var rgb = [r, g, b].sort((a, b) => a[1] - b[1])
        if (r[1] > b[1] && g[1] > b[1]) return `#${rgb[0][0]}${rgb[2][0]}${rgb[1][0]}`
        else return x;
    }
    return names[x.toLowerCase()] || x;
}

yellowBeGone('#E6BF7B')