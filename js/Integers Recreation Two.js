function prod2sum(a, b, c, d) {
    var [x0, y0] = [Math.abs(a * d - b * c), Math.abs(a * c + b * d)].sort((x, y) => x - y);
    var [x1, y1] = [Math.abs(a * c - b * d), Math.abs(a * d + b * c)].sort((x, y) => x - y);
    return x0===x1&&y0===y1?[[x0,y0]]:[[x0,y0],[x1,y1]].sort((x,y)=>x[0]-y[0]);
}