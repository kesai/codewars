function chessBishopDream(boardSize, initPosition, initDirection, k) {
    var [MAX_X, MAX_Y] = boardSize;
    var [x, y] = [].concat(initPosition);
    var d = [].concat(initDirection);
    var routes = []
    for (var i = 0; i < k; i++) {
        x = x + d[0];
        y = y + d[1];
        if ((x === MAX_X && y === MAX_Y) || (x === -1 && y === -1) || (x === -1 && y === MAX_Y) || (x === MAX_X && y === -1)) {
            x = (x === -1 ? 0 : x - 1);
            y = (y === -1 ? 0 : y - 1);
            d[0] *= -1;
            d[1] *= -1;
        } else if (x < 0 || x === MAX_X) {
            x = x < 0 ? 0 : x - 1;
            d[0] *= -1;
        } else if (y < 0 || y === MAX_Y) {
            y = y < 0 ? 0 : y - 1;
            d[1] *= -1;
        }
        if (x === initPosition[0] && y === initPosition[1] && d[0] === initDirection[0] && d[1] === initDirection[1]) {
            console.log('回到初始状态了,循环周期为' + (i + 1));
            return k % (i + 1) ? routes[k % (i + 1) - 1] : [x, y]
        }
        routes.push([x, y])
    }
    return [x, y]
}