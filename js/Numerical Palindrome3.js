function palindrome(num) {
    var isPalindrom = (x) => [...x].reverse().join('') === x;
    if (!Number.isInteger(num) || num <= 0) return 'Not valid';
    var s = num.toString();
    var n = 0;
    for (var i = 2; i <= s.length; i++) {
        for (var j = 0; j <= s.length - i; j++) {
            if (isPalindrom(s.slice(j, j + i))) n++;
        }
    }
    return n
}
