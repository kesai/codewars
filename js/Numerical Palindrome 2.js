var f = (x) => x.length > 1 && x === [...x].reverse().join('');

function palindrome(num, s = num.toString()) {
	if(!Number.isInteger(num)||num<0)return 'Not valid'
    for (var i = 0; i < s.length; i++) {
        for (var j = i + 1; j <= s.length; j++) {
            if (f(s.slice(i, j))) return true;
        }
    }
    return false;
}

console.log(palindrome(123322367))