function off(n) {
    var arr = Array.from({ length: n }, (_, i) => true);
    var k = 0;
    for (var i = 0; i < n; i++) {
        for (var j = k; j < n; j += k + 1) { arr[j] = !arr[j]; }
        k++;
    }
    return arr.map((v, i) => [v, i + 1]).filter(v => !v[0]).map(v => v[1])
}