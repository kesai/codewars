Array.prototype.except = function(keys) {
    if (!Array.isArray(keys)) keys = [keys]
    return this.filter((v, i) => !keys.includes(i))
}