var f = function(i) {
    console.log('Launch missile #' + i + '...')
}

function launchAll(launchMissile) {
    for (var i = 0; i < 5; i++) setTimeout(launchMissile, i * 1000, i + 1);
}

launchAll(f)