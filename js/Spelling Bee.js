howManyBees = function(hive) {
    if (!hive || !hive.length) return 0;
    let flip = hive[0].map((_, i) => hive.map(row => row[i]));
    var count = (hive) => hive.map(v => v.join('')).reduce((s, v) => s += (v.match(/(?=bee|eeb)/g) || []).length, 0);
    return count(hive) + count(flip)
}