var log = (a, b) => Math.log10(b) / Math.log10(a); //a为底的对数
function compare(number1, number2) {
    let a = number1[0];
    let x = number1[1];
    let b = number2[0];
    let y = number2[1];
    return x / log(a, 10) > y / log(b, 10) ? number1 : number2
}