function sc(apple, n) {
    for (var i = 0; i < n; i++) apple = infect(apple);
    return apple;
}

function infect(apple) {
    var rows = apple.filter(v => v.includes('V'))
    var pos = []
    for (row of rows) {
        var i = apple.indexOf(row);
        var cols = []
        for (var j = 0; j < row.length; j++) {
            if (row[j] === 'V') cols.push(j)
        }
        pos = pos.concat(cols.map(v => [i, v]))
    }
    for (p of pos) {
        var i = p[0];
        var j = p[1];
        if (i - 1 > -1) apple[i - 1][j] = 'V';
        if (i + 1 < apple.length) apple[i + 1][j] = 'V';
        if (j - 1 > -1) apple[i][j - 1] = 'V';
        if (j + 1 < apple[0].length) apple[i][j + 1] = 'V'
    }
    return apple
}