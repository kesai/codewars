function windComponents(rwy, windDirection, windSpeed) {
    var hw = Math.abs(Math.round(windSpeed * Math.cos(Math.PI * windDirection / 180)));
    var cw = Math.abs(Math.round(windSpeed * Math.sin(Math.PI * windDirection / 180)));
    return `Headwind ${hw} knots. Crosswind ${cw} knots from your left.`
}
