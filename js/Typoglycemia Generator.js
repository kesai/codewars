ScrambleWords = function(str) {
    return str.split(' ').map(v => repl(v)).join(' ')
};

function repl(x) {
    return x.replace(/[a-z](.*)[a-z]/g, (s, v) => {
        var chars = [...v.replace(/[^a-z]/g, '')].sort();
        var r = ''
        for (var j = 0; j < v.length; j++) {
            if (!/[a-z]/.test(v[j])) {
                chars.splice(j, 0, v[j]);
            }
        }
        return s[0] + chars.join('') + s.slice(-1)
    })
}

console.log(repl('card-carrying'))