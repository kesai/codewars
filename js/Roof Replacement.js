function roofFix(s, x) {
    return [...x].every((v, i) => /[\\\/]/.test(v) ? s[i] === ' ' : true)
}