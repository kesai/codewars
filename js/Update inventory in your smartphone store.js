function updateInventory(curStock, newStock) {
    for (x of newStock) {
        var idx = curStock.findIndex(v => v[1] === x[1]);
        if (idx > -1) curStock[idx] = [curStock[idx][0] + x[0], x[1]]
        else curStock.push(x)
    }
    return curStock.sort((a, b) => a[1].localeCompare(b[1]));
}

currentStock = [
    [33, 'Sony']
]
newStock = [
    [100, 'Saony']
]

console.log(updateInventory(currentStock, newStock))