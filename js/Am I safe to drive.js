function drive(drinks, finished, drive_time) {
    var alcohol = Math.round(drinks.reduce((s, v) => s += v[0] * v[1] / 1000, 0) * 100) / 100;
    finished = +finished.split(':')[0] * 60 + +finished.split(':')[1]
    drive_time = +drive_time.split(':')[0] * 60 + +drive_time.split(':')[1];
    if (drive_time < finished) drive_time += 24 * 60;
    var time_gap = drive_time - finished;
    return [alcohol, alcohol * 60 < time_gap]
}