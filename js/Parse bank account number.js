function parseBankAccount(bankAccount) {
    var arr = bankAccount.split('\n');
    var s = ''
    var numbers = { ' _ \n| |\n|_|': 0, '   \n  |\n  |': 1, ' _ \n _|\n|_ ': 2, ' _ \n _|\n _|': 3, '   \n|_|\n  |': 4, ' _ \n|_ \n _|': 5, ' _ \n|_ \n|_|': 6, ' _ \n  |\n  |': 7, ' _ \n|_|\n|_|': 8, ' _ \n|_|\n _|': 9 }
    for (var i = 0; i < arr[0].length; i += 3) {
        var temp = arr[0].slice(i, i + 3) + '\n' + arr[1].slice(i, i + 3) + '\n' + arr[2].slice(i, i + 3);
        s += numbers[temp];
    }
    return +s;
}