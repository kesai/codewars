function originalNumber(s) {
    var m = { 'Z': 0, 'G': 8, 'U': 4, 'X': 6, 'W': 2, 'T': 3, 'F': 5, 'S': 7, 'I': 9, 'N': 1 }
    var words = ['ZERO', 'ONE', 'TWO', 'THREE', 'FOUR', 'FIVE', 'SIX', 'SEVEN', 'EIGHT', 'NINE']
    s = [...s].sort().join('')
    var result = ''
    for (x of 'ZGUXWTFSIN') {
        var n = (s.match(new RegExp(x, 'g')) || []).length;
        result += ('' + m[x]).repeat(n)
        var word = words[m[x]];
        for (c of word) s = s.replace(c.repeat(n), '')
    }
    return [...result].sort().join('')
}
