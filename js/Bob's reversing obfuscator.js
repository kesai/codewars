function decoder(encoded, marker) {
    arr = encoded.split(marker).join('');
    var evens = arr.filter((v, i) => i % 2 === 0).join('');
    var odds = [...arr.filter((v, i) => i % 2).join('')].reverse().join('')
    return evens + odds
}