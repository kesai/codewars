const months = {
    1: "A",
    2: "B",
    3: "C",
    4: "D",
    5: "E",
    6: "H",
    7: "L",
    8: "M",
    9: "P",
    10: "R",
    11: "S",
    12: "T"
};

function fiscalCode(person) {
    var sur = (x) => {
        var consonants = x.replace(/[aeiou]/gi, '').toUpperCase();
        var vowels = x.replace(/[^aeiou]/gi, '').toUpperCase();
        return (consonants + vowels).padEnd(3, 'X').substr(0, 3);
    };
    var na = (x) => {
        var consonants = x.replace(/[aeiou]/gi, '').toUpperCase();
        var vowels = x.replace(/[^aeiou]/gi, '').toUpperCase();
        return consonants.length > 3 ? consonants[0] + consonants[2] + consonants[3] : (consonants + vowels).padEnd(3, 'X').substr(0, 3)
    }

    var birth = (x, isMale = true) => {
        var [d, m, y] = x.split('/');
        return y.slice(-2) + months[+m] + (isMale ? d.padStart(2, '0') : +d + 40)
    }
    return sur(person.surname) + na(person.name) + birth(person.dob, person.gender === 'M')
}
