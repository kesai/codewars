function diagonal(chessboard, kr, kc, d) {
    var moves = [[-1, 1],[1, 1],[1, -1],[-1, -1]]
    var out = []
    var [row, col] = [kr, kc];
    while (row > -1 && col > -1 && row < 8 && col < 8) {
        out.push(chessboard[row][col]);
        row += moves[d][0];
        col += moves[d][1];
    }
    return out.join('')
}

function kingIsInCheck(chessboard) {
	var pawn_moves=[[-1, -1],[-1, 1]];
    var kningt_moves = [[-1, -2],[-2, -1],[-2, 1],[-1, 2],[1, 2],[2, 1],[2, -1],[1, -2]];
    var kr = chessboard.findIndex(v => v.includes('♔'));
    var kc = chessboard[kr].findIndex(v => v === '♔');
    //check by pawn
    for ([x, y] of pawn_moves) {
        if (chessboard[x + kr] && chessboard[x + kr][y + kc] && /[♟]/.test(chessboard[x + kr][y + kc])) return true;
    }
    //check by knight
    for ([x, y] of kningt_moves) {
        if (chessboard[x + kr] && chessboard[x + kr][y + kc] && /[♞]/.test(chessboard[x + kr][y + kc])) return true;
    }
    //check by rook or queen by file
    //check row
    if (/([♜♛]\s*♔)|(♔\s*[♜♛])/.test(chessboard[kr].join(''))) return true;
    //check column
    if (/([♜♛]\s*♔)|(♔\s*[♜♛])/.test(chessboard.map(v => v[kc]).join(''))) return true;

    //check by bishop or queen
    for(var i=0;i<4;i++)if( /([♝♛]\s*♔)|(♔\s*[♝♛])/.test(diagonal(chessboard, kr, kc, i)))return true;
	return false;
}






