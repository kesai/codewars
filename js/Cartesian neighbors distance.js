function cartesianNeighborsDistance(x, y, range) {
    var out = new Set()
    for (var i = 0; i <= range; i++) {
        for (var j = 1; j <= range; j++) {
            out.add(Math.sqrt(i * i + j * j))
        }
    }
    return [...out]
}
console.log(cartesianNeighborsDistance(0, 0, 2))