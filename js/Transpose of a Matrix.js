Array.prototype.transpose = function() {
    if (this.length === 0) return []
    var out = Array.from({ length: this[0].length }, (_, i) => new Array(this.length))
    if(this[0].length===0)return [[]]
    for (var i = 0; i < this.length; i++) {
        for (var j = 0; j < this[0].length; j++) {
            out[j][i] = this[i][j]
        }
    }
    return out;
};
