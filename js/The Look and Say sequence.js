var getLines=(n)=>isNaN(n)||n<1?-1:(x=Array(n).fill('1')).map((v,i)=>i?x[i]=x[i-1].replace(/(\d)\1*/g,(v,i)=>`${v.length}${i}`):v).join(',');
console.log(getLines(5))