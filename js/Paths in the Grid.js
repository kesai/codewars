var comb = (m, n) => Array.from({ length: n }, (_, i) => m - i).reduce((s, v) => s *= v, 1) / Array.from({ length: n }, (v, i) => i).reduce((s, v) => s *= (v + 1), 1)

function numberOfRoutes(m, n) {
    return comb(m + n, n)
}

