class Router {
    constructor() {
        this.paths = {}
    }
    bind(url, method, rep) {
        this.paths[url + '(' + method + ')'] = rep;
    }
    runRequest(url, method) {
        if (!Object.keys(this.paths).includes(url + '(' + method + ')')) return 'Error 404: Not Found';
        return this.paths[url + '(' + method + ')']()
    }
}