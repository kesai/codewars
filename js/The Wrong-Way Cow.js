findWrongWayCow = function(fields) {
    function getPos0(x) {
        for (var i = 0; i < fields.length; i++) {
            var s = fields[i].join('')
            if (s.includes(x)) {
                var idx = s.indexOf(x) + x.indexOf('c');
                return [idx, i]
            }
        }
    }
    function getPos1(x) {
        for (var j = 0; j < fields[0].length; j++) {
            var s = ''
            for (var i = 0; i < fields.length; i++) s += fields[i][j];
            if (s.includes(x)) {
                var idx = s.indexOf(x) + x.indexOf('c')
                return [j, idx]
            }
        }
    }

    var horizonals = { 'cow': 0, 'woc': 0 }
    var verticals = { 'cow': 0, 'woc': 0 }
    //horizonal
    for (var x of fields) {
        x = x.join('')
        horizonals['cow'] += (x.match(/cow/g) || []).length;
        horizonals['woc'] += (x.match(/woc/g) || []).length;
    }
    //vertical
    for (var j = 0; j < fields[0].length; j++) {
        var s = ''
        for (var i = 0; i < fields.length; i++) s += fields[i][j];
        verticals['cow'] += (s.match(/cow/g) || []).length;
        verticals['woc'] += (s.match(/woc/g) || []).length;
    }
    if (horizonals['cow'] === 1) { return getPos0('cow') }
    if (horizonals['woc'] === 1) { return getPos0('woc') }
    if (verticals['cow'] === 1) { return getPos1('cow') }
    if (verticals['woc'] === 1) { return getPos1('woc') }
}
