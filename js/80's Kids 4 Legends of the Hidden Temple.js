function markSpot(n) {
	if(!Number.isInteger(n)||n%2===0||n<=0)return '?'
    var arr = Array.from({ length: n / 2 | 0 }, (_, i) => ' '.repeat(2 * i) + 'X' + ' '.repeat(2 * n - 4 * i - 3) + 'X')
    var out = []
    out = out.concat(arr)
    out.push(' '.repeat(n - 1) + 'X')
    out = out.concat(arr.reverse())
    return out.join('\n')+'\n'
}