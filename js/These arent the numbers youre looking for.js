function findNumber(compare) {
    var x = 0;
    while (true) {
        var t = compare(x);
        if (t === 0) return x;
        if (t === -1) x += 0.00002;
        if (t === 1) return x -= 0.00002;
    }
    return 0;
}