function interpreter(tape) {
    var p = 0;
    var mem = []
    var out = ''
    for (cmd of tape) {
        switch (cmd) {
            case '>':p++;break;
            case '<':p--;break;
            case '+':
                if (!mem[p]) mem[p] = 0;mem[p]++;if (mem[p] === 256) mem[p] = 0;break;
            case '*':
                out+=String.fromCharCode(mem[p]);break;

        }
    }
    return out
}