function highlight(code) {
    var m = {
        'F': (x) => `<span style="color: pink">${x}</span>`,
        'L': (x) => `<span style="color: red">${x}</span>`,
        'R': (x) => `<span style="color: green">${x}</span>`,
        'D': (x) => `<span style="color: orange">${x}</span>`
    }
    return code.replace(/(F|R|L|\d+)\1*/g, (v) => /\d/.test(v) ? m['D'](v) : m[v[0]](v))
}
console.log(highlight('FFFR345F22LL'))