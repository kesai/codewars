function zoom(n) {
    var p = ((n + 1) / 2) % 2 ? '■□' : '□■';
    var x = p[0];
    var out = []
    for (var i = 0; i < n / 2; i++) {
        out.push((left=''.padStart(i, p)) + x.repeat(n - 2 * i) + [...left].reverse().join(''))
        x = x === '■' ? '□' : '■'
    }
    return out.concat(out.slice(0, -1).reverse()).join('\n')
}
