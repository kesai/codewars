function getColumnTitle(num) {
    if (!Number.isInteger(num) || num <= 0) throw 'invalid length'
    var s = 'ZABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var out = ''
    while (num > 0) {
        res = num % 26;
        num = num / 26 | 0
        if (res === 0) {res = 26;num--;}//different from normal base converte
        out = s[res] + out;
    }
    return out;
}
