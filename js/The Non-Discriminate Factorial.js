function factorial(n) {
    return n > 0 ? factorial(n - 1) * n : n === 0 ? 1 : Math.abs(n) % 2 ? -factorial(-n) : factorial(-n)
}