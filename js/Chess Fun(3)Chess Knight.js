function chessKnight(cell) {
    var s = 'abcdefgh';
    var x = s.indexOf(cell[0])
    var y = +cell[1] - 1;
    var moves = [[1, 2],[2, 1],[2, -1],[1, -2],[-1, -2],[-2, -1],[-2, 1],[-1, 2]]
    return moves.map(v=>[x+v[0],y+v[1]]).filter(p=>p[0]>-1&&p[1]>-1&&p[0]<8&&p[1]<8   ).length
}