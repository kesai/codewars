var f = (x) => x > 0 ? x * f(x - 1) : 1;

function routes(n) {
    return f(2 * n) / f(n) ** 2
}
console.log(routes(20))