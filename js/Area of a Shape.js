function area_of_the_shape(f) {
    var N = 1000;
    var inside = 0;
    var total = N * N;
    for (var x = 0; x <= N; x++) {
        for (var y = 0; y <= N; y++) {
            if (f(x / N, y / N)) inside++;
        }
    }
    return inside / total
}