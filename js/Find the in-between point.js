function middle_point(x1, y1, z1, x2, y2, z2, x3, y3, z3) {
    var arr1 = [x1, x2, x3].sort((a, b) => a - b);
    var arr2 = [y1, y2, y3].sort((a, b) => a - b);
    var arr3 = [z1, z2, z3].sort((a, b) => a - b);
    if (arr1[1] === x1 && arr2[1] === y1 && arr3[1] === z1) return 1
    if (arr1[1] === x2 && arr2[1] === y2 && arr3[1] === z2) return 2
    return 3
}