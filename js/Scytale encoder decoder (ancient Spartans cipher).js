class Scytale {
    static Decode(message, cylinderSides) {
        var arr = message.match(eval(`/.{1,${cylinderSides}}/g`)) || [];
        var result = ''
        for (var i = 0; i < cylinderSides; i++) {
            for (var j = 0; j < arr.length; j++) {
                result += arr[j][i] || ' '
            }
        }
        return result.trim();
    }

    static Encode(message, cylinderSides) {
        var l = Math.ceil(message.length / cylinderSides);
        if (!message) return ''
        var arr = (message.match(eval(`/.{1,${l}}/g`)) || [])
        var result = '';
        for (var i = 0; i < arr[0].length; i++) {
            for (var j = 0; j < cylinderSides; j++) {
                if (arr[j]) result += arr[j][i] || ' '
                else
                    result += ' '
            }
        }
        return result.trim()
    }
}
