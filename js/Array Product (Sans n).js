function productSansN(n,product=1n,zeros=0) {
    n.forEach((v) =>(!v)?zeros++:product *= BigInt(v));
    return n.map(v => !zeros ? (product / BigInt(v)).toString() :zeros>1?'0':(v?'0':product.toString()));
}
