function getLengthOfMissingArray(arr) {
    if (!arr || !arr.length) return 0;
    if ((arr.filter(v => v === null || (Array.isArray(v) && v.length === 0)) || []).length) return 0;
    arr = arr.map(v => Array.isArray(v) ? v.length : 1).sort((a, b) => a - b)
    for (var i = 0; i < arr.length; i++) {
        if (arr[i + 1] - arr[i] > 1) return arr[i] + 1
    }
}