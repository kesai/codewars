function isValid(n) {
    var primes = [2, 3, 5, 7, 11];
    for (p of primes) {
        var x = n ** (1 / p);
        if (Math.abs(Math.round(x) - x) < 0.000000001) return true;
    }
    return false;
}

function count(n) {
    var i = 2;
    var cnt = 0
    while (true) {
        var x = n ** (1 / i);
        if (Math.round(x) < 2) return cnt;
        if (Math.abs(Math.round(x) - x) < 0.000000001) cnt++;
        i++;
    }
    return cnt;
}

function largestPower(n) {
    for (var i = n - 1; i > 1; i--) {
        if (isValid(i)) return [i, count(i)]
    }
    return [1, -1]
}
