function multiply(n, l) {
    var out = [].concat(l)
    var i = 0;
    while (i < l.length) {
        var j = 1;
        var add = out[i]
        while (j < Math.abs(n)) {
            out[i] += add;
            j++;
        }
        out[i] = n >= 0 ? out[i] : -out[i]
        i++;
    }
    return out;
}
console.log(multiply(-2, [1, 2]))