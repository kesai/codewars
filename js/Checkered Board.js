function checkeredBoard(n = 5) {
    var a = Array.from({ length: n }, (_, i) => i % 2 ? '■' : '□').join(' ')
    var b = Array.from({ length: n }, (_, i) => i % 2 === 0 ? '■' : '□').join(' ')
    return !Number.isInteger(n)||n < 2 ? false : Array.from({ length: n }, (_, i) => i % 2 === n % 2 ? a : b).join('\n')
}

