//abandoned way,Recursive can't pass.
function flatten(arr) {
    var out = []
    for (x of arr) {
        if (Array.isArray(x)) {
            out = out.concat(flatten(x))
        } else {
            out.push(x);
        }
    }
    return out;
}

function print(arr) {
    for (x of arr) {
        if (Array.isArray(x)) print(x)
        else console.log(x)
    }
}

function nested(arr) {
    if (arr.length <= 2) return arr;
    else {
        var out = [arr[0]]
        out.push(nested(arr.slice(1)))
        return out;
    }
}

function flatten_es6(arr) {
    while (arr.some(item => Array.isArray(item))) {
        arr = [].concat(...arr);
    }
    return arr;
}

function reverseList(list) {
    if (list[0] === 'c') print(list)
    return Array.isArray(list) ? nested((flatten(list).reverse().slice(1).concat([null]))) : list
}


//right way.
function reverseList(arr) {
    var out = null
    while (arr) {
        out = [arr[0], out]
        arr = arr[1]
    }
    return out;
}