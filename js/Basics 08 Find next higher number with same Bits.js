function nextHigher(n) {
    var m = n.toString(2).replace(/0/g, '').length;
    for (var i = n + 1;; i++) {
        if (i.toString(2).replace(/0/g, '').length === m) return i;
    }
}
