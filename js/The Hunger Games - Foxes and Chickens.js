 var hungryFoxes = function(farm) {
     var outside = farm.replace(/\[[CF\.]*\]/g, '')
     var arr = farm.match(/(\[[CF\.]*\])|([CF\.]+)/g).map(v => /\[[CF\.]*\]/.test(v) ? /F/.test(v) ? v.replace(/C/g, '.') : v : /F/.test(outside) ? v.replace(/C/g, '.') : v)
     return arr.join('')
 }

 hungryFoxes('CC.C.FC....[....FC.C.CCCC.C.C.CC.][]..F[]')