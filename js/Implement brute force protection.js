var history = {}
function bruteForceDetected(loginRequest) {
    if (loginRequest.successful) {
        history[loginRequest.sourceIP] = 0;
    } else {
        history[loginRequest.sourceIP] = history[loginRequest.sourceIP] ? history[loginRequest.sourceIP] + 1 : 1;
        if (history[loginRequest.sourceIP] >= 20) return true;
    }
    return false;
}