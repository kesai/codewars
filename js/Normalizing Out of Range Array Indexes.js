function normIndex(array, index) {
    index = index % array.length
    if (index < 0) index += array.length
    return array[index]
}