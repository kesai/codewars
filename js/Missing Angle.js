function missingAngle(h, a, o) {
    if (h === 0) return Math.round(180 * Math.atan2(o, a) / Math.PI);
    if (a === 0) return Math.round(180 * Math.asin(o / h) / Math.PI);
    if (o === 0) return Math.round(180 * Math.acos(a / h) / Math.PI);
}