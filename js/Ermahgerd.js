function ermahgerd(text) {
    var repl = (text) => {
        text = text.replace(/[aeiou]/gi, 'ER').replace(/MY/g, 'MAH').replace(/(ERER)|(ERH)/g, 'ER').replace(/RR/, 'R')
        if (text.length > 4 && text.endsWith('ER')) text = text.replace(/ER$/, '');
        return text;
    }
    return text.toUpperCase().replace(/[A-Z]+/gi,repl);
}

console.log(ermahgerd('oh'))