function getMissingIngredients(recipe, added) {
    var n = Math.max(...Object.keys(added).map(k => Math.ceil(added[k] / recipe[k])).concat([1]));
    var o = {}
    for (k of Object.keys(recipe)) {
        var needed = recipe[k] * n - (added[k] ? added[k] : 0);
        if (needed) o[k] = needed;
    }
    return o;
}