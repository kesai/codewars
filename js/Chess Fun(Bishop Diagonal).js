function bishopDiagonal(a, b) {
    var s = 'abcdefgh';
    var [a_x, a_y] = [s.indexOf(a[0]), +a[1] - 1];
    var [b_x, b_y] = [s.indexOf(b[0]), +b[1] - 1];
    if (Math.abs(a_x - b_x) === Math.abs(a_y - b_y)) {
        k = (a_y - b_y) / (a_x - b_x);
        b = a_y - k * a_x
        var arr = [[0, b],[7, 7 * k + b],[-b / k, 0],[(7 - b) / k, 7]]
        var out = []
        for ([x, y] of arr) {
            if (y + 1 > 8 || y < 0 || !s[x]) continue;
            var temp = s[x] + (y + 1);
            if (!out.includes(temp)) out.push(temp)
        }
        return out.sort()
    } else {
        return [a, b].sort()
    }
}