function evaluate(arr) {
    if (/[+*]/.test(arr.filter((v, i) => i % 2 === 0).join(''))) return undefined
    if (/\d+/.test(arr.filter((v, i) => i % 2).join(''))) return undefined
    exp = arr.join('');
    exp = exp.replace(/(\d+)\*(\d+)/g, (_, x, y) => +x * +y)
    while (/\+/.test(exp)) exp = exp.replace(/(\d+)\+(\d+)/g, (_, x, y) => +x + +y);
    return /^\d+$/g.test(exp)?+exp:undefined;
}
evaluate(['10', '+', '20', '*', '3', '+', '1'])