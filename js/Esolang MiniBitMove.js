function interpreter(tape, array) {
    if (tape.indexOf('0') == -1) return array;
    array = [...array]
    var p = 0;
    while (p < array.length) {
        for (cmd of tape) {
            if (cmd === '1') array[p] = '' + +array[p] ^ 1;
            if (cmd === '0') p++;
            if (p >= array.length) break;
        }
    }
    return array.join('')
}
interpreter("0100", "1111111111")