function lookAndSay(data, len) {
    return Array.from({ length: len }, (i) => data = data.replace(/(\d)\1*/g, (v) => v.length + v[0]))
}