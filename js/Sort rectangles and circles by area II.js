function sortByArea(array) {
    var f = (x) => Array.isArray(x) ? (x[0] * x[1]) : Math.PI * x ** 2;
    return [].concat(array).sort((a, b) => f(a) - f(b));
}
