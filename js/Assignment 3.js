function keywordCipher(string, keyword) {
    var s = 'abcdefghijklmnopqrstuvwxyz';
    var key = ''
    for (c of keyword) { if (!key.includes(c)) key += c; }
    key = key + s.replace(eval(`/[${key}]/g`), '')
    return string.toLowerCase().replace(/[a-z]/gi, (v) => key[s.indexOf(v)])
}