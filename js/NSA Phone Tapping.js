// Create the NSA object
var NSA = {};
NSA.log = function(p) {
    var log = p.log;
    p.log = 'No Entries'
    return p.log
}

// Create the Person constructor
var Person = function(name) {
    this.name = name;
    this.log = 'No Entries'
    this.call = function(cellphone, callee) {
        this.log = `${this.name} called ${callee.name} from ${cellphone.owner.name}'s phone(${cellphone.number})`
    }
    this.text = function(cellphone, ...persons) {
        var logs = persons.map(p => `${this.name} texted ${p.name} from ${cellphone.owner.name}'s phone(${cellphone.number})`)
        this.log = logs.join('\n')
    }
}