function colorProbability(color, texture) {
    var bags = {
        'smooth': { 'count': 3, 'data': { 'red': 1, 'yellow': 1, 'green': 1 } },
        'bumpy': { 'count': 7, 'data': { 'red': 4, 'yellow': 2, 'green': 1 } }
    }
    return Math.floor((bags[texture].data[color] / bags[texture].count) * 100) / 100
}