function List() {}

function EmptyList() {}
EmptyList.prototype = new List();
EmptyList.prototype.constructor = EmptyList;

EmptyList.prototype.toString = function() { return '()'; };
EmptyList.prototype.isEmpty = function() { return true; };
EmptyList.prototype.length = function() { return 0; };
EmptyList.prototype.push = function(x) {
    var node = new ListNode(x);
    node.next = this;
    return node;
};
EmptyList.prototype.remove = function(x) { /* implement this */ };
EmptyList.prototype.append = function(xs) { /* implement this */ };

function ListNode(value, next) {
    this.value = value;
    this.next = next;
}
ListNode.prototype = new List();
ListNode.prototype.constructor = ListNode;
ListNode.prototype.isEmpty = function() {
    if (this instanceof EmptyList) return true;
    return false;
};

ListNode.prototype.toString = function() {
    var temp = this;
    var arr = []
    while (temp) {
        arr.push(temp.value)
        temp = temp.next;
    }
    return `(${arr.join(' ').trim()})`;
};

ListNode.prototype.head = function() { return this.next; };
ListNode.prototype.tail = function() {
    return this.next;
};
ListNode.prototype.length = function() {
    var temp = this;
    var len = 0;
    while (temp) {
        len += 1;
        temp = temp.next;
    }
    return len + 1;
};
ListNode.prototype.push = function(x) {
    var node = new ListNode(x);
    if (this instanceof ListNode) this.next = node;
    return node;
};
ListNode.prototype.remove = function(x) {
    var first = this;
    var temp = this;
    var prev = null;
    var history = []
    while (temp) {
        if (!prev) prev = temp;
        if (temp.value === x) {
            if (prev) prev.next = temp.next;
            else prev = temp.next;
        } else {
            history.push(temp)
            prev = temp;
        }
        temp = temp.next;
    }
    return history.length ? history[0] : null;
};
ListNode.prototype.append = function(xs) {
    var first = this;
    var temp = this;
    var prev = null;
    while (!temp.isEmpty()) {
        prev = temp;
        temp = temp.next;
    }
    temp = prev;
    while (xs) {
        var node = new ListNode(xs.value);
        xs = xs.next;
        if (xs instanceof EmptyList) break;
        temp.next = node;
    }
    return first;
};

var list0 = new EmptyList(); // => "()"
var list1 = list0.push(3); // => "(3)"
var list2 = list1.push(2); // => "(2 3)"
var list3 = list2.push(1); // => "(1 2 3)"
var list13 = list1.append(list3);
console.log(list1.toString())