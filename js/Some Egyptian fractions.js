var gcd = (x = Math.abs(x), y = Math.abs(y)) => {
    while (y) {
        var t = y;
        y = x % y;
        x = t;
    }
    return x;
}

function decompose(n) {
    var fraction = (a, b) => [1, Math.ceil(b / a)];
    var simplify = ([a, b]) => [a / gcd(a, b), b / gcd(a, b)]
    var decimal_to_rational = (n) => [+n.replace('.', ''), 10 ** n.split('.')[1].length];
    if (/^0\//.test(n) || n === '0') return []
    var [a, b] = /\d+\.\d+/.test(n) ? simplify(decimal_to_rational(n)) : simplify(n.split('/'));
    if (b === 1) return ['' + a]
    var out = []
    while (a != 1) {
        var [x, y] = fraction(a, b);
        [a, b] = simplify([a * y - b * x, b * y]);
        out.push(x == y ? '1' : `${x}/${y}`)
    }
    out.push(`${a}/${b}`)
    return out;
}