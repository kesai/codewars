function atomicNumber(num) {
    var i = 1;
    var out = []
    while (num > 0) {
        var temp = 2 * Math.pow(i++, 2);
        out.push(Math.min(temp, num));
        num -= temp;
    }
    return out;
}
atomicNumber(23)