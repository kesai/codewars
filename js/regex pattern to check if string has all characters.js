function regexContainsAll(str) {
  return [...str].map(v=>`(?=.*${v})`).join('')
}