String.prototype.parseManaCost = function() {
    //your code here - remember to put the output with key in order "*wubrg"
    var s = 'wubrg';
    if (!/^[\dwubrg]*$/i.test(this)) return null;
    var o = {}
    if (/^(\d+)/.test(this)) { var n = +RegExp.$1; if (n) o['*'] = n; }
    for (x of 'wubrg') {
        var n = (this.toLowerCase().match(eval(`/${x}/g`)) || []).reduce((s, v) => s += v.length, 0);
        if(n)o[x]=n;
    }
    return o;
}

console.log('rbrg'.parseManaCost())