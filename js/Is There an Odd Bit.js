function anyOdd(x) {
    var s = x.toString(2).padStart(32, '0');
    for (var i = 0; i < 32; i += 2) {
        if (s[i] === '1') return 1
    }
    return 0
}
