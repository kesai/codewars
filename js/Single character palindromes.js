
function solve(s) {
	var f = (x) => x === [...x].reverse().join('')
    return f(s) ? 'OK' : [...s].some((v, i) => f(s.slice(0, i) + s.slice(i + 1))) ? 'remove one' : 'not possible';
};

console.log(solve("abbaa"))