function damagedOrSunk(board, attacks) {
    var rows = board.length;
    var boats = {};
    for (var i = 0; i < board.length; i++) {
        for (var j = 0; j < board[0].length; j++) {
            var d = board[i][j];
            if (d > 0) { boats[d] = boats[d] ? boats[d] + 1 : 1; }
        }
    }
    var copy = { ...boats };
    for ([x, y] of attacks) {
        var [i, j] = [rows - y, x - 1];
        var d = board[i][j];
        if (d) boats[d] -= 1;
        board[i][j] = 0;
    }
    var sunks = 0;
    var untoucheds = 0;
    var damages = 0;
    var points = 0;
    for (k of Object.keys(boats)) {
        if (boats[k] <= 0) { sunks++; points++; }
        else if (boats[k] === copy[k]) { untoucheds++; points--; }
        else if (boats[k] < copy[k]) { damages++; points += 0.5; }
    }
    return { sunk: sunks, damaged: damages, notTouched: untoucheds, points: points };
}

