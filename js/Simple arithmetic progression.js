function solve(arr) {
    var cnt = 0
    for (var i = 0; i < arr.length; i++) {
        for (var j = i + 1; j < arr.length; j++) {
            for (var k = j + 1; k < arr.length; k++) {
                if (arr[k] - arr[j] === arr[j] - arr[i]) cnt++;
            }
        }
    }
}