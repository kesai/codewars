function regression_line(x, y) {
    var n = x.length;
    var sum_xx = x.reduce((s, v) => s += v ** 2, 0);
    var sum_xy = x.reduce((s, v, i) => s += v * y[i], 0);
    var sum_x = x.reduce((s, v) => s += v, 0);
    var sum_y = y.reduce((s, v) => s += v, 0);
    var a = Math.round(10000 * (sum_xx * sum_y - sum_x * sum_xy) / (n * sum_xx - sum_x ** 2)) / 10000
    var b = Math.round(10000 * (n * sum_xy - sum_x * sum_y) / (n * sum_xx - sum_x ** 2)) / 10000
    return [a, b]
}
regression_line([25, 30, 35, 40, 45, 50], [78, 70, 65, 58, 48, 42])