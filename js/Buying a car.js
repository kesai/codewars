function nbMonths(old, buy, save, percent, month = 0, rest = old - buy) {
    while (rest < 0) {
        if (month % 2) percent += 0.5;
        old *= (100 - percent) / 100;
        buy *= (100 - percent) / 100;
        rest = old + (++month) * save - buy;
    }
    return [month, Math.round(rest)]
}
console.log(nbMonths(2000, 8000, 1000, 1.5))