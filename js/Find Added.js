function findAdded(st1, st2) {
    for (x of st1) st2 = st2.replace(x, '')
    return [...st2].sort().join('');
}