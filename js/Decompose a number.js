function decompose(num) {
    var out = []
    for (var i = 2; num > 1; i++) {
        var k = (Math.log(num) / Math.log(i)) | 0;
        if (k <= 1) break;
        num -= i ** k;
        out.push(k)
    }
    return [out, num]
}
console.log(decompose(8331299))