function howmuch(m, n) {
    var out = []
    for (var i = Math.min(m, n); i <= Math.max(m, n); i++) {
        if ((i - 2) % 7 === 0 && (i - 1) % 9 === 0)
            out.push([`M: ${i}`, `B: ${(i-2)/7}`, `C: ${(i-1)/9}`])
    }
    return out;
}
howmuch(1, 100)