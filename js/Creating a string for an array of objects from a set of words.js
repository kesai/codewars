function wordsToObject(input) {
    var arr = input.match(/(\S+\s\S+)/g).map(v => `{name : '${v.split(' ')[0]}', id : '${v.split(' ')[1]}'}`);
    return `[${arr.join(', ')}]`
}
console.log(wordsToObject('red 1 yellow 2 black 3 white 4'))

'[{name : \'#@&fhds\', id : \'123F3f\'}, {name : \'2vn2#\', id : \'2%y6D\'}, {name : \'@%fd3\', id : \'@!#4fs\'}, {name : \'W@R^g\', id : \'WE56h%\'}]', 
'[{name : \'fhds\', id : \'123F3f\'}, {name : \'4fs\', id : \'W\'}, {name : \'g\', id : \'WE56h\'}]'