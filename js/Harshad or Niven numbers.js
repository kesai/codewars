/**
 * Utility class for Harshad numbers (also called Niven numbers).
 *
 * @namespace Harshad
 */
var Harshad = (function() {
    'use strict';

    return {
        /**
         * Returns true when the given number is a valid Harshad number.
         *
         * @param {Number} number The given number
         * @returns {Boolean}
         * @function Harshad.isValid
         */
        isValid: function(number) {
            var sum = [...'' + number].reduce((s, v) => s += +v, 0)
            return number % sum === 0
        },
        /**
         * Gets the next Harshad number after the given number.
         *
         * @param {Number} number The given number
         * @returns {Number}
         * @function Harshad.getNext
         */
        getNext: function(number) {
            for (var i = number + 1;; i++) {
                if (Harshad.isValid(i)) return i;
            }
        },
        /**
         * Returns the suite of Harshad numbers, starting after a given number.
         *
         * @param {Number} count The number of elements to return
         * @param {Number} start The number after which the serie should start;
         *  defaults to 0
         * @returns {Array}
         * @function Harshad.getSerie
         */
        getSerie: function(count, start) {
            var out = []
            for (var i = (start || 0) + 1; out.length < count; i++) {
                if (Harshad.isValid(i)) out.push(i)
            }
            return out;
        }
    };

}());
