var rnd = (min, max) => parseInt(Math.random() * (max - min + 1) + min, 10);

function numberGenerator() {
    var numbers = Array.from({ length: 49 }, (_, i) => i + 1);

    function draw() {
        var i = rnd(0, numbers.length - 1);
        var out = numbers[i];
        numbers.splice(i, 1)
        return out;
    }
    var arr = Array.from({ length: 6 }, (_, i) => draw()).sort((a, b) => a - b)
    arr.push(rnd(0, 9));
    return arr;
}

function checkForWinningCategory(checkCombination, winningCombination) {
    var superzahl_match = { 6: 1, 5: 3, 4: 5, 3: 7, 2: 9, 1: -1 }
    var match = { 6: 2, 5: 4, 4: 6, 3: 8, 2: -1, 1: -1, 0: -1 }
    var match_cnt = (winningCombination.slice(0, -1).filter(v => checkCombination.slice(0, -1).includes(v)) || []).length;
    return checkCombination[6] === winningCombination[6] ? superzahl_match[match_cnt] : match[match_cnt]
}