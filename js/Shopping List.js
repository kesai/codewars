function shoppingListCost(arr) {
    var cost = 0;
    for ([name, count] of arr) {
        var good = groceries[name];
        if (good.bogof) cost += Math.round(count / 2) * good.price * (100 - good.discount) / 100;
        else cost += good.price * count * (100 - good.discount) / 100;
    }
    return Math.round(100 * cost) / 100;
}