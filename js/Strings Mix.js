function cmp(a, b) {
    var s = '123456789abcdefghijklmnopqrstuvwxyz=';
    if (a.length === b.length) {
        for (var i = 0; i < Math.max(a.length, b.length); i++) {
            if (i >= a.length && i < b.length) return -1;
            if (i >= b.length && i < a.length) return 1;
            if (a[i] === b[i]) continue;
            return s.indexOf(a[i]) - s.indexOf(b[i]);
        }
    } else return b.length - a.length;
}
function mix(s1, s2) {
    var chars = new Set(s1 + s2);
    var out = [];
    for (c of chars) {
        if (/[a-z]/.test(c)) {
            var a = (s1.match(new RegExp(c, 'g')) || []).length;
            var b = (s2.match(new RegExp(c, 'g')) || []).length;
            var m = Math.max(a, b);
            if (m <= 1) continue;
            var temp = `${a > b ? '1' : a < b ? '2' : '='}:${c.repeat(m)}`
            out.push(temp)
        }
    }
    return out.sort(cmp).join('/');
}
