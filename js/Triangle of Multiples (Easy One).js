function multTriangle(n) {
    var a = 0;
    var c = 0;
    for (var i = 1; i <= n; i++) {
        a += i ** 3;
        if (i % 2) c += i * i + i * ((i - 1) ** 2) / 2;
    }
    return [a, a - c, c]
}
multTriangle(10)