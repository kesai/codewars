function swapper(a, b) {
    a = a ^ b;
    b = a ^ b; // a^b^b ==> a
    a = a ^ b; // a^b^a ==> b
    return [a, b]
}
