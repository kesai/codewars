function isMadhavArray(arr) {
    if (arr.length <= 1) return false;
    var len = arr.length;
    var blocks = (Math.sqrt(1 + 8 * len) - 1) / 2
    if (!Number.isInteger(blocks)) return false;
    var pos = 1;
    for (var i = 1; i < blocks; i++) {
        var block = arr.slice(pos, pos + i + 1);
        console.log(arr.slice(pos, pos + i + 1))
        if (block.reduce((s, v) => s += v, 0) != arr[0]) return false
        pos += i + 1;
    }
    return true;
}
isMadhavArray([2, 1, 1, 4, -1, -1])