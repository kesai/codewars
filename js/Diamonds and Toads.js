function diamondsAndToads(s, fairy) {
    if (fairy != 'good') {
        var python = (s.match(/p/g) || []).length + (s.match(/P/g) || []).length * 2;
        var squirrel = (s.match(/s/g) || []).length + (s.match(/S/g) || []).length * 2;
        return { 'python': python, 'squirrel': squirrel }
    } else {
        var ruby = (s.match(/r/g) || []).length + (s.match(/R/g) || []).length * 2;
        var crystal = (s.match(/c/g) || []).length + (s.match(/C/g) || []).length * 2;
        return { 'ruby': ruby, 'crystal': crystal }
    }
}
diamondsAndToads("Ruby and Crystal", "good")