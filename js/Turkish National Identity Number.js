function checkValidTrNumber(n) {
    var s = n.toString()
    if (!/^[1-9]\d{10}$/g.test(s)) return false;
    var a = [...s].reduce((s, v, i) => s += [0, 2, 4, 6, 8].includes(i) ? +v : 0, 0);
    var b = [...s].reduce((s, v, i) => s += [1, 3, 5, 7].includes(i) ? +v : 0, 0);
    var first_ten = [...s.slice(0, -1)].reduce((s, v) => s += +v, 0);
    if ((7 * a - b) % 10 != +s[9]) return false;
    if (first_ten % 10 != +s[10]) return false;
    return true;
}

console.log(checkValidTrNumber(10000000146))