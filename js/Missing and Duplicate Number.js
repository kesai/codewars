function solution(array, N = array.length) {
    var sum = (1 + N) * N / 2;
    var arr_sum = 0;
    var temp = Array(N + 1).fill(0);
    var [x, y] = [null, null]
    for (v of array) {
        arr_sum += v
        if (temp[v]) {
        	x=v
        } else {
            temp[v] = 1;
        }
    }
    y=sum - arr_sum + x
    return [y,x]
}

