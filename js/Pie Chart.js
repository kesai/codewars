function pieChart(str) {
    var obj = JSON.parse(str);
    var total = Object.values(obj).reduce((s, v) => s += +v, 0)
    var json = {}
    for (k of Object.keys(obj)) {
        json[k] = Math.round(+obj[k] * 36000 / total) / 100
    }
    return JSON.stringify(json)
}