function encode(c) {
    var lowers = 'abcdefghijklmnopqrstuvwxyz'
    var uppers = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    var numbers = '0123456789'
    if (/[a-z]/.test(c)) return lowers[(lowers.indexOf(c) - 13 + lowers.length) % lowers.length];
    if (/[A-Z]/.test(c)) return uppers[(uppers.indexOf(c) - 13 + uppers.length) % uppers.length];
    if (/[0-9]/.test(c)) return numbers[(numbers.indexOf(c) - 5 + numbers.length) % numbers.length];
    return c;
}

function ROT135(input) {
    return input.replace(/./g, (c) => encode(c))
}