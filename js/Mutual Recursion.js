const F = (n) => n ? n - M(F(n - 1)) : 1;
const M = (n) => n ? n - F(M(n - 1)) : 0;