function triangleArea(t) {
    return (1 / 2) * (t.a.x * t.b.y + t.b.x * t.c.y + t.c.x * t.a.y - t.a.x * t.c.y - t.b.x * t.a.y - t.c.x * t.b.y)
}