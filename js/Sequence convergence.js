function convergence(n) {
    var next = (n) => n < 10 ? n = 2 * n : [...'' + n].reduce((s, v) => s *= +v === 0 ? 1 : +v, 1) + n;
    var a = 1;
    var b = n;
    var k = 0;
    while (a != b) {
        if (a < b) a = next(a)
        else {
            b = next(b);
            k++;
        }
    }
    return k
}
