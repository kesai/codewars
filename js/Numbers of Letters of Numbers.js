function numbersOfLetters(n) {
    var number_words = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    var temp = [...'' + n].map(x => number_words[+x]).join('');
    var out = []
    while (temp.length >= number_words.length || number_words[temp.length] != temp) {
        out.push(temp)
        temp = temp.length >= number_words.length ? [...'' + temp.length].map(x => number_words[+x]).join('') : number_words[temp.length];
    }
    out.push(temp)
    return out;
}
numbersOfLetters(37)