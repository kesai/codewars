function shortesttoChar(S, C) {
    var indies = S.search(eval(`/${C}/g`))
    if (!indies.length) return []
    out = [...S].map((v, i) => Math.min(...indies.map(x => Math.abs(x.index - i))))
    return out;
}

String.prototype.search = function(exp) {
    var out = []
    try {
        while ((result = exp.exec(this)) != null) {
            out.push(result);
        }
    } catch (e) {
        console.log(e);
    } finally {
        return out;
    }
}

shortesttoChar("", "")