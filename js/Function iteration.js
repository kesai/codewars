var createIterator = function(func, n) {
    return function(x) {
        for (var i = 0; i < n; i++) {
            x = func(x);
        }
        return x;
    }
};