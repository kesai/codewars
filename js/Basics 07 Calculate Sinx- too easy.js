var f = (x) => x > 1 ? x * f(x - 1) : 1

function sin(x) {
    var N = 10;
    var r = 0;
    x = Math.PI * x / 180;
    for (var i = 0; i <= N; i++) {
        var n = 2 * i + 1;
        var k = (i % 2 ? -1 : 1) * (x ** n) / f(n)
        r += k
    }
    return r
}

