function uniqueDigitProducts(a) {
    return new Set(a.map(v => [...'' + v].reduce((s, x) => s *= +x, 1))).size
}