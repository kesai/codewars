function dismember(func) {
    var s = func.toString().match(/\(.*\)/g)[0].slice(1, -1).replace(/\s/g, '')
    return s?s.split(','):[];
}

