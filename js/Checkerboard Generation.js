function checkerboard(size) {
    var a = Array.from({ length: size }, (_, i) => i % 2 ? '[b]' : '[r]').join('')
    var b = Array.from({ length: size }, (_, i) => i % 2 ? '[r]' : '[b]').join('')
    return Array.from({ length: size }, (_, i) => i % 2 ? b : a).join('\n') + '\n'
};