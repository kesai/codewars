function Guesser() {
    this.guesses = 0;
    this.num = 1; //Math.floor(Math.random() * 1000 + 1);
    this.guess = function(num) {
        if (this.guesses < 10) {
            this.guesses += 1;
        } else {
            throw "You failed! Should have been " + this.num + ".";
        }
        if (num == this.num) {
            return "Correct!";
        } else if (num > this.num) {
            return "Too high!";
        } else if (num < this.num) {
            return "Too low!";
        }
    }

    this.test = function(num) {
        return num == this.num;
    }

}
Guesser.prototype.getNumber = function() {
    return 1;
}