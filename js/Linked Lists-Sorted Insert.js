function Node(data) {
    this.data = data;
    this.next = null;
}

function sortedInsert(head, data) {
    var first = head;
    var node = new Node(data);
    var prev = null;
    while (head && head.data < data) {
        prev = head;
        head = head.next;
    }
    if (head) node.next = head;
    if (prev) prev.next = node;
    return first && first.data < data ? first : node;
}

//test code
function buildOneTwoThree() {
    var head = null;
    head = push(head, 3);
    head = push(head, 2);
    head = push(head, 1);
    return head;
}

function push(head, data) {
    var newNode = new Node(data);
    newNode.next = head;
    return newNode;
}
head = buildOneTwoThree();
console.log(head)
p = sortedInsert(head, 3.5)
console.log(p)