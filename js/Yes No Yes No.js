function yesNo(arr) {
    var out = []
    var check = true;
    while (arr.length) {
        var stays = []
        for (var i = 0; i < arr.length; i++) {
            if (check) out.push(arr[i])
            else stays.push(arr[i])
            check = !check;
        }
        arr = stays;
    }
    return out;
}
yesNo([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])