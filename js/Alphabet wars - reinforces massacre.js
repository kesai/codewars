function alphabetWar(reinforces, airstrikes) {
    reinforces = reinforces.map(v => v.split(''))
    var battle = reinforces.shift();

    for (strike of airstrikes) {
        for (var i = 0; i < strike.length; i++) {
            if (strike[i] === '*') {
                if (battle[i]) battle[i] = '_';
                if (battle[i - 1]) battle[i - 1] = '_';
                if (battle[i + 1]) battle[i + 1] = '_';
            }
        }
        //reinforces coming
        if (!reinforces.length) continue;
        if (reinforces[0] === '_'.repeat(reinforces[0].length)) reinforces.shift();
        for (var j = 0; j < battle.length; j++) {
            if (battle[j] === '_') {
                var next = 0;
                while (next < reinforces.length - 1 && reinforces[next][j] === '_') {
                    next++;
                }
                battle[j] = reinforces[next][j];
                reinforces[next][j] = '_';
            }
        }
    }
    return battle.join('')
}