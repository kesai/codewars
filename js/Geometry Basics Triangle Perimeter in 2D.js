function trianglePerimeter(t) {
    var dist = (a, b) => Math.sqrt((a.x - b.x) ** 2 + (a.y - b.y) ** 2);
    return Math.round((dist(t.a,t.b) + dist(t.b,t.c) + dist(t.a,t.c)) * 1000000) / 1000000;
}