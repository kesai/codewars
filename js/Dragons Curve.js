Dragon = function(n) {
    var s = 'Fa';
    for (var i = 0; i < n; i++) {
        s = i ? s.replace(/(a|b)/g, (x) => x === 'a' ? 'aRbFR' : 'LFaLb') : s.replace(/a/g, 'aRbFR')
    }
    return Number.isInteger(n) && n >-1 ? s.replace(/(a|b)/g, '') : ''
}