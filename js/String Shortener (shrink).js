function shorten(s, n, glue = '...') {
    if (s.length <= n) return s;
    var l = n - glue.length;
    if (l <= 1) return s.slice(0, n);
    return s.slice(0, l / 2 | 0) + glue + s.slice(-(l % 2 + l / 2 | 0))
}
var sentence = "hello world";
var res = shorten(sentence, 5, 'this glue is too long to fit');
console.log(res)