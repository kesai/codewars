function Node(data) {
    this.data = data;
    this.next = null;
}

function insertNth(head, index, data) {
    var first = head;
    var i = 0;
    var prev = null;
    while (i < index) {
        prev = head;
        head = head.next;
        i++;
    }
    var node = new Node(data);
    if (prev) prev.next = node;
    node.next = head;
    return first && index ? first : node;

}

//test code
function push(head, data) {
    var newNode = new Node(data);
    newNode.next = head;
    return newNode;
}

function buildOneTwoThree() {
    var head = null;
    head = push(head, 3);
    head = push(head, 2);
    head = push(head, 1);
    return head;
}
var head = buildOneTwoThree();
console.log(head)
p = insertNth(head, 2, 23)
console.log(head)