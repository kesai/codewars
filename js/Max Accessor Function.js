function max(data, accessor) {
    if (!accessor) return Math.max(...data);
    return Math.max(...data.map(accessor));
}