function oneDown(str) {
    var s = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    return typeof(str) === 'string' ? str.replace(/[a-z]/gi, (x) => s[(s.indexOf(x) - 1 + s.length) % s.length]) : 'Input is not a string'
}