function majorScale(melody) {
	if(/##/g.test(melody))return 'No major scale'
    var p = 'C->C#->D->D#->E->F->F#->G->G#->A->A#->B'.split('->');
    var arr = [...new Set(melody.match(/[A-G](#)?/g))].sort((a, b) => p.indexOf(a) - p.indexOf(b))
    var patterns = arr.map((v, i) => i === arr.length - 1 ? p.indexOf(arr[0]) + p.length - p.indexOf(v) : p.indexOf(arr[i + 1]) - p.indexOf(v))
    for (var i = 0; i < patterns.length; i++) {
        if (patterns.join(',') === '2,2,1,2,2,2,1') {
            return `${arr[0]} major scale`
        } else {
            patterns = [patterns[patterns.length - 1]].concat(patterns.slice(0, -1))
            arr = [arr[arr.length - 1]].concat(arr.slice(0, -1))
        }
    }
    return 'No major scale'
}
console.log(majorScale('F#D#A#BC#D#BD#EBG#A#B'))