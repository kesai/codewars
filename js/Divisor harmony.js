var gcd = (x = Math.abs(x), y = Math.abs(y)) => {
    while (y) {
        var t = y;
        y = x % y;
        x = t;
    }
    return x;
}
function simpify(a, b) {
    return [a / gcd(a, b), b / gcd(a, b)].join('/')
}

function get_ratio(x) {
    var sum = 0;
    for (var i = 1; i <= x; i++) {
        if (x % i === 0) sum += i;
    }
    return simpify(sum, x)
}

function solve(a, b) {
    var ratios = {};
    var result = 0;
    var pair_ratios = []
    for (var i = a; i < b; i++) {
        var ratio = get_ratio(i);
        if (Object.values(ratios).includes(ratio)) {
            if (!pair_ratios.includes(ratio)) pair_ratios.push(ratio);
        }
        ratios[i] = ratio;
    }
    for (ratio of pair_ratios) {
        var nums = []
        for (k of Object.keys(ratios)) {
            if (ratios[k] === ratio) nums.push(+k)
        }
        result += Math.min(...nums)
    }
    return result;
};