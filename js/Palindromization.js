function palindromization(element, n) {
	if(element===''||n<=1)return 'Error!'
    if (n <= element.length * 2) {
        var a = [...element.slice(0, n / 2 | 0)].reverse().join('')
        return element.slice(0, (n / 2 | 0) + n % 2) + a
    } else {
        var b = element.padEnd((n / 2 | 0) + n % 2, element);
        var c = element.padEnd((n / 2 | 0), element);
        return b + [...c].reverse().join('')
    }
}
