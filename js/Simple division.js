function get_facotors(x) {
    var d = 2;
    var out = []
    while (x > 1) {
        if (!(x % d)) {
            if (!out.includes(d)) out.push(d);
            x /= d;
        } else d++;
    }
    return out;
}


function solve(a, b) {
    return get_facotors(b).every(v => !(a % v))
};