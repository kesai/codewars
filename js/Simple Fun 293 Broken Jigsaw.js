function brokenJigsaw(p) {
    var misses = p.map(v => (v.match(/O/g) || []).length);
    var m = Math.min(...misses);
    return misses.reduce((s, v) => s += v, 0) - m * misses.length;
}