function getRootProperty(object, val) {
    for (var i in object) {
        if (typeof object[i] == 'object') {
            if (getRootProperty(object[i], val)) {
                return i;
            }
        } else {
            if (object[i] == val) {
                return i;
            }
        }
    }
    return null;
}