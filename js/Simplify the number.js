function simplify(number, str = number.toString()) {
    return [...str].map(
        (v, i) => v === '0' ? '' : str.length - i - 1 ? `${v}*${10**(str.length-i-1)}` : `${v}`).filter(v=>v).join('+')
}