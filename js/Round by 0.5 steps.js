function solution(n) {
    d = n - parseInt(n);
    return parseInt(n) + (d < 0.25) ? 0 : d >= 0.75 ? 1 : 0.5;
}