function calculateOptimalFare(D, T, V1, R, V2) {
    var T2 = D / V2;
    if (T2 <= T / 60) return "0.00";
    money = R * (D - ((T / 60 - D / V1) * V1 / (V1 - V2)) * V2);
    if (D * 60 / V1 > T && money != 0) return "Won't make it!";
    return money.toFixed(2)
}