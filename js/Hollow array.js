function isHollow(x){
  return /^(1*)0{3,}\1$/.test(x.map(x=>x?1:0).join(''))
}