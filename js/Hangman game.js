class Hangman {
    constructor(word) {
        this.word = word;
        this.miss = '';
        this.match = Array(this.word.length).fill('_')
        this.isEnd = false;
    }
    guess(letter) {
        if (this.isEnd) return 'The game has ended.'
        if (!this.word.includes(letter)) {
            if (this.miss.length >= 6) { this.isEnd = true; return `You got hung! The word was ${this.word}.`; }
            if(!this.miss.includes(letter))this.miss += letter;
        } else {
            for (var i = 0; i < this.word.length; i++) {
                if (this.word[i] === letter) this.match[i] = letter;
            }
            if (this.match.join('') === this.word) { this.isEnd = true; return `You found the word! (${this.word})` }
        }
        return `${this.match.join(' ')}` + (this.miss != '' ? ` # ${this.miss}` : '')

    }
}