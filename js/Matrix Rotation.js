function rotateClockwise(matrix) {
    if (matrix.length === 0) return []
    var out = []
    for (var j = 0; j < matrix[0].length; j++) {
        var row = ''
        for (var i = matrix.length - 1; i > -1; i--) {
            row += matrix[i][j];
        }
        out.push(row)
    }
    return out
}