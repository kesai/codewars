deadAntCount = function(ants) {
	if(!ants)return 0;
    ants = ants.replace(/ant/g, '');
    var a = (ants.match(/a/g) || []).length;
    var n = (ants.match(/n/g) || []).length;
    var t = (ants.match(/t/g) || []).length;
    return Math.max(...[a, n, t]);
}