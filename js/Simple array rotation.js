function solve(arr) {
  const
    a = arr[0] < arr[1], b = arr[1] < arr[2], c = arr[arr.length - 1] < arr[0],
    m = a == b ? a : c;
  return (c == m ? 'R' : '') + (m ? 'A' : 'D');
}