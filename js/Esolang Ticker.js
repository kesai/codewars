function interpreter(tape) {
    var p = 0;
    var mem = [0]
    var out = ''
    for (cmd of tape) {
        switch (cmd) {
            case '>': p++; break;
            case '<': p--; break;
            case '+':
                if (p < mem.length && p > -1) mem[p] = (mem[p] + 1) % 256; break;
            case '-':
                if (p < mem.length && p > -1) { mem[p]--; if (mem[p] < 0) mem[p] += 256; } break;
            case '*':
                out += String.fromCharCode(mem[p] || 0); break;
            case '/':
                if (p < mem.length) { mem[p] = 0; } break;
            case '!':
                mem.push(0); break;
        }
    }
    return out
}
