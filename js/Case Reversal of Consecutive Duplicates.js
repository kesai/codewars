function reverseCase(s) {
    return s.replace(/(.)\1+/g, (x) => /[a-z]/.test(x) ? x.toUpperCase() : x.toLowerCase())
}