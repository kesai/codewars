const { exec } = require('child_process');
var execute = (command, callback) => exec(command, (error, stdout, stderr) => error ? callback(error) : callback(error, stdout));