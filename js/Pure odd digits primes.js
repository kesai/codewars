function isPrime(num) {
    if (num < 2) return false;
    if (num === 2 || num === 3) return true;
    if (num % 6 != 1 && num % 6 != 5) return false
    var n = parseInt(Math.sqrt(num));
    for (var i = 5; i < n + 1; i += 6) {
        if (num % i === 0 || num % (i + 2) === 0) return false
    }
    return true
}
var caches = []

function preCache(n) {
    for (var i = 1; i <= n; i += 2) {
        if (isPrime(i) && /^[13579]+$/g.test(i)) caches.push(i)
    }
}
preCache(100000)

function onlyOddDigPrimes(n) {
    var arr = caches.filter(v => v <= n);
    return [arr.length, arr[arr.length - 1], caches[arr.length]]
}
onlyOddDigPrimes(20)